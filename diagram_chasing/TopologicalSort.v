From mathcomp Require Import all_ssreflect.
From DiagramChasing Require Import Graph Utils.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.




Module TopologicalSort.

Module toposorted_data.

Record toposorted_data (V : eqType) := {
  vertex : seq V;
  graph : FiniteOrientedGraph.type nat;
  acyclic : bool;
}.

End toposorted_data.

Section toposort.

Variable (V : eqType).

Implicit Type (u v w : V) (arc : V -> seq V) (n : nat).

Fixpoint toposort arc n u (marked : seq V) {struct n} :=
  if u \in marked then
    marked
  else if n is n'.+1 then
    u :: foldr (toposort arc n') marked (arc u)
  else
    marked.

Definition toposort_list arc n marked l := foldr (toposort arc n) marked l.


Import FiniteOrientedGraph toposorted_data.

Definition toposort_graph (G : FiniteOrientedGraph.type V) : toposorted_data V :=
  let relevant := relevant_vertex_seq G in
  let n := size relevant in
  let rev_toposorted := rev (toposort_list (LocallyFiniteOrientedGraph.arc G) n.+1 [::] relevant) in
  {|
    vertex := rev_toposorted ;
    graph := {| arc := sort (fun a b => a.1 >= b.1) (undup (arc (pushforward (index^~ rev_toposorted) G))) |} ;
    acyclic := size rev_toposorted == n
  |}.

End toposort.

End TopologicalSort.


Module Theory.

Export TopologicalSort.

Section Theory.

Import FiniteOrientedGraph.

Variables
  (V : eqType).

Implicit Type (u v w : V) (n : nat) (marked l : seq V) (p : seq V).

Section preliminary_lemmas.

Lemma mem_split (T : eqType) (x : T) before after : x \in before ++ x :: after.
Proof.
  by rewrite mem_cat in_cons orbC eq_refl.
Qed.

Lemma double_split u l1 l2 l1' l2' : l1 ++ l2 == l1' ++ u :: l2' -> u \notin l1 -> exists l1'', (l1' == l1 ++ l1'') && (l2 == l1'' ++ u :: l2').
Proof.
  elim: l1 l1'=> [l1'|v l1 H l1'] ; first by exists l1' ; rewrite eq_refl.
  case: l1' => [/=/eqP[]->| ? l1'] ; first by rewrite in_cons eq_refl.
  rewrite !cat_cons=> /eqP[] <-/eqP.
  move=> /H {H} ; rewrite in_cons orbC ; case: (_ \in _)=> // /(_ isT) [] l1'' /andP[]/eqP l1'_eq ? _.
  exists l1'' ; rewrite l1'_eq cat_cons eq_refl ; done.
Qed.

Lemma marked_cat arc n v marked : exists before, toposort arc n v marked == before ++ marked /\ (forall u, u \in before -> u \notin marked).
Proof.
  elim: n v marked=> [*/=|n H v marked/=] ; first by exists [::] ; case: (_ \in _).
  case: (@idP (_ \in _))=> [|/negP v_notin_marked] ; first by exists [::].
  (* TODO : unshelve eexists (v :: _). *)
  elim: (arc v)=> [|v' l []before' []] ;
    first by exists [:: v] ; split=> // u ; rewrite in_cons orbC/==> /eqP->.
  case: before'=> [/=|? before''/=/eqP[] <- -> disjoint'] ;
    first by case: marked v_notin_marked => //=??/[swap]/eqP[]-> ; rewrite in_cons eq_refl.
  move: {H} (H v' (before'' ++ marked))=> []before []/eqP H disjoint.
  exists (v :: before ++ before'') ; split.
  - by rewrite H cat_cons -catA.
  - move=> u ; rewrite in_cons mem_cat=> /orP[/eqP->// | /orP[/disjoint | ]].
    - rewrite mem_cat orbC ; case: (_ \in _) ; done.
    - move: (disjoint' u) ; rewrite in_cons orbC ; case: (_ \in _) ; done.
Qed.

End preliminary_lemmas.


Section toposort_split.

Import LocallyFiniteOrientedGraph.

Variable (G : LocallyFiniteOrientedGraph.type V).

Definition toposort_split_auxP n marked l before u after :=
  u \notin marked
  -> exists p' marked',
  sorted (OrientedGraph.arc G) (rcons p' u)
  && subseq p' before
  && (head u p' \in l)
  && (u \notin marked')
  && (u :: after == toposort (arc G) (n - size p') u marked').

Definition toposort_splitP n := forall marked v before u after,
  toposort (arc G) n v marked == before ++ u :: after
  -> toposort_split_auxP n marked [:: v] before u after.

Definition toposort_list_splitP n := forall marked l before u after,
  toposort_list (arc G) n marked l == before ++ u :: after
  -> toposort_split_auxP n marked l before u after.


Lemma toposort_split_0 : toposort_splitP 0.
Proof.
  move=> marked v ???/= ; rewrite/toposort_split_auxP.
  case: (_ \in _)=> /eqP-> ; rewrite mem_split ; done.
Qed.

Lemma toposort_split_step1 n : toposort_list_splitP n -> toposort_splitP n.+1.
Proof.
  move=> toposort_list_split marked v before u after/= ; rewrite/toposort_split_auxP.
  case: (_ \in _)=> [/eqP->|] ; first by rewrite mem_split.
  case: before=> [/eqP[]-><- /negPf u_notin_marked|v' before /eqP[]<-/eqP].
    exists [::] ; exists marked ; rewrite/= u_notin_marked in_cons eq_refl/= ; done.
  move=> toposort_list_eq ; move/(toposort_list_split marked (arc G v) before u after toposort_list_eq)=> []p' []marked'.
  do !(move=>/andP[]) ; move=>path_*.
  exists (v :: p') ; exists marked'.
  do !(apply/andP ; split)=> //.
  - move: path_ ; rewrite rcons_cons headI/= andbC=> -> ; done.
  - by rewrite/= eq_refl.
Qed.

Lemma toposort_split_step2 n : toposort_splitP n -> toposort_list_splitP n.
Proof.
  move=> toposort_split marked l before u after/= ; rewrite/toposort_split_auxP.
  elim: l marked before => [??/=/eqP->|v l H marked before/=] ; first by rewrite mem_split.
  set marked'' := toposort_list _ _ _ _.
  move: (mem_split u before after)=> /[swap]/[dup]/[dup] toposort_split_u /eqP{2}<-.
  move: (marked_cat (arc G) n v marked'')=> []before''[] /eqP-> /(_ u)/[swap] double_decomp ; rewrite mem_cat.
  case: (@idP (u \in before''))=> [_ /(_ isT) u_notin_marked' _|/negP u_notin_before'' _ /= _] u_notin_marked.
  - move: (toposort_split _ _ _ _ _ toposort_split_u u_notin_marked')=> []p' []marked'.
    do !(move=>/andP[]) ; move=>?? head_in*.
    exists p' ; exists marked'.
    do !(apply/andP ; split)=> //.
    - move: head_in ; rewrite !in_cons in_nil orbC/==> -> ; done.
  - move: (double_split double_decomp u_notin_before'')=> []before' /andP[]/eqP->.
    move/(H marked before')/(_ u_notin_marked)=> []p' []marked'.
    do !(move=>/andP[]) ; move=>*.
    exists p' ; exists marked'.
    do !(apply/andP ; split)=> //.
    - by rewrite (cat_subseq (sub0seq _)).
    - rewrite in_cons ; apply/orP ; right ; done.
Qed.


Lemma toposort_list_split n : toposort_list_splitP n.
Proof.
  elim: n=> [|*] ; apply toposort_split_step2 ; [apply toposort_split_0 | apply toposort_split_step1] ; done.
Qed.

End toposort_split.


Section corrolaries.

Import LocallyFiniteOrientedGraph.

Variable (G : LocallyFiniteOrientedGraph.type V).

Lemma toposort_list_path n marked l v : v \in toposort_list (arc G) n marked l -> v \in marked \/ exists u p, (u \in l) && OrientedGraph.path G u p v.
Proof.
  case: (@idP (v \in marked))=> [|/negP v_notin_marked//] ; first by left.
  case/splitPr decomp: {1}(toposort_list _ _ _ _) / => [before after] ; right. (* TODO : ??? *)
  move/eqP/toposort_list_split: decomp=> []// p[] marked'.
  do!(move=> /andP[]) ; move=>sorted_ _ head_in_l _ _.
  exists (head v p) ; exists (behead (rcons p v)).
  apply/andP ; split=> // {head_in_l}.
  rewrite/OrientedGraph.path.
  move:sorted_.
  case: p=> //= u p -> ; rewrite last_rcons/= ; done.
Qed.

End corrolaries.


Section toposort_correct.

Import FiniteOrientedGraph.
Import Graph.Theory.

Lemma subset_toposort_list arc n marked l : {subset l <= (toposort_list arc n.+1 marked l)}.
Proof.
  apply/allP.
  elim: l=> [|v l'/=] ; first by apply/allP=> u ; rewrite in_nil.
  set marked' := toposort_list _ _ _ _.
  move: (marked_cat arc n.+1 v marked')=> []? []/eqP/=.
  case: (@idP (v \in marked'))=> [->//|_] ; rewrite in_cons eq_refl/==> -> _ /allP H.
  apply/allP=> u/= ; rewrite mem_cat orbC=> /H/= -> ; done.
Qed.

Definition toposorted arc s :=
  pairwise (fun u v => ~~ arc v u) s && all (fun u => ~~ arc u u) s.

Lemma toposortedP u0 (arc : rel V) s : reflect
  (forall i j, i < size s -> j < size s -> arc (nth u0 s i) (nth u0 s j) -> i < j)
  (toposorted arc s).
Proof.
  apply (iffP idP).
  - move=> /andP[]/pairwiseP pairwise_ /allP all_ i j i_lt_ j_lt_ arc_.
    rewrite ltnNge leq_eqVlt ; apply/negP=> /orP[/eqP j_eq_i | j_lt_i].
    - move: (all_ (nth u0 s i)).
      rewrite mem_nth// -{2}j_eq_i arc_ => /(_ isT) ; done.
    - move: (pairwise_ u0 j i j_lt_ i_lt_ j_lt_i) ; rewrite arc_ ; done.
  - move=> H ; apply/andP ; split.
    - apply/(pairwiseP u0) => j i j_lt_ i_lt_ j_lt_i ; apply /negP=> arc_.
      move: (H i j i_lt_ j_lt_ arc_) j_lt_i ; case: ltngtP ; done.
    - apply/allP => u /[dup]/(nth_index u0) {-1}<- ; rewrite -{1}index_mem.
      set i := index _ _ => i_lt_ ; apply/negP => arc_.
      move: (H i i i_lt_ i_lt_ arc_) ; rewrite ltnn ; done.
Qed.


Variable (G : FiniteOrientedGraph.type V).

Let n := size (relevant_vertex_seq G).

Let vertex_sorted := toposort_list (LocallyFiniteOrientedGraph.arc G) n.+1 [::] (relevant_vertex_seq G).


Lemma relevant_sorted : relevant_vertex_seq G =i vertex_sorted.
Proof.
  move=> v ; case: (@idP (_ \in vertex_sorted)) ; last apply contra_notF.
  - move=> /toposort_list_path[]//[]u []p /andP[]/[swap].
    case: {p} (lastP p)=> [|p u'] ; first by rewrite path0=> /eqP->.
    rewrite rcons_path=> /andP[]/andP[] _ /[swap] /eqP ->.
    by move/arc2_relevant.
  - apply subset_toposort_list.
Qed.

Lemma toposorted_arc_increasing : toposorted (OrientedGraph.arc G) vertex_sorted -> forall u v, (OrientedGraph.arc G) u v -> index u vertex_sorted < index v vertex_sorted.
Proof.
  move=>/[swap] u /(toposortedP u) toposorted_ v /[dup]/[dup]
    /arc1_relevant/[swap] /arc2_relevant ;
  rewrite !relevant_sorted=> v_in u_in.
  rewrite -{1}(nth_index u u_in) -{1}(nth_index u v_in)=> arc_.
  move: (toposorted_ (index u vertex_sorted) (index v vertex_sorted)).
  rewrite !index_mem=> /(_ u_in v_in arc_) ; case: ltngtP ; done.
Qed.

Lemma toposorted_path_increasing : toposorted (OrientedGraph.arc G) vertex_sorted -> forall u p v, p != [::] -> OrientedGraph.path G u p v -> index u vertex_sorted < index v vertex_sorted.
Proof.
  move/toposorted_arc_increasing=> arc_increasing u p v.
  elim: p u=> [|u' p H u _] ; first by rewrite eq_refl.
  case: p H=> [_|u'' p H].
    rewrite cons_path path0=> /andP[]/arc_increasing/[swap]/eqP-> ; done.
  rewrite cons_path=> /andP[]/arc_increasing u_lt_u' /H/=/(_ isT)/(ltn_trans u_lt_u') ; done.
Qed.


Lemma toposorted_Ncyclic : toposorted (OrientedGraph.arc G) vertex_sorted -> ~ cyclic G.
Proof.
  move=> toposorted_ []u[]p/andP[]??.
  suff: false by [].
  rewrite -(ltnn (index u vertex_sorted)).
  apply (toposorted_path_increasing toposorted_ (p := p)) ; done.
Qed.

Lemma uniq_toposorted : uniq vertex_sorted -> toposorted (OrientedGraph.arc G) vertex_sorted.
Proof.
  case: {2}vertex_sorted (erefl vertex_sorted)=> [->//|u0 ? _].
  move=> uniq_.
  suff arc_non_decreasing: forall u v before after, ~ ((vertex_sorted == before ++ u :: after) && (OrientedGraph.arc G u v) && (v \in (rcons before u))).
    apply/(toposortedP u0)=> i j ??.
    set u := nth _ _ _ ; set v := nth _ _ _=> ?.
    case: leqP=> // ; rewrite -ltnS=> j_lt_Si.
    move: (arc_non_decreasing u v (take i vertex_sorted) (drop i.+1 vertex_sorted)).
    rewrite -[false]/(~~true) ; apply contra_notN=> _ ; do!(apply/andP ; split)=> //.
    - apply/eqP ; rewrite -cat_rcons -take_nth// cat_take_drop ; done.
    - rewrite -take_nth// /v -(nth_take u0 j_lt_Si) mem_nth// size_take ; case: (i.+1 < _) ; done.
  move=> u v before after /andP[]/andP[] /[dup]/eqP decomp /toposort_list_split[]// p[] marked ; do!(move/andP=> []) ; move=> _ subseq_p_before _ /negPf u_notin_marked.
  case: (@leqP n (size p))=> [|size_p_lt_n].
  - have/(uniq_leq_size uniq_) : {subset vertex_sorted <= relevant_vertex_seq G}
      by move=> ? ; rewrite relevant_sorted.
    have : subseq [:: u] (u :: after) by rewrite/= eq_refl sub0seq.
    move/(cat_subseq subseq_p_before)/size_subseq ; rewrite -decomp size_cat addnS addn0.
    move=> h1 /(leq_trans h1).
    case: (leqP n (size p)) ; done.
  - rewrite subSn ; last by apply ltnW.
    move: size_p_lt_n ; rewrite ltnNge -subn_eq0 ; case: (_ - _)=> [//|n' _].
    rewrite -[toposort _ _ _ _]/(if _ then _ else u :: (toposort_list _ n'.+1 _ _)).
    rewrite u_notin_marked=> /eqP[] after_eq_.
    have: {subset (LocallyFiniteOrientedGraph.arc G u) <= after}.
      by rewrite after_eq_ ; apply subset_toposort_list.
    move=>/[swap] h1 /(_ v h1) ? ?.
    move: decomp uniq_ isT ; rewrite -cat_rcons=> -> ; rewrite cat_uniq=> /andP[] _ /andP[] /negP/[swap] _.
    apply contra_not=> _.
    apply /hasP ; exists v ; done.
Qed.

Lemma Nuniq_cyclic : ~~ uniq vertex_sorted -> cyclic G.
Proof.
  case: {2}vertex_sorted (erefl vertex_sorted)=> [->//|u0 ? _].
  move/(uniqPn u0)=> []i []j []i_lt_j j_lt_size.
  have/(split_nth u0): i < size vertex_sorted by apply (ltn_trans i_lt_j).
  set u := nth _ _ i=> /[swap] u_eq_nthj.
  have u_in_after: u \in drop i.+1 vertex_sorted.
    rewrite u_eq_nthj -(@subnKC i.+1 j)// -nth_drop mem_nth// size_drop ltn_sub2r//.
    move: i_lt_j ; rewrite -ltnS=> /leq_trans/(_ j_lt_size) ; done.
  move/eqP/toposort_list_split=> []// ? []marked /andP[] /andP[] _ /negPf u_notin_marked.
  case: (_ - _) => [/=|n'/=] ; first by rewrite u_notin_marked ; move: u_notin_marked => /[swap]/eqP <- ; rewrite in_cons eq_refl.
  rewrite u_notin_marked=> /eqP[] ; move: u_in_after=> /[swap]->.
  move/toposort_list_path=> [] ; first by rewrite u_notin_marked.
  move=> []u'[]p*.
  exists u ; exists (u' :: p); rewrite cons_path/= ; done.
Qed.

End toposort_correct.


Section toposort_graph.

Import Graph.Theory.

Variable (G : FiniteOrientedGraph.type V).

Lemma uniq_size_vertex_sorted : uniq (toposorted_data.vertex (toposort_graph G)) = toposorted_data.acyclic (toposort_graph G).
Proof.
  apply uniq_size_uniq.
  - rewrite/relevant_vertex_seq undup_uniq ; done.
  - move=> ? ; rewrite mem_rev ; apply relevant_sorted.
Qed.

Lemma cyclicP : reflect (cyclic G) (~~ toposorted_data.acyclic (toposort_graph G)).
Proof.
  apply/(iffP idP) ; rewrite -uniq_size_vertex_sorted rev_uniq.
  - apply Nuniq_cyclic ; done.
  - apply contraPN => /uniq_toposorted/toposorted_Ncyclic ; done.
Qed.

Lemma acyclicP : reflect (acyclic G) (toposorted_data.acyclic (toposort_graph G)).
Proof.
  move: (negPP cyclicP) ; rewrite negbK=> ?.
  apply (@iffP (~ cyclic G))=> //.
  - move=> Ncyclic u p ? ; move: I ; apply contraPT=> ? _ ; apply Ncyclic.
    exists u ; exists p ; apply/andP=> [] ; done.
  - move=> acyclic[]u[]p/andP[]/negPf p_Nnil ?.
    suff: p == [::] by rewrite p_Nnil.
    apply (acyclic u p) ; done.
Qed.

Lemma toposort_graph_decreasing_arc : acyclic G -> forall i j, OrientedGraph.arc (toposorted_data.graph (toposort_graph G)) i j -> i > j.
Proof.
  move=>/acyclicP/cyclicP/(@contra_notT _ _ (@Nuniq_cyclic _))/[dup] uniq_
    /uniq_toposorted/toposortedP toposorted_ i j.
  rewrite mem_arc mem_sort mem_undup=> /mapP [][]u v /mem_arcP/[dup]/[dup] arc_uv /arc1_relevant /[swap] /arc2_relevant/=.
  rewrite !relevant_sorted -mem_rev -[u \in _]mem_rev=> v_in_ u_in_ [] i_eq_ j_eq_.
  move: (u_in_) (v_in_) ; rewrite -!index_mem => ?/[dup]? ; rewrite size_rev.
  set m := size (toposort_list _ _ _ _)=> _.
  suff : m - i.+1 < m - j.+1.
    apply contraPT.
    rewrite -leqNgt -ltnS=> /(leq_sub2l m) ; rewrite leqNgt=> /negP ; done.
  apply (toposorted_ u).
  - 1,2: rewrite !ltn_subrL/m ; case: toposort_list u_in_ ; done.
  - rewrite/m -!nth_rev ?i_eq_ ?j_eq_ ?nth_index// -[X in _ < X]size_rev ; done.
Qed.

End toposort_graph.

End Theory.

End Theory.
