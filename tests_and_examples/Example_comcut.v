(* We use `comcut`to prove that the following diagram
    * --> * --> * --> * --> *
    |     |     |     |     |
    v     v     v     v     v
    * --> * --> * --> * --> *
  provided the squares commutes. *)
From mathcomp Require Import all_ssreflect.
From Comcut Require Import Abelian_category Diagram Comcut.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Section Five_lemma_commutativity_theory.

Import CategoryTheory FiniteOrientedGraph Diagram.

Close Scope ring_scope.

(** Graph of the five-lemma **)
Let G := FiniteOrientedGraph.Build ( ((0,4),(1,4)) :: flatten [seq [:: ((0,i),(1,i)) ; ((0,i),(0,i.+1)) ; ((1,i),(1,i.+1)) ] | i <- iota 0 4]).


Variables (Cat : CategoryWithZero.type) (D : Diagram.type (nat_eqType * nat_eqType) Cat).

Hypothesis (commutative_square : forall i, i <= 3 -> map D (0,i) (0,i.+1) >> map D (0,i.+1) (1,i.+1) = map D (0,i) (1,i) >> map D (1,i) (1,i.+1)).

Lemma commutative_G : commutative D G.
Proof.
  apply (comcut_correct (0,0)).
  apply/acyclicP ; done.
  set l := comcut _ _ ; compute in l ; rewrite/l=> {l} F.
  do!rewrite in_cons=> /orP[]//.
  all:move/eqP->=> /= ; rewrite loop_id !compmid ; rewrite commutative_square ; done.
Qed.

End Five_lemma_commutativity_theory.
