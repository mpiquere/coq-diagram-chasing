(* In this file we use FanL to prove that if the composition of two morphisms *)
(* is monic then the first morphism is. *)

From mathcomp Require Import all_ssreflect all_algebra.
From DiagramChasing Require Import FanL.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Definition monoQ := {Q [:: (0,1);(0,1);(0,2);(1,2)]}.

Definition monoQD  := Eval compute in quiver_dual monoQ.



Open Scope fanl_scope.


Definition monoF :=
  Lambda_arc [:: mapQ]
  (Forall monoQ (
    EqD (Restr {sA [:: 3]} $0) $1
    -=> Commute (Restr {sA [:: 0 ; 2 ; 3]} $0)
    -=> Commute (Restr {sA [:: 1 ; 2 ; 3]} $0)
    -=> Commute (Restr {sA [:: 0 ; 1]} $0))) .

Definition mono_monomPF :=
  formula_fill_vertices [::] (
    Forall compQ (
      Commute $0
      -=> monoF App (Restr {sA [:: 1]} $0)
      -=> monoF App (Restr {sA [:: 0]} $0))).

Definition epiF :=
  Lambda_arc [:: mapQD]
  (Forall monoQD (
    EqD (Restr {sA [:: 3]} $0) $1
    -=> Commute (Restr {sA [:: 0 ; 2 ; 3]} $0)
    -=> Commute (Restr {sA [:: 1 ; 2 ; 3]} $0)
    -=> Commute (Restr {sA [:: 0 ; 1]} $0))).

Definition epi_mepiPF :=
  formula_fill_vertices [::] (
    Forall compQD (
      Commute $0
      -=> epiF App (Restr {sA [:: 1]} $0)
      -=> epiF App (Restr {sA [:: 0]} $0))).

Close Scope fanl_scope.

Section mono_coq.

Open Scope fanl_coq_scope.

Variable (diagram : diagram_type).

Definition mono2 :=
  funD D :: diagram on [:: mapQ] =>
  forallD D' :: diagram on monoQ,
    eqD diagram (diagram_restr_arc [:: 3] D') D ->
    commute (diagram_restr_arc [:: 0;2;3] D') ->
    commute (diagram_restr_arc [:: 1;2;3] D') ->
    commute (diagram_restr_arc [:: 0;1] D').

(* Another equivalent definition which is defitionaly equal to what we will get
    evaluating the corresponding Fanl formula *)
Definition mono :=
  funD D :: diagram on [:: mapQ] =>
  forallD D' :: diagram on monoQ,
    eqD diagram (diagram_restr diagram {sQ [:: 3] <o monoQ} D') D ->
    commute (diagram_restr diagram {sQ [:: 0;2;3] <o monoQ} D') ->
    commute (diagram_restr diagram {sQ [:: 1;2;3] <o monoQ} D') ->
    commute (diagram_restr diagram {sQ [:: 0;1] <o monoQ} D').

Definition mono_monomP :=
  forallD D :: diagram on compQ,
    @commute diagram D ->
    mono (diagram_restr diagram {sQ [:: 1] <o compQ} D) ->
    mono (diagram_restr diagram {sQ [:: 0] <o compQ} D).

Definition epi :=
  funD D :: diagram on [::mapQD] =>
  forallD D' :: diagram on monoQD,
  eqD diagram (diagram_restr diagram {sQ [:: 3] <o monoQ} D') D ->
  commute (diagram_restr diagram {sQ [:: 0;2;3] <o monoQ} D') ->
  commute (diagram_restr diagram {sQ [:: 1;2;3] <o monoQ} D') ->
  commute (diagram_restr diagram {sQ [:: 0;1] <o monoQ} D').

Definition epi_mepiP :=
  forallD D :: diagram on compQD,
    @commute diagram D ->
    epi (diagram_restr diagram {sQ [:: 1] <o compQ} D) ->
    epi (diagram_restr diagram {sQ [:: 0] <o compQ} D).

Close Scope fanl_coq_scope.

End mono_coq.



Section Test.

Open Scope fanl_scope.

Variable (diagram : diagram_type).

Goal (forall D, base diagram D = mapQ -> @mono diagram D) =
  @formula_eval diagram [::] (Forall mapQ (monoF App $0)).
Proof.
  done.
Qed.

Goal (forall D, base diagram D = mapQD -> @epi diagram D) =
  @formula_eval diagram [::] (Forall mapQD (epiF App $0)).
Proof.
  done.
Qed.

Goal mono_monomP diagram = @formula_eval diagram [::] mono_monomPF.
  done.
Qed.

Goal epi_mepiP diagram = @formula_eval diagram [::] epi_mepiPF.
  done.
Qed.

Goal formula_dual mono_monomPF = epi_mepiPF.
Proof. done. Qed.

Goal forall diagram, epi_mepiP diagram = @formula_eval diagram [::] (formula_dual mono_monomPF).
Proof. done. Qed.

Close Scope fanl_scope.

End Test.

Definition sQ54 := (subquiver_Build [::0;3;2] [::6;5;1;3]).

Definition mono_monom_pf : proof := [::
  IntroAll ;
  (Merge 2) ;
  (ApplyEFT Comp
    [:: aT (Restr (subquiver_Build [:: 0; 2; 3] [:: 2; 4]) (Var 2))]) ;
  (Merge 7) ;
  (Comauto 4 (subquiver_Build (iota 0 4)  [:: 0; 1; 2; 3; 4; 5])) ;
  (Comauto 4 (subquiver_Build (iota 0 4)  [:: 0; 1; 2; 3; 4; 6])) ;
  (Comauto 4 (subquiver_Build [:: 0; 3; 2] [:: 5; 1; 3])) ;
  (Comauto 4 (subquiver_Build [:: 0; 3; 2] [:: 6; 1; 3])) ;
  (ApplyEFT (EqD_refl mapQ) [::
    aT (Restr (subquiver_Build [:: 3; 2]  [:: 3]) (Var 4)) ] ) ;
  (ApplyFT [:: aT (Restr sQ54 (Var 4)); aP 15; aP 14; aP 13] 1) ;
  (ExactN 16)
].


(* Some tactics to be able to follow the proof step by step *)
Tactic Notation "fanl_goal" constr(g) := (
  let s := fresh "s" in
  pose pf := [::] : proof ;
  pose s := Some (formula_to_sequent g) ;
  vm_compute in s ).
Tactic Notation "fanl_step" constr(t) := (
  match goal with
  | pf := _ : proof, s := _ : option sequent |- _ =>
    let s' := fresh "s'" in
    let pf' := fresh "pf'" in
    pose s' := if s is Some s then t s else None ;
    pose pf' := rcons pf t ; cbn in pf' ; move {pf} ;
      set pf := pf' ; hnf in pf ; move {pf'} ;
    vm_compute in s' ; move {s} ; set s := s' ; hnf in s ; move {s'}
  end ).
Tactic Notation "fanl_end" := (
    match goal with
    | pf := _ : proof |- _ => exact pf
    end ).

Definition mono_monom_pf' : proof.
Proof.
  pose sQ54 := (subquiver_Build [::0;3;2] [::6;5;1;3]).
  fanl_goal mono_monomPF.
  fanl_step IntroAll.
  fanl_step (Merge 2).
  fanl_step (ApplyEFT Comp
    [:: aT (Restr (subquiver_Build [:: 0; 2; 3] [:: 2; 4]) (Var 2))]).
  fanl_step (Merge 7).
  fanl_step (Comauto 4 (subquiver_Build (iota 0 4)  [:: 0; 1; 2; 3; 4; 5])).
  fanl_step (Comauto 4 (subquiver_Build (iota 0 4)  [:: 0; 1; 2; 3; 4; 6])).
  fanl_step (Comauto 4 (subquiver_Build [:: 0; 3; 2] [:: 5; 1; 3])).
  fanl_step (Comauto 4 (subquiver_Build [:: 0; 3; 2] [:: 6; 1; 3])).
  fanl_step (ApplyEFT (EqD_refl mapQ) [::
    aT (Restr (subquiver_Build [:: 3; 2]  [:: 3]) (Var 4)) ] ).
  fanl_step (ApplyFT [:: aT (Restr sQ54 (Var 4)); aP 15; aP 14; aP 13] 1).
  fanl_step (ExactN 16).
  fanl_end.
Defined.


Definition mono_monom_vpf (diagram : category_diagram_type) : valid_proof diagram.
Proof. validify mono_monom_pf. Defined.

Lemma mono_monom (diagram : category_diagram_type) : @formula_eval diagram [::] mono_monomPF.
Proof.
  apply: (check_proof_valid (pf := mono_monom_vpf diagram)) ; first by [].
  vm_compute ; done.
Qed.

Definition epi_mepi_vpf (diagram : category_diagram_type) : valid_proof diagram.
Proof. dualify mono_monom_pf. Defined.

Lemma epic_mepic (diagram : category_diagram_type) :
  @formula_eval diagram [::] epi_mepiPF.
Proof.
  apply: (check_proof_valid (pf := epi_mepi_vpf diagram)) ; first by [].
  vm_compute ; done.
Qed.



Section DiagramInstance.

Record zmod_diagram : Type := ZModDiagram {
  zmob : nat -> zmodType;
  zmmap : forall u v : nat, nat -> {additive zmob u -> zmob v};
  zmdiag_to_quiver : quiver;
}.


Record zmod_iso (A B : zmodType) :=
  ZModIso {zmiso_of :> {additive A -> B} ; zmisoP : bijective zmiso_of}.

Lemma zmod_iso_bij A B (phi : zmod_iso A B) : bijective phi.
Proof. by case: phi. Qed.

Hint Resolve zmod_iso_bij.

Definition zmod_iso_id A : zmod_iso A A.
apply: (@ZModIso _ _ (GRing.idfun_additive A)).
exact: inv_bij.
Defined.

Definition pre_zmod_diag_eq (d1 d2 : zmod_diagram) : Prop :=
  exists phi : forall u (*TODO in quiver *), {additive (zmob d1 u) -> (zmob d2 u)},
  exists psi : forall u (*TODO in quiver *), {additive (zmob d2 u) -> (zmob d1 u)},
      (forall u, cancel (psi u) (phi u)  /\ cancel (phi u) (psi u)) /\
         (forall u v k, (phi v) \o (zmmap d1 u v k) =1 (zmmap d2 u v k) \o (phi u)).


Definition zmod_diag_eq  (d1 d2 : zmod_diagram) : Prop :=
 (pre_zmod_diag_eq d1 d2) /\ (zmdiag_to_quiver d1 = zmdiag_to_quiver d2).

Lemma zmod_diag_eq_equiv : Relation_Definitions.equivalence _ zmod_diag_eq.
Proof.
split.
- move=> d; split=> //.
  by exists (fun u => GRing.idfun_additive _); exists (fun u => GRing.idfun_additive _); split.
- move=> d1 d2 d3 [[phi1 [psi1 [bij1 e_phi] e1]]] [[phi2 [psi2 [bij2 e_psi] e2]]].
  split; last by rewrite -e2.
  move=> {e1 e2}.
  exists (fun u => [additive of (phi2 u) \o (phi1 u)]).
  exists (fun u => [additive of (psi1 u) \o (psi2 u)]); split.
  + move=> u; case: (bij1 u) => {bij1} bij1 jib1; case: (bij2 u)=> {bij2} bij2 jib2.
    split=> x /=; first by rewrite bij1.
    by rewrite jib2.
  + move=> u v k x /=.
    by rewrite [X in phi2 v X]e_phi -[RHS]e_psi.
  + move=> u v [[phi [psi [bij_phi e_phi] e]]].
    split=> // {e}.
    exists psi, phi; split.
    * by move=> x; case: (bij_phi x).
    * move=> x y k l.
      case: (bij_phi x) => bijx jibx.
      case: (bij_phi y) => bijy jiby.
      by rewrite -[in LHS](bijx l) /comp /= -[X in psi y X]e_phi [LHS]jiby.
Qed.


End DiagramInstance.
