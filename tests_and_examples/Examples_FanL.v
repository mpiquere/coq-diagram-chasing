(* Some examples to do proofs with low level tactics with FanL *)

From mathcomp Require Import all_ssreflect.
From DiagramChasing Require Import FanL.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import FanL.

Open Scope fanl_scope.

Definition Taut_id Q := Forall Q (Commute $0 -=> Commute $0).

Lemma Taut_id_v Q : demo (Taut_id Q).
Proof.
  fanl Intro.
  fanl Intro.
  fanl (Exact 0).
  fanl_done.
Defined.



Definition Taut_andC Q := Forall Q (Forall Q (Commute $0 /\ Commute $1 -=> Commute $1 /\ Commute $0)).

Lemma Taut_andC_v Q : demo (Taut_andC Q).
Proof.
  fanl Intro.
  fanl Intro.
  fanl Intro.
  fanl (Split 0).
  fanl (Conj 2 1).
  fanl (Exact 3).
  fanl_done.
Defined.


Lemma Taut_andC_pf : proof.
Proof.
  fanlu Intro.
  fanlu Intro.
  fanlu Intro.
  fanlu (Split 0).
  fanlu (Conj 2 1).
  fanlu (Exact 3).
  fanlu_done.
Defined.

Lemma Taut_andC_v' Q : demo (Taut_andC Q).
Proof.
  exists Taut_andC_pf.
  vm_compute ; done.
Defined.


Goal forall diagram Q, quiver_wf Q -> @formula_eval diagram [::] (Taut_andC Q).
  move=> diagram Q Q_wf.
  apply: (@check_proof_valid _ _) ; first by rewrite /= !Q_wf.
  apply: (projT2 (Taut_andC_v Q)).
Qed.


Definition Taut_exists_default Q := (Forall Q (Commute $0)) -=> (Exists Q FTrue) -=> Exists Q (Commute $0).

Lemma Taut_exists_default_v Q : demo (Taut_exists_default Q).
Proof.
  fanl Intro.
  fanl Intro.
  fanl (Split 1).
  fanl (Exist 0) ; last by do?(rewrite /= eqxx).
  fanl (Apply 0 0) ; last by do?(rewrite /= eqxx).
  fanl (Exact 3) ; last by do?(rewrite /= eqxx).
  fanl_done ; last by do?(rewrite /= eqxx).
Defined.

Goal forall diagram Q, quiver_wf Q -> @formula_eval diagram [::] (Taut_exists_default Q).
  move=> diagram Q Q_wf.
  apply: (@check_proof_valid _ _) ; first by rewrite /= Q_wf.
  apply: (projT2 (Taut_exists_default_v Q)).
Qed.


Definition Taut_Modus_Ponens Q Q' := Forall Q (Forall Q' ((Commute $1 -=> Commute $0) -=> Commute $1 -=> Commute $0)).

Lemma Taut_Modus_Ponens_v Q Q' : demo (Taut_Modus_Ponens Q Q').
Proof.
  fanl Intro ; fanl Intro ; fanl Intro ; fanl Intro.
  fanl (Apply 1 0).
  fanl (Exact 2).
  fanl_done.
Defined.

Goal forall diagram Q Q', quiver_wf Q -> quiver_wf Q' ->
  @formula_eval diagram [::] (Taut_Modus_Ponens Q Q').
Proof.
  move=> diagram Q Q' Q_wf Q'_wf.
  apply: check_proof_valid ; first by rewrite/= Q_wf Q'_wf.
  apply: (projT2 (Taut_Modus_Ponens_v _ _)).
Qed.


Definition EqD_reflP Q := Forall Q (EqD $0 $0).

Lemma EqD_reflP_v Q : quiver_wf Q -> demo (EqD_reflP Q).
Proof.
  move=> Q_wf.
  fanl (Use (EqD_refl Q)); last by rewrite/= Q_wf.
  fanl (Exact 0); last by rewrite/= Q_wf/= eqxx.
  fanl_done ; rewrite/= Q_wf/= eqxx ; done.
Defined.

Goal forall diagram Q, quiver_wf Q -> @formula_eval diagram [::] (EqD_reflP Q).
  move=> diagram Q Q_wf.
  apply: check_proof_valid ; first by rewrite/= Q_wf/=.
  apply: (projT2 (EqD_reflP_v _)) ; done.
Qed.


Definition Rewrite1 Q := Forall Q (Forall Q (EqD $0 $1 /\ Commute $0 -=> Commute $1)).

Lemma Rewrite1_v Q : demo (Rewrite1 Q).
Proof.
  fanl Intro ; fanl Intro ; fanl Intro ; fanl (Split 0).
  fanl (Rewrite 1 2 0).
  fanl (Exact 3).
  fanl_done.
Defined.

Goal forall (diagram : compatible_diagram_type) Q,
    quiver_wf Q -> @formula_eval diagram [::] (Rewrite1 Q).
  move=> diagram Q Q_wf.
  apply: check_proof_valid ; first by rewrite /= Q_wf eqxx.
  apply: (projT2 (Rewrite1_v _)).
Qed.


(* 0 -0- 1 -4- 2
   |     |     |
   |1    |3    |6
   3 -2- 4 -5- 5  *)


Definition doubleSquare := {Q  [:: (0,1);(0,3);(3,4);(1,4);(1,2);(4,5);(2,5)]}.
Definition firstSquare  := {sQ [:: 0;1;2;3] <o doubleSquare}.
Definition secondSquare := {sQ [:: 3;4;5;6] <o doubleSquare}.

Definition DoubleSquare :=
  Forall doubleSquare
    ( Commute (Restr firstSquare  $0) /\
      Commute (Restr secondSquare $0)
    -=> Commute $0 ).


Compute formula_eval_proof DoubleSquare [:: Intro ; Intro ; Split 0 ;
(Comauto 0 (subquiver_full doubleSquare)) ;
Use (EqD_full doubleSquare) ;
Apply 0 4 ;
Rewrite 5 3 0 ;
Exact 6].



Compute osequent_eval_proof (Some (formula_to_sequent DoubleSquare)) [:: Intro ; Intro ; Split 0 ;
(Comauto 0 (subquiver_full doubleSquare)) ;
Use (EqD_full doubleSquare) ;
Apply 0 4 ;
Rewrite 5 3 0 ;
Exact 6].



Definition DoubleSquare_pf : proof.
Proof.
  fanlu Intro.
  fanlu Intro.
  fanlu (Split 0).
  fanlu (Comauto 0 (subquiver_full doubleSquare)).
  fanlu (Use (EqD_full doubleSquare)).
  fanlu (Apply 0 4).
  fanlu (Rewrite 5 3 0).
  fanlu (Exact 6).
  fanlu_done.
Defined.

Lemma DoubleSquare_v : demo (DoubleSquare).
Proof.
  exists DoubleSquare_pf.
  vm_compute ; done.
Qed.




Definition square5 := {Q  [:: (00,01);(00,10);(10,11);(01,11);(01,02);(11,12);(02,12);(02,03);(12,13);(03,13);(03,04);(13,14);(04,14);(04,05);(14,15);(05,15)]}.
Definition square15  := {sQ [:: 0;1;2;3]   <o square5}.
Definition square25 := {sQ [:: 3;4;5;6]    <o square5}.
Definition square35 := {sQ [:: 6;7;8;9]    <o square5}.
Definition square45 := {sQ [:: 9;10;11;12] <o square5}.
Definition square55 := {sQ [::12;13;14;15] <o square5}.

Definition Square5 :=
  Forall square5
    ( Commute (Restr square15  $0) /\
      Commute (Restr square25  $0) /\
      Commute (Restr square35  $0) /\
      Commute (Restr square45  $0) /\
      Commute (Restr square55  $0)
    -=> Commute $0 ).

(* The next line takes a lot of time. *)
(* Compute formula_eval_proof Square5
[:: Intro ; Intro ; Split 0 ; Split 2 ; Split 4 ; Split 6 ;
  Comauto 0 (subquiver_full square5) ;
  Use (EqD_full square5) ;
  Apply 0 10 ;
  Rewrite 11 9 0 ;
  Exact 12 ]. (* 77 *) *)
