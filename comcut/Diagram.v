From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(********************************************************************************)
(* STRUCTURE DEFINITION                                                         *)
(* Definition of three slightly different structures for graphs and of diagrams *)
(********************************************************************************)
Module OrientedGraph.

Structure type (vertex : eqType) := Build {
  arc : rel vertex;
}.

Arguments arc : simpl never.


Section Theory.

Definition path (V : eqType) (G : OrientedGraph.type V) (u : V) (p : seq V) (v : V) :=
  path.path (arc G) u p && (last u p == v).

Definition pullback (V V' : eqType) (f : V -> V') (G' : OrientedGraph.type V') := {| arc := relpre f (arc G') |}.

Definition subgraph (V : eqType) (G G' : OrientedGraph.type V) := subrel (arc G) (arc G').

Definition morph (V V' : eqType) (f : V -> V') (G : OrientedGraph.type V) (G' : OrientedGraph.type V') := subgraph G (pullback  f G').

End Theory.

End OrientedGraph.


Module LocallyFiniteOrientedGraph.

Structure type (vertex : eqType) := Build {
  arc : vertex -> seq vertex;
}.

Arguments arc : simpl never.

Definition to_OrientedGraph (V : eqType) (G : LocallyFiniteOrientedGraph.type V) := OrientedGraph.Build (fun u v => v \in arc G u).

Coercion to_OrientedGraph : LocallyFiniteOrientedGraph.type >-> OrientedGraph.type.
Canonical to_OrientedGraph.

End LocallyFiniteOrientedGraph.


Module FiniteOrientedGraph.

Structure type (vertex : eqType) := Build {
  arc : seq (vertex * vertex);
}.


Section Coercions.

Variables (V : eqType).
Implicit Type (G : FiniteOrientedGraph.type V).

Definition to_LocallyFiniteOrientedGraph G := LocallyFiniteOrientedGraph.Build (fun v => undup (map snd (filter (fun a => a.1 == v) (arc G)))).

Definition to_OrientedGraph G := LocallyFiniteOrientedGraph.to_OrientedGraph (to_LocallyFiniteOrientedGraph G).

End Coercions.

Coercion to_LocallyFiniteOrientedGraph : FiniteOrientedGraph.type >-> LocallyFiniteOrientedGraph.type.
Canonical to_LocallyFiniteOrientedGraph.

Coercion to_OrientedGraph : type >-> OrientedGraph.type.
Canonical to_OrientedGraph.


Section Theory.

Variables (V : eqType).
Implicit Type (G : FiniteOrientedGraph.type V).

Lemma mem_arc G : forall u v : V, OrientedGraph.arc G u v = ((u, v) \in arc G).
Proof.
  move=> u v.
  case: (@idP (_ \in _)) ; rewrite/OrientedGraph.arc/= mem_undup.
  - move=> uv_in_arc ; apply /mapP ; exists (u, v) => // ; rewrite mem_filter eq_refl ; done.
  - apply contra_notF => /mapP[] a ; rewrite mem_filter => /andP[] /eqP<- ? -> ; rewrite -surjective_pairing ; done.
Qed.

Lemma mem_arcP G : forall u v : V, reflect ((u,v) \in arc G) (OrientedGraph.arc G u v).
Proof.
  move=> * ; rewrite -mem_arc ; apply idP.
Qed.

Definition relevant_vertex_seq G := undup (unzip1 (arc G) ++ unzip2 (arc G)).

Definition relevant_vertex G := { v : V | v \in relevant_vertex_seq G }.


Definition vertex_from_relevant_vertex G : relevant_vertex G -> V.
move=>[] ; done.
Defined.

Coercion vertex_from_relevant_vertex : relevant_vertex >-> Equality.sort.


Definition add_arc (V : eqType) (G : FiniteOrientedGraph.type V) (a1 a2 : V) := FiniteOrientedGraph.Build ((a1, a2) :: (FiniteOrientedGraph.arc G)).

Definition pushforward (V V' : eqType) (f : V -> V') (G : FiniteOrientedGraph.type V) := {| arc :=  map (fun a => (f a.1, f a.2)) (arc G) |}.

End Theory.

End FiniteOrientedGraph.



Module Diagram.

From Comcut Require Export Abelian_category.

Export CategoryTheory.

Structure type (vertex : Type) (Cat : Category.type) := Build {
    obj : vertex -> Cat;
    map : forall u v : vertex, Map (obj u) (obj v);
    loop_id : forall u : vertex, map u u = id_ (obj u);
}.


Module Builders.

Import FiniteOrientedGraph.

Definition eq_id (Cat : Category.type) (u v : Cat) : u == v -> Map u v.
  by move=> /eqP-> ; apply id_.
Defined.

Definition obj_from_obj_seq (V : eqType) (G : FiniteOrientedGraph.type V) (Cat : CategoryWithZero.type) (obj_seq : seq Cat) := let l := relevant_vertex_seq G in fun u => nth CategoryWithZeroTheory.Zero obj_seq (index u l).

Definition enumerate_obj (V : eqType) (G : FiniteOrientedGraph.type V) (Cat : CategoryWithZero.type) (obj' : forall u : relevant_vertex G, Cat) : V -> Cat.
  move => u; move: obj' ; rewrite/relevant_vertex.
  move/(_ (exist _ u _)).
  case: (u \in relevant_vertex_seq G).
  - move/(_ isT) => a ; exact a.
  - move => _ ; exact CategoryWithZeroTheory.Zero.
Defined.

(* (* TODO : to rewrite with id_loop *)
Definition from_FiniteGraph_map (V : eqType) (Cat : CategoryWithZero.type) (G : FiniteOrientedGraph.type V) (obj : V -> Cat) (map : forall u v : V, (u, v) \in (arc G) -> Map (obj u) (obj v)) : forall u v : V, Map (obj u) (obj v).
  move=> u v.
  move: (@eq_id Cat (obj u) (obj v)).
  move: (introT (@eqP _ (obj u) (obj v))).
  move: (@congr1 _ _ obj u v).
  move: (elimT (@eqP _ u v)).
  case (u == v)=> [/(_ isT) h1 /(_ h1) h2| _].
  case (@idP (u == v))=> [|_].
    move/eqP/(congr1 obj)/eqP/eq_id ; done.
  case (@idP ((u,v) \in arc G)) => [|_] ; first by apply map.
  apply CategoryWithZeroTheory.zero_map.
Defined.

Definition from_FiniteGraph (V : eqType) (Cat : CategoryWithZero.type) (G : FiniteOrientedGraph.type V) (obj : V -> Cat) (map : forall u v : V, (u, v) \in (arc G) -> Map (obj u) (obj v)) : Diagram.type V Cat. *)

End Builders.


Section Theory.

Import OrientedGraph.

Variable (V V' : eqType) (Cat : Category.type).

Fixpoint comp_path (D : Diagram.type V Cat) u p v : Map (obj D u) (obj D v) :=
  if p is u' :: p' then
    map D u u' >> comp_path D u' p' v
  else
    map D u v.

Definition commutative (D : Diagram.type V Cat) (G : OrientedGraph.type V) := forall (u v : V) (p p' : seq V), path G u p v -> path G u p' v -> comp_path D u p v = comp_path D u p' v.

Definition transformation_type (D D' : Diagram.type V Cat) := forall u : V, Map (obj D u) (obj D' u).

Definition transformation (D D' : Diagram.type V Cat) (f : transformation_type D D') (G : OrientedGraph.type V) := forall u v : V, arc G u v -> f u >> map D' u v = map D u v >> f v.

Definition pullback (f : V -> V') (D' : Diagram.type V' Cat) : Diagram.type V Cat.
  apply (@Diagram.Build V Cat ((obj D') \o f) (fun u v => (map D') (f u) (f v))).
  move=> u ; apply (loop_id D').
Defined.

End Theory.

End Diagram.



Module Theory.

Section OrientedGraphTheory.

Import OrientedGraph.

Variable (V : eqType) (G : OrientedGraph.type V).

Implicit Type (u v w : V) (p : seq V).

Lemma cons_path u w p v : path G u (w::p) v = arc G u w && path G w p v.
Proof.
  rewrite/path/= ; by rewrite andbA.
Qed.

Lemma last_path u p v : path G u p v -> last u p = v.
Proof.
  by rewrite/path=> /andP[] _ /eqP.
Qed.

Lemma path0 u v : path G u [::] v = (u == v).
Proof.
  done.
Qed.

Lemma cat_path u v p p' : path G u (p ++ p') v = path G u p (last u p) && path G (last u p) p' v.
Proof.
  rewrite/path path.cat_path last_cat eq_refl [_ && true]andbC !andbA ; done.
Qed.

Definition first_arc u p v := (u, head v p).

Lemma first_arc_arc u p v : let: (a1, a2) := first_arc u p v in p != [::] -> path G u p v -> arc G a1 a2.
Proof.
  by case: p => //= ?? _ ; rewrite cons_path=> /andP[].
Qed.

Definition last_arc u p v := (last u (belast u p), v).

Lemma last_arc_arc u p v : let: (a1, a2) := last_arc u p v in p != [::] -> path G u p v -> arc G a1 a2.
Proof.
  move=> /[swap]/andP[].
  case: (lastP p)=> [//|??].
  rewrite rcons_path belast_rcons last_rcons last_cons=> /andP[] _ /[swap] /eqP-> ; done.
Qed.

Definition cyclic G := exists u p, (p != [::]) && OrientedGraph.path G u p u.

Definition acyclic G := forall u p, OrientedGraph.path G u p u -> p == [::].

End OrientedGraphTheory.


Section FiniteOrientedGraphTheory.

Import FiniteOrientedGraph OrientedGraph.

Variable (V : eqType) (G : FiniteOrientedGraph.type V).

Lemma arc1_relevant u v : arc G u v -> u \in relevant_vertex_seq G.
  move=>/mem_arcP uv_arc.
  rewrite /relevant_vertex_seq mem_undup mem_cat ; apply/orP ; left.
  rewrite -[u]/((u,v).1) ; apply map_f ; done.
Qed.

Lemma arc2_relevant u v : arc G u v -> v \in relevant_vertex_seq G.
  move=>/mem_arcP uv_arc.
  rewrite /relevant_vertex_seq mem_undup mem_cat ; apply/orP ; right.
  rewrite -[v]/((u,v).2) ; apply map_f ; done.
Qed.

Lemma add_arc_arc a1 a2 u v : arc (add_arc G a1 a2) u v = ((u, v) == (a1, a2)) || arc G u v.
Proof.
  rewrite !mem_arc in_cons ; done.
Qed.

Lemma path_subset_relevant u p v : p != [::] -> OrientedGraph.path G u p v -> {subset u :: p <= relevant_vertex_seq G }.
Proof.
  case: p => [//|u' p _].
  elim: p u u'=> [u u'|u'' l H u u'] ;
  rewrite cons_path=> /andP[] arc_uu' ? w ;
  rewrite in_cons ; case: (@idP (_ == _))=> [/eqP-> _|_ /=] ;
  rewrite ?(arc1_relevant arc_uu')//.
  - rewrite in_cons orbC in_nil/= => /eqP-> ; apply (arc2_relevant arc_uu').
  - by apply H.
Qed.

End FiniteOrientedGraphTheory.


Section DiagramTheory.

Import Diagram OrientedGraph.

Variables (V : eqType) (G : OrientedGraph.type V) (Cat : Category.type).

Implicit Type (u v w : V) (p : seq V) (D : Diagram.type V Cat).

Lemma cons_comp_path D u w p v : comp_path D u (w :: p) v = map D u w >> comp_path D w p v.
Proof.
  done.
Qed.

Lemma cat_comp_path D u p p' v : (forall u, map D u u = id_ (obj D u)) -> comp_path D u (p ++ p') v = comp_path D u p (last u p) >> comp_path D (last u p) p' v.
Proof.
  elim: p u => [?/= -> | u' p /= /[swap]?/[swap]? -> //] ; rewrite ?compidm ?compmA ; done.
Qed.

End DiagramTheory.


Module EqMap.

Import Diagram.

Record type (Cat : Category.type) (V : eqType) (D : Diagram.type V Cat) u v := {
  map : Map (obj D u) (obj D v);
  inverse_map : Map (obj D v) (obj D u);
  id_uvu : map >> inverse_map = 'id_(obj D u);
  id_vuv : inverse_map >> map = 'id_(obj D v);
  compmiso : forall w, Diagram.map D w u >> map = Diagram.map D w v;
  compisom : forall w, map >> Diagram.map D v w = Diagram.map D u w;
}.

Definition to_isomorphic (Cat : Category.type) (V : eqType) (D : Diagram.type V Cat) u v (eq_map : EqMap.type D u v) : Isomorphism.isomorphic (obj D u) (obj D v) :=
  Isomorphism.Build_isomorphic (id_uvu eq_map) (id_vuv eq_map).

Coercion to_isomorphic : EqMap.type >-> Isomorphism.isomorphic.
Canonical to_isomorphic.

Definition to_isomorphism (Cat : Category.type) (V : eqType) (D : Diagram.type V Cat) u v (eq_map : EqMap.type D u v) : Isomorphism.isomorphism (map eq_map) :=
  Isomorphism.inverse (id_uvu eq_map) (id_vuv eq_map).

Coercion to_isomorphism : EqMap.type >-> Isomorphism.isomorphism.
Canonical to_isomorphism.

End EqMap.


Section EqMap.

Import Diagram.

Lemma eq_map (Cat : Category.type) (V : eqType) (D : Diagram.type V Cat) u v (eq_uv : u = v) : EqMap.type D u v.
  by rewrite eq_uv ; exists 'id_(obj D v) 'id_(obj D v) => [||?|?] ; rewrite ?compmid ?compidm.
Qed.

End EqMap.

Section GraphMorphismTheory.

Import FiniteOrientedGraph OrientedGraph.

Variables (V V' : eqType) (f : V -> V').

Lemma morph_pullback (G' : OrientedGraph.type V') : morph f (pullback f G') G'.
Proof.
  done.
Qed.

Lemma morph_pushforward (G : FiniteOrientedGraph.type V) : morph f G (pushforward f G).
Proof.
  move=> u v /mem_arcP ? ; apply/mem_arcP.
  apply/mapP ; exists (u, v) ; done.
Qed.

Lemma sub_path (G G' : OrientedGraph.type V) u p v : subgraph G G' -> path G u p v -> path G' u p v.
Proof.
  rewrite/path => sub_GG'/andP[]?/eqP-> ; rewrite andbC eq_refl/=.
  apply (sub_path sub_GG') ; done.
Qed.

Lemma morph_path (G : OrientedGraph.type V) (G' : OrientedGraph.type V') u p v : morph f G G' -> path G u p v -> path G' (f u) (map f p) (f v).
Proof.
  move=> morph_f ; rewrite/path path_map last_map=> /andP[] /[swap]/eqP-> ; rewrite eq_refl andbC/=.
  apply path.sub_path ; done.
Qed.

End GraphMorphismTheory.


Section DiagramMorphismTheory.

Import FiniteOrientedGraph Diagram OrientedGraph.

Variables (V V' : eqType) (Cat : Category.type).

Lemma pullback_finite (f : V -> V') (G' : FiniteOrientedGraph.type V') : forall u v : V, arc (OrientedGraph.pullback f G') u v = ((f u, f v) \in FiniteOrientedGraph.arc G').
Proof.
  move=> u v ; rewrite/arc/=/arc/=/LocallyFiniteOrientedGraph.arc/= mem_undup.
  case: (@idP ((_,_) \in _))=> [?|] ; last apply contra_notF.
  - apply/mapP ; exists (f u, f v) => // ; rewrite mem_filter eq_refl ; done.
  - move/mapP=> [][]a1 a2 ; rewrite mem_filter/= => /andP[]/eqP-> /[swap] <- ; done.
Qed.

Lemma pullback_comp_path (f : V -> V') (D' : Diagram.type V' Cat) u p v : comp_path D' (f u) (seq.map f p) (f v) = comp_path (Diagram.pullback f D') u p v.
Proof.
  by elim: p u=> [//|??/[swap]?/=->].
Qed.

Lemma transformation_comp_path (D D' : Diagram.type V Cat) (G : OrientedGraph.type V) (f : transformation_type D D') u p v : transformation f G -> path G u p v -> comp_path D u p v >> f v = f u >> comp_path D' u p v.
Proof.
  elim: p u=> [? _/=|u' p H u transfo] ; first by rewrite path0=> /eqP-> ; rewrite !loop_id compidm compmid.
  rewrite cons_path=> /andP[]* ; rewrite !cons_comp_path -compmA H// !compmA transfo ; done.
Qed.

Lemma pullback_commutative (f : V -> V') (D' : Diagram.type V' Cat) (G' : OrientedGraph.type V') : commutative D' G' -> commutative (Diagram.pullback f D') (OrientedGraph.pullback f G').
Proof.
  move=> comm_D' ?*.
  rewrite -!pullback_comp_path.
  apply comm_D' ; apply (morph_path (G := OrientedGraph.pullback f G')) ; done.
Qed.

Lemma subdiagram_commutative (D : Diagram.type V Cat) (G G' : OrientedGraph.type V) : subgraph G G' -> commutative D G' -> commutative D G.
Proof.
  move=> ? comm?*.
  apply comm ; apply (Theory.sub_path (G := G)) ; done.
Qed.

Lemma transformation_commutative (D D' : Diagram.type V Cat) (G : OrientedGraph.type V) (f : transformation_type D D') : transformation f G -> (forall u v : V, (arc G u v) -> mono (f v)) -> commutative D' G -> commutative D G.
Proof.
  move=> ? mono_f comm * u v p1 p2.
  case: (@idP ((p1 == [::]) && (p2 == [::])))=> [/andP[]/eqP->/eqP->//|/negP].
  wlog p1_nil : p1 p2 / p1 != [::]=> [H|].
    case: (@idP (p1 == [::]))=> /= [_ |/negP] /[dup]/negPf p_nil * ; [apply esym|] ;
    apply H=> // ; rewrite p_nil ; done.
  move=> _ /[dup]/(last_arc_arc p1_nil) arc__v *.
  apply (mono_f _ v arc__v).
  rewrite !(transformation_comp_path (G := G))//.
  apply congr1.
  apply comm ; done.
Qed.

End DiagramMorphismTheory.

End Theory.
