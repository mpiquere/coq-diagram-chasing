From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Require Import Commerge Utils.


Definition bipath n m :=
  mkseq
    (fun i => (if i == n then 0 else i, if i == (n+m).-1 then n else i.+1))
    (n + m).



Section bipath.

Variables (n m : nat).

Hypothesis (n_gt_0 : n > 0) (m_gt_0 : m > 0).

Definition B := bipath n m.

Lemma size_B_nm : size B = n + m.
Proof. rewrite size_mkseq ; done. Qed.


Lemma pathB_iota u i p v : path B u (i::p) v ->
  p = iota i.+1 (size p).
Proof.
  elim: p u i=> [//|i' p H] u i /cons_path_fwd[] i_lt []/eqP u_eq /[dup]path_i'p /=.
  suff -> : i' = i.+1.
    move=> /H -> ; rewrite size_iota ; done.
  move/cons_path_fwd: path_i'p=> [] i'_lt []/eqP + _.
  rewrite !nth_mkseq -?size_B_nm//=.
  case: ifP ; case: ifP=> //.
  - move=> _ _ n_eq_0 ; move: n_gt_0 ; rewrite n_eq_0 ; done.
  - rewrite eq_sym => /eqP ; done.
Qed.


Lemma cons_pathB u i p v:
  path B u (i::p) v ->
  u = if i == n then 0 else i.
Proof.
  move=> /cons_path_fwd [] i_lt []/eqP-> _ ; rewrite nth_mkseq -?size_B_nm ; done.
Qed.

Lemma last_pathB u i p v :
  path B u (i::p) v ->
  v = if (i + size p) == (n+m).-1 then n else (i + size p).+1.
Proof.
  move=> /[dup]/pathB_iota -> ; rewrite size_iota.
  rewrite -[_ :: _]/(iota i (size p).+1) iota_rcons
    rcons_path => /andP[] /andP[] pi_lt _ /eqP <-.
  rewrite nth_mkseq -?size_B_nm ; done.
Qed.


Lemma pathB_leq u i p v :
  path B u (i::p) v -> i < n -> i + size p < n.
Proof.
  move=> /[dup]/pathB_iota {1}-> + i_lt.
  rewrite -[_ :: _]/(iota _ _.+1)=> /path_unzip12 h.
  rewrite ltnNge ; apply/negP => n_le.
  move: h=> /(congr1 ((nth 0)^~ (n-i))).
  have le_p: n-i <= size p by rewrite leq_subCl leq_subLR addnC n_le.
  have gt_0 : 0 < n-i by rewrite subn_gt0.
  have n_lt : n < n + m by rewrite -{1}[n]addn0 ltn_add2l.
  rewrite nth_rcons !size_map size_iota ltnS le_p.
  rewrite -{1}[n-i]prednK//.
  rewrite -[LHS]/(nth _ _ _.-1) !(nth_map (0,0)) ?(nth_map 0) /fst/snd
    ?size_map ?size_iota ?nth_iota ?add0n ?ltnS// ;
    (try by apply (leq_trans (leq_pred _))) ;
    rewrite ?predn_sub ?subnKC// -?[_ <= _.-1]ltnS ?prednK// ;
    try by rewrite ltnW.
  rewrite eq_refl ; case: ifP=> _ ; move: n_gt_0=> /[swap] -> ; done.
Qed.


Lemma most_pathB_uniq u p v :
  path B u p v ->
  u = 0 /\ v = n \/
  p = iota (if (u == 0) && (v > n) then n else u)
    (if (u > n) && (v == n) then n+m-u else
      if (u == 0) && (v > n) then v - n else v - u).
Proof.
  case: p=> [|i p].
  - move=> /path0_fwd/eqP-> ; case: (ltngtP v n) ; rewrite/= subnn andbC/= ;
    try tauto ; case: eqP=> [->//|] ; tauto.
  - move=> /[dup]/[dup]/[dup] /cons_pathB + /pathB_leq +
      /last_pathB + /pathB_iota ->.
    case: eqP ; case: eqP ; first by tauto.
    - move=> ? -> -> _ -> ; rewrite eq_refl ltnS leq_addr ltn0/= -addnS
        -addnBAC// subnn add0n ; tauto.
    1,2: move=> + + -> ; case: (ltngtP i n)=> //.
    - move=> _ -> _ /(_ isT) ;
        rewrite prednK ?ltn_addr// leqNgt -addn1 leq_add2l m_gt_0 ; done.
    - move=> _ /(congr1 (fun x => x.+1 - i)) + _ _ ->.
      rewrite ltnn andbC eq_refl/= -addnS -addnBAC// subnn add0n prednK ?ltn_addr
        // => <- ; tauto.
    - move=> _ _ _ /(_ isT) + ->/= ; rewrite ltnS [n <= _]leqNgt andbC => ->/=.
      rewrite -addnS -addnBAC// subnn add0n ; tauto.
    - case: eqP=> [->//|] /= _ + _ _ _ -> ; case: eqP=> [<-|] ; first by
        rewrite ltnNge -addnS leq_addr.
      rewrite -addnS -addnBAC// subnn add0n ; tauto.
Qed.

Lemma pathB_0pn p : path B 0 p n -> p = iota 0 n \/ p = iota n m.
Proof.
  case: p n_gt_0=> [/[swap]/path0_fwd/eqP<-//|i p _].
  move=> /[dup]/[dup]/[dup] /cons_pathB + /pathB_leq +
      /last_pathB + /pathB_iota ->.
  case: eqP=> [-> _ _|_ <- /(_ n_gt_0)].
  - case: eqP=> [/eqP|_ /eqP] ; last by rewrite eqn_leq ltnNge andbC leq_addr.
    rewrite -[m]prednK// addnS/= eqn_add2l => /eqP -> ; tauto.
  - case: eqP=> [->|_ _ ->] ; first by
      rewrite ltnNge -[m]prednK// addnS/= -{1}[n]addn0 leq_add2l.
    tauto.
Qed.

Lemma two_paths u p q v :
  path B u p v ->
  path B u q v ->
  p = q \/
  u = 0 /\ v = n /\ (p = iota 0 n /\ q = iota n m \/
    q = iota 0 n /\p = iota n m).
Proof.
  case e: ((u == 0) && (v == n)).
  - move: e=> /andP[] /eqP-> /eqP-> /pathB_0pn[] -> /pathB_0pn[] -> ; tauto.
  - have: ~(u = 0 /\ v = n) by move: e=> /[swap] [] [] -> -> ; rewrite !eq_refl.
    move=> + /most_pathB_uniq[]// -> /most_pathB_uniq[]// -> ; tauto.
Qed.


Lemma bipath_path_total (r : path_relation) :
  r 0 n (iota 0 n) (iota n m) -> path_total B r.
Proof.
  move=> r_p1q2 u p q v /two_paths /[apply] [] [->|[]-> []-> ] ;
    first by apply π_equiv.
  move=> [[]-> ->|[]-> ->]//.
  apply Relation_Definitions.equiv_sym=> // ; apply π_equiv ; done.
Qed.

End bipath.
