From mathcomp Require Import all_ssreflect.
From DiagramChasing Require Import Commerge.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Import Commerge Graph.

Check isT : commerge (FiniteOrientedGraph.Build [:: ]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1) ]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1) ; (1,2) ]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,2) ; (2,1) ]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1) ; (2,3) ]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1) ; (1,2) ; (0,3)]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1) ; (1,2) ; (0,2)]) [:: [:: 0; 1 ; 2 ]].
Time Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1);(1,2);(2,3);(0,2);(1,3)]) [:: [:: 0;1;3]; [:: 1;2;4]].
Check isT : ~~ commerge (FiniteOrientedGraph.Build [:: (0,1) ; (0,1) ]) [::].
Check isT : ~~ commerge (FiniteOrientedGraph.Build [:: (0,1) ; (1,2) ; (0,2)]) [::].
Check isT : commerge (FiniteOrientedGraph.Build [:: (0,1);(1,2) ]) [::].
Time Compute commerge (FiniteOrientedGraph.Build
  [:: (0,1);(1,3);(0,2);(2,3)])
  [:: [::0;1;2;3]].
Time Compute commerge (FiniteOrientedGraph.Build
  [:: (0,1);(1,2);(3,4);(4,5);(0,3);(1,4);(2,5)])
  [:: [::0;2;4;5]; [::1;3;5;6]].
Time Compute commerge (FiniteOrientedGraph.Build
    [:: (0,1);(1,2);(2,3);(4,5);(5,6);(6,7);(0,4);(1,5);(2,6);(3,7)])
    [:: [::0;3;6;7]; [::1;4;7;8]; [::2;5;8;9]].
Time Compute commerge (FiniteOrientedGraph.Build (* 14s *)
  [:: (0,1);(1,2);(2,3);(3,4);(5,6);(6,7);(7,8);(8,9);(0,5);(1,6);(2,7);(3,8);(4,9)])
  [:: [::0;4;8;9]; [::1;5;9;10]; [::2;6;10;11]; [::3;7;11;12]].
Time Compute ~~ commerge (FiniteOrientedGraph.Build (* 19s *)
  [:: (0,1);(1,2);(2,3);(3,4);(5,6);(6,7);(7,8);(8,9);(0,5);(1,6);(2,7);(3,8);(4,9);(0,1)])
  [:: [::0;4;8;9]; [::1;5;9;10]; [::2;6;10;11]; [::3;7;11;12]].
