From mathcomp Require Export all_ssreflect all_algebra.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Reserved Infix ">>" (at level 49, left associativity).

Module Category.

Record mixin_of Ob := Mixin {
  Map : Ob -> Ob -> Type;
  id_ : forall (A : Ob), Map A A;
  comp : forall {A B C : Ob} (f_AB : Map A B) (f_BC : Map B C), Map A C where "f >> g" := (comp f g);
  compmA : forall (A B C D : Ob) (f_AB : Map A B) (f_BC : Map B C) (f_CD : Map C D), f_AB >> (f_BC >> f_CD) = (f_AB >> f_BC) >> f_CD;
  compidm : forall (A B : Ob), left_id (id_ A) (comp (C := B));
  compmid : forall (A B : Ob), right_id (id_ B) (comp (A := A));
}.
Record class_of (T : Type) := Class {
  base : Equality.class_of T;
  mixin : mixin_of T;
}.

Structure type := Pack { sort ; class : class_of sort }.

Definition to_eqType (T : type) := Equality.Pack (base (class T)).


Module Exports.

Coercion sort : type >-> Sortclass.
Coercion to_eqType : type >-> eqType.
Canonical to_eqType.
Coercion class : type >-> class_of.
Coercion mixin : class_of >-> mixin_of.
Coercion base : class_of >-> Equality.class_of.

End Exports.

End Category.

Export Category.Exports.



Module CategoryTheory.

Import Category.

Section Definitions.

Variable (Cat : type).

Definition Ob := @Category.sort.
Definition Map T := @Map _ (class T).
Definition id_ T := @id_ _ (class T).
Definition comp T {A B C : sort T} (f_AB : Map A B) (f_BC : Map B C) : Map A C:= @comp _ (class T) _ _ _ f_AB f_BC.

Local Infix ">>" := comp.

Definition mono [B C : Cat] (f_BC : Map B C) := forall (A : Cat) (f_AB f'_AB : Map A B), f_AB >> f_BC = f'_AB >> f_BC -> f_AB = f'_AB.

Definition epi [A B : Cat] (f_AB : Map A B) := forall (C : Cat) (f_BC f'_BC : Map B C), f_AB >> f_BC = f_AB >> f'_BC -> f_BC = f'_BC.

End Definitions.

Notation "''id_' A" := (id_ A) (at level 8, A at level 2, format "''id_' A").
Infix ">>" := comp.
Arguments comp {_ _ _ _}.


Section Theory.

Variable (Cat : type).

Lemma compmA : forall (A B C D : Cat) (f_AB : Map A B) (f_BC : Map B C) (f_CD : Map C D), f_AB >> (f_BC >> f_CD) = (f_AB >> f_BC) >> f_CD.
Proof. by apply @compmA. Qed.

Lemma compidm : forall (A B : Cat), left_id (id_ A) (comp (C := B)).
Proof. by apply compidm. Qed.

Lemma compmid : forall (A B : Cat), right_id (id_ B) (comp (A := A)).
Proof. by apply compmid. Qed.

End Theory.

End CategoryTheory.



Module Isomorphism.

Import CategoryTheory.

Module Isomorphism.

Record isomorphism (Cat : Category.type) (A B : Cat) (f_AB : Map A B) := inverse {
	map := f_AB;
  inverse_map : Map B A;
	id_ABA : f_AB >> inverse_map = id_ A;
	id_BAB : inverse_map >> f_AB = id_ B;
}.

End Isomorphism.


Module Isomorphic.

Record isomorphic (Cat : Category.type) (A B : Cat) := {
  map : Map A B;
  inverse_map : Map B A;
	id_ABA : map >> inverse_map = id_ A;
	id_BAB : inverse_map >> map = id_ B;
}.

Definition to_isomorphism (Cat : Category.type) (A B : Cat) (iso : isomorphic A B) := Isomorphism.inverse (id_ABA iso) (id_BAB iso).

Coercion to_isomorphism : isomorphic >-> Isomorphism.isomorphism.
Canonical to_isomorphism.

End Isomorphic.


Definition isomorphism := @Isomorphism.isomorphism.
Definition inverse := @Isomorphism.inverse.
Definition isomorphic := @Isomorphic.isomorphic.
Definition Build_isomorphic := @Isomorphic.Build_isomorphic.

Lemma isomorphism_unique (Cat : Category.type) (A B : Cat) (f_AB : Map A B) : forall (i i' : isomorphism f_AB), Isomorphism.inverse_map i = Isomorphism.inverse_map i'.
Proof.
move=> [_ f_BA id_ABA id_BAB] [_ f'_BA id'_ABA id'_BAB] /=.
have : (f_BA >> f_AB) >> f_BA = (f'_BA >> f_AB) >> f_BA by rewrite id_BAB id'_BAB.
by rewrite -!compmA !id_ABA !compmid.
Qed.

Lemma iso_mono (Cat : Category.type) (B C : Cat) (f_BC : Map B C) (iso_f_BC : Isomorphism.isomorphism f_BC) : mono f_BC.
Proof.
  move: iso_f_BC => [] ? f_CB BCB_id *??? /(congr1 (comp^~ f_CB)).
  rewrite -!compmA BCB_id !compmid ; done.
Qed.

End Isomorphism.



Module CategoryWithZero.

Import Category.

Record mixin_of Ob (Cat : Category.class_of Ob) := Mixin {
	Zero : Ob;
	initial_map : forall B : Ob, Map Cat Zero B;
	initial_unique : forall B (f : Map Cat Zero B), f = initial_map B;

	final_map : forall A : Ob, Map Cat A Zero;
	final_unique : forall A (f : Map Cat A Zero), f = final_map A;
}.

Record class_of Ob := Class {
  base : Equality.class_of Ob ;
  cat_mixin : Category.mixin_of Ob ;
  mixin : @mixin_of Ob (Category.Class base cat_mixin) }.

Structure type := Pack { sort ; class : class_of sort }.

Definition to_eqType (Cat : type) := Equality.Pack (base (class Cat)).

Definition to_Category_class (Cat : type) := Category.Class (base (class Cat)) (cat_mixin (class Cat)).
Definition to_Category (Cat : type) := Category.Pack (to_Category_class Cat).


Module Exports.

Coercion sort : type >-> Sortclass.
Coercion to_eqType : type >-> eqType.  (* TODO : warning *)
Canonical to_eqType.
Coercion to_Category : type >-> Category.type.
Canonical to_Category.
Coercion class : type >-> class_of.
Coercion mixin : class_of >-> mixin_of.
Coercion base : class_of >-> Equality.class_of.
Coercion cat_mixin : class_of >-> Category.mixin_of.

End Exports.

End CategoryWithZero.

Export CategoryWithZero.Exports.


Module CategoryWithZeroTheory.

Import CategoryWithZero.
Export CategoryTheory.

Section Definitions.

Variable (Cat : CategoryWithZero.type).

Definition Zero {T} := Zero (class T).
Definition initial_map T := initial_map (class T).
Definition final_map T := final_map (class T).

Variable (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C).

Definition zero_map (A B : Cat) := (final_map A) >> (initial_map B).

End Definitions.

Arguments Zero {_}.

Arguments zero_map {_ _ _}.
Notation "0" := zero_map.


Section Theory.

Variable (Cat : CategoryWithZero.type).

Lemma initial_unique : forall (B : Cat) (f : Map Zero B), f = initial_map B. Proof. move=> B f ; rewrite/initial_map. apply (CategoryWithZero.initial_unique f). Qed.

Lemma final_unique : forall (A : Cat) (f : Map A Zero), f = final_map A. Proof. move=> A f ; rewrite/final_map. apply (CategoryWithZero.final_unique f). Qed.

Lemma compm0 : forall (A B C : Cat) (f_AB : Map A B), f_AB >> (0 : Map B C) = 0.
Proof.
by move => * ; rewrite /zero_map compmA [X in X >> _]final_unique.
Qed.

Lemma comp0m : forall (A B C : Cat) (f_BC : Map B C), (0 : Map A B) >> f_BC = 0.
Proof.
by move => * ; rewrite /zero_map -compmA [X in _ >> X]initial_unique.
Qed.

Definition epi (A B : Cat) (f_AB : Map A B) := forall (C : Cat) (f_BC : Map B C), f_AB >> f_BC = 0 -> f_BC = 0.

Definition mono (B C : Cat) (f_BC : Map B C) := forall (A : Cat) (f_AB : Map A B), f_AB >> f_BC = 0 -> f_AB = 0.

End Theory.

End CategoryWithZeroTheory.



Import GRing.Theory.
Open Scope ring_scope. (* TODO : put inside modules *)

Module PreAbCategory.

Import Category.

Record mixin_of Ob (cat_mixin : Category.mixin_of Ob) := Mixin {
  zmodMap : forall A B : Ob, GRing.Zmodule.class_of (Map cat_mixin A B);
}.

Record class_of Ob := Class {
  base : Equality.class_of Ob ;
  cat : Category.mixin_of Ob ;
  mixin : mixin_of cat }.

Structure type := Pack { sort ; class : class_of sort }.


Section Coercion.

Variable (Cat : type).

Let Ob := sort Cat.

Definition to_eqType := Equality.Pack (base (class Cat)).

Definition to_Category_class := Category.Class (base (class Cat)) (cat (class Cat)).
Definition to_Category := Category.Pack to_Category_class.

Definition from_mapType_to_zmodType (A B : Ob) : zmodType := GRing.Zmodule.Pack (zmodMap (mixin (class Cat)) A B).

End Coercion.


Module Exports.

Coercion sort : type >-> Sortclass.
Coercion to_eqType : type >-> eqType.
Canonical to_eqType.
Coercion to_Category : type >-> Category.type.
Canonical to_Category.
Coercion class : type >-> class_of.
Coercion mixin : class_of >-> mixin_of.
Coercion cat : class_of >-> Category.mixin_of.
Coercion base : class_of >-> Equality.class_of.
Canonical from_mapType_to_zmodType.

End Exports.

End PreAbCategory.

Export PreAbCategory.Exports.



Module AbCategory.

Import CategoryTheory.

Record mixin_of (Cat : PreAbCategory.type) := Mixin {
  compmDr : forall (A B C : Cat) (f_AB : Map A B), additive (comp (C := C) f_AB);
  compmDl : forall (A B C : Cat) (f_BC : Map B C), additive ((comp (A := A)^~ f_BC));
}.

Record class_of Ob := Class {
  base : Equality.class_of Ob ;
  cat_mixin : Category.mixin_of Ob ;
  preab_mixin : PreAbCategory.mixin_of cat_mixin;
  mixin : mixin_of (PreAbCategory.Pack (PreAbCategory.Class base preab_mixin));
}.

Structure type := Pack { sort ; class : class_of sort }.


Section Coercion.

Variable (Cat : type).

Let Ob := sort Cat.

Definition to_eqType := Equality.Pack (base (class Cat)).

Definition to_Category_class := Category.Class (base (class Cat)) (cat_mixin (class Cat)).
Definition to_Category := Category.Pack to_Category_class.

Definition to_PreAbCategory_class := PreAbCategory.Class (base (class Cat)) (preab_mixin (class Cat)).
Definition to_PreAbCategory := PreAbCategory.Pack to_PreAbCategory_class.

End Coercion.


Module Exports.

Coercion sort : type >-> Sortclass.
Coercion to_eqType : type >-> eqType.
Canonical to_eqType.
Coercion to_Category : type >-> Category.type.
Canonical to_Category.
Coercion to_PreAbCategory : type >-> PreAbCategory.type.
Canonical to_PreAbCategory.
Coercion class : type >-> class_of.
Coercion base : class_of >-> Equality.class_of.
Coercion cat_mixin : class_of >-> Category.mixin_of.
Coercion preab_mixin : class_of >-> PreAbCategory.mixin_of.
Coercion mixin : class_of >-> mixin_of.

End Exports.

End AbCategory.

Export AbCategory.Exports.



Module AbCategoryTheory.

Import AbCategory.
Export CategoryTheory.

Section Theory.

Variable Cat : type.

Lemma compmDr : forall (A B C : Cat) (f_AB : Map A B), additive ((comp (C := C)) f_AB ).
Proof. apply (@compmDr _ Cat). Qed.

Lemma compmDl : forall (A B C : Cat) (f_BC : Map B C), additive ((comp (A := A))^~ f_BC).
Proof. apply (@compmDl _ Cat). Qed.

End Theory.

End AbCategoryTheory.



Module AbCategoryWith.

Import AbCategoryTheory.

Record mixin_of (Cat : AbCategory.type) := Mixin {
  Zero : Cat;
  Zero_initial : forall (B : Cat) (f : Map Zero B), f = 0;
  Zero_final : forall (A : Cat) (f : Map A Zero), f = 0;


  ker_ob : forall (A B : Cat) (f : Map A B), Cat;
  ker_map : forall (A B : Cat) (f : Map A B), Map (ker_ob f) A;
  ker_univ : forall (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C), f_AB >> f_BC = 0 -> Map A (ker_ob f_BC);

  ker0 : forall (A B : Cat) (f : Map A B), (ker_map f) >> f = 0;
  ker_univ_com : forall (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0), ker_univ H >> (ker_map f_BC) = f_AB;
  ker_univ_unique : forall (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0) (f_AK : Map A (ker_ob f_BC)), f_AK >> (ker_map f_BC) = f_AB -> f_AK = ker_univ H;


  coker_ob : forall (A B : Cat) (f : Map A B), Cat;
  coker_map : forall (A B : Cat) (f : Map A B), Map B (coker_ob f);
  coker_univ : forall (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C), f_AB >> f_BC = 0 -> Map (coker_ob f_AB) C;

  coker0 : forall (A B  : Cat) (f : Map A B), f >> (coker_map f) = 0;
  coker_univ_com : forall (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0), (coker_map f_AB >> coker_univ H = f_BC);
  coker_univ_unique : forall (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0) (f_NC : Map (coker_ob f_AB) C), (coker_map f_AB) >> f_NC = f_BC -> f_NC = coker_univ H;


  product_ob : forall (A B : Cat), Cat;
  product_mapA : forall (A B : Cat), Map (product_ob A B) A;
  product_mapB : forall (A B : Cat), Map (product_ob A B) B;

  product_univ_map : forall (A B E : Cat) (f_EA : Map E A) (f_EB : Map E B), Map E (product_ob A B);
  product_univ_comA : forall (A B E : Cat) (f_EA : Map E A) (f_EB : Map E B), (product_univ_map f_EA f_EB) >> (product_mapA A B) = f_EA;
  product_univ_comB : forall (A B E : Cat) (f_EA : Map E A) (f_EB : Map E B), (product_univ_map f_EA f_EB) >> (product_mapB A B) = f_EB;
  product_univ_unique : forall (A B E : Cat) (f_EA : Map E A) (f_EB : Map E B) (f_EP : Map E (product_ob A B)), f_EP >> (product_mapA A B) = f_EA -> f_EP >> (product_mapB A B) = f_EB -> f_EP = product_univ_map f_EA f_EB;


  coproduct_ob : forall (A B : Cat), Cat;
  coproduct_mapA : forall (A B : Cat), Map A (coproduct_ob A B);
  coproduct_mapB : forall (A B : Cat), Map B (coproduct_ob A B);

  coproduct_univ_map : forall (A B E : Cat) (f_AE : Map A E) (f_BE : Map B E), Map (coproduct_ob A B) E;
  coproduct_univ_comA : forall (A B E : Cat) (f_AE : Map A E) (f_BE : Map B E), (coproduct_mapA A B) >> (coproduct_univ_map f_AE f_BE) = f_AE;
  coproduct_univ_comB : forall (A B E : Cat) (f_AE : Map A E) (f_BE : Map B E), (coproduct_mapB A B) >> (coproduct_univ_map f_AE f_BE) = f_BE;
  coproduct_univ_unique : forall (A B E : Cat) (f_AE : Map A E) (f_BE : Map B E) (f_CE : Map (coproduct_ob A B) E), (coproduct_mapA A B) >> f_CE = f_AE -> (coproduct_mapB A B) >> f_CE = f_BE -> f_CE = coproduct_univ_map f_AE f_BE;
}.

Record class_of Ob := Class {
  base : Equality.class_of Ob ;
  cat_mixin : Category.mixin_of Ob ;
  preAbCat_mixin : PreAbCategory.mixin_of cat_mixin;
  abCat_mixin : AbCategory.mixin_of (PreAbCategory.Pack (PreAbCategory.Class base preAbCat_mixin)) ;
  mixin : mixin_of (AbCategory.Pack (AbCategory.Class abCat_mixin)) }.

Structure type := Pack { sort ; class : class_of sort }.


Section Coercion.

Variable Cat : type.

Definition to_eqType := Equality.Pack (base (class Cat)).

Let Ob := sort Cat.
Let Zero : Ob := (Zero (mixin (class Cat))).

Definition to_Category_class := Category.Class (base (class Cat)) (cat_mixin (class Cat)).
Definition to_Category := Category.Pack to_Category_class.

Definition to_PreAbCategory_class := PreAbCategory.Class (base (class Cat))(preAbCat_mixin (class Cat)).
Definition to_PreAbCategory := PreAbCategory.Pack to_PreAbCategory_class.

Definition to_AbCategory_class := AbCategory.Class (abCat_mixin (class Cat)).
Definition to_AbCategory := AbCategory.Pack to_AbCategory_class.


Let initial_map (B : Ob) := (0 : @Map to_AbCategory Zero B).
Lemma initial_unique : forall (B : Ob) (f : @Map to_AbCategory Zero B), f = initial_map B.
Proof. by move => B f ; rewrite (Zero_initial f). Qed.

Let final_map (A : Ob) := (0 : @Map to_AbCategory A Zero).
Lemma final_unique : forall (A : Ob) (f : @Map to_AbCategory A Zero), f = final_map A.
Proof. by move => A f ; rewrite (Zero_final f). Qed.

Definition to_CategoryWithZero_mixin := @CategoryWithZero.Mixin Ob to_Category_class Zero initial_map (@initial_unique) final_map (@final_unique).
Definition to_CategoryWithZero_class := CategoryWithZero.Class to_CategoryWithZero_mixin.
Definition to_CategoryWithZero := CategoryWithZero.Pack to_CategoryWithZero_class.

End Coercion.


Module Exports.

Coercion sort : type >-> Sortclass.
Coercion to_Category : type >-> Category.type.
Canonical to_Category.
Coercion to_eqType : type >-> eqType.
Canonical to_eqType.
Coercion to_PreAbCategory : type >-> PreAbCategory.type.
Canonical to_PreAbCategory.
Coercion to_AbCategory : type >-> AbCategory.type.
Canonical to_AbCategory.
Coercion to_CategoryWithZero : type >-> CategoryWithZero.type.
Canonical to_CategoryWithZero.
Coercion base : class_of >-> Equality.class_of.
Coercion class : type >-> class_of.
Coercion cat_mixin : class_of >-> Category.mixin_of.
Coercion preAbCat_mixin : class_of >-> PreAbCategory.mixin_of.
Coercion abCat_mixin : class_of >-> AbCategory.mixin_of.
Coercion mixin : class_of >-> mixin_of.

End Exports.

End AbCategoryWith.

Export AbCategoryWith.Exports.


Module AbCategoryWithTheory.

Export AbCategoryTheory.
Import AbCategoryWith.

Section Definitions.

Definition Zero {T} := @Zero _ (class T).
Definition ker_ob T := @ker_ob _ (class T).
Definition ker_map T := @ker_map _ (class T).
Definition ker_univ T := @ker_univ _ (class T).
Definition coker_ob T := @coker_ob _ (class T).
Definition coker_map T := @coker_map _ (class T).
Definition coker_univ T := @coker_univ _ (class T).
Definition product_ob T := @product_ob _ (class T).
Definition product_mapA T := @product_mapA _ (class T).
Definition product_mapB T := @product_mapB _ (class T).
Definition product_univ_map T := @product_univ_map _ (class T).
Definition coproduct_ob T := @coproduct_ob _ (class T).
Definition coproduct_mapA T := @coproduct_mapA _ (class T).
Definition coproduct_mapB T := @coproduct_mapB _ (class T).
Definition coproduct_univ_map T := @coproduct_univ_map _ (class T).

End Definitions.


Section Theory.

Variable (Cat : type).

Implicit Types (A B C : Cat).

Lemma Zero_initial : forall B (f : Map Zero B), f = 0.
Proof. by apply (@Zero_initial _ Cat). Qed.
Lemma Zero_final : forall A (f : Map A Zero), f = 0.
Proof. by apply (@Zero_final _ Cat). Qed.

Lemma ker0 : forall A B (f : Map A B), (ker_map f) >> f = 0.
Proof. by apply (@ker0 _ Cat). Qed.
Lemma ker_univ_com : forall A B C (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0), ker_univ H >> (ker_map f_BC) = f_AB.
Proof. by apply (@ker_univ_com _ Cat). Qed.
Lemma ker_univ_unique : forall A B C (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0) (f_AK : Map A (ker_ob f_BC)), f_AK >> (ker_map f_BC) = f_AB -> f_AK = ker_univ H.
Proof. by apply (@ker_univ_unique _ Cat). Qed.

Lemma coker0 : forall A B (f : Map A B), f >> (coker_map f) = 0.
Proof. by apply (@coker0 _ Cat). Qed.
Lemma coker_univ_com : forall A B C (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0), (coker_map f_AB >> coker_univ H = f_BC).
Proof. by apply (@coker_univ_com _ Cat). Qed.
Lemma coker_univ_unique : forall A B C (f_AB : Map A B) (f_BC : Map B C) (H : f_AB >> f_BC = 0) (f_NC : Map (coker_ob f_AB) C), (coker_map f_AB) >> f_NC = f_BC -> f_NC = coker_univ H.
Proof. by apply (@coker_univ_unique _ Cat). Qed.

Lemma product_univ_comA : forall A B E (f_EA : Map E A) (f_EB : Map E B), (product_univ_map f_EA f_EB) >> (product_mapA A B) = f_EA.
Proof. by apply (@product_univ_comA _ Cat). Qed.
Lemma product_univ_comB : forall A B E (f_EA : Map E A) (f_EB : Map E B), (product_univ_map f_EA f_EB) >> (product_mapB A B) = f_EB.
Proof. by apply (@product_univ_comB _ Cat). Qed.
Lemma product_univ_unique : forall A B E (f_EA : Map E A) (f_EB : Map E B) (f_EP : Map E (product_ob A B)), f_EP >> (product_mapA A B) = f_EA -> f_EP >> (product_mapB A B) = f_EB -> f_EP = product_univ_map f_EA f_EB.
Proof. by apply (@product_univ_unique _ Cat). Qed.

Lemma coproduct_univ_comA : forall A B E (f_AE : Map A E) (f_BE : Map B E), (coproduct_mapA A B) >> (coproduct_univ_map f_AE f_BE) = f_AE.
Proof. by apply (@coproduct_univ_comA _ Cat). Qed.
Lemma coproduct_univ_comB : forall A B E (f_AE : Map A E) (f_BE : Map B E), (coproduct_mapB A B) >> (coproduct_univ_map f_AE f_BE) = f_BE.
Proof. by apply (@coproduct_univ_comB _ Cat). Qed.
Lemma coproduct_univ_unique : forall A B E (f_AE : Map A E) (f_BE : Map B E) (f_CE : Map (coproduct_ob A B) E), (coproduct_mapA A B) >> f_CE = f_AE -> (coproduct_mapB A B) >> f_CE = f_BE -> f_CE = coproduct_univ_map f_AE f_BE.
Proof. by apply (@coproduct_univ_unique _ Cat). Qed.


Definition compl (A B C : Cat) (f_AB : Map A B) := Additive (compmDr (C := C) f_AB).


Section factor_zero.

Import CategoryWithZeroTheory.

Lemma factor_zero (A B : Cat) : @zero_map _ A B = 0%R.
Proof.
rewrite /zero_map -(final_unique (0%R : Map A Zero)).
set a := (_ >> _).
move: (erefl (a - a)).
by rewrite -{1}compmDl !subrr.
Qed.

End factor_zero.


Definition epi (A B : Cat) (f_AB : Map A B) := forall (C : Cat) (f_BC : Map B C), f_AB >> f_BC = 0 -> f_BC = 0.

Definition mono (B C : Cat) (f_BC : Map B C) := forall (A : Cat) (f_AB : Map A B), f_AB >> f_BC = 0 -> f_AB = 0.


Lemma compm0 : forall (A B C : Cat) (f_AB : Map A B), f_AB >> (0 : Map B C) = 0.
Proof.
by move=> A B C f_AB ; rewrite -!factor_zero CategoryWithZeroTheory.compm0.
Qed.

Lemma comp0m : forall (A B C : Cat) (f_BC : Map B C), (0 : Map A B) >> f_BC = 0.
Proof.
by move=> * ; rewrite -!factor_zero CategoryWithZeroTheory.comp0m.
Qed.


Lemma epi_Category_epi (A B : Cat) (f_AB : Map A B) : epi f_AB <-> CategoryTheory.epi f_AB.
Proof.
split.
- move=> epi0_AB c f_BC f'_BC.
  move/(congr1 (fun x => x - f_AB >> f'_BC)) ; rewrite -!compmDr subrr compm0.
  by move/epi0_AB/(congr1 (+%R^~ f'_BC)) ; rewrite subrK add0r.
- move=> epi_AB C f_BC f_ABC_eq0 ; apply epi_AB ; by rewrite compm0.
Qed.


Lemma coker_map_epi : forall (A B : Cat) (f : Map A B), epi (coker_map f).
move => A B f_AB C. set N := coker_ob f_AB. set f_BN := coker_map f_AB => f_NC ; set f_BC := f_BN >> f_NC => /[dup] f_BC0.
move/(congr1 (compl _ f_AB)) => /= ; rewrite compm0 => H.
have -> : f_NC = coker_univ H by apply coker_univ_unique.
apply Logic.eq_sym ; apply coker_univ_unique.
by rewrite compm0.
Qed.

Section Image.
  Variables (A B : Cat) (f_AB : Map A B).

  Let N := coker_ob f_AB.
  Let f_BN := coker_map f_AB.

  Definition im_ob := ker_ob f_BN.
  Definition im_map := ker_map f_BN.

  Let I := im_ob.
  Let f_IB := im_map.


  Let K := ker_ob f_AB.
  Let f_KA := ker_map f_AB.

  Definition coim_ob := coker_ob f_KA.
  Definition coim_map := coker_map f_KA.

  Let I' := coim_ob.
  Let f_AI' := coim_map.

  Let f_I'B : Map I' B.
  apply (coker_univ (f_BC := f_AB)).
  apply ker0.
  Defined.

  Definition coim2im : Map coim_ob im_ob.
  apply (ker_univ (f_AB := f_I'B)).
  have epi_f_AI' : epi f_AI'. apply coker_map_epi.
  apply epi_f_AI'.
  rewrite compmA.
  have -> : f_AI' >> f_I'B = f_AB by rewrite coker_univ_com.
  by apply coker0.
  Defined.

  Lemma factorize_coim_im : f_AI' >> coim2im >> f_IB = f_AB.
  Proof.
  by rewrite -compmA ker_univ_com coker_univ_com.
  Qed.
End Image.

End Theory.

End AbCategoryWithTheory.




Module AbelianCategory.

Export AbCategoryWithTheory.
Import Isomorphism.

Record mixin_of (Cat : AbCategoryWith.type) := Mixin {
	isomorphism_coim_im : forall (A B : Cat) (f_AB : Map A B), isomorphism (coim2im f_AB);
}.

Record class_of Ob := Class {
  base : Equality.class_of Ob;
  cat_mixin : Category.mixin_of Ob;
  preAbCat_mixin : PreAbCategory.mixin_of cat_mixin;
  abCat_mixin : AbCategory.mixin_of (PreAbCategory.Pack (PreAbCategory.Class base preAbCat_mixin));
  abCatWith_mixin : AbCategoryWith.mixin_of (AbCategory.Pack (AbCategory.Class abCat_mixin));
  mixin : mixin_of (AbCategoryWith.Pack (AbCategoryWith.Class abCatWith_mixin));
}.

Structure type := Pack {sort; class : class_of sort}.


Section Coercion.

Variable (Cat : type).

Definition to_eqType := Equality.Pack (base (class Cat)).

Definition to_Category_class := Category.Class (base (class Cat)) (cat_mixin (class Cat)).
Definition to_Category := Category.Pack to_Category_class.

Definition to_PreAbCategory_class := PreAbCategory.Class (base (class Cat))(preAbCat_mixin (class Cat)).
Definition to_PreAbCategory := PreAbCategory.Pack to_PreAbCategory_class.

Definition to_AbCategory_class := AbCategory.Class (abCat_mixin (class Cat)).
Definition to_AbCategory := AbCategory.Pack to_AbCategory_class.

Definition to_AbCategoryWith_class := AbCategoryWith.Class (abCatWith_mixin (class Cat)).
Definition to_AbCategoryWith := AbCategoryWith.Pack to_AbCategoryWith_class.

Definition to_CategoryWithZero_mixin := @CategoryWithZero.Mixin (sort Cat) to_Category_class (AbCategoryWith.Zero to_AbCategoryWith) _ (@AbCategoryWith.initial_unique to_AbCategoryWith) _ (@AbCategoryWith.final_unique to_AbCategoryWith).
Definition to_CategoryWithZero_class := CategoryWithZero.Class to_CategoryWithZero_mixin.
Definition to_CategoryWithZero := CategoryWithZero.Pack to_CategoryWithZero_class.

End Coercion.


Module Exports.

Coercion sort : type >-> Sortclass.
Coercion to_eqType : type >-> eqType.
Canonical to_eqType.
Coercion to_Category : type >-> Category.type.
Canonical to_Category.
Coercion to_AbCategory : type >-> AbCategory.type.
Canonical to_AbCategory.
Coercion to_PreAbCategory : type >-> PreAbCategory.type.
Canonical to_PreAbCategory.
Coercion to_AbCategoryWith : type >-> AbCategoryWith.type.
Canonical to_AbCategoryWith.
Coercion to_CategoryWithZero : type >-> CategoryWithZero.type.
Canonical to_CategoryWithZero.
Coercion base : class_of >-> Equality.class_of.
Coercion class : type >-> class_of.
Coercion cat_mixin : class_of >-> Category.mixin_of.
Coercion preAbCat_mixin : class_of >-> PreAbCategory.mixin_of.
Coercion abCat_mixin : class_of >-> AbCategory.mixin_of.
Coercion abCatWith_mixin : class_of >-> AbCategoryWith.mixin_of.
Coercion mixin : class_of >-> mixin_of.

End Exports.

End AbelianCategory.

Export AbelianCategory.Exports.


Module Theory.

Export AbCategoryWithTheory.
Import Isomorphism.

Section Theory.

Variable (Cat : AbelianCategory.type).

Lemma isomorphism_coim_im : forall (A B : Cat) (f_AB : Map A B), isomorphism (coim2im f_AB).
Proof. by apply (@AbelianCategory.isomorphism_coim_im _ (AbelianCategory.mixin (AbelianCategory.class Cat))). Qed.


Definition im2coim (A B : Cat) (f_AB : Map A B) := Isomorphism.inverse_map (isomorphism_coim_im f_AB).

Definition compr (A B C : Cat) (f_BC : Map B C) := Additive (compmDl (A := A) f_BC).

Lemma comp0m : forall (A B C : Cat) (f_BC : Map B C), (0 : Map A B) >> f_BC = 0.
Proof.
by move => * ; rewrite -[LHS]/(compr _ _ 0) raddf0.
Qed.

Lemma compm0 : forall (A B C : Cat) (f_AB : Map A B), f_AB >> (0 : Map B C) = 0.
Proof.
by move => * ; rewrite -[LHS]/(compl _ _ 0) raddf0.
Qed.


Definition mono (B C : Cat) (f_BC : Map B C) := forall (A : Cat) (f_AB : Map A B), f_AB >> f_BC = 0 -> f_AB = 0.

Lemma ker_map_mono : forall (A B : Cat) (f : Map A B), mono (ker_map f).
move => B C f_BC A ; set K := ker_ob f_BC ; set f_KB := ker_map f_BC => f_AK ; set f_AB := f_AK >> f_KB => /[dup] f_AB0.
move/(congr1 (compr _ f_BC)) => /= ; rewrite comp0m => H.
have -> : f_AK = ker_univ H by apply ker_univ_unique.
apply Logic.eq_sym ; apply ker_univ_unique.
by rewrite comp0m.
Qed.


Lemma epi_epi (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C) : epi f_AB -> epi f_BC -> epi (f_AB >> f_BC).
Proof.
move=> epi_AB epi_BC E f_CE.
rewrite -compmA.
by move/epi_AB/epi_BC.
Qed.

Lemma epi_fepi (A B C : Cat) (f_AB : Map A B) (f_BC : Map B C) : epi (f_AB >> f_BC) -> epi (f_BC).
move=> epi_comp E g.
move/(congr1 (compl _ f_AB))=>/= ; rewrite compmA compm0.
by apply epi_comp.
Qed.

Lemma id_epi (A : Cat) : epi (id_ A).
by move=> E g ; rewrite compidm.
Qed.


(* Lemma inverseC (A B : Cat) (f_AB : Map A B) (f_BA : Map B A) : isomorphism f_AB f_BA <-> isomorphism f_BA. *)
(* split; move=> [? ?] ; by []. *)
(* Qed. *)

Lemma inverse_epif (A B : Cat) (f_AB : Map A B) : isomorphism f_AB -> epi f_AB.
Proof.
move=> [_ f_BA _ id_BAB].
apply (epi_fepi (f_AB := f_BA)).
rewrite id_BAB ; apply id_epi.
Qed.

Lemma inverse_monof (A B : Cat) (f_AB : Map A B) : isomorphism f_AB -> mono f_AB.
move=> [_ f_BA ABA_id _] E f_EA.
move/(congr1 (compr _ f_BA))=>/=.
by rewrite -compmA ABA_id compmid comp0m.
Qed.



Section ker0.

Variables (A B : Cat).

Let f_AB := 0 : Map A B.
Let K := ker_ob f_AB.
Let f_KA := ker_map f_AB.

Definition ker_0_inverse_map : Map A K.
have : (id_ A) >> (0 : Map A B) = 0 by apply compm0.
by move/ker_univ.
Defined.

Let f_AK := ker_0_inverse_map.

Lemma isom_ker0_left : f_AK >> f_KA = id_ A.
Proof. by apply ker_univ_com. Qed.

Lemma isom_ker0_right : f_KA >> f_AK = id_ K.
Proof.
have f_KB0 : f_KA >> f_AB = 0 by apply ker0.
have -> : f_KA >> f_AK = ker_univ f_KB0.
  by apply ker_univ_unique ; rewrite -compmA isom_ker0_left compmid.
have -> : id_ _ = ker_univ f_KB0.
  by apply ker_univ_unique ; rewrite compidm.
reflexivity.
Qed.

End ker0.





Section mono_epi_inverse.

Variables (A B : Cat) (f_AB : Map A B).

Hypotheses (mono_AB : mono f_AB)
           (epi_AB : epi f_AB).

Let I' := coim_ob f_AB.
Let I := im_ob f_AB.
Let f_II' := im2coim f_AB.

Let f_AI' := coim_map f_AB.
Let f_I'I := coim2im f_AB.
Let f_IB := im_map f_AB.

Let f_BI : Map B I.
have : id_ B >> coker_map f_AB = 0.
  have -> : coker_map f_AB = 0 by apply epi_AB ; apply coker0.
  by rewrite compm0.
apply ker_univ.
Defined.

Let f_I'A : Map I' A.
have : ker_map f_AB >> id_ A = 0.
  have -> : ker_map f_AB = 0 by apply mono_AB ; apply ker0.
  by rewrite comp0m.
apply coker_univ.
Defined.

Lemma id_IBI : f_IB >> f_BI = id_ I.
Proof.
have f_IB0 : f_IB >> coker_map f_AB = 0 by apply ker0.
have -> : f_IB >> f_BI = ker_univ f_IB0.
  by apply ker_univ_unique ; rewrite -compmA ker_univ_com compmid.
have -> : id_ _ = ker_univ f_IB0.
  by apply ker_univ_unique ; rewrite compidm.
reflexivity.
Qed.

Lemma id_I'AI' : f_I'A >> f_AI' = id_ I'.
Proof.
have f_0AI' : ker_map f_AB >> f_AI' = 0 by apply coker0.
have -> : f_I'A >> f_AI' = coker_univ f_0AI'.
  by apply coker_univ_unique ; rewrite compmA coker_univ_com compidm.
have -> : id_ _ = coker_univ f_0AI'.
  by apply coker_univ_unique ; rewrite compmid.
reflexivity.
Qed.

Definition mono_epi_inverse_map := f_BI >> f_II' >> f_I'A.

Lemma id_mono_epi : f_AB >> mono_epi_inverse_map = id_ A.
rewrite/mono_epi_inverse_map.
rewrite -{1}(factorize_coim_im f_AB) compmA compmA -[_ >> f_BI]compmA.
rewrite id_IBI compmid -[_ >> f_II']compmA /f_II'/im2coim.
move: (isomorphism_coim_im f_AB) => [_ ?/= -> _] ; rewrite compmid.
by rewrite coker_univ_com.
Qed.

Lemma id_epi_mono : mono_epi_inverse_map >> f_AB = id_ B.
rewrite/mono_epi_inverse_map.
rewrite -{3}(factorize_coim_im f_AB) compmA compmA -[_ >> f_AI']compmA.
rewrite id_I'AI' compmid -[_ >> f_I'I]compmA  /f_II'/im2coim.
move: (isomorphism_coim_im f_AB) => [_ ? _ ->] ; rewrite compmid.
by rewrite ker_univ_com.
Qed.

Definition isomorphism_mono_epi := inverse id_mono_epi id_epi_mono.

End mono_epi_inverse.



(** Product and coproducts are isomorphic. *)
Section Biproduct.

Variables (A B : Cat).

Let C := coproduct_ob A B.
Let f_AC := coproduct_mapA A B.
Let f_BC := coproduct_mapB A B.

Let f_CA : Map C A := coproduct_univ_map (id_ A) 0.
Let f_CB : Map C B := coproduct_univ_map 0 (id_ B).

Let P := product_ob A B.
Let f_PA := product_mapA A B.
Let f_PB := product_mapB A B.

Let f_CP : Map C P.
apply product_univ_map. exact f_CA. exact f_CB.
Defined.

Let f_PC := (f_PA >> f_AC) + (f_PB >> f_BC).


Lemma id_PCP : f_PC >> f_CP = 'id_P.
rewrite (product_univ_unique (_ : id_ P >> f_PA = f_PA) (_ : id_ P >> f_PB = f_PB)) ?compidm//.
apply product_univ_unique.
  by rewrite -compmA product_univ_comA /f_PC -[LHS]/(compr _ f_CA _)raddfD/= -!compmA coproduct_univ_comA compmid coproduct_univ_comB compm0 addr0.
by rewrite -compmA product_univ_comB /f_PC -[LHS]/(compr _ f_CB _)raddfD/= -!compmA coproduct_univ_comB compmid coproduct_univ_comA compm0 add0r.
Qed.

Lemma id_CPC : f_CP >> f_PC = id_ C.
rewrite (coproduct_univ_unique (_ : f_AC >> id_ C = f_AC) (_ : f_BC >> id_ C = f_BC)) ?compmid//.
apply coproduct_univ_unique.
  by rewrite /f_PC -[f_CP >> _]/(compl _ f_CP _)raddfD/= !compmA product_univ_comA product_univ_comB -[LHS]/(compl _ f_AC _)raddfD/= !compmA !coproduct_univ_comA compidm comp0m addr0.
by rewrite /f_PC -[f_CP >> _]/(compl _ f_CP _)raddfD/= !compmA product_univ_comB product_univ_comA -[LHS]/(compl _ f_BC _)raddfD/= !compmA !coproduct_univ_comB compidm comp0m add0r.
Qed.

Definition isomorphism_biproduct := inverse id_CPC id_PCP.

End Biproduct.



(**
Now we have an abelian category.
The next step is to simulate a diagram chasing.
An "element" of A will be any map (E -> A) and we authorize to precompose by any epimorphism : (F ->> E -> A).
The key point is to show that if we have an epimorphism A ->> B and an element E -> B in B then there is a pull back F and the map F -> E is an epimorphism :
F ->> E
|     |
v     v
A ->> B
i.e., we found a preimage of the element E -> B.
*)

Section Pullback.
(** Let us start we the existence of pullback of A -> C <- B *)

Variables (A B C : Cat) (f_AC : Map A C) (f_BC : Map B C).

Let P := product_ob A B.
Let f_PA := product_mapA A B.
Let f_PB := product_mapB A B.
Let f_PC := (f_PA >> f_AC) - (f_PB >> f_BC).

Definition pullback_ob := ker_ob f_PC.
Definition pullback_mapA := ker_map f_PC >> f_PA.
Definition pullback_mapB := ker_map f_PC >> f_PB.

Lemma pullback_com : pullback_mapA >> f_AC = pullback_mapB >> f_BC.
apply (subIr (pullback_mapB >> f_BC)) ; rewrite addrN.
rewrite/pullback_mapA/pullback_mapB -!compmA -compmDr.
by apply ker0.
Qed.

Variables (E : Cat) (f_EA : Map E A) (f_EB : Map E B).
Hypothesis E_coalgebra : f_EA >> f_AC = f_EB >> f_BC.

Definition pullback_univ_map : Map E pullback_ob.
set f_EP := product_univ_map f_EA f_EB.
suff : f_EP >> ((f_PA >> f_AC) - (f_PB >> f_BC)) = 0 by apply ker_univ.
rewrite compmDr !compmA.
have -> : f_EP >> f_PA = f_EA by apply (product_univ_comA f_EA f_EB).
have -> : f_EP >> f_PB = f_EB by apply (product_univ_comB f_EA f_EB).
by rewrite E_coalgebra ; apply (addrN (_ >> f_BC)). (* TODO : precision not needed *)
Defined.

Variable (f_EU : Map E pullback_ob).

Hypotheses
  (EU_comA : f_EU >> pullback_mapA = f_EA)
  (EU_comB : f_EU >> pullback_mapB = f_EB).


Lemma pullback_univ_unique : f_EU = pullback_univ_map.
apply ker_univ_unique.
apply product_univ_unique ; rewrite -compmA.
  by rewrite EU_comA.
by rewrite EU_comB.
Qed.

End Pullback.



Section pullback_epiA.

Variables (A B C : Cat) (f_AC : Map A C) (f_BC : Map B C).

Hypothesis epi_AC : epi f_AC.

Let P := product_ob A B.
Let f_PA := product_mapA A B.
Let f_PB := product_mapB A B.

Let f_PC := f_PA >> f_AC - (f_PB >> f_BC).

Lemma epi_PC : epi f_PC.
Proof.
pose f_AP : Map A P := product_univ_map (id_ A) 0.
have : epi (f_AP >> f_PA >> f_AC).
  by apply epi_epi=>// ; rewrite product_univ_comA ; apply id_epi.
suff -> : f_AP >> f_PA >> f_AC = f_AP >> f_PC by apply epi_fepi.
by rewrite /f_PC compmDr !compmA product_univ_comB comp0m subr0.
Qed.

Let C' := coim_ob f_PC.
Let f_PC' := coim_map f_PC.

Let C'' := im_ob f_PC.
Let f_C''C := im_map f_PC.
Let f_CC'' : Map C C''. (* TODO *)
have : id_ C >> coker_map f_PC = 0.
  have -> : coker_map f_PC = 0 by apply epi_PC ; apply coker0.
  by rewrite compm0.
by move/ker_univ.
Defined.

Let f_C'C'' := coim2im f_PC.
Let f_C''C' := im2coim f_PC.

Let f_CC' := f_CC'' >> f_C''C'.
Let f_C'C := f_C'C'' >> f_C''C.

Lemma id_CC'C : f_CC' >> f_C'C = id_ C.
Proof.
rewrite/f_CC'/f_C'C compmA -[X in X >> _]compmA /f_C''C'/im2coim.
move: (isomorphism_coim_im f_PC) => [_ ? _ ->] ; rewrite compmid.
  by apply ker_univ_com.
Qed.

Lemma id_C'CC' : f_C'C >> f_CC' = id_ C'.
rewrite/f_CC'/f_C'C compmA -[X in X >> _]compmA.
suff -> : f_C''C >> f_CC'' = id_ C''.
  by rewrite compmid /f_C''C'/im2coim ; move: (isomorphism_coim_im f_PC) => [_ ?/= -> _].
pose f_CN := coker_map f_PC.
have f_C''C0 : f_C''C >> f_CN = 0 by apply ker0.
have -> : f_C''C >> f_CC'' = ker_univ f_C''C0.
  by apply ker_univ_unique ; rewrite -compmA ker_univ_com compmid.
have -> : id_ _ = ker_univ f_C''C0.
  by apply ker_univ_unique ; rewrite compidm.
reflexivity.
Qed.

Definition isom_CC' := inverse id_CC'C id_C'CC'.


Let E := pullback_ob f_AC f_BC.
Let f_EP := ker_map f_PC.
Let f_EA := pullback_mapA f_AC f_BC.
Let f_EB := pullback_mapB f_AC f_BC.


Section Main_proposition.

Variables (N : Cat) (f_BN : Map B N).
Hypothesis f_EN0 : (f_EB >> f_BN = 0).

Let f_AN : Map A N := 0.

Let f_PN := f_PA >> f_AN + (f_PB >> f_BN).

Let f_C'N : Map C' N.
suff : f_EP >> f_PN = 0 by apply coker_univ.
by rewrite -[LHS]/(compl _ f_EP _)raddf/= !compmA -[f_EP >> f_PA]/f_EA -[f_EP >> f_PB]/f_EB f_EN0 compm0 addr0.
Defined.

Let P' := coproduct_ob A B.
Let f_AP' := coproduct_mapA A B.
Let f_BP' := coproduct_mapB A B.

Let isom_PP' := isomorphism_biproduct A B.

Let f_P'P := Isomorphism.map isom_PP'.
Let f_PP' := Isomorphism.inverse_map isom_PP'.
Let f_AC' := f_AP' >> f_P'P >> f_PC'.
Let f_BC' := f_BP' >> f_P'P >> f_PC'.

Lemma fact_BC'N : f_BC' >> f_C'N = f_BN.
Proof.
by rewrite/f_BC' -compmA coker_univ_com /f_PN -[LHS]/(compl _ _ _)raddf/= !compmA -[X in X >> _]compmA -[X in _ + (X >> _)]compmA product_univ_comA product_univ_comB !coproduct_univ_comB comp0m add0r compidm.
Qed.

Lemma fact_AC'N : f_AC' >> f_C'N = f_AN.
Proof.
by rewrite/f_AC' -compmA coker_univ_com /f_PN -[LHS]/(compl _ _ _)raddf/= !compmA -[X in X >> _]compmA -[X in _ + (X >> _)]compmA product_univ_comA product_univ_comB !coproduct_univ_comA comp0m addr0 compidm.
Qed.

Lemma epi_AC' : epi f_AC'.
Proof.
suff <- : f_AC >> f_CC' = f_AC'.
  apply epi_epi ; first by []. apply (inverse_epif isom_CC').
rewrite/f_AC'.
suff -> : f_PC' = f_PC >> f_CC'.
  by rewrite [RHS]compmA ; apply (congr1 (comp^~ f_CC')) ; rewrite /f_PC compmDr -!compmA [X in _ >> X]compmA [X in _ - (_ >> X)]compmA product_univ_comA product_univ_comB !compmA !coproduct_univ_comA comp0m subr0 compidm.
suff : f_PC' >> f_C'C >> f_CC' = f_PC >> f_CC'.
  by rewrite -compmA ; rewrite (Isomorphism.id_BAB isom_CC') ; rewrite !compmid.
by rewrite /f_PC'/f_C'C/f_C'C'' !compmA factorize_coim_im.
Qed.

Lemma BN_0 : f_BN = 0.
Proof.
rewrite -fact_BC'N.
suff -> : f_C'N = 0 by rewrite compm0.
apply epi_AC'.
by rewrite fact_AC'N.
Qed.

End Main_proposition.

Proposition pullback_epiA : epi f_EB.
Proof.
move=> N f_BN f_EN0.
by apply BN_0.
Qed.

End pullback_epiA.



Section Diagram_chasing.

(* U  -> V   or  U -> V -> W *)
(* |     |                   *)
(* v     v                   *)
(* V' -> W                   *)
Variables U V V' W : Cat.
Variables (f_UV : Map U V)
         (f_VW : Map V W)
				 (f_UV' : Map U V')
				 (f_V'W : Map V' W).

Definition commutative_square :=
  f_UV >> f_VW = f_UV' >> f_V'W.

Definition diff :=
  f_UV >> f_VW = 0.

Let I' := coim_ob f_UV.
Let f_UI' := coim_map f_UV.
Let I := im_ob f_UV.
Let f_IV := im_map f_UV.
Let f_II' := im2coim f_UV.
Let f_I'I := coim2im f_UV.
Let K := ker_ob f_VW.
Let f_KV := ker_map f_VW.

Definition diff_im2ker : diff -> Map I K.
move => diff.
suff : f_IV >> f_VW = 0 by move/ker_univ.
have -> : f_IV = f_II' >> f_I'I >> f_IV by rewrite (Isomorphism.id_BAB (isomorphism_coim_im f_UV)) compidm.
rewrite -2!compmA.
suff -> : f_I'I >> f_IV >> f_VW = 0 by rewrite compm0.
have : f_UI' >> f_I'I >> f_IV >> f_VW = 0.
  by rewrite factorize_coim_im.
rewrite -!compmA.
have : epi f_UI' by apply coker_map_epi.
apply.
Defined.

Definition exact (pdiff : diff) := epi (diff_im2ker pdiff).


Section Preimage.

Hypothesis pdiff : diff.
Hypothesis pexact : exact pdiff.

Variables (E : Cat) (f_EV : Map E V).

Hypothesis f_EW0 : f_EV >> f_VW = 0.

Let f_EK := ker_univ f_EW0.
Let f_IK := diff_im2ker pdiff.
Let f_UK := f_UI' >> f_I'I >> f_IK.

Definition preimage_ob := pullback_ob f_UK f_EK.
Definition preimage_refinement := pullback_mapB f_UK f_EK.
Definition preimage_map := pullback_mapA f_UK f_EK.

Inductive preimage_record :=
  | preimage_intro (ob : Cat) (refinement : Map ob E) (map : Map ob U) (epi_refine : epi refinement) (com : refinement >> f_EV = map >> f_UV).

Lemma preimage_epi_refine : epi preimage_refinement.
Proof.
have : epi f_UK. apply epi_epi. apply epi_epi.
      by apply coker_map_epi.
    by apply inverse_epif ; apply isomorphism_coim_im.
  by apply @pexact.
by apply pullback_epiA.
Qed.

Lemma preimage_com : preimage_refinement >> f_EV = preimage_map >> f_UV.
Proof.
have <- : f_EK >> f_KV = f_EV by apply ker_univ_com.
have <- : f_UK >> f_KV = f_UV by rewrite /f_UK -compmA ker_univ_com factorize_coim_im.
by rewrite !compmA pullback_com.
Qed.

Definition preimage := preimage_intro preimage_epi_refine preimage_com.

(* Lemma commute {x : U} : commutative_square -> f_VW (f_UV x) = f_V'W (f_UV' x).
Proof. by apply. Qed.

Lemma exact_diff :
	exact -> diff.
Proof.
by move=> exact x ; apply (exact (f_UV x)) ; exists x.
Qed. *)

End Preimage.

End Diagram_chasing.



Lemma diff_f0 (A B : Cat) (f_AB : Map A B) (C : Cat) : diff f_AB (0 : Map B C).
Proof. by apply compm0. Qed.


Section Exact_epi0.

Variables (A B C : Cat) (f_AB : Map A B).
Let f_BC := 0 : Map B C.

Let B' := ker_ob f_BC.
Let f_IB' := diff_im2ker (diff_f0 f_AB C).
Let I := im_ob f_AB.
Let f_IB := im_map f_AB.
Let f_B'B := ker_map f_BC.
Let f_BB' := ker_0_inverse_map B C.
Let f_AI' := coim_map f_AB.
Let f_I'I := coim2im f_AB.


(* Créer plus de lemmes *)
Lemma exact_epi0 : epi f_AB -> exact (diff_f0 f_AB C).
Proof.
move=> epi_f ; rewrite /exact -/f_IB'.
have fact_IBB' : f_IB' = f_IB >> f_BB'.
  have -> : f_IB' = f_IB' >> f_B'B >> f_BB'.
    by rewrite -compmA isom_ker0_right compmid.
  by rewrite ker_univ_com.
have epi_BB' : epi f_BB'.
  by apply inverse_epif ; eapply inverse ; [apply isom_ker0_left | apply isom_ker0_right].
have epi_IB : epi f_IB.
  by apply (epi_fepi (f_AB := f_AI' >> f_I'I)) ; rewrite factorize_coim_im.
by move: fact_IBB' -> ; apply epi_epi.
Qed.

End Exact_epi0.

Arguments exact_epi0 {_ _} _ {_}.

End Theory.

End Theory.
