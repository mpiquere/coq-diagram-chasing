(* Let G be a directed graph, and G_1, ..., G_n be some subgraphs.     *)
(* Here is an algorithm computing the truthness of                     *)
(* "any diagram on G is commutative if and only if                     *)
(*     it is commutative on G_1, ..., G_n".                            *)


From mathcomp Require Import all_ssreflect.
From DiagramChasing Require Import Graph TopologicalSort PathMatrix Utils.
Require Import Relations.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Import Graph LocallyFiniteOrientedGraph FiniteOrientedGraph.

Definition multigraph := FiniteOrientedGraph.type nat.
Coercion FiniteOrientedGraph.arc : FiniteOrientedGraph.type >-> seq.
Definition subgraph := seq nat. (* induced subgraph: list of arc indices *)

Definition sub H G : multigraph := FiniteOrientedGraph.Build (map (nth (0,0) G) H).

Definition is_subgraph (G : seq (nat * nat)) (H : subgraph) :=
  let n := size G in
  all (gtn n) H.

Definition path G u p v := all (gtn (size G)) p && (u :: unzip2 (sub p G) == rcons (unzip1 (sub p G)) v).

Definition path_sub G (H : subgraph) u p v := path G u p v && all (mem H) p.


(* The main example of path_relation is equality of compositions for a given diagram *)
Record path_relation := {
  π_r :> forall u v : nat, relation (seq nat) ;
  π_equiv : forall u v, equivalence _ (π_r u v) ;
  π_cat_stable : forall u v w p p' q q', π_r u v p p' -> π_r v w q q' -> π_r u w (p ++ q) (p' ++ q') ;
}.

(* For the above example, corresponds to commutativity of the diagram on G *)
Definition path_total G (r : nat -> nat -> relation (seq nat)) :=
  forall u p p' v, path G u p v -> path G u p' v -> r u v p p'.

Definition path_total_sub G H (r : path_relation) :=
  forall u p p' v, path_sub G H u p v -> path_sub G H u p' v -> r u v p p'.

Lemma path_total_eq l r1 r2 :
  (forall u v, r1 u v =2 r2 u v) -> path_total l r1 -> path_total l r2.
Proof.
move=> e h; rewrite /path_total=> *; rewrite -e; exact: h.
Qed.  

Lemma cons_path G u i p v : path G u (i :: p) v = (i < size G) && (u == (nth (0,0) G i).1) && path G (nth (0,0) G i).2 p v.
Proof.
  rewrite/path/= {1 3}/eq_op/= !andbA -[_ && _ && (_ == _)]andbA [_ && (_ == _)]andbC andbA ; done.
Qed.

Lemma rcons_inj' (T : eqType) (a : T) a' l l' : rcons l a == rcons l' a' = (l == l') && (a == a').
Proof.
  move=> *.
  case: (@idP (_ && _)) ; last apply contra_notF.
  - move=> /andP[]/eqP-> /eqP-> ; rewrite eq_refl ; done.
  - move=> /eqP/rcons_inj/eqP ; rewrite{1}/eq_op/= ; done.
Qed.

Lemma rcons_path G u i p v : path G u (rcons p i) v = (i < size G) && path G u p (nth (0,0) G i).1 && ((nth (0,0) G i).2 == v).
Proof.
  rewrite/path/=/unzip1/unzip2 !map_rcons -rcons_cons all_rcons rcons_inj' !andbA ; done.
Qed.

Lemma path0 G u v : path G u [::] v = (u == v).
Proof.
  rewrite/path/eq_op/= andbC ; done.
Qed.

Lemma cat_path G u p v p' w : path G u p v -> path G v p' w -> path G u (p ++ p') w.
Proof.
  elim: p u => [?| u' p H u] ; first by rewrite path0 => /eqP->.
  rewrite cat_cons !cons_path=> /andP[] -> /H /[apply] ; done.
Qed.

Lemma finite_path_path (G : multigraph) u p v : path G u p v -> OrientedGraph.path G u (unzip2 (sub p G)) v.
Proof.
  elim: p u=> [_ /eqP[]->|i p H u/=] ; first by apply eq_refl.
  rewrite cons_path Graph.Theory.cons_path=> /andP[]/andP[] i_lt_size /eqP-> /H ->.
  rewrite andbC mem_arc -surjective_pairing/=.
  by apply mem_nth.
Qed.

Lemma path_finite_path (G : multigraph) u p v : OrientedGraph.path G u p v -> exists p', path G u p' v.
Proof.
  elim: p u=> [? /eqP/=->|u' p H u/=] ; first by exists [::] ; rewrite /path eq_refl.
  rewrite Theory.cons_path mem_arc=> /andP[] /(nthP (0,0))[] i i_lt_size nth_i_eq /H[] p' path_p'.
  exists (i :: p').
  rewrite cons_path i_lt_size nth_i_eq eq_refl ; done.
Qed.

Lemma cons_path_sub G H u i p v : is_subgraph G H -> path_sub G H u (i :: p) v = (i \in H) && (u == (nth (0,0) G i).1) && path_sub G H (nth (0,0) G i).2 p v.
Proof.
  move=> H_subgraph.
  rewrite/path_sub cons_path/= !andbA.
  case (all _ _) ; last by rewrite -!andbA /= !andbA andbC [RHS]andbC.
  apply congr2=> //.
  rewrite andbC !andbA ; do 3!(apply congr2=> //).
  case: (@idP (i \in H))=> [|//].
  move: H_subgraph=> /allP /[apply] ; done.
Qed.

Lemma path_path_sub G H u p v : is_subgraph G H -> path (sub H G) u p v -> path_sub G H u [seq nth 0 H i | i <- p] v.
Proof.
  move=> ?.
  elim: p u=> [?|i p Hr u] ; first by rewrite/path_sub andbC !path0.
  rewrite cons_path_sub// cons_path size_map=> /andP[]/andP[] /[dup] /mem_nth-> ?+ /Hr.
  rewrite (nth_map 0)// => -> ; done.
Qed.

Lemma pushforward_path (G : multigraph) f u p v : path G u p v -> path (FiniteOrientedGraph.pushforward f G) (f u) p (f v).
Proof.
  elim: p u=> [?|u' p H u] ; first by rewrite !path0=> /eqP->.
  rewrite !cons_path size_map.
  case: (@idP (_ < _))=> [?|//].
  rewrite (nth_map (0,0))//==> /andP[]/eqP->/H -> ; rewrite eq_refl ; done.
Qed.


Module PathMatrix_.

Import PathMatrix.Theory.
Import Graph.Theory.

Lemma path_decreasing0 G u p : decreasing_arc_prop G -> OrientedGraph.path G u p u -> p == [::].
Proof.
  case: p => [//|u' p] decreasing_arc.
  rewrite cons_path=> /andP[]/decreasing_arc u'_lt_u /(path_decreasing decreasing_arc).
  rewrite leqNgt=> /negP ; done.
Qed.

End PathMatrix_.

Lemma path_decreasing G u p v : {in arc G, forall e, e.1 > e.2} -> path G u p v -> u >= v.
Proof.
  move=> arc_decreasing path_upv.
  have {}arc_decreasing : PathMatrix.Theory.decreasing_arc_prop G.
    move=>?? /mem_arcP /arc_decreasing ; done.
  apply: PathMatrix.Theory.path_decreasing.
  - apply arc_decreasing.
  - apply finite_path_path ; apply path_upv.
Qed.

Lemma path_decreasing0 G u p : {in arc G, forall e, e.1 > e.2} -> path G u p u -> p == [::].
Proof.
  move=> arc_decreasing path_upv.
  have {}arc_decreasing : PathMatrix.Theory.decreasing_arc_prop G.
    move=>?? /mem_arcP /arc_decreasing ; done.
  move: (PathMatrix_.path_decreasing0 arc_decreasing (finite_path_path path_upv)).
  case p ; done.
Qed.


Module Commerge.

Import Graph TopologicalSort PathMatrix.


Section commerge.


Fixpoint dfs (V : eqType) (g : V -> seq V) (n : nat) (v : seq V) (x : V) :=
  if x \in v then v else
  if n is n'.+1 then foldl (dfs g n') (x :: v) (g x) else v.


Variables (G : seq (nat * nat))
          (Hi : seq subgraph).

Definition tsd : toposorted_data.toposorted_data _ := toposort_graph (Build G).

(* If `G` is acyclic :
  - `G'` is isomorphic to `G` but with anti-toposorted vertices (arrows are decreasing),
  - `Ĝ` is `G'` where multiarrows has been merge in one arrow.
  - Note that we can consider that `pm_G' = pm_Ĝ`. *)
Definition G' := FiniteOrientedGraph.pushforward (index^~ (toposorted_data.vertex tsd)) (Build G).
Definition Ĝ := toposorted_data.graph tsd.
Definition pm_G' := compute_pm Ĝ.
Definition pm_Hi' := [seq (compute_pm (sort (fun a b => a.1 >= b.1) (undup (sub H G'))), H) | H <- Hi ].

Definition acyclic_G'_prop := toposorted_data.acyclic tsd.

Definition enum_G' := zip G' (iota 0 (size G')).


Section commerge_aux.

Variables (u v : nat).

(* Neighbours of `u` from which we can go to `v` *)
Definition ngh_u := [seq (p.1.2, p.2) | p <- enum_G' & (p.1.1 == u) && is_path pm_G' p.1.2 v].
(* Pre-neighbours of `v` accessible from `u` *)
Definition ngh_v := [seq (p.1.1, p.2) | p <- enum_G' & (p.1.2 == v) && is_path pm_G' u p.1.1].

Fixpoint couple T (l : seq T) :=
  match l with
  | x :: ((y :: _) as l') => (x, y) :: couple l'
  | _ => [::]
  end.

(* Concatenation of `ngh_u` and `ngh_v` if they would have been calculated in `H` *)
Definition vertices_from_H (H : subgraph) pm_H :=
  [seq p.2 | p <- ngh_u & ((p.2 \in H) && is_path pm_H p.1 v)] ++
  [seq p.2 | p <- ngh_v & ((p.2 \in H) && is_path pm_H u p.1)].

(* Pair of (indices of) arcs `(e, f)` such that there is a path from `u` to `v`
  starting with the arc `e` and finishing with the arc `f` (and `e` \neq `f`). *)
Definition edges_from_G pm_G := [seq (p.1.2, p.2.2) |
  p <- [seq (q, r) | q <- ngh_u, r <- ngh_v] &
  is_path pm_G p.1.1 p.2.1 ].

(* Constructing a new graph \mathcal G *)
Definition edges := (edges_from_G pm_G') ++ flatten [seq couple (vertices_from_H p.2 p.1) | p <- pm_Hi'].

Definition vertices := undup (unzip2 (ngh_u ++ ngh_v)).

(* Neighbors and preneighbors of `i` in \mathcal G *)
Definition g i := [seq p.2 | p <- edges & p.1 == i] ++ [seq p.1 | p <- edges & p.2 == i].

(* Check that \mathcal G is connected *)
Definition commerge_aux :=
  if vertices is w :: _ then
    size (dfs g (size vertices) [::] w) == size vertices
  else
    true.

End commerge_aux.


Definition vertex := FiniteOrientedGraph.relevant_vertex_seq G'.

(* Check that all graphs \mathcal G for all pair of vertices are connected *)
Definition commerge : bool :=
  all (fun p => commerge_aux p.1 p.2) [seq (u, v) | u <- vertex, v <- vertex].




Section Theory.

Import PathMatrix.Theory Graph.Theory TopologicalSort.Theory.

Section Preliminary_Lemmas.

Definition is_pathP_prop G := forall u v,
  reflect (exists p, path G u p v) (is_path (compute_pm G) u v).

Lemma compute_is_path_correct H :
  all (fun a => a.1 > a.2) H ->
  sorted (fun a b => a.1 >= b.1) H ->
  uniq H ->
  is_pathP_prop H.
Proof.
  move=> /allP h1 h2 h3 ??.
  set pm := compute_pm _.
  have : path_matrix_correct_prop (Build H) pm.
    rewrite/pm ; elim: (H) h1 h2 h3 => [_ _ _/=| a H' Hr/= decreasing_arc sorted_arc uniq_arc].
    - split=> [u p v| u v].
      - case: p=> //= ; rewrite Theory.path0 /is_path=> -> ; done.
      - rewrite/is_path/==> /orP[/eqP->|] ; last by rewrite nth_nil has_nil.
        case: v=> //v ; rewrite/compute_path/= eq_refl path0 ; done.
    - rewrite {1}(surjective_pairing a) [Build _]/(add_arc (Build H') a.1 a.2).
      apply path_matrix_add_arc_correct=> /=.
      - move=> u v /mem_arcP/= uv_in_H'.
        apply (decreasing_arc (u, v)).
        rewrite in_cons orbC uv_in_H' ; done.
      - apply decreasing_arc ; rewrite in_cons eq_refl ; done.
      - move=> u v /mem_arcP uv_in.
        move: sorted_arc.
        set leT := fun _ _ => _.
        have leT_trans : ssrbool.transitive leT by move=>??? /[swap] ; apply leq_trans.
        move/(order_path_min leT_trans)=> /allP/(_ _ uv_in) ; done.
      - apply/negP=> /mem_arcP.
        move: uniq_arc ; rewrite -surjective_pairing => /andP[]/negP ; done.
      - apply Hr.
        - move=> u u_in ; apply decreasing_arc ; rewrite in_cons orbC u_in ; done.
        - move/path_sorted: sorted_arc ; done.
        - move: uniq_arc=> /andP[] ; done.
  move=> [] path_is_path is_path_path.
  apply/(iffP idP).
  - move/is_path_path/path_finite_path ; done.
  - rewrite [H]/(arc (Build H)).
    move=>[]p /(@finite_path_path (Build H)) /path_is_path ; done.
Qed.

Lemma dfs_uniq (V : eqType) g n v (x : V) : uniq v -> uniq (dfs g n v x).
Proof.
  elim: n v x=> [v x /=|n H v x uniq_v /=] ; first by case (x \in v).
  case: (@idP (x \in v))=> [//|/negP/negPf x_notin].
  have : uniq (x :: v).
    rewrite/= x_notin ; done.
  elim: (g x) (x :: v) => [//|x1 ngh H1 {uniq_v x_notin} l /=].
  move/H/H1 ; done.
Qed.

Lemma dfs_path (V : eqType) g n v (x : V) : {in dfs g n v x, forall y, y \in v \/ exists l, OrientedGraph.path (LocallyFiniteOrientedGraph.Build g) x l y }.
Proof.
  elim: n v x=> [*/=|n H v x /=] ; first by case: (_ \in _) ; left.
  case: (@idP (x \in v))=> [|/negP/negPf _] ; first by left.
  move=> y y_in.
  set h := exists _, _.
  suff : y \in x :: v \/ h.
    move=> [|] ; last by right.
    rewrite in_cons /h=> /orP[/eqP->|->] ; [right | left]=> //.
    exists [::] ; rewrite path0 ; done.
  have : {subset g x <= g x } by [].
  move: {-3}(g x) y_in => l.
  rewrite/h.
  elim: l (x :: v) => [|x' ngh H1 {}v/= /H1 + in_gx] ; first by left.
  have /[swap]/[apply] : {subset ngh <= g x}.
    move=> z z_in.
    apply in_gx.
    rewrite in_cons orbC z_in ; done.
  move=> [/H|] ; last by right.
  move=> [] ; first by left.
  move=> [] l path_x'ly ; right ; exists (x' :: l).
  rewrite cons_path /OrientedGraph.arc/=.
  move: (in_gx x') ; rewrite in_cons eq_refl => /(_ isT) -> ; done.
Qed.

End Preliminary_Lemmas.


Section commerge_correct.

Variable
  (r : path_relation).

Hypotheses
  (acyclic_G : Graph.Theory.acyclic (Build G))
  (sub_total : {in Hi, forall H, path_total_sub G H r})
  (Hi_subgraph : all (is_subgraph (Build G)) Hi)
  (do_commerge : commerge).

Definition morph_GG' := index^~ (toposorted_data.vertex tsd).

Definition morph_G'G := nth 0 (toposorted_data.vertex tsd).

Definition r'_r u v p p' := r (morph_G'G u) (morph_G'G v) p p'.

Lemma r'_equiv u v : equivalence (seq nat) (r'_r u v).
Proof. apply π_equiv. Qed.

Lemma r'_cat_stable u v w p p' q q' :
  r'_r u v p p' -> r'_r v w q q' -> r'_r u w (p ++ q) (p' ++ q').
Proof. apply π_cat_stable. Qed.

Definition r' := Build_path_relation r'_equiv r'_cat_stable.

Arguments toposorted_data.vertex : simpl never.

Lemma inverse_pushforward : G = pushforward morph_G'G G'.
  apply/eqP.
  have : {subset G <= G} by [].
  rewrite/G' ; elim: {-2}G => [//|a H Hr sub_aH_G].
  have sub_HG: {subset H <= G}.
    move=> x x_in ; apply sub_aH_G ; rewrite in_cons x_in orbC ; done.
  move/eqP: (Hr sub_HG) => {1}->/=.
  apply/eqP ; apply congr2=> //.
  rewrite/morph_G'G !nth_index.
  - apply surjective_pairing.
  all:rewrite -mem_rev revK -relevant_sorted.
  all:have : a \in arc {| arc := G |} by move: (sub_aH_G a) ; rewrite in_cons eq_refl=> /(_ isT).
  all:rewrite {1}(surjective_pairing a) -mem_arc.
  - move/arc2_relevant ; done.
  - move/arc1_relevant ; done.
Qed.

Lemma inverse_GG'G : {in relevant_vertex_seq {|arc := G|}, forall u, morph_G'G (morph_GG' u) = u}.
Proof.
  move=> u ?.
  apply nth_index.
  rewrite mem_rev -relevant_sorted ; done.
Qed.

Lemma sub_total_G' : {in Hi, forall H, path_total_sub G' H r'}.
Proof.
  move=> H /sub_total sub_total_G u p p' v /andP[] + ? /andP[] + ?.
  move=> /(pushforward_path morph_G'G) + /(pushforward_path morph_G'G).
  rewrite -inverse_pushforward=> ??.
  apply sub_total_G ; apply /andP ; done.
Qed.

Lemma arc_decreasing_tsd_graph : {in arc Ĝ, forall a, a.1 > a.2}.
Proof.
  move=> ?*.
  apply (toposort_graph_decreasing_arc acyclic_G).
  apply/mem_arcP ; rewrite -surjective_pairing ; done.
Qed.

Lemma arc_decreasing_G' : {in arc G', forall a, a.1 > a.2}.
Proof.
  move=> a a_in.
  have ? : a \in arc Ĝ.
    rewrite -[X in a \in X]/(sort _ (undup G')).
    erewrite perm_mem ; last by rewrite perm_sort.
    rewrite mem_undup ; done.
  apply arc_decreasing_tsd_graph ; done.
Qed.

Lemma decreasing_arc_G' : decreasing_arc_prop G'.
Proof.
  move=>u v*.
  rewrite -[v < u]/((u,v).2 < (u,v).1).
  apply arc_decreasing_G'.
  apply/mem_arcP ; done.
Qed.

Lemma is_path_ĜP : is_pathP_prop Ĝ.
Proof.
  apply compute_is_path_correct.
  - apply/allP=> ??.
    apply arc_decreasing_tsd_graph ; done.
  - apply sort_sorted ; move=> * ; apply leq_total.
  - rewrite sort_uniq undup_uniq ; done.
Qed.

Lemma is_path_G'P u v : reflect (exists p : seq nat, path G' u p v) (is_path pm_G' u v).
Proof.
  have eq_mem_G'Ĝ : OrientedGraph.arc Ĝ =2 OrientedGraph.arc G'.
    move=> ?? ; rewrite !mem_arc -[arc Ĝ]/(sort _ (undup (arc G'))).
    rewrite mem_sort mem_undup ; done.
  apply (iffP (is_path_ĜP u v)).
  all: move=> [] p /finite_path_path ; rewrite/OrientedGraph.path.
  1: rewrite (eq_path eq_mem_G'Ĝ).
  2: rewrite -(eq_path eq_mem_G'Ĝ).
  all: move/path_finite_path ; done.
Qed.

Lemma is_path_Hi'P_aux H : H \in Hi -> is_pathP_prop (sort (fun a b => a.1 >= b.1) (undup (sub H G'))).
Proof.
  move=> H_in.
  apply compute_is_path_correct.
  - apply/allP=> a a_in.
    have ? : a \in arc G'.
      move: a_in ; erewrite perm_mem ; last by rewrite perm_sort.
      rewrite !mem_undup /sub=> /mapP[] i i_in_H ->.
      apply mem_nth.
      move/allP: Hi_subgraph=> /(_ H H_in) ; rewrite/is_subgraph=> /allP /(_ i i_in_H)/=.
      rewrite size_map ; done.
    apply arc_decreasing_G' ; done.
  - apply sort_sorted ; move => * ; apply leq_total.
  - rewrite sort_uniq undup_uniq ; done.
Qed.

Lemma is_path_Hi'P u v H : H \in Hi ->
  let pm_H := compute_pm (sort (fun a b => a.1 >= b.1) (undup (sub H G'))) in
  reflect (exists p, path (sub H G') u p v) (is_path pm_H u v).
Proof.
  move=> H_in.
  have eq_mem_HĤ : OrientedGraph.arc (FiniteOrientedGraph.Build (sort (fun a b => a.1 >= b.1) (undup (sub H G')))) =2 OrientedGraph.arc (sub H G').
    move=> ?? ; rewrite !mem_arc -[arc _]/(sort _ (undup (arc _))).
    rewrite mem_sort mem_undup ; done.
  apply (iffP (is_path_Hi'P_aux H_in u v)).
  all: move=> [] p /(finite_path_path (G := Build _)) ; rewrite/OrientedGraph.path.
  1: rewrite (eq_path eq_mem_HĤ).
  2: rewrite -(eq_path eq_mem_HĤ).
  all: move/path_finite_path ; done.
Qed.


Section step.

Definition Hr n := forall k, k <= n -> forall u p p', path G' (u+k) p u -> path G' (u+k) p' u -> r' (u+k) u p p'.

Definition suit (G : multigraph) u v p e := path G u p v &&
  if p is i :: p' then (e == i) || (e == last i p') else false.

Lemma suitP (H : multigraph) u p v e : reflect ((exists p', p = e :: p' \/ p = rcons p' e) /\ path H u p v) (suit H u v p e).
Proof.
  apply (iffP idP).
  - rewrite/suit ; case: p => [|f p' /andP[] -> /orP[] /eqP e_eq] ; first by rewrite andbC.
    1,2: split=> //.
    - exists p' ; left ; rewrite e_eq ; done.
    - exists (belast f p') ; right ; rewrite e_eq -lastI ; done.
  - move=> [] []p' [] p_eq path_upv.
    all: apply/andP ; split=> //.
    - rewrite p_eq eq_refl ; done.
    - rewrite p_eq ; case p' => [/=|??] ; first by rewrite eq_refl.
      rewrite rcons_cons last_rcons orbC eq_refl ; done.
Qed.

Definition R G u v : relation nat := fun e e' => forall p p', suit G u v p e -> suit G u v p' e' -> r' u v p p'.


Variable (n : nat) (v : nat).

Hypothesis (Hr_n : Hr n).

Let u := v + n.+1.


Lemma zip_1 (T U : eqType) (a : T * U) s t : a \in zip s t -> a.1 \in s.
Proof.
  elim: s t=> [[]//|?? H []// ??/=].
  rewrite !in_cons=> /orP [/eqP->/=|] ; first by rewrite eq_refl.
  rewrite orbC=> /H-> ; done.
Qed.

Lemma toposorted_graph_vertex : relevant_vertex_seq Ĝ =i vertex.
Proof.
  have same_arcs : arc Ĝ =i arc G'.
    rewrite/Ĝ -[toposorted_data.graph _]/(Build (_ _ (undup G'))) => w.
    rewrite -[w \in arc G']mem_undup.
    apply perm_mem ; rewrite perm_sort ; done.
  move=> w ; rewrite/vertex/relevant_vertex_seq !mem_undup.
  rewrite !mem_cat ; apply congr2 ; apply eq_mem_map ; done.
Qed.

Lemma ngh_uv_arc : ngh_u u v != [::] \/ ngh_v u v != [::] -> (u \in vertex) && (v \in vertex).
Proof.
  move=> [] ; [ set ngh := ngh_u | set ngh := ngh_v].
  all: case: {-1}ngh (erefl (ngh u v))=> [//|e l ngh_eq _].
  all: have : e \in ngh u v by rewrite ngh_eq in_cons eq_refl.
  all: move/mapP=>[]a ; rewrite mem_filter => /andP[] /andP[] ++ /zip_1 + _.
  all: rewrite {3}(surjective_pairing a.1)=> /eqP->.
  - move=> + /[dup] /mem_arcP /arc1_relevant ->.
    case: (@idP (a.1.2 == v))=> [/eqP-> _|/negP/negPf a12_neq_v].
      - move=> /mem_arcP/arc2_relevant-> ; done.
      - move/is_path_ĜP=> []p /finite_path_path.
        case: p=> [|??] ; first by rewrite path0 a12_neq_v.
        move/last_arc_arc=> /(_ isT)/arc2_relevant.
        rewrite toposorted_graph_vertex/= ; done.
  - move=> + /[dup] /mem_arcP /arc2_relevant ->.
    case: (@idP (u == a.1.1))=> [/eqP<- _|/negP/negPf a11_neq_u].
      - move=> /mem_arcP/arc1_relevant-> ; done.
      - move/is_path_ĜP=> []p /finite_path_path.
        case: p=> [|??] ; first by rewrite path0 a11_neq_u.
        move/first_arc_arc=> /(_ isT)/arc1_relevant.
        rewrite toposorted_graph_vertex=> -> ; done.
Qed.

Lemma commerge_aux_u_v : commerge_aux u v.
Proof.
  case: (@idP ((u \in vertex) && (v \in vertex)))=> [/andP[] u_in v_in | NuAv_in].
  - move/allP/(_ (u, v)): do_commerge=> h ; apply h => {h}.
    apply/allpairsP ; exists (u,v) ; done.
  - suff : vertices u v == [::].
      rewrite/commerge_aux=> /eqP-> ; done.
    case: (@idP ((ngh_u u v != [::]) || (ngh_v u v != [::]))) => [/orP/ngh_uv_arc// | /negP].
    rewrite negb_or /vertices !negbK=> /andP[] /eqP-> /eqP-> ; done.
Qed.

Lemma couple_sub (T : eqType) x (l : seq T) : x \in couple l -> (x.1 \in l) && (x.2 \in l).
Proof.
  elim: l=> [//|? l H/=].
  case: l H=> [//|?? H].
  rewrite !in_cons=> /orP[/eqP->//|/H/andP[]] ; first rewrite !eq_refl/= orbC//.
  rewrite !in_cons=> /orP[]-> /orP[]->.
  all: do!case: (_ == _)=> //=.
Qed.

Lemma g_sub_vertices e : {subset g u v e <= vertices u v}.
Proof.
  move=> f ; rewrite/g.
  rewrite mem_cat => /orP[] /mapP[] ?.
  all: rewrite mem_filter=> /andP[]  _.
  all: rewrite/edges mem_cat=> /orP[|/flattenP[] ? /mapP[] ? _ ->].
  1,3: rewrite/edges_from_G=> /mapP[] ?.
  1,2: rewrite mem_filter=> /andP[] _ /flattenP[] ? /mapP[] ? ? -> /mapP[] ? ? -> -> ->.
  1,2: rewrite/=/vertices mem_undup map_f// mem_cat ; apply/orP ; try (right ; done) ; left ; done.
  all: move/couple_sub=> /andP[].
  move=> _.
  2:move=> + _.
  all: rewrite/vertices_from_H !mem_cat=> /orP[] /mapP[] ?.
  all: rewrite mem_filter /vertices mem_undup /unzip2 map_cat mem_cat => /andP[] _ /(map_f snd) /[swap]->/[swap]-> ? ; apply/orP.
  1,3:left ; done.
  all:right ; done.
Qed.

Lemma dfs_sub_vertices E m e : e \in vertices u v -> {subset E <= vertices u v} -> {subset dfs (g u v) m E e <= vertices u v }.
Proof.
  elim: m E e=> [*/=|m H E e/=] ; first by case: (_ \in _).
  case: (e \in E) => [//|] e_in E_sub.
  have: {subset e :: E <= vertices u v}.
    move=> x ; rewrite in_cons=> /orP[/eqP->//|]; apply E_sub ; done.
  elim: (g _ _ _) (e :: E) (g_sub_vertices (e := e)) => [//|{e_in}e l H1 {E_sub}E el_sub E_sub].
  apply H1=> //.
  move=> x x_in ; apply el_sub ; rewrite in_cons x_in orbC//.
  apply H=> //.
  apply el_sub ; rewrite in_cons eq_refl ; done.
Qed.

Definition g_arc := OrientedGraph.arc (LocallyFiniteOrientedGraph.Build (g u v)).

Definition g_path := OrientedGraph.path (LocallyFiniteOrientedGraph.Build (g u v)).

Lemma rev_arc e f : g_arc e f -> g_arc f e.
Proof.
  rewrite -![g_arc _ _]/(_ \in _ ++ _) !mem_cat=> /orP[] /mapP[] p.
  all: rewrite mem_filter=> /andP[]/eqP eq_e p_in f_eq.
  all: apply/orP. right. 2:left.
  all: apply/mapP ; exists p=> //.
  all: rewrite mem_filter f_eq eq_refl ; done.
Qed.

Lemma rev_path e f p : g_path e p f -> exists q, g_path f q e.
Proof.
  rewrite/g_path.
  elim: p e=> [?|e' p H e].
    rewrite path0=> /eqP-> ; exists [::] ; rewrite path0 ; done.
  rewrite cons_path=> /andP[] /rev_arc arc_e'e /H[] p' path_fp'e'.
  exists (rcons p' e).
  rewrite rcons_path.
  move/last_path: (path_fp'e') => ->.
  rewrite path_fp'e'/= eq_refl andbC ; done.
Qed.

Lemma path_connected : {in vertices u v &, forall e1 e2, exists l, g_path e1 l e2}.
Proof.
  move=> e1 e2 e1_in e2_in.
  move: commerge_aux_u_v.
  rewrite/commerge_aux.
  case: {-1}vertices (erefl (vertices u v)) e1_in => [//|e0 ? /[dup] vertices_eq <- e1_in /eqP same_size].
  set dfs_uv := dfs _ _ _ _ in same_size.
  have dfs_vertices : dfs_uv =i vertices u v.
    apply uniq_min_size.
    - apply dfs_uniq ; done.
    - apply dfs_sub_vertices => //.
      rewrite vertices_eq in_cons eq_refl ; done.
    - rewrite same_size ; done.
  suff accessible_e0 : {in vertices u v, forall e, exists l, g_path e0 l e}.
    move: (accessible_e0 _ e1_in) (accessible_e0 _ e2_in)=> []? /rev_path []l1 path_e1l1e0 []l2 ?.
    pose last_l1_e0 := last_path path_e1l1e0.
    exists (l1 ++ l2).
    rewrite/g_path cat_path last_l1_e0 -/g_path path_e1l1e0 ; done.
  move=> x ; rewrite -dfs_vertices.
  move/dfs_path=> [] ; done.
Qed.

Lemma same_start_r i p p' : path G' u (i::p) v -> path G' u (i::p') v -> r' u v (i::p) (i::p').
Proof.
  rewrite !Commerge.cons_path.
  set u' := _.2=> /andP[] /andP[] i_lt_size /eqP u_eq path_u'pv /andP[] _ path_u'p'v.
  rewrite -cat1s -[_ :: p']cat1s.
  apply (π_cat_stable (v := u')).
  - apply π_equiv ; done.
  - have ? : u > u'.
      rewrite u_eq.
      apply arc_decreasing_G'.
      rewrite mem_nth ; done.
    have u'_ge_v : u' >= v.
      apply (path_decreasing (G := G') (p := unzip2 (sub p G'))).
      - move=>?? /mem_arcP /arc_decreasing_G' ; done.
      - apply finite_path_path ; done.
    pose eq_u' := subnKC u'_ge_v.
    rewrite -eq_u' ; apply Hr_n ; try by rewrite eq_u'.
    rewrite -ltnS.
    apply (leq_trans (n := u - v)).
    - apply ltn_sub2r ; rewrite// /u addnS ltnS leq_addr ; done.
    - rewrite/u addnC -addnBA// subnn addn0 ; done.
Qed.

Lemma same_end_r i p p' : path G' u (rcons p i) v -> path G' u (rcons p' i) v -> r' u v (rcons p i) (rcons p' i).
Proof.
  rewrite !Commerge.rcons_path.
  set v' := _.1=> /andP[] /andP[] i_lt_size path_upv' /eqP eq_v /andP[] /andP[] _ path_up'v' _.
  rewrite -!cats1.
  apply (π_cat_stable (v := v')) ; last first.
  - apply π_equiv ; done.
  - have ? : v' > v.
      rewrite -eq_v.
      apply arc_decreasing_G'.
      rewrite mem_nth ; done.
    have u_le_v' : u >= v'.
      apply (path_decreasing (G := G') (p := unzip2 (sub p G'))).
      - move=>?? /mem_arcP /arc_decreasing_G' ; done.
      - apply finite_path_path ; done.
    pose eq_u := subnKC u_le_v'.
    rewrite -eq_u ; apply Hr_n ; try by rewrite eq_u.
    rewrite/u addnS -addSn addnC -subnBA// leq_subr ; done.
Qed.

Lemma suit_r i p p' : suit G' u v p i -> suit G' u v p' i -> r' u v p p'.
Proof.
  move=> /suitP[][] q []-> path_upv /suitP[][] q' []-> path_up'v.
  - apply same_start_r ; done.
  - move: q q' path_upv path_up'v => q' q path_up'v path_upv.
    1,2: have/eqP-> : q == [::].
      1,3: move: path_up'v path_upv ; rewrite Commerge.cons_path=> /andP[]/andP[] _/eqP-> _.
      1,2: rewrite Commerge.rcons_path=> /andP[]/andP[] _ /(path_decreasing0 arc_decreasing_G') ; done.
    1,2: have/eqP-> : q' == [::].
      1,3: move: path_upv path_up'v ; rewrite Commerge.rcons_path=> /andP[] _ /eqP<-.
      1,2: rewrite Commerge.cons_path=> /andP[] _ /(path_decreasing0 arc_decreasing_G') ; done.
    1,2: apply π_equiv ; done.
  - apply same_end_r ; done.
Qed.

Lemma path_ngh_u p : p \in ngh_u u v -> path G' u [:: p.2] p.1.
Proof.
  move=> /mapP[] x +->.
  rewrite mem_filter (enum_nth (0,0))=> /andP[]/andP[]/eqP eq_u _ /andP[] lt_size /eqP nth_eq.
  rewrite/path/= lt_size nth_eq eq_u/= ; done.
Qed.

Lemma path_v_ngh p : p \in ngh_v u v -> path G' p.1 [:: p.2] v.
Proof.
  move=> /mapP[] x +->.
  rewrite mem_filter (enum_nth (0,0))=> /andP[]/andP[]/eqP eq_u _ /andP[] lt_size /eqP nth_eq.
  rewrite/path/= lt_size nth_eq eq_u/= ; done.
Qed.

Lemma R_edges_from_G : {in edges_from_G u v pm_G', forall p, R G' u v p.1 p.2}.
Proof.
  move=> ? /mapP[] ?.
  rewrite mem_filter=> /andP[] /is_path_G'P[] p /[swap] /flattenP[] ? /mapP[] e e_in_ngh_u -> /mapP[] f f_in_ngh_v -> /[swap] -> path_e1pf1/=.
  move=> p1 p2 suit_p1_e2 suit_p2_f2.
  pose p' := e.2 :: (rcons p f.2).
  have path_up'v : path G' u p' v.
    rewrite/p' -cat1s -cats1.
    apply (Commerge.cat_path (v := e.1)) ; first by apply path_ngh_u.
    apply (Commerge.cat_path (v := f.1)) ; last by apply path_v_ngh.
    done.
  have: suit G' u v p' f.2.
    rewrite/suit andbC last_rcons eq_refl orbC ; done.
  have: suit G' u v p' e.2.
    rewrite/suit andbC eq_refl ; done.
  move=> /(suit_r suit_p1_e2) + /suit_r/(_ suit_p2_f2).
  apply π_equiv ; done.
Qed.

Lemma vertices_from_H_path pm_HH : pm_HH \in pm_Hi' -> let: (pm_H, H) := pm_HH in
  {in vertices_from_H u v H pm_H, forall e, exists p, path_sub G' H u p v && suit G' u v p e}.
Proof.
  move=> /mapP[] H H_in_Hi -> i.
  have subgraph_H: is_subgraph G' H.
    rewrite/is_subgraph/G' size_map.
    move: Hi_subgraph=> /allP/(_ H H_in_Hi) ; done.
  rewrite mem_cat=> /orP[] /mapP[] a.
  all: rewrite mem_filter=> /[swap]-> /andP[]/andP[] a2_in_H /(is_path_Hi'P _ _ H_in_Hi) []? /(path_path_sub subgraph_H).
  all: set p := map _ _ => /andP[] path_p p_sub_H.
  1: move/path_ngh_u=> ? ; exists (a.2 :: p) ; rewrite/suit -cat1s.
  2: move/path_v_ngh=> ? ; exists (rcons p a.2) ; rewrite/suit -cats1.
  all: apply/andP ; split ; rewrite ?/path_sub ?(Commerge.cat_path (v := a.1))// ?cats1 ?all_rcons/= ?a2_in_H// ?eq_refl//.
  case p=> [|??]/= ; rewrite ?last_rcons eq_refl// orbC ; done.
Qed.

Lemma R_vertices_from_H pm_HH : pm_HH \in pm_Hi' -> {in vertices_from_H u v pm_HH.2 pm_HH.1 &, forall e e', R G' u v e e'}.
Proof.
  move=> /[dup]/mapP[] H H_in -> /vertices_from_H_path=> to_path.
  move=> e e' /to_path[] p1 /andP[] ?? /to_path[] p2 /andP[] ?? p p' ??.
  apply (equiv_trans _ _ (π_equiv r' u v) _ p1) ;
  last apply (equiv_trans _ _ (π_equiv r' u v) _ p2).
  - apply (@suit_r e) ; done.
  - apply (sub_total_G' H_in) ; done.
  - apply (@suit_r e') ; done.
Qed.

Lemma R_edges : {in edges u v, forall t, R G' u v t.1 t.2}.
Proof.
  move=> t ; rewrite mem_cat=> /orP[] ; first by apply R_edges_from_G.
  move=> /flattenP[] ? /mapP[] pm_HH ? -> /couple_sub/andP[]*.
  apply (@R_vertices_from_H pm_HH) ; done.
Qed.

Lemma R_refl : reflexive _ (R G' u v).
Proof.
  move=> e* ; rewrite/R ; apply (@suit_r e) ; done.
Qed.

Lemma R_sym : symmetric _ (R G' u v).
Proof.
  move=> e e' H ????.
  apply π_equiv.
  apply H ; done.
Qed.

Lemma vertices_suit e : e \in vertices u v -> exists p, suit G' u v p e.
Proof.
  rewrite mem_undup=> /mapP[] ? ; rewrite mem_cat=> /orP[]/[dup] /mapP[]? ;
  rewrite mem_filter=> /andP[]/andP[] ? /is_path_G'P [] p path_p _ -> + ->/=.
  1: move=>/path_ngh_u/Commerge.cat_path/(_ path_p).
  2: move=>/path_v_ngh/(Commerge.cat_path path_p).
  1,2: set p' := cat _ _ => ? ; exists p' ; apply/andP ; split=> ///=.
  - rewrite eq_refl ; done.
  - rewrite/p' ; case p =>/= [|??] ; rewrite ?eq_refl// orbC cats1 last_rcons eq_refl ; done.
Qed.

Lemma R_trans_in_vertices : {in vertices u v & &, transitive _ (R G' u v)}.
Proof.
  move=> e f g _ /vertices_suit[] p'? _ Ref Rfg p p'' ??.
  apply (equiv_trans _ (r' u v) (r'_equiv u v) _ p').
  - apply Ref ; done.
  - apply Rfg ; done.
Qed.

Lemma R_e_ge : {in vertices u v, forall e, {in g u v e, forall f, R G' u v e f}}.
Proof.
  move=> e? f ; rewrite mem_cat=> /orP[]/mapP[] t.
  all:rewrite mem_filter=> /andP[] /eqP<- /[swap] -> ?.
  2: apply R_sym.
  all: apply R_edges ; done.
Qed.

Lemma R_vertices : {in vertices u v &, forall e f, R G' u v e f}.
Proof.
  move=> e f e_in f_in.
  move: (path_connected e_in f_in)=> []p.
  elim: p e e_in=> [??|e' p H e e_in] ; first by rewrite/g_path path0=> /eqP -> ; apply R_refl.
  rewrite/g_path cons_path=> /andP[]/[dup]/g_sub_vertices e'_in /(R_e_ge e_in) ? /(@H _ e'_in) ?.
  apply (R_trans_in_vertices (y := e')) ; done.
Qed.

Lemma path_suit p : path G' u p v -> exists e, e \in vertices u v /\ suit G' u v p e.
Proof.
  case: p=> [|e p /[dup] path_] ; first by rewrite Commerge.path0 -(addn0 v) eqn_add2l.
  rewrite Commerge.cons_path ; set u' := _.2=> /andP[]/andP[] e_lt_size /eqP u_eq ?.
  exists e ; split.
  - rewrite mem_undup ; apply/mapP ; exists (u', e) ; last by [].
    rewrite mem_cat ; apply/orP ; left ; apply/mapP ; exists ((u, u'), e) ; last by [].
    rewrite mem_filter eq_refl/= ; apply/andP ; split.
    - apply/is_path_G'P ; exists p ; done.
    - rewrite (enum_nth (0,0)) e_lt_size u_eq /u' -surjective_pairing eq_refl ; done.
  - rewrite/suit path_ eq_refl ; done.
Qed.

Lemma r'_uv p p' : path G' u p v -> path G' u p' v -> r' u v p p'.
Proof.
  move=> /path_suit[] ? [] e_in ? /path_suit[] ? [] f_in ?.
  apply (R_vertices e_in f_in) ; done.
Qed.

End step.

Lemma r_total_step n : Hr n -> Hr n.+1.
Proof.
  move=> Hr_n k ; rewrite leq_eqVlt=> /orP[/eqP-> v|] ; last by rewrite ltnS ; apply Hr_n.
  apply r'_uv ; done.
Qed.

Lemma r_total_init : Hr 0.
  move=> k ; rewrite leqn0=> /eqP-> u ?? ; rewrite addn0.
  move=> /(path_decreasing0 arc_decreasing_G')/eqP-> /(path_decreasing0 arc_decreasing_G')/eqP->.
  apply π_equiv ; done.
Qed.


Theorem commerge_correct' : path_total G' r'.
Proof.
  move=> u ?? v.
  case: (leqP v u)=> [/subnKC <-|].
  - suff : Hr (u - v).
      move=> h* ; apply h ; done.
    elim: (u-v).
    - apply r_total_init.
    - apply r_total_step.
  - move=> ? /(Commerge.path_decreasing arc_decreasing_G').
    rewrite leqNgt=> /negP ; done.
Qed.

Theorem commerge_correct : path_total G r.
Proof.
  move=> u p p' v.
  case: p => [|??].
  - rewrite Commerge.path0=> /eqP->.
    move/(@pushforward_path {|arc := G|} morph_GG')/(path_decreasing0 arc_decreasing_G')=> /eqP->.
    apply π_equiv.
  - move=> /[dup] ? /(@finite_path_path {|arc := G|}) /[dup]/first_arc_arc/=/(_ isT)/arc1_relevant u_in_vertex.
    move=> /last_arc_arc/=/(_ isT)/arc2_relevant v_in_vertex ?.
    set p := cons _ _.
    have : r' (morph_GG' u) (morph_GG' v) p p'.
      apply commerge_correct' ; apply pushforward_path ; done.
    rewrite/r'/=/r'_r !inverse_GG'G ; done.
Qed.


End commerge_correct.

End Theory.

End commerge.

Definition auto_commerge G Hi :=
  TopologicalSort.toposorted_data.acyclic (TopologicalSort.toposort_graph (FiniteOrientedGraph.Build G)) &&
  all (is_subgraph {| arc := G |}) Hi &&
  commerge G Hi.

Theorem auto_commerge_correct G Hi r :
  auto_commerge G Hi ->
  {in Hi, forall H, path_total_sub G H r} ->
  path_total G r.
Proof.
  move=> /andP[] /andP[] *.
  apply (Commerge.commerge_correct (Hi := Hi))=> //.
  apply/TopologicalSort.Theory.acyclicP ; done.
Qed.

End Commerge.

