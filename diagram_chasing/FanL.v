From mathcomp Require Import all_ssreflect.
Require Import Commerge Utils.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



Declare Scope fanl_scope.
Delimit Scope fanl_scope with fanl.

Declare Scope fanl_coq_scope.
Delimit Scope fanl_coq_scope with fanl_coq.



(* Convention for restriction :
  the second parameter is restricting according to the first one
  (except for `nths` where we follow `nth` convention) *)
Definition preinter (T : eqType) (H0 H : seq T) :=
  map (index^~ H0) (filter (mem H0) H).

Definition preinter2 (T : eqType) (H0 H : seq T) :=
  map (index^~ H) (filter (mem H0) H).


Lemma preinter_nths H1 H2 :
  nths 0 H1 (preinter H1 H2) =
  nths 0 H2 (preinter2 H1 H2).
Proof.
  rewrite/nths/preinter -!map_comp -eq_in_map=> i.
  rewrite mem_filter/= => /andP[] /(nth_index 0) -> /(nth_index 0) -> ; done.
Qed.



Section quiver.

Record quiver := quiver_Build {
  quiver_nb_vertex : nat;
  quiver_arc : seq (nat * nat);
}.

Definition quiver_wf '(quiver_Build n A) :=
  all (fun a => (a.1 < n) && (a.2 < n)) A.

Definition quiver_Empty :=
  quiver_Build 0 [::].

(* Most of the time, our quivers do not contains lonely vertices. All along this
   file, we write  functions to automatically compute the vertices part in these
   cases. *)
Definition quiver_from_arc A :=
  quiver_Build
    (foldr (fun a m => (maxn (maxn a.1.+1 a.2.+1) m)) 0 A)
    A.

Definition quiver_dual '(quiver_Build n A) :=
  quiver_Build n (map (fun a => (a.2,a.1)) A).


Section subquiver.

Record subquiver := subquiver_Build {
  subquiver_vertex : seq nat;
  subquiver_arc : seq nat;
}.

Definition subquiver_full '(quiver_Build n A) :=
  subquiver_Build (iota 0 n) (iota 0 (size A)).

Definition subquiver_from_arc '(quiver_Build n A) sA :=
  let A' :=  map (nth (0,0) A) sA in
  subquiver_Build
    (undup (sort leq (unzip1 A' ++ unzip2 A')))
    sA.

Definition subquiver_spanning sA '(quiver_Build n A) :=
  subquiver_Build (iota 0 n) sA.

Definition subquiver_restr_V ssV '(subquiver_Build sV sA) :=
  subquiver_Build (nths 0 sV ssV) sA.

Definition subquiver_restr_A ssA '(subquiver_Build sV sA) :=
  subquiver_Build sV (nths 0 sA ssA).

Definition subquiver_restr '(subquiver_Build ssV ssA) sQ :=
  subquiver_restr_V ssV (subquiver_restr_A ssA sQ).

Definition subquiver_preinter '(subquiver_Build sV sA) '(subquiver_Build sV0 sA0) :=
  subquiver_Build (preinter sV0 sV) (preinter sA0 sA).

Definition subquiver_preinter2 '(subquiver_Build sV sA) '(subquiver_Build sV0 sA0) :=
  subquiver_Build (preinter2 sV sV0) (preinter2 sA sA0).

End subquiver.


Definition quiver_restr_A_wf sA '(quiver_Build n A) :=
  all (gtn (size A)) sA.

Definition quiver_restr_A sA '(quiver_Build n A) :=
  quiver_Build n (nths (0,0) A sA).

Definition quiver_restr_V_wf sV '(quiver_Build n A) :=
  uniq sV &&
  all (gtn n) sV &&
  all (fun a => (a.1 \in sV) && (a.2 \in sV)) A.

Definition quiver_restr_V sV '(quiver_Build n A) :=
  quiver_Build (size sV) (map (fun a => (index a.1 sV, index a.2 sV)) A).

Definition quiver_restr_wf '(subquiver_Build sV sA) Q :=
  quiver_restr_A_wf sA Q &&
  quiver_restr_V_wf sV (quiver_restr_A sA Q).

Definition quiver_restr '(subquiver_Build sV sA) Q :=
  quiver_restr_V sV (quiver_restr_A sA Q).

End quiver.


(* Forgetting vertices: we get back the structures from Commerge *)
Coercion quiver_to_arc := quiver_arc.
Coercion subquiver_to_arc := subquiver_arc.


(* Boilerplate code for endowing the type and quiver with
  structures of eq/choice/count/Type *)

(* Bijection btw quiver and (seq nat) * (seq (nat * nat * nat)), as the
   latter triggers automated inference of eq/choice/count-type structures*)
Definition pair_of_quiver (pQ : quiver) :=
  let: quiver_Build n A := pQ  in (n, A).

Definition quiver_of_pair '(n, A) := quiver_Build n A.

Lemma pair_of_quiverK : cancel pair_of_quiver quiver_of_pair.
Proof. by case. Qed.

Definition quiver_eqMixin := CanEqMixin pair_of_quiverK.
Canonical quiver_eqType :=
  Eval hnf in EqType quiver quiver_eqMixin.

Definition quiver_choiceMixin := CanChoiceMixin pair_of_quiverK.
Canonical quiver_choiceType :=
  Eval hnf in ChoiceType quiver quiver_choiceMixin.

Definition quiver_countMixin := CanCountMixin pair_of_quiverK.
Canonical quiver_countType :=
  Eval hnf in CountType quiver quiver_countMixin.


Definition pair_of_subquiver (pQ : subquiver) :=
  let: subquiver_Build V A := pQ  in (V, A).

Definition subquiver_of_pair '(V, A) := subquiver_Build V A.

Lemma pair_of_subquiverK : cancel pair_of_subquiver subquiver_of_pair.
Proof. by case. Qed.

Definition subquiver_eqMixin := CanEqMixin pair_of_subquiverK.
Canonical subquiver_eqType :=
  Eval hnf in EqType subquiver subquiver_eqMixin.

Definition subquiver_choiceMixin := CanChoiceMixin pair_of_subquiverK.
Canonical subquiver_choiceType :=
  Eval hnf in ChoiceType subquiver subquiver_choiceMixin.

Definition subquiver_countMixin := CanCountMixin pair_of_subquiverK.
Canonical subquiver_countType :=
  Eval hnf in CountType subquiver subquiver_countMixin.



Notation "{ 'Q' A }" := (quiver_from_arc A).
Notation "{ 'sQ' sA <o Q }" := (subquiver_from_arc Q sA).
Notation "{ 'sA' sA }" := (subquiver_Build [::] sA).

(* Convention : when possible, arcs should be increasing and given in lexico-
   graphical order *)
Definition mapQ := {Q [:: (0,1)]}.
Definition bimapQ := {Q [:: (0,1); (1,2)]}.
Definition compQ := {Q [:: (0,1);(0,2);(1,2)]}.

Definition mapQD   := Eval compute in quiver_dual mapQ.
Definition bimapQD := Eval compute in quiver_dual bimapQ.
Definition compQD  := Eval compute in quiver_dual compQ.



Definition quiver_pushforward_wf
    '(subquiver_Build sV1 sA1 as sQ1)
    '(subquiver_Build sV2 sA2 as sQ2)
    Q1 Q2
    '(quiver_Build n A as Q) :=
  (quiver_restr sQ1 Q == Q1) &&
  (quiver_restr sQ2 Q == Q2) &&
  all (mem (sV1 ++ sV2)) (iota 0 n) &&
  all (mem (sA1 ++ sA2)) (iota 0 (size A)).

Definition quiver_pushforward_V sV1 sV2 :=
  (foldr maxn 0 (sV1 ++ sV2)).+1.

Definition quiver_pushforward_A
    '(subquiver_Build sV1 sA1)
    '(subquiver_Build sV2 sA2)
    '(quiver_Build n1 A1)
    '(quiver_Build n2 A2) :=
  map
    (fun i =>
      if i \in sA1 then
        let (u,v) := nth (0,0) A1 (index i sA1) in
          (nth 0 sV1 u, nth 0 sV1 v)
      else
        let (u,v) := nth (0,0) A2 (index i sA2) in
          (nth 0 sV2 u, nth 0 sV2 v))
    (iota 0 (quiver_pushforward_V sA1 sA2)).

Definition quiver_pushforward
    '(subquiver_Build sV1 _ as sQ1)
    '(subquiver_Build sV2 _ as sQ2)
    Q1 Q2 :=
  quiver_Build (quiver_pushforward_V sV1 sV2) (quiver_pushforward_A sQ1 sQ2 Q1 Q2).



Section quiver_Theory.

Implicit Type (Q : quiver) (sQ : subquiver).

Lemma quiver_restr_full_wf Q :
  quiver_wf Q ->
  quiver_restr_wf (subquiver_full Q) Q.
Proof.
  move: Q => [n A]/= /allP Q_wf.
  do!(apply/andP ; split) ; try apply/allP=> x.
  - 1,3: rewrite mem_iota ; done.
  - apply iota_uniq ; done.
  - move=> /mapP[] i ; rewrite !mem_iota !add0n=> /(mem_nth (0,0)) x_in ->/=.
    apply Q_wf ; done.
Qed.

Lemma quiver_restr_fullV n A :
  quiver_wf (quiver_Build n A) ->
  quiver_restr_V (iota 0 n) (quiver_Build n A) = (quiver_Build n A).
Proof.
  move=> /allP Q_wf ; congr quiver_Build.
  - rewrite size_iota ; done.
  - rewrite -[A in RHS]map_id -eq_in_map=> [] [u v]/= /Q_wf/andP[] *.
    rewrite !iota_index0 ; done.
Qed.

Lemma quiver_restr_spanning_wf sA Q :
  quiver_wf Q ->
  quiver_restr_A_wf sA Q ->
  quiver_restr_wf (subquiver_spanning sA Q) Q.
Proof.
  move: Q=> [n A] /= /allP Q_wf /[dup] A_wf' /allP A_wf.
  rewrite A_wf' iota_uniq.
  have ->/= : all (gtn n) (iota 0 n).
    apply/allP=> i ; rewrite mem_iota ; done.
  apply/allP=> a /mapP[] i /A_wf /(mem_nth (0,0)) a_in ->.
  rewrite !mem_iota/= ; apply Q_wf ; done.
Qed.

Lemma quiver_restr_A_wf_valid sA Q :
  quiver_wf Q ->
  quiver_restr_A_wf sA Q ->
  quiver_wf (quiver_restr_A sA Q).
Proof.
  move: Q=> [n A]/= /allP Q_wf /allP sA_wf.
  apply/allP=> i /mapP[] j /sA_wf/= /(mem_nth (0,0)) i_in ->.
  apply Q_wf ; done.
Qed.

Lemma quiver_restr_V_wf_valid sV Q :
  quiver_restr_V_wf sV Q ->
  quiver_wf (quiver_restr_V sV Q).
Proof.
  move: Q=> [n A]/= /andP[] /andP[] sV_uq sV_lt /allP sV_wf.
  apply/allP=> i /mapP[] j /sV_wf/= + ->.
  rewrite !index_mem ; done.
Qed.

Lemma quiver_restr_wf_valid sQ Q :
  quiver_wf Q ->
  quiver_restr_wf sQ Q ->
  quiver_wf (quiver_restr sQ Q).
Proof.
  move: sQ=> [sV sA] Q_wf /andP[] sV_wf sA_wf.
  apply quiver_restr_V_wf_valid ; done.
Qed.

Lemma quiver_restr_full Q :
  quiver_wf Q ->
  quiver_restr (subquiver_full Q) Q = Q.
Proof.
  move: Q => [n A]/= /allP Q_wf ; congr quiver_Build.
  - rewrite size_iota ; done.
  - rewrite/nths map_nth_iota0// take_size -[A in RHS]map_id.
    apply eq_in_map=> [] [u v] /Q_wf/= /andP[] ??.
    rewrite !iota_index0 ; done.
Qed.

Lemma quiver_restr_VA_C sV sA Q :
  quiver_restr_A_wf sA Q ->
  quiver_restr_A sA (quiver_restr_V sV Q) = quiver_restr_V sV (quiver_restr_A sA Q).
Proof.
  move: Q => [n A] /allP sA_lt ; congr quiver_Build.
  rewrite /nths -map_comp ; apply eq_in_map=> i /sA_lt ? /=.
  rewrite -[A in LHS]zip_unzip -zip_map nth_zip ?size_map// !(nth_map (0,0))
    ?size_zip ?size_map ?minnn// !zip_unzip ; done.
Qed.

Lemma index_in_map (T U : eqType) (f : T -> U) x s :
  {in s &, injective f} -> x \in s -> index (f x) (map f s) = index x s.
Proof.
  elim: s=> // y s Hr f_inj x_in/=.
  move: (f_inj y x) ; rewrite in_cons eqxx x_in=> /(_ isT isT).
  case: eqP=> [-> /(_ (erefl _)) ->|] ; first by rewrite eqxx.
  case: eqP x_in=> [->//|].
  rewrite in_cons eq_sym=> /eqP/negP ; case: eqP=>//= _ _ x_in _ _.
  congr S ; apply Hr=> // ????.
  apply f_inj ; rewrite in_cons ; apply/orP ; right ; done.
Qed.

Lemma quiver_restr_VV sV1 sV2 Q :
  quiver_restr_V_wf sV2 Q ->
  quiver_restr_V_wf sV1 (quiver_restr_V sV2 Q) ->
  quiver_restr_V sV1 (quiver_restr_V sV2 Q) = quiver_restr_V (nths 0 sV2 sV1) Q.
Proof.
  move: Q => [n A] /andP[] /andP[] /uniqP sV2_uq /allP sV2_lt /allP sV2_wf
    /andP[] /andP[] sV1_uq /allP sV1_lt /allP.
  set f := (X in map X)=> sV1_wf.
  congr quiver_Build ; first by rewrite size_map.
  rewrite -map_comp -eq_in_map=> a /[dup] /sV2_wf /andP[] a1_in a2_in.
  move=> /(map_f f) /sV1_wf/= /andP[] ia1_in_sV1 ia2_in_sV1.
  rewrite -{2}(nth_index 0 a1_in) -{2}(nth_index 0 a2_in).
  rewrite !index_in_map=> // x y /sV1_lt? /sV1_lt ; apply sV2_uq ; done.
Qed.

Lemma quiver_restr_AV_wf sA sV Q :
  quiver_restr_A_wf sA (quiver_restr_V sV Q) = quiver_restr_A_wf sA Q.
Proof.
  move: Q=> [n A]/= ; rewrite size_map ; done.
Qed.

Lemma quiver_restr_VA_wf sV sA Q :
  quiver_restr_A_wf sA Q ->
  quiver_restr_V_wf sV Q -> quiver_restr_V_wf sV (quiver_restr_A sA Q).
Proof.
  move: Q=> [n A]/= /allP sA_wf /andP[]/andP[] -> ->/= /allP sV_wf.
  apply/allP=> a /mapP[] i /sA_wf /(mem_nth (0,0)) /sV_wf + -> ; done.
Qed.

Lemma quiver_restr_AA sA1 sA2 Q :
  quiver_restr_A_wf sA1 (quiver_restr_A sA2 Q) ->
  quiver_restr_A sA1 (quiver_restr_A sA2 Q) = quiver_restr_A (nths 0 sA2 sA1) Q.
Proof.
  move: Q=> [n A]/= ; rewrite size_map => sA1_wf.
  congr quiver_Build ; rewrite (nths_map 0) ; done.
Qed.

Lemma quiver_restr_QQ_wf sQ1 sQ2 Q :
  quiver_restr_wf sQ1 (quiver_restr sQ2 Q) ->
  quiver_restr_wf sQ2 Q ->
  quiver_restr_wf (subquiver_restr sQ1 sQ2) Q.
Proof.
  move: sQ1 sQ2 Q=> [sV1 sA1] [sV2 sA2] [n A].
  move=> /andP[] /allP ; rewrite !size_map=> sA1_lt /andP[] /andP[] sV1_uq /allP sV1_lt /allP sV1_wf.
  move=> /andP[] /allP sA2_lt /andP[] /andP[] /uniqP sV2_uq /allP sV2_lt /allP sV2_wf.
  apply/andP ; split ; last do!(apply/andP ; split).
  - apply/allP=> i /mapP[] j /sA1_lt /(mem_nth 0) /sA2_lt + -> ; done.
  - rewrite map_inj_in_uniq// => i j /sV1_lt i_lt /sV1_lt j_lt.
    apply sV2_uq ; done.
  - apply/allP=> i /mapP[] j /sV1_lt /(mem_nth 0) /sV2_lt + -> ; done.
  - set f := (X in map X) in sV1_wf ; set s := (X in mem X) in sV1_wf.
    apply/allP=> a /mapP[] i /mapP[] j j_in i_eq a_eq ; have /sV1_wf : f a \in s ; rewrite/s/f.
      apply/mapP ; exists j=> // ; rewrite (nth_map (0,0)) ?a_eq ?i_eq ?(nth_map 0)// ?size_map ; apply sA1_lt ; done.
    have : (a.1 \in sV2) && (a.2 \in sV2).
      apply sV2_wf ; rewrite a_eq ; apply map_f ; rewrite i_eq mem_nth// ; apply sA1_lt ; done.
    move=> /andP[] a1_in a2_in.
    rewrite/= -{2}[a.1](nth_index 0 (s := sV2)) -1?{2}[a.2](nth_index 0 (s := sV2))//.
    move=> /andP[] ia1_in ia2_in ; apply/andP ; split ; apply map_f ; done.
Qed.

Lemma quiver_restr_QQ sQ1 sQ2 Q :
  quiver_restr_wf sQ2 Q ->
  quiver_restr_wf sQ1 (quiver_restr sQ2 Q) ->
  quiver_restr sQ1 (quiver_restr sQ2 Q) = quiver_restr (subquiver_restr sQ1 sQ2) Q.
Proof.
  move: sQ1 sQ2=> [sV1 sA1] [sV2 sA2]/= /andP[] ++ /andP[].
  rewrite quiver_restr_AV_wf=> ????.
  rewrite/quiver_restr quiver_restr_VA_C// quiver_restr_VV ; first by rewrite quiver_restr_AA.
  - rewrite quiver_restr_VA_wf ; done.
  - rewrite -quiver_restr_VA_C ; done.
Qed.


Lemma path_restr_A sA Q u p v :
  quiver_restr_A_wf sA Q ->
  path (quiver_restr_A sA Q) u (map (index^~ sA) p) v <->
    path_sub Q sA u p v.
Proof.
  move: Q=> [n A] /allP sA_wf/=.
  rewrite/path_sub/path size_map/=.
  set a := all _ _ ; have -> : a = all (mem sA) p ; rewrite{}/a.
    rewrite all_map ; apply eq_all=> i/= ; apply index_mem ; done.
  case p_in: (all _ _) ; rewrite /= andbC//=.
  move/allP: p_in=> p_in.
  have ->/= : all (gtn (size A)) p.
    apply/allP=> i /p_in/sA_wf ; done.
  set a := map _ _ ; set b := map _ _ ; have -> : a = b ; rewrite{}/a{}/b//.
  rewrite -map_comp -eq_in_map=> i i_in/=.
  rewrite (nth_map 0) ; last by rewrite index_mem ; apply p_in.
  rewrite nth_index// ; last by apply p_in.
Qed.

Lemma path_in A u p v :
  path A u p v -> (p == [::]) \/
  (exists i j,
    (i \in p) /\ (j \in p) /\
    (u == (nth (0,0) A i).1) /\
    (v == (nth (0,0) A j).2)).
Proof.
  case: p=> [|i p] ; first by left.
  case: (lastP p) => [|{}p j] /= h ; right.
  - exists i ; exists i.
    move: h ; rewrite/path/= in_cons => /andP[]/andP[] i_lt _ /eqP[] -> <-.
    rewrite !eqxx ; done.
  - move: h ; rewrite/path/= all_rcons/= => /andP[]/andP[] i_lt /andP[] j_lt _ /eqP [].
    rewrite -!map_cons -rcons_cons !map_rcons=> -> /rcons_inj [] _ <-.
    exists i ; exists j.
    rewrite !mem_rcons !in_cons !eqxx/= orbC ; done.
Qed.

Lemma inj_in_index (T : eqType) s (i j : T) :
  i \in s -> j \in s ->
  index i s == index j s = (i == j).
Proof.
  move=> * ; case: (@eqP _ i) ; last apply contra_notF.
  - move=> ->/= ; rewrite eqxx ; done.
  - move=> /eqP /(congr1 (nth i s)) ; rewrite !nth_index// => -> ; done.
Qed.

Lemma path_restr_V sV Q u p v :
  quiver_restr_V_wf sV Q ->
  u \in sV -> v \in sV ->
  path (quiver_restr_V sV Q) (index u sV) p (index v sV) <->
    path Q u p v.
Proof.
  move: Q=> [n A]/= /andP[] /andP[] sV_uq sV_lt.
  rewrite all_predI=> /andP[] /allP sV_wf1 /allP sV_wf2 u_in v_in.
  elim: p u u_in=> [|i p Hr] u u_in.
    rewrite !path0 inj_in_index ; done.
  rewrite !cons_path !size_map.
  case i_lt: (_ < _)=> //=.
  rewrite -{3}zip_map/= nth_zip/= ?size_map// !(nth_map (0,0))//=.
  set a := (_ == _) ; set b := (_ == _) ; have -> : a = b ; rewrite{}/a{}/b.
    rewrite inj_in_index//.
    apply sV_wf1 ; apply mem_nth ; done.
  case u_eq: (_ == _)=> //=.
  apply Hr ; apply sV_wf2 ; apply mem_nth ; done.
Qed.

End quiver_Theory.



Section diagram.

Record equiv_prel (T : Type) : Type :=
  EquivPRelPack { pequiv :> T -> T -> Prop;  _ : Relation_Definitions.equivalence _ pequiv}.

Record diagram_package (diagram : Type) := diagram_Pack {
  diagram_to_quiver : diagram -> quiver;
  diagram_restr : subquiver -> diagram -> diagram;
  eqD : equiv_prel diagram;
  eq_comp : diagram -> path_relation;
}.

Structure diagram_type := diagram_type_Build {
  diagram_sort :> Type;
  diagram_to_package :> diagram_package diagram_sort;
}.

End diagram.

Notation base := diagram_to_quiver.


Definition diagram_restr_arc (diagram : diagram_type) sA (D : diagram) :=
  let sQ := subquiver_from_arc (base diagram D) sA in
  diagram_restr diagram sQ D.

Definition commute (diagram : diagram_type) (D : diagram) :=
  path_total (base diagram D) (eq_comp diagram D).


  Definition predicate_coq (diagram : diagram_type) (lQ : seq quiver) :=
  foldr (fun _ p => diagram -> p) Prop lQ.

Notation "'funD' D1 .. Dn '::' diagram 'on' lQ => P" :=
  ((fun D1 => ( .. (fun Dn => P) .. )) : predicate_coq diagram lQ)
  (at level 200, D1 binder, Dn binder, lQ at level 10, right associativity)
  : fanl_coq_scope.

Notation "'forallD' D :: diagram 'on' Q , P" :=
  (forall D, base diagram D = Q -> P)
  (at level 200, D name, Q at level 200, right associativity)
  : fanl_coq_scope.

Notation "'existsD' D :: diagram 'on' Q , P" := (
  exists D, base diagram D = Q /\ P)
  (at level 200, D name, Q at level 200, right associativity)
  : fanl_coq_scope.


Section compatible_diagram.

Record compatible_diagram_package (diagram : diagram_type) :=
    compatible_diagram_Pack {
  eqD_eq_base : forall (D1 D2 : diagram), eqD diagram D1 D2 -> base diagram D1 = base diagram D2;
  eqD_restr : forall sQ D1 D2, eqD diagram D1 D2 ->
    eqD diagram (diagram_restr diagram sQ D1) (diagram_restr diagram sQ D2);
  eqD_comp : forall (D1 D2 : diagram), eqD diagram D1 D2 ->
    forall u p q v, eq_comp diagram D1 u v p q -> eq_comp diagram D2 u v p q;
  base_restr : forall sQ D,
    base diagram (diagram_restr diagram sQ D) = quiver_restr sQ (base diagram D);
  restr_restr : forall sQ1 sQ2 D,
    eqD diagram (diagram_restr diagram sQ1 (diagram_restr diagram sQ2 D)) (diagram_restr diagram (subquiver_restr sQ1 sQ2) D);
  restr_comp : forall D sV sA u p q v, (
    let D' := diagram_restr diagram (subquiver_Build sV sA) D in
    path (base diagram D') u p v -> path (base diagram D') u q v ->
    eq_comp diagram D (nth 0 sV u) (nth 0 sV v) (map (nth 0 sA) p) (map (nth 0 sA) q)
      <-> eq_comp diagram D' u v p q);
  eqD_full : forall (D : diagram),
    eqD diagram (diagram_restr diagram (subquiver_full (base diagram D)) D) D;
}.

Record compatible_diagram_mixin (diagram : Type) := compatible_diagram_Mix {
  compatible_diagram_to_diagram_ :> diagram_package diagram;
  compatible_diagram_to_package :>
    compatible_diagram_package (diagram_type_Build compatible_diagram_to_diagram_)
}.

Structure compatible_diagram_type := compatible_diagram_type_Build {
  compatible_diagram_sort :> Type;
  compatible_diagram_to_mixin :>
    compatible_diagram_mixin compatible_diagram_sort;
}.

End compatible_diagram.

Coercion compatible_diagram_to_diagram (diagram : compatible_diagram_type) :=
  diagram_type_Build diagram.
Canonical compatible_diagram_to_diagram.



Section category_diagram.

Open Scope fanl_coq_scope.

Definition pushforward diagram sQ1 sQ2 Q1 Q2 Q :=
  funD D1 D2 D :: diagram on [::Q1;Q2;Q] =>
  eqD diagram (diagram_restr diagram sQ1 D) D1 /\
  eqD diagram (diagram_restr diagram sQ2 D) D2.

Record category_diagram_package (diagram : diagram_type) := category_diagram_Pack {
  pushforward_ex :
    forall sQ1 sQ2 Q1 Q2 Q, quiver_pushforward_wf sQ1 sQ2 Q1 Q2 Q ->
    forallD D1 :: diagram on Q1, forallD D2 :: diagram on Q2,
    eqD diagram (diagram_restr diagram (subquiver_preinter sQ2 sQ1) D1) (diagram_restr diagram (subquiver_preinter2 sQ1 sQ2) D2) ->
    existsD D :: diagram on Q, pushforward sQ1 sQ2 Q1 Q2 Q D1 D2 D;
  pushforward_uq :
    forall sQ1 sQ2 Q1 Q2 Q, quiver_pushforward_wf sQ1 sQ2 Q1 Q2 Q ->
    forallD D1 :: diagram on Q1, forallD D2 :: diagram on Q2,
    forallD D :: diagram on Q, forallD D' :: diagram on Q,
    pushforward sQ1 sQ2 Q1 Q2 Q D1 D2 D ->
    pushforward sQ1 sQ2 Q1 Q2 Q D1 D2 D' ->
    eqD diagram D D';
  comp :
    forallD D :: diagram on {Q bimapQ},
    existsD D' :: diagram on {Q compQ},
    eqD diagram (diagram_restr diagram {sQ [:: 0;2] <o compQ} D') D /\
    commute D';
}.

Record category_diagram_mixin (diagram : Type) := category_diagram_Mix {
  category_diagram_to_diagram_ :> diagram_package diagram;
  category_diagram_to_compatible_ :>
    compatible_diagram_package (diagram_type_Build category_diagram_to_diagram_);
  category_diagram_to_package :>
    category_diagram_package (diagram_type_Build category_diagram_to_diagram_);
}.

Structure category_diagram_type := category_diagram_type_Build {
  category_diagram_sort : Type;
  category_diagram_to_mixin :> category_diagram_mixin category_diagram_sort;
}.

Close Scope fanl_coq_scope.

End category_diagram.

Coercion category_diagram_to_compatible (diagram : category_diagram_type) :=
  compatible_diagram_type_Build (compatible_diagram_Mix diagram).
Canonical category_diagram_to_compatible.
Coercion category_diagram_to_diagram (diagram : category_diagram_type) :=
  diagram_type_Build diagram.
Canonical category_diagram_to_diagram.



Section fanl.

Inductive term :=
  | Var of nat
  | Restr of subquiver & term.

Inductive formula :=
  | Forall of quiver & formula
  | Exists of quiver & formula
  | Imply of formula & formula
  | And of formula & formula
  | FTrue
  | Commute of term
  | EqD of term & term.

Inductive predicate :=
  | Lambda of seq quiver & formula.


(* Endowing the above types with the expected eq/choice/count structures.
 we do not use gentree for equality as some equality tests will be
 computed in practice at some point. *)

Fixpoint term_eq (t1 t2 : term) :=
  match t1, t2 with
  | Var n1, Var n2 => n1 == n2
  | Restr sQ1 t1, Restr sQ2 t2 =>
      (sQ1 == sQ2) && (term_eq t1 t2)
  | _, _ => false
  end.

Lemma term_eqP : Equality.axiom term_eq.
Proof.
  move=> t1 t2; apply: (iffP idP); last first.
    by move->; elim: t2 => // n /=; rewrite ?eqxx.
  elim: t1 t2 => [n1 | pg1 t1 iht] [n2 | pg2 t2] //=.
  - by move/eqP->.
  - by case/andP=> /eqP-> /iht->.
Qed.

Canonical term_eqMixin := EqMixin term_eqP.
Canonical term_eqType := Eval hnf in EqType term term_eqMixin.

(* CANONICAL DECL WARNING, we are inside a section and a module... *)

(* count/choice structures for term, via an encoding to GenTree.tree,
    see the sources of choice.v *)
Fixpoint tree_of_term (t : term) : GenTree.tree nat :=
  match t with
  | Var n => GenTree.Leaf n
  | Restr sQ t => GenTree.Node (pickle sQ) [:: tree_of_term t]
  end.

Fixpoint term_of_tree (t : GenTree.tree nat) : option term :=
  match t with
  | GenTree.Leaf n => Some (Var n)
  | GenTree.Node k l =>
      if unpickle k is Some n then
        if l is hd :: _ then
          if term_of_tree hd is Some u then Some (Restr n u)
          else None
        else None
      else None
  end.

Lemma tree_of_term_term_pK : pcancel tree_of_term term_of_tree.
Proof. by elim=> [] // p t iht /=; rewrite pickleK iht. Qed.

Definition term_choiceMixin := PcanChoiceMixin tree_of_term_term_pK.
Canonical term_choiceType :=
  Eval hnf in ChoiceType term term_choiceMixin.

Definition term_countMixin := PcanCountMixin tree_of_term_term_pK.
Canonical term_countType :=
  Eval hnf in CountType term term_countMixin.


Fixpoint formula_eq f1 f2 :=
  match f1, f2 with
  | Forall Q1 f1, Forall Q2 f2
  | Exists Q1 f1, Exists Q2 f2 => (Q1 == Q2) && formula_eq f1 f2
  | Imply f11 f12, Imply f21 f22
  | And f11 f12, And f21 f22 => (formula_eq f11 f21) && (formula_eq f12 f22)
  | FTrue, FTrue => true
  | Commute t1, Commute t2 => t1 == t2
  | EqD t11 t12, EqD t21 t22 => (t11 == t21) && (t12 == t22)
  | _, _ => false
  end.

Lemma formula_eqP : Equality.axiom formula_eq.
Proof.
  move=> f1 f2; apply: (iffP idP) ; last by
    move-> ; elim: f2=> //= ? ; rewrite ?eq_refl//= => ->.
  elim: f1 f2.
  1,2: move=> ?? H []//= ?? /andP[] /eqP-> /H-> ; done.
  1,2: move=> ? H1 ? H2 []//= ?? /andP[] /H1-> /H2-> ; done.
  - by case.
  - move=> ? [] //= ? /eqP -> ; done.
  - move=> ?? [] //= ?? /andP[] /eqP-> /eqP-> ; done.
Qed.

Canonical formula_eqMixin := EqMixin formula_eqP.
Canonical formula_eqType := Eval hnf in EqType formula formula_eqMixin.



Fixpoint tree_of_formula (f : formula) : GenTree.tree term :=
  match f with
  | Forall Q f => GenTree.Node (pickle (0, Q)) [:: tree_of_formula f]
  | Exists Q f => GenTree.Node (pickle (1, Q)) [:: tree_of_formula f]
  | Imply f1 f2 =>
    GenTree.Node (pickle (2, quiver_Empty))
      [:: tree_of_formula f1 ; tree_of_formula f2]
  | And f1 f2 =>
    GenTree.Node (pickle (3, quiver_Empty))
      [:: tree_of_formula f1 ; tree_of_formula f2]
  | FTrue => GenTree.Node (pickle (4, quiver_Empty)) [::]
  | Commute t => GenTree.Node (pickle (5, quiver_Empty)) [:: GenTree.Leaf t]
  | EqD t1 t2 => GenTree.Node (pickle (6, quiver_Empty)) [:: GenTree.Leaf t1 ; GenTree.Leaf t2]
  end.

Fixpoint formula_of_tree (tr : GenTree.tree term) : option formula :=
  match tr with
  | GenTree.Node n l =>
    match unpickle n, l with
    | Some (0, Q), [:: tr] => option_map (Forall Q) (formula_of_tree tr)
    | Some (1, Q), [:: tr] => option_map (Exists Q) (formula_of_tree tr)
    | Some (2, Q), [:: tr1 ; tr2] => option_map2 Imply (formula_of_tree tr1) (formula_of_tree tr2)
    | Some (3, Q), [:: tr1 ; tr2] => option_map2 And (formula_of_tree tr1) (formula_of_tree tr2)
    | Some (4, Q), [::] => Some FTrue
    | Some (5, Q), [:: GenTree.Leaf t] => Some (Commute t)
    | Some (6, Q), [:: GenTree.Leaf t1 ; GenTree.Leaf t2] => Some (EqD t1 t2)
    | _, _ => None
    end
  | GenTree.Leaf _ => None
  end.

(* count/choice structures for formula, via an encoding to GenTree.tree,
    see the sources of choice.v *)

Lemma tree_of_formula_formula_pK : pcancel tree_of_formula formula_of_tree.
Proof.
  elim=> // [Q f|Q f|f1 /= -> f2|f1 /= -> f2]/= -> // ; rewrite pickleK ; done.
Qed.

Definition formula_choiceMixin := PcanChoiceMixin tree_of_formula_formula_pK.
Canonical formula_choiceType :=
  Eval hnf in ChoiceType formula formula_choiceMixin.

Definition formula_countMixin := PcanCountMixin tree_of_formula_formula_pK.
Canonical formula_countType :=
  Eval hnf in CountType formula formula_countMixin.

(* End of structure-related boilerplate *)


(* Create a formula with empty vertices for `subquivers`,
  and the following functions will compute the vertices part for you *)
Fixpoint term_get_quiver (lQ : seq quiver) (t : term) :=
  match t with
  | Var n => nth quiver_Empty lQ n
  | Restr _ t' => term_get_quiver lQ t'
  end.

Fixpoint term_fill_vertices (Q : quiver) (t : term) :=
  match t with
  | Restr sA t =>
    let sQ := (subquiver_from_arc Q sA) in
    Restr sQ (term_fill_vertices (quiver_restr sQ Q) t)
  | _ => t
  end.

Fixpoint formula_fill_vertices (lQ : seq quiver) (f : formula) :=
  match f with
  | Forall Q f' => Forall Q (formula_fill_vertices (Q :: lQ) f')
  | Exists Q f' => Exists Q (formula_fill_vertices (Q :: lQ) f')
  | Imply f1 f2 =>
    Imply (formula_fill_vertices lQ f1) (formula_fill_vertices lQ f2)
  | And f1 f2 => And (formula_fill_vertices lQ f1) (formula_fill_vertices lQ f2)
  | FTrue => FTrue
  | Commute t => Commute (term_fill_vertices (term_get_quiver lQ t) t)
  | EqD t1 t2 => EqD
    (term_fill_vertices (term_get_quiver lQ t1) t1)
    (term_fill_vertices (term_get_quiver lQ t2) t2)
  end.

Definition Lambda_arc lQ f :=
  Lambda lQ (formula_fill_vertices lQ f).



(* Substitution varia *)

Fixpoint term_shift_depth depth (t : term) :=
  match t with
  | Var n => Var (depth + n)
  | Restr Q t' => Restr Q (term_shift_depth depth t')
  end.

Fixpoint term_assign_var t0 depth t :=
  match t with
  | Var n =>
    if n == depth then
      term_shift_depth depth t0
    else if n > depth then
      Var n.-1
    else
      Var n
  | Restr Q t' =>
    Restr Q (term_assign_var t0 depth t')
  end.

Fixpoint assign_Vk t0 k f :=
  match f with
  | Forall Q f' => Forall Q (assign_Vk t0 k.+1 f')
  | Exists Q f' => Exists Q (assign_Vk t0 k.+1 f')
  | Imply f1 f2 => Imply (assign_Vk t0 k f1) (assign_Vk t0 k f2)
  | And f1 f2 => And (assign_Vk t0 k f1) (assign_Vk t0 k f2)
  | FTrue => FTrue
  | Commute t => Commute (term_assign_var t0 k t)
  | EqD t1 t2 => EqD (term_assign_var t0 k t1) (term_assign_var t0 k t2)
  end.

Definition predicate_assign t0 '(Lambda stack f) :=
  Lambda (behead stack) (assign_Vk t0 0 f).

Definition predicate_to_formula '(Lambda stack f) := f.

End fanl.


Notation "P /\ Q" := (And P Q) : fanl_scope.
Notation "P -=> Q" := (Imply P Q) (at level 99, right associativity)
  : fanl_scope.
Notation "$ n" := (Var n) (at level 2) : fanl_scope.
Notation "p 'App' a1 .. an" :=
  (predicate_to_formula (predicate_assign a1 .. (predicate_assign an p) .. ))
  (at level 10, a1, an at level 9)
  : fanl_scope.



Section fanl.

Fixpoint term_obase (stack : seq quiver) t : option quiver :=
  match t with
  | Var n => onth stack n
  | Restr sQ t' =>
    if term_obase stack t' is Some Q then
      if quiver_restr_wf sQ Q then
        Some (quiver_restr sQ Q)
      else
        None
    else
      None
  end.

Definition term_wf : _ -> _ -> bool := term_obase.

Fixpoint formula_wf (stack : seq quiver) f :=
  match f with
  | Forall Q f'
  | Exists Q f' => quiver_wf Q && formula_wf (Q :: stack) f'
  | Imply f1 f2
  | And f1 f2 => formula_wf stack f1 && formula_wf stack f2
  | FTrue => true
  | Commute t' => term_wf stack t'
  | EqD t1 t2 =>
    match term_obase stack t1, term_obase stack t2 with
    | Some Q1, Some Q2 => Q1 == Q2
    | _, _ => false
    end
  end.

Definition predicate_wf '(Lambda stack f) :=
  formula_wf stack f.



Section eval.

Implicit Type diagram : diagram_type.

Open Scope fanl_coq_scope.

Fixpoint term_oeval diagram (stack : seq diagram) (t : term) :=
  match t with
  | Var n => onth stack n
  | Restr sQ t' =>
    if term_oeval stack t' is Some D then
      Some (diagram_restr diagram sQ D)
    else
      None
  end.

Fixpoint formula_eval diagram stack f : Prop :=
  match f with
  | Forall Q f => forallD D :: diagram on Q, formula_eval diagram (D :: stack) f
  | Exists Q f => existsD D :: diagram on Q, formula_eval diagram (D :: stack) f
  | Imply f1 f2 => formula_eval diagram stack f1 -> formula_eval diagram stack f2
  | And f1 f2 => formula_eval diagram stack f1 /\ formula_eval diagram stack f2
  | FTrue => True
  | Commute t => if term_oeval stack t is Some D then commute D else False
  | EqD t1 t2 =>
    match @term_oeval diagram stack t1, @term_oeval diagram stack t2 with
    | Some DG1, Some DG2 => eqD diagram DG1 DG2
    | _, _ => False
    end
  end.



Fixpoint formula_dual f :=
  match f with
  | Forall Q f => Forall (quiver_dual Q) (formula_dual f)
  | Exists Q f => Exists (quiver_dual Q) (formula_dual f)
  | Imply f1 f2 => Imply (formula_dual f1) (formula_dual f2)
  | And f1 f2 => And (formula_dual f1) (formula_dual f2)
  | _ => f
  end.

Definition predicate_dual '(Lambda lQ f) :=
  Lambda (map quiver_dual lQ) (formula_dual f).

Close Scope fanl_coq_scope.

End eval.

End fanl.





Section Proofs.

Variable (diagram : diagram_type).

Notation base := (diagram_to_quiver diagram).
Notation diagram_restr := (diagram_restr diagram).

Implicit Type (lD : seq diagram).


Inductive sequent := sequent_Build {
  π_context : seq quiver;
  π_premises : seq formula;
  π_goal : formula;
}.

Notation Seq := sequent_Build.


(* Boilerplate for Structures *)
Definition triple_of_sequent '(Seq lQ pr g) := (lQ, pr, g).

Definition sequent_of_triple '(lQ, pr, g) := Seq lQ pr g.

Lemma triple_of_sequentK : cancel triple_of_sequent sequent_of_triple.
Proof. by case. Qed.

Definition sequent_eqMixin := CanEqMixin triple_of_sequentK.
Canonical sequent_eqType :=
  Eval hnf in EqType sequent sequent_eqMixin.

Definition sequent_choiceMixin := CanChoiceMixin triple_of_sequentK.
Canonical sequent_choiceType :=
  Eval hnf in ChoiceType sequent sequent_choiceMixin.

Definition sequent_countMixin := CanCountMixin triple_of_sequentK.
Canonical sequent_countType :=
  Eval hnf in CountType sequent sequent_countMixin.


(* True sequent: no quantifier, empty premises *)
Definition True_sequent := Seq [::] [::] FTrue.

Definition sequent_wf '(Seq lQ pr g) :=
  all quiver_wf lQ &&
  all (formula_wf lQ) pr &&
  formula_wf lQ g.

Definition eval_sequent '(Seq lQ pr g) :=
  forall lD, map base lD = lQ ->
  {in pr, forall f, formula_eval lD f} ->
  formula_eval lD g.

(* Dual of a sequent: dualize all formulas *)
Definition sequent_dual '(Seq lQ pr g) :=
  Seq (map quiver_dual lQ) (map formula_dual pr) (formula_dual g).


Definition formula_to_sequent f :=
  Seq [::] [::] f.


Definition tactic := sequent -> option sequent.

Definition proof := seq tactic.


Fixpoint osequent_eval_proof (os : option sequent) (pf : proof) :=
  match os, pf with
  | Some s, tac :: pf' => osequent_eval_proof (tac s) pf'
  | _, [::] => os
  | _, _ => None
  end.

Definition formula_eval_proof (f : formula) (pf : proof) :=
  osequent_eval_proof (Some (formula_to_sequent f)) pf.

Definition osequent_check_proof (os : option sequent) (pf : proof) :=
  osequent_eval_proof os pf == Some True_sequent.

Definition check_proof (f : formula) (pf : proof) :=
  osequent_check_proof (Some (formula_to_sequent f)) pf.


(* Nice way to do proofs *)
Definition demo f :=
  {pf & check_proof f pf}.

Definition cons_demo os (tac : tactic)
  (pf_ok : {pf & osequent_check_proof (if os is Some s then tac s else None) pf})
  (tac_wf : if os is Some s then tac s else None) :
  {pf & osequent_check_proof os pf}.
Proof.
  exists (tac :: (projT1 pf_ok)).
  case: os tac_wf pf_ok=> // s ? [] ; done.
Defined.


Definition tactic_wfP (tac : tactic) :=
  forall s s', sequent_wf s -> tac s = Some s' ->
  sequent_wf s'.

Definition tactic_validP (tac : tactic) :=
  forall s s', sequent_wf s -> tac s = Some s' ->
  (eval_sequent s' -> eval_sequent s).

Definition bitactic_DP (tac dtac : tactic) :=
  forall s,
    dtac (sequent_dual s) =
    option_map sequent_dual (tac s).

Structure valid_tactic := valid_tactic_Build {
  valid_tactic_to_tactic : tactic;
  valid_tactic_wf : tactic_wfP valid_tactic_to_tactic;
  valid_tactic_valid : tactic_validP valid_tactic_to_tactic;
}.

Structure bitactic := bitactic_Build {
  bitactic_primal : tactic;
  bitactic_dual : tactic;
}.

Structure valid_proof := valid_proof_Build {
  valid_proof_to_proof : proof;
  valid_proof_wf : All tactic_wfP valid_proof_to_proof;
  valid_proof_valid : All tactic_validP valid_proof_to_proof;
}.

Structure biproof := biproof_Build {
  biproof_primal : proof;
  biproof_dual : proof;
  biproof_eq_size : size biproof_primal == size biproof_dual;
}.

Definition biproofD '(biproof_Build pf dpf eq_size) :=
  All (fun '(tac, dtac) => bitactic_DP tac dtac) (zip pf dpf).


Arguments valid_tactic_wf (_) [_ _] (_ _).
Arguments valid_tactic_valid (_) [_ _] (_ _).

Coercion valid_tactic_to_tactic : valid_tactic >-> tactic.
Coercion valid_proof_to_proof : valid_proof >-> proof.

Canonical cons_valid_proof vtac vpf :=
  @valid_proof_Build
    (valid_tactic_to_tactic vtac :: valid_proof_to_proof vpf)
    (conj (valid_tactic_wf vtac) (valid_proof_wf vpf))
    (conj (valid_tactic_valid vtac) (valid_proof_valid vpf)).

Canonical empty_valid_proof :=
  @valid_proof_Build [::] I I.

Lemma valid_proof_ind P :
  P empty_valid_proof -> (forall vtac vpf, P vpf -> P (cons_valid_proof vtac vpf)) -> forall vpf, P vpf.
Proof.
  move=> epf H [] ; elim=> [|tac pf H' [] tac_wf pf_wf [] tac_v pf_v].
  - move: epf ; rewrite/empty_valid_proof.
    move=> + [] [] ; done.
  - pose vtac := valid_tactic_Build tac_wf tac_v.
    pose vpf := valid_proof_Build pf_wf pf_v.
    apply: (H vtac vpf) ; apply H' ; done.
Qed.


Canonical cons_biproof (btac : bitactic) (bpf : biproof) : biproof :=
  @biproof_Build
    (bitactic_primal btac :: biproof_primal bpf)
    (bitactic_dual btac :: biproof_dual bpf)
    (biproof_eq_size bpf).

Canonical empty_biproof :=
  @biproof_Build [::] [::] (eq_refl 0).

Lemma biproof_ind P :
  P empty_biproof -> (forall btac bpf, P bpf -> P (cons_biproof btac bpf)) -> forall bpf, P bpf.
Proof.
  move=> epf H [pf dpf].
  elim: dpf pf=> [[]// eq_size | dtac dpf H' []// tac pf eq_size].
  - move: epf ; rewrite/empty_biproof.
    by have -> : (eqxx 0) = eq_size by apply bool_irrelevance.
  - pose btac := bitactic_Build tac dtac.
    pose bpf := @biproof_Build pf dpf eq_size.
    apply: (H btac bpf) ; apply H' ; done.
Qed.



Section Theory.

Lemma formula_to_sequent_wf f :
  sequent_wf (formula_to_sequent f) <-> formula_wf [::] f.
Proof. by []. Qed.

Lemma formula_to_sequent_valid f :
  formula_wf [::] f
  -> eval_sequent (formula_to_sequent f)
  -> @formula_eval diagram [::] f.
Proof. by move=> f_wf /(_ [::]) s_v ; apply s_v. Qed.


Lemma osequent_check_proof_valid s (vpf : valid_proof) :
  sequent_wf s -> osequent_check_proof (Some s) vpf -> eval_sequent s.
Proof.
  rewrite/osequent_check_proof.
  elim: vpf s=> [|vtac vpf H] s s_wf/= ; first by move=> /eqP[] ->.
  case eq_s': ((vtac : tactic) s)=> [s'|]; last by case: (vpf : proof) {H}=> /=.
  move=> eq_True ; apply: (valid_tactic_valid vtac _ eq_s')=> //.
  apply H=> //.
  apply: (valid_tactic_wf vtac _ eq_s') ; done.
Qed.


Theorem check_proof_valid f (pf : valid_proof) :
  formula_wf [::] f -> check_proof f pf -> @formula_eval diagram [::] f.
Proof.
  move=> f_wf pf_ok.
  apply formula_to_sequent_valid=> //.
  apply: (osequent_check_proof_valid _ pf_ok).
  rewrite formula_to_sequent_wf ; done.
Qed.


Lemma quiver_dual_inv : involutive quiver_dual.
Proof.
  move=> [V A]/= ; congr quiver_Build=> /=.
  elim: A=> // [] [] [] * ; congr cons ; done.
Qed.

Definition quiver_dual_inj := inv_inj quiver_dual_inv.

Lemma size_quiver_dual q : size (quiver_dual q) = size q.
Proof. by case: q=> * //=; rewrite size_map. Qed.


Lemma formula_dual_inv : involutive formula_dual.
Proof.
  elim=> //=.
  1,2: move=> Q f -> ; rewrite quiver_dual_inv ; done.
  1,2: move=> f1 -> f2 -> ; done.
Qed.

Definition formula_dual_inj := inj_eq (inv_inj formula_dual_inv).

Lemma sequent_dual_inv : involutive sequent_dual.
Proof.
  move=> [lQ pr g]/= ; congr (Seq _ _ _) ; last by apply formula_dual_inv.
  - elim: lQ=> //= Q lQ -> ; rewrite quiver_dual_inv ; done.
  - elim: pr=> //= f pr -> ; rewrite formula_dual_inv ; done.
Qed.


Lemma quiver_wfD g :
  quiver_wf (quiver_dual g) = quiver_wf g.
Proof.
  move: g=> [n A]/=.
  elim: A=> // [[]] ??? H /=.
  rewrite [X in X && _]andbC H ; done.
Qed.

Arguments quiver_wf : simpl never.

Lemma quiver_restrD sQ Q :
  quiver_restr sQ (quiver_dual Q) = quiver_dual (quiver_restr sQ Q).
Proof.
  move: sQ Q=> [sV sA] [n A]/= ; congr quiver_Build.
  elim: sA=> //= i sA -> ; congr ((_,_) :: _) ;
  rewrite -{2}[A]zip_unzip -zip_map !nth_zip ?size_map ; done.
Qed.

Arguments quiver_restr : simpl never.

Lemma quiver_restr_wfD sQ Q :
  quiver_restr_wf sQ (quiver_dual Q) = quiver_restr_wf sQ Q.
Proof.
  move: sQ Q=> [sV sA] [n A]/= ; congr andb ; first by rewrite size_map.
  congr andb.
  rewrite !all_map ; apply eq_in_all=> i i_in/=.
  rewrite -{3 4}[A]zip_unzip -zip_map !nth_zip ?size_map//= andbC.
  congr andb ; done.
Qed.

Arguments quiver_restr_wf : simpl never.

Lemma term_obaseD lQ t :
  term_obase (map quiver_dual lQ) t = option_map quiver_dual (term_obase lQ t).
Proof.
  elim: t=> /= [n|sQ t ->].
  - case: (ltnP n (size lQ))=> ? ; last rewrite/onth !nth_default ?size_map //.
    rewrite/onth !(nth_map quiver_Empty) ?size_map ; done.
  - case: term_obase=> // Q /= ; rewrite quiver_restr_wfD.
    case: quiver_restr_wf=> //= ; rewrite quiver_restrD ; done.
Qed.

Lemma term_wfD lQ t :
  term_wf (map quiver_dual lQ) t = term_wf lQ t.
Proof.
  rewrite/term_wf term_obaseD ; case: term_obase ; done.
Qed.

Lemma formula_wfD lQ f :
  formula_wf (map quiver_dual lQ) (formula_dual f) = formula_wf lQ f.
Proof.
  elim: f lQ => //=.
  1,2: move=> Q f H lQ ; rewrite -H quiver_wfD ; done.
  1,2: move=> f1 H1 f2 H2 lQ ; rewrite H1 H2 ; done.
  - move=> * ; apply term_wfD ; done.
  - move=> t1 t2 lQ ; rewrite !term_obaseD.
    case: term_obase ; case: term_obase=> //= G1 G2.
    apply (inj_eq quiver_dual_inj) ; done.
Qed.

Lemma sequent_wfD s :
  sequent_wf (sequent_dual s) = sequent_wf s.
Proof.
  move: s => [lQ pr g] ; rewrite/sequent_wf/=.
  rewrite !all_map formula_wfD ; congr (_ && _ && _) ;
  set h := preim _ _ ; rewrite (@eq_all _ _ h)//= /h => f/=.
  - rewrite quiver_wfD ; done.
  - rewrite -formula_wfD ; done.
Qed.

Lemma osequent_eval_proofD os (bpf : biproof) :
  biproofD bpf ->
  osequent_eval_proof (option_map sequent_dual os) (biproof_dual bpf) =
  option_map sequent_dual (osequent_eval_proof os (biproof_primal bpf)).
Proof.
  elim: bpf os=> /= [[]//|] [] tac dtac [pf pfd eq_size] H [s|]//= []tacD ?.
  rewrite tacD H// ; apply bpfD ; done.
Qed.

Lemma osequent_check_proofD os (bpf : biproof) :
  biproofD bpf ->
  osequent_check_proof (option_map sequent_dual os) (biproof_dual bpf) =
  osequent_check_proof os (biproof_primal bpf).
Proof.
  move=> bpfD ; rewrite/osequent_check_proof osequent_eval_proofD//.
  case: osequent_eval_proof=> //= [] [[] [] []]; done.
Qed.


Theorem duality_theorem f (bpf : biproof) :
  biproofD bpf ->
  check_proof (formula_dual f) (biproof_dual bpf) =
  check_proof f (biproof_primal bpf).
Proof.
  move=> bpfD ; rewrite/check_proof -osequent_check_proofD ; done.
Qed.



(* Some premilinary lemmae to help proving tactics validity and duality *)

(* Premilinary lemmae about wf *)
Lemma term_obase_higher_depth lQ lQ' t :
  term_wf lQ t -> term_obase (lQ ++ lQ') t = term_obase lQ t.
Proof.
  rewrite/term_wf.
  elim: t=> [n | sQ t] /=.
  - rewrite/onth onth_size map_cat nth_cat size_map=> -> ; done.
  - case: term_obase=> // Q /(_ isT) -> ; done.
Qed.

Lemma term_higher_depth_wf lQ lQ' t :
  term_wf lQ t -> term_wf (lQ ++ lQ') t.
Proof. rewrite/term_wf=> /[dup]/term_obase_higher_depth -> ; done. Qed.

Lemma formula_higher_depth_wf lQ lQ' f :
  formula_wf lQ f -> formula_wf (lQ ++ lQ') f.
Proof.
  elim: f lQ => //=.
  1,2: move=> Q f H lQ ; rewrite -cat_cons=> /andP[] ->/= /H ; done.
  1,2: move=> f1 H1 f2 H2 lQ /andP[] /H1 -> /H2 ; done.
  - move=> t lQ /term_higher_depth_wf ; done.
  - move=> t1 t2 lQ.
    case eq_Q1: term_obase=> [Q1|//].
    case eq_Q2: term_obase=> [Q2|//].
    rewrite !term_obase_higher_depth /term_wf ?eq_Q1 ?eq_Q2 ; done.
Qed.

Lemma term_shift_depth_obase lQ' lQ t:
  term_obase (lQ' ++ lQ) (term_shift_depth (size lQ') t) = term_obase lQ t.
Proof.
  elim: t=> /= [n | sQ t -> //].
  rewrite/onth map_cat nth_cat size_map.
  rewrite -{2 5}[size lQ']addn0 ltn_add2l/= subnDl subn0 ; done.
Qed.

Lemma term_shift_depth_wf lQ' lQ t :
  term_wf lQ t = term_wf (lQ' ++ lQ) (term_shift_depth (size lQ') t).
Proof. rewrite/term_wf term_shift_depth_obase ; done. Qed.

Lemma term_assign_var_obase lQ0 t0 Q0 lQ t :
  term_obase lQ0 t0 = Some Q0 ->
  term_obase (lQ ++ lQ0) (term_assign_var t0 (size lQ) t)
    = term_obase (lQ ++ Q0 :: lQ0) t.
Proof.
  move=> eq_Q0.
  elim: t=> /= [n | sQ t -> //].
  case: (ltngtP n (size lQ))=> /= n_lng ; rewrite/onth.
  - rewrite !map_cat !nth_cat !size_map n_lng ; done.
  - rewrite -cat_rcons !map_cat !nth_cat !size_map size_rcons ltnS.
    case: n n_lng=> //= n /ltn_geF -> ; rewrite subSS ; done.
  - rewrite term_shift_depth_obase map_cat nth_cat size_map n_lng ltnn subnn ; done.
Qed.

Lemma term_assign_var_wf lQ0 t0 Q0 lQ t :
  term_obase lQ0 t0 = Some Q0 ->
  term_wf (lQ ++ lQ0) (term_assign_var t0 (size lQ) t) =
  term_wf (lQ ++ Q0 :: lQ0) t.
Proof. move=> ? ; rewrite/term_wf (term_assign_var_obase (Q0 := Q0)) ; done. Qed.

Lemma assign_Vk_wf lQ0 t0 Q0 lQ f :
  term_obase lQ0 t0 = Some Q0 ->
  formula_wf (lQ ++ lQ0) (assign_Vk t0 (size lQ) f) =
  formula_wf (lQ ++ Q0 :: lQ0) f.
Proof.
  rewrite/assign_Vk=> eq_Q0.
  elim: f lQ=> //=.
  1,2: move=> Q f H lQ ; rewrite -cat_cons H ; done.
  1,2: move=> f1 H1 f2 H2 lQ ; congr andb ; rewrite ?H1 ?H2 ; done.
  - move=> * ; apply: term_assign_var_wf ; done.
  - move=> * ; rewrite !(term_assign_var_obase _ _ eq_Q0) ; done.
Qed.


(* Premilinary lemmae about valid *)
Lemma term_higher_depth_valid lD lD0 t :
  term_wf (map base lD) t ->
  term_oeval (lD ++ lD0) t = term_oeval lD t.
Proof.
  rewrite/term_wf.
  elim: t=> /= [n|Q t H t_wf].
  - rewrite onth_size /onth map_cat nth_cat !size_map => -> ; done.
  - rewrite H// ; case: term_obase t_wf ; done.
Qed.

Lemma formula_higher_depth_valid lD lD0 f :
  formula_wf (map base lD) f ->
  formula_eval (lD ++ lD0) f <-> formula_eval lD f.
Proof.
  elim: f lD=> //=.
  1,2: move=> Q f H lD /andP[] Q_wf f_wf ; split.
    1,2: move=> + D ; move/(_ D).
    3,4: move=> [] D h ; exists D ; move: h.
    1,2: move=> + eq_Q ; move/(_ eq_Q).
    3,4: move=> [] eq_Q.
    1-4: rewrite -cat_cons (H (D :: lD))//= eq_Q ; done.
  1,2: move=> f1 H1 f2 H2 lD /= /andP[] /(H1 lD) -> /(H2 lD) -> ; done.
  - move=> t lD /= /term_higher_depth_valid -> ; done.
  - move=> t1 t2 lD.
    case t1_wf: term_obase=> // ; case t2_wf: term_obase=> //.
    rewrite !term_higher_depth_valid /term_wf ?t1_wf ?t2_wf ; done.
Qed.

Lemma term_shift_depth_valid lD0 lD t :
  @term_oeval diagram (lD0 ++ lD) (term_shift_depth (size lD0) t) = term_oeval lD t.
Proof.
  elim: t=> /= [n| Q t -> //].
  rewrite/onth map_cat nth_cat size_map.
  rewrite -{2 5}[size lD0]addn0 ltn_add2l/= subnDl subn0 ; done.
Qed.

Lemma term_assign_var_valid t0 D0 lD lD0 t :
  @term_oeval diagram lD0 t0 = Some D0 ->
  term_oeval (lD ++ lD0) (term_assign_var t0 (size lD) t) =
    term_oeval (lD ++ D0 :: lD0) t.
Proof.
  move=> eq_D0.
  elim: t=> /= [n | sQ t -> //].
  case: (ltngtP n (size lD))=> /= n_lng ; rewrite/onth.
  - rewrite !map_cat !nth_cat !size_map n_lng ; done.
  - rewrite -cat_rcons !map_cat !nth_cat !size_map size_rcons ltnS.
    case: n n_lng=> //= n /ltn_geF -> ; rewrite subSS ; done.
  - rewrite term_shift_depth_valid map_cat nth_cat size_map n_lng ltnn subnn ; done.
Qed.

Lemma assign_Vk_valid lD0 t0 D0 lD f :
  @term_oeval diagram lD0 t0 = Some D0 ->
  formula_eval (lD ++ lD0) (assign_Vk t0 (size lD) f)
    <-> formula_eval (lD ++ D0 :: lD0) f.
Proof.
  rewrite/assign_Vk=> eq_D0.
  elim: f lD=> //=.
  1,2: move=> Q f H lD ; split.
    1,2: move=> + D ; move/(_ D).
    3,4: move=> [] D h ; exists D ; move: h.
    1-4: rewrite -cat_cons ; rewrite (H (D :: lD)) ; done.
  1,2: move=> f1 H1 f2 H2 lD ; rewrite H1 // H2 ; done.
  - move=> t lD /= ; rewrite (term_assign_var_valid (D0 := D0)) ; done.
  - move=> t1 t2 lD /= ; rewrite !(term_assign_var_valid (D0 := D0)) ; done.
Qed.


(* Premilinary lemmae about duality *)
Lemma assign_VkD t0 k f : formula_dual (assign_Vk t0 k f) = assign_Vk t0 k (formula_dual f).
Proof.
  rewrite/assign_Vk.
  elim: f k=> //=.
  1,2: move=> Q f H k ; rewrite H ; done.
  1,2: move=> f1 H1 f2 H2 k ; rewrite H1 H2 ; done.
Qed.

Lemma dual_FTrue : FTrue = formula_dual FTrue.
Proof. by []. Qed.

End Theory.

End Proofs.

Notation Seq := sequent_Build.
Arguments cons_demo [_] (_).


(* Some LTac Tactic to help proving formulas (cf. the example of `mono_monom`)*)

(* Tactics to do proofs checking at each step that tactics are valid *)
Tactic Notation "fanl" constr(t) := (apply (@cons_demo _ t)=> //=).
Tactic Notation "fanl_done" := (exists [::]=> //).
(* When computations becomes to long, replace `fanl` by `fanlu`, you get the
   start of the proof. Then computes the sequent you obtain from this partial
   proof. Then continue from this sequent using `fanl`. *)
Tactic Notation "fanlu" constr(t) := (refine (cons t _)).
Tactic Notation "fanlu_done" := (apply [::] ; done).

(* Tactics to automoatically compute the `valid_proof` and the `biproof`
   associated to some `proof` *)
Definition validify_aux diagram (vpf : valid_proof diagram)
  := (vpf, Phantom _ (valid_proof_to_proof vpf)).

Tactic Notation "validify" constr(t) :=
  (apply (@fst _ (phantom _ t)) ; apply: validify_aux ; done).

Definition dualify_aux (bpf : biproof)
  := (bpf, Phantom _ (biproof_primal bpf)).

Tactic Notation "dualify" constr(t) := ( let dpf := fresh "dpf" in
  have@ : proof ; first (
    apply biproof_dual ; apply (@fst _ (phantom _ t)) ; apply: dualify_aux) ; move=> dpf ; validify dpf).


Section Basic_tactics.

Variable (diagram : diagram_type).

Notation base := (@base diagram).

Implicit Type (lD : seq diagram).



Fixpoint NewTac (lTac : seq tactic) (s : sequent) : option sequent :=
  match lTac with
  | nil => Some s
  | tac :: lTac' => option_omap (NewTac lTac') (tac s)
  end.

Lemma NewTac_wf (lTac : seq tactic) :
  (All tactic_wfP lTac) -> tactic_wfP (NewTac lTac).
Proof.
  elim: lTac => /=.
  * by move=> [] s s' s_wf [] <-.
  * move=> tac lTac IH [tac_wf lTac_wf] s s' s_wf.
    case ss'' : (tac s) => [s''|] // /=.
    apply: IH => //.
    exact: (tac_wf _ _ _ ss'').
Qed.

Lemma NewTac_valid (lTac : seq tactic) :
  (All tactic_wfP lTac) -> (All (tactic_validP diagram) lTac) -> tactic_validP diagram (NewTac lTac).
Proof.
  elim: lTac => /=.
  * by move=> [] [] s s' s_wf [] <-.
  * move=> tac lTac IH [tac_wf lTac_wf] [tac_valid lTac_valid] s s' s_wf.
    case ss'' : (tac s) => [s''|] // /= s''s' s'_eval.
    apply: (tac_valid _ _ _ ss'') => //.
    apply: (IH _ _ _ _ _ s''s') => //.
    exact: (tac_wf _ _ _ ss'').
Qed.

Canonical NewTac_v vpf := valid_tactic_Build
  (@NewTac_wf (valid_proof_to_proof vpf) (valid_proof_wf vpf))
  (@NewTac_valid (valid_proof_to_proof vpf) (valid_proof_wf vpf) (valid_proof_valid vpf)).

Lemma NewTac_D (lTac : seq tactic) (blTac : seq tactic) (Hsize : size lTac == size blTac):
  (All (fun '(tac, btac) => bitactic_DP tac btac) (zip lTac blTac)) -> bitactic_DP (NewTac lTac) (NewTac blTac).
Proof.
  elim: lTac blTac Hsize => [|tac lTac' IH] /=.
  * case => //.
  * elim => [//|btac blTac' bIH] /= Hsize [bi_tac_btac bi_lTac_blTac] s.
    rewrite eqSS in Hsize.
    rewrite bi_tac_btac.
    case (tac s) => [s'|] // /=.
    exact: IH.
Qed.

Canonical Newtac_b bpf := bitactic_Build (NewTac (biproof_primal bpf)) (NewTac (biproof_dual bpf)).



Definition Intro '(Seq lQ pr g) :=
  match g with
  | Forall Q f =>
    Some (Seq (rcons lQ Q) pr (assign_Vk (Var (size lQ)) 0 f))
  | Imply f1 f2 => Some (Seq lQ (rcons pr f1) f2)
  | _ => None
  end.

Lemma Intro_wf : tactic_wfP Intro.
Proof.
  move=> [lQ pr g] s' s_wf s_eq.
  case: g s_wf s_eq=> // [Q f | f1 f2] + /= [] <-.
  - (* Forall *)
    move=> /andP[] /andP[] lQ_wf /allP pr_wf /andP[] Q_wf f_wf.
    do!(apply/andP ; split).
    * rewrite all_rcons Q_wf ; done.
    * apply/allP=> p /pr_wf.
      rewrite -cats1 ; apply formula_higher_depth_wf ; done.
    * rewrite -[rcons _ _]cat0s ; rewrite (assign_Vk_wf (Q0 := Q))//= ;
        last by apply onth_rcons.
      rewrite -rcons_cons -cats1 ; apply formula_higher_depth_wf ; done.
  - (* Imply *)
    rewrite/sequent_wf all_rcons /= => /andP[] /andP[] -> -> /andP[] -> ; done.
Qed.

Lemma Intro_valid : tactic_validP diagram Intro.
  move=> [lQ pr g] s' s_wf s_eq.
  case: g s_wf s_eq=> // [Q f | f1 f2] + /= [] <-.
  - (* Forall *)
    move=> /andP[] /andP[] lQ_wf /allP pr_wf /= /andP[] Q_wf f_wf.
    move=> + lD eq_lQ pr_v D eq_Q.
    move/(_ (rcons lD D)) ; rewrite map_rcons eq_Q eq_lQ=> /(_ (erefl _)) H.
    rewrite -(formula_higher_depth_valid [::D])/= ?eq_Q ?eq_lQ//.
    rewrite -[_ :: _]cat0s -(assign_Vk_valid _ (t0 := Var (size lQ))) /= cats1.
    + apply H=> p /[dup] ? /pr_v ; rewrite -cats1.
      apply formula_higher_depth_valid ; rewrite eq_lQ.
      apply pr_wf ; done.
    + rewrite onth_rcons// -eq_lQ !size_map ; done.
  - (* Imply *)
    move=> /andP[] pr_wf/= /andP[] f1_wf f2_wf.
    move=> + lD size_eq pr_v f1_v.
    move/(_ lD size_eq)=> f2_v ; apply f2_v=> p.
    rewrite mem_rcons in_cons => /orP[/eqP -> // | /pr_v] ; done.
Qed.

Canonical Intro_v := valid_tactic_Build Intro_wf Intro_valid.

Lemma Intro_D : bitactic_DP Intro Intro.
Proof.
  move=> [lQ pr []]//= Q f;
  rewrite map_rcons => //.
  congr (Some (Seq _ _ _)).
  rewrite assign_VkD size_map ; done.
Qed.

Canonical Intro_b := bitactic_Build Intro Intro.



Definition Exact k '(Seq lQ pr g) :=
  if g == nth FTrue pr k then
    Some True_sequent
  else
    None.

Lemma Exact_wf k : tactic_wfP (Exact k).
Proof.
  move=> [lQ pr g] s' s_wf /= ; case: (_ == _)=> // [[]] <- ; done.
Qed.

Lemma Exact_valid k : tactic_validP diagram (Exact k).
Proof.
  move=> [lQ pr g] s' s_wf /= ; case: (@idP (_ == _))=> // /eqP -> [] <- _.
  rewrite/eval_sequent=> /= ? _ pr_v.
  case: (ltnP k (size pr)).
  - move=> /(mem_nth FTrue) /pr_v ; done.
  - move=> /(nth_default FTrue)-> ; done.
Qed.

Canonical Exact_v k := valid_tactic_Build (@Exact_wf k) (@Exact_valid k).

Lemma Exact_D k : bitactic_DP (Exact k) (Exact k).
Proof.
  move=> [lQ pr g]/=.
  rewrite {1}dual_FTrue nth_map_default formula_dual_inj.
  case: (_ == _) ; done.
Qed.

Canonical Exact_b k := bitactic_Build (Exact k) (Exact k).



Definition Clear k '(Seq lQ pr g) :=
  Some (Seq lQ ((take k pr) ++ (drop (k+1) pr)) g).

Lemma Clear_wf k : tactic_wfP (Clear k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf /allP pr_wf g_wf /=; case=> <- /=.
  apply/andP; split => //.
  apply/andP; split => //.
  apply/allP => p p_in.
  apply pr_wf.
  rewrite mem_cat in p_in.
  move: p_in => /orP [p_in|p_in].
  * by apply: mem_take p_in.
  * by apply: mem_drop p_in.
Qed.

Lemma Clear_valid k : tactic_validP diagram (Clear k).
Proof.
  move=> [lQ pr g] s' s_wf; case=> <- /= H lD lDlQ eval_pr.
  apply H => // f f_in.
  apply eval_pr.
  rewrite mem_cat in f_in.
  move: f_in => /orP [f_in|f_in].
  * by apply: mem_take f_in.
  * by apply: mem_drop f_in.
Qed.

Canonical Clear_v k := valid_tactic_Build (@Clear_wf k) (@Clear_valid k).

Lemma Clear_D k : bitactic_DP (Clear k) (Clear k).
Proof.
  move=> [lQ pr g] /=.
  suff e : (take k [seq formula_dual i | i <- pr] ++ drop (k + 1) [seq formula_dual i | i <- pr])
         = [seq formula_dual i | i <- take k pr ++ drop (k + 1) pr]
  by rewrite e.
  apply: (eq_from_nth (x0 := FTrue)).
  * by rewrite size_map !size_cat !size_take !size_drop !size_map.
  * move=> i Hi.
    rewrite size_cat size_take size_drop !size_map in Hi.
    rewrite nth_cat (nth_map FTrue FTrue); last by rewrite size_cat size_take size_drop.
    case ik : (i < size (take k [seq formula_dual i0 | i0 <- pr]));
    rewrite size_take size_map in ik.
    + case ik' : (i < k).
      - have ipr : i < size pr.
          case kpr : (k < size pr);
          rewrite kpr in Hi => /=;
          rewrite kpr in ik => /= //.
          exact: (ltn_trans ik).
        rewrite (nth_take _ ik') (nth_map FTrue FTrue _ ipr) nth_cat size_take.
        case kpr : (k < size pr).
        ** by rewrite ik' nth_take.
        ** by rewrite ipr nth_take.
      - case kpr : (k < size pr);
        rewrite kpr in Hi => /=;
        rewrite kpr in ik => /=;
        try by rewrite ik' in ik.
        have ki : (k <= i) by rewrite leqNgt ik'.
        by rewrite (leq_ltn_trans ki ik) in kpr.
    + rewrite nth_drop (nth_map FTrue FTrue).
      - case kpr : (k < size pr);
        rewrite kpr in Hi => /=;
        rewrite kpr in ik => /=;
        by rewrite nth_cat !size_take size_map !kpr ik nth_drop.
      - rewrite size_take size_map.
        case kpr : (k < size pr);
        rewrite kpr in Hi;
        rewrite kpr in ik;
        rewrite -ltn_subRL ltn_psubLR => //.
        1: rewrite -(ltn_add2l k).
        2: rewrite -(ltn_add2l (size pr)).
        1,2: rewrite addn0; apply: (leq_ltn_trans _ Hi); by rewrite leqNgt ik.
Qed.

Canonical Clear_b k := bitactic_Build (Clear k) (Clear k).




Definition Split k '(Seq lQ pr g) :=
  match nth FTrue pr k with
  | And f1 f2 => Some (Seq lQ (rcons (rcons pr f1) f2) g)
  | Exists Q f => Some (Seq (rcons lQ Q) (rcons pr (assign_Vk (Var (size lQ)) 0 f)) g)
  | _ => None
  end.
Lemma Split_wf k : tactic_wfP (Split k).
Proof.
  move=> [lQ pr g] s' /= /andP[] /andP[] lQ_wf /[dup] pr_wf' /allP pr_wf g_wf.
  case: (ltnP k (size pr))=> [/(mem_nth FTrue)/pr_wf|?] ; last by rewrite nth_default.
  case: nth=> // [Q f /= /andP[] Q_wf f_wf | f1 f2 /= /andP[] f1_wf f2_wf] [] <- /=;
  rewrite !all_rcons.
  + (* Exists *)
    do!(apply/andP ; split) => //.
    * rewrite -[rcons _ _]cat0s (assign_Vk_wf (Q0 := Q)) ; last by apply onth_rcons.
      rewrite cat0s -rcons_cons -cats1.
      exact: formula_higher_depth_wf.
    1,2: rewrite -cats1.
    * apply/allP=> p /pr_wf ; exact: formula_higher_depth_wf.
    * exact: formula_higher_depth_wf.
  + (* And *)
    by rewrite lQ_wf f1_wf f2_wf pr_wf'.
Qed.

Lemma Split_valid k : tactic_validP diagram (Split k).
Proof.
  move=> [lQ pr g] s' /=.
  case: (ltnP k (size pr))=> [/(mem_nth FTrue)|?] ; last by rewrite nth_default.
  case: nth=> // [Q f Exists_in /andP[] /andP[] lQ_wf /allP pr_wf g_wf | f1 f2 And_in _] [] <- /=.
  + (* Exists *)
    move=> s'_v lD /= eq_lQ pr_v.
    move: (pr_wf _ Exists_in) (pr_v _ Exists_in)=> /= /andP [] Q_wf f_wf [] D [] eq_Q f_v.
    rewrite -(formula_higher_depth_valid [::D]) ?eq_lQ// cats1.
    apply s'_v ; first by rewrite map_rcons eq_Q eq_lQ.
    move=> p/= ; rewrite mem_rcons in_cons -cats1 => /orP[/eqP-> | /[dup] /pr_wf ? /pr_v] ;
      last by apply formula_higher_depth_valid ; rewrite eq_lQ.
    rewrite -[_ ++ _]cat0s (assign_Vk_valid _ (D0 := D))/= ;
      last by rewrite cats1 ; apply onth_rcons ; rewrite -eq_lQ size_map.
    by rewrite -cat_cons formula_higher_depth_valid//= eq_Q eq_lQ.
  + (* And *)
    rewrite/eval_sequent/= => h ? ? in_pr_wf ; apply h=> //.
    move: (in_pr_wf _ And_in)=> /=[] *.
    move=> f ; rewrite mem_rcons in_cons mem_rcons in_cons => /orP[/eqP -> | /orP[/eqP -> | ]] //.
    exact: in_pr_wf.
Qed.

Canonical Split_v k := valid_tactic_Build (@Split_wf k) (@Split_valid k).

Lemma Split_D k : bitactic_DP (Split k) (Split k).
Proof.
  move=> [lQ pr g]/=.
  rewrite {1}dual_FTrue nth_map_default.
  case: nth=> //= Q f; rewrite !map_rcons => //.
  by rewrite assign_VkD size_map.
Qed.

Canonical Split_b k := bitactic_Build (Split k) (Split k).



Definition DestructAnd k '(Seq lQ pr g) :=
  match nth FTrue pr k with
  | And f1 f2 => Some (Seq lQ (rcons (rcons pr f1) f2) g)
  | _ => None
  end.

Lemma DestructAnd_wf k : tactic_wfP (DestructAnd k).
  Proof.
    move=> [lQ pr g] s' /= /andP[] /andP[] lQ_wf /[dup] pr_wf' /allP pr_wf g_wf.
    case: (ltnP k (size pr))=> [/(mem_nth FTrue)/pr_wf|?] ; last by rewrite nth_default.
    case: nth=> // f1 f2 /= /andP[] f1_wf f2_wf [] <- /=.
    by rewrite !all_rcons lQ_wf f1_wf f2_wf pr_wf'.
  Qed.

Lemma DestructAnd_valid k : tactic_validP diagram (DestructAnd k).
Proof.
  move=> [lQ pr g] s' /=.
  case: (ltnP k (size pr))=> [/(mem_nth FTrue)|?] ; last by rewrite nth_default.
  case: nth=> // f1 f2 And_in _ [] <- /=.
  rewrite/eval_sequent/= => h ? ? in_pr_wf ; apply h=> //.
  move: (in_pr_wf _ And_in)=> /=[] *.
  move=> f ; rewrite mem_rcons in_cons mem_rcons in_cons => /orP[/eqP -> | /orP[/eqP -> | ]] //.
  exact: in_pr_wf.
Qed.

Canonical DestructAnd_v k := valid_tactic_Build (@DestructAnd_wf k) (@DestructAnd_valid k).

Lemma DestructAnd_D k : bitactic_DP (DestructAnd k) (DestructAnd k).
Proof.
  move=> [lQ pr g]/=.
  rewrite {1}dual_FTrue nth_map_default.
  case: nth=> //= f1 f2.
  by rewrite !map_rcons.
Qed.

Canonical DestructAnd_b k := bitactic_Build (DestructAnd k) (DestructAnd k).



Notation DestructAndC := (fun k => NewTac [:: DestructAnd k; Clear k]).

Lemma DestructAndC_D k : bitactic_DP (DestructAndC k) (DestructAndC k).
Proof.
  apply NewTac_D => //; repeat split.
  * apply DestructAnd_D.
  * apply Clear_D.
Qed.



Definition DestructEx k '(Seq lQ pr g) :=
  match nth FTrue pr k with
  | Exists Q f => Some (Seq (rcons lQ Q) (rcons pr (assign_Vk (Var (size lQ)) 0 f)) g)
  | _ => None
  end.

Lemma DestructEx_wf k : tactic_wfP (DestructEx k).
Proof.
  move=> [lQ pr g] s' /= /andP[] /andP[] lQ_wf /[dup] pr_wf' /allP pr_wf g_wf.
  case: (ltnP k (size pr))=> [/(mem_nth FTrue)/pr_wf|?] ; last by rewrite nth_default.
  case: nth=> // Q f /= /andP[] Q_wf f_wf [] <-/=; rewrite !all_rcons.
  do!(apply/andP ; split) => //.
  * rewrite -[rcons _ _]cat0s (assign_Vk_wf (Q0 := Q)) ; last by apply onth_rcons.
    rewrite cat0s -rcons_cons -cats1.
    exact: formula_higher_depth_wf.
  1,2: rewrite -cats1.
  * apply/allP=> p /pr_wf ; exact: formula_higher_depth_wf.
  * exact: formula_higher_depth_wf.
Qed.

Lemma DestructEx_valid k : tactic_validP diagram (DestructEx k).
Proof.
  move=> [lQ pr g] s' /=.
  case: (ltnP k (size pr))=> [/(mem_nth FTrue)|?] ; last by rewrite nth_default.
  case: nth=> // Q f Exists_in /andP[] /andP[] lQ_wf /allP pr_wf g_wf [] <- /= s'_v lD /= eq_lQ pr_v.
  move: (pr_wf _ Exists_in) (pr_v _ Exists_in)=> /= /andP [] Q_wf f_wf [] D [] eq_Q f_v.
  rewrite -(formula_higher_depth_valid [::D]) ?eq_lQ// cats1.
  apply s'_v ; first by rewrite map_rcons eq_Q eq_lQ.
  move=> p/= ; rewrite mem_rcons in_cons -cats1 => /orP[/eqP-> | /[dup] /pr_wf ? /pr_v] ;
    last by apply formula_higher_depth_valid ; rewrite eq_lQ.
  rewrite -[_ ++ _]cat0s (assign_Vk_valid _ (D0 := D))/= ;
    last by rewrite cats1 ; apply onth_rcons ; rewrite -eq_lQ size_map.
  rewrite -cat_cons formula_higher_depth_valid//= eq_Q eq_lQ ; done.
Qed.

Canonical DestructEx_v k := valid_tactic_Build (@DestructEx_wf k) (@DestructEx_valid k).

Lemma DestructEx_D k : bitactic_DP (DestructEx k) (DestructEx k).
Proof.
  move=> [lQ pr g]/=.
  rewrite {1}dual_FTrue nth_map_default.
  case: nth=> //= Q f.
  by rewrite !map_rcons assign_VkD size_map.
Qed.

Canonical DestructEx_b k := bitactic_Build (DestructEx k) (DestructEx k).



Notation DestructExC k := (NewTac [:: DestructEx k; Clear k]).

Lemma DestructExC_D k : bitactic_DP (DestructExC k) (DestructExC k).
Proof.
  apply NewTac_D => //; repeat split.
  * apply DestructEx_D.
  * apply Clear_D.
Qed.



Definition Destruct k '(Seq lQ pr g as s) :=
  match nth FTrue pr k with
  | And f1 f2 => DestructAndC k s
  | Exists Q f => DestructExC k s
  | _ => None
  end.

Lemma Destruct_wf k : tactic_wfP (Destruct k).
Proof.
  move=> [lQ pr g] s' s_wf; unfold Destruct.
  case: nth=> // [Q f | f1 f2];
  exact: valid_tactic_wf.
Qed.

Lemma Destruct_valid k : tactic_validP diagram (Destruct k).
Proof.
  move=> [lQ pr g] s' s_wf; unfold Destruct.
  case: nth=> // [Q f | f1 f2];
  exact: valid_tactic_valid.
Qed.

Canonical Destruct_v k := valid_tactic_Build (@Destruct_wf k) (@Destruct_valid k).

Lemma Destruct_D k : bitactic_DP (Destruct k) (Destruct k).
Proof.
  move=> [lQ pr g]; unfold Destruct.
  rewrite DestructExC_D DestructAndC_D => /=.
  rewrite {1}dual_FTrue nth_map_default.
  by case: nth.
Qed.

Canonical Destruct_b k := bitactic_Build (Destruct k) (Destruct k).



Definition Conj k1 k2 '(Seq lQ pr g) :=
  if (k1 < size pr) && (k2 < size pr) then
    Some (Seq lQ (rcons pr (And (nth FTrue pr k1) (nth FTrue pr k2))) g)
  else
    None.

Lemma Conj_wf k1 k2 : tactic_wfP (Conj k1 k2).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf /= /[dup] pr_wf' /allP pr_wf g_wf.
  case: (@idP (_ && _))=> // /andP[] /(mem_nth FTrue)/pr_wf f1_wf /(mem_nth FTrue)/pr_wf f2_wf [] <-.
  by rewrite/sequent_wf all_rcons /= lQ_wf f1_wf f2_wf pr_wf'.
Qed.

Lemma Conj_valid k1 k2 : tactic_validP diagram (Conj k1 k2).
Proof.
  move=> [lQ pr g] s' /= _.
  case: (@idP (_ && _))=> // /andP[] /(mem_nth FTrue) f1_in /(mem_nth FTrue) f2_in [] <- /= s'_v ?? in_pr.
  apply s'_v=> // f /= ; rewrite mem_rcons in_cons=> /orP[/eqP->/=|/in_pr//].
  split ; apply in_pr ; done.
Qed.

Canonical Conj_v k1 k2 := valid_tactic_Build (@Conj_wf k1 k2) (@Conj_valid k1 k2).

Lemma ConjD k1 k2 : bitactic_DP (Conj k1 k2) (Conj k1 k2).
Proof.
  move=> [lQ pr g]/=.
  rewrite size_map ; case: andb => //=.
  by rewrite !map_rcons dual_FTrue !nth_map_default.
Qed.

Canonical Conj_b k1 k2 := bitactic_Build (Conj k1 k2) (Conj k1 k2).



Definition Exist d '(Seq lQ pr g) :=
  match g with
  | Exists Q f =>
    if onth lQ d == Some Q then
      Some (Seq lQ pr (assign_Vk (Var d) 0 f))
    else
      None
  | _ => None
  end.

Lemma Exist_wf d : tactic_wfP (Exist d).
Proof.
  move=> [lQ pr g] s' /andP[]/= pr_wf g_wf.
  case: g g_wf=> //= Q f /andP[] Q_wf f_wf.
  case: eqP=> // eq_Q [] <-.
  rewrite/sequent_wf pr_wf/=.
  rewrite -[lQ]cat0s (assign_Vk_wf (Q0 := Q)) ; done.
Qed.

Lemma Exist_valid d : tactic_validP diagram (Exist d).
Proof.
  move=> [lQ pr g] s' _ /=.
  case: g => // Q f.
  case: eqP=> // eq_Q [] <- s'_v lD /= eq_lQ pr_v.
  move: eq_Q ; rewrite -eq_lQ onth_map.
  case eq_D: onth=> [D|//]/= [] eq_Q.
  exists D ; split=> //.
  rewrite -[_ :: _]cat0s -(@assign_Vk_valid _ _ (Var d))=> //.
  apply s'_v ; done.
Qed.

Canonical Exist_v d := valid_tactic_Build (@Exist_wf d) (@Exist_valid d).

Lemma ExistD d : bitactic_DP (Exist d) (Exist d).
Proof.
  move=> [lQ pr g]/=.
  case: g=> //= Q f.
  rewrite onth_map ; case: onth=> //= Q'.
  rewrite !(inj_eq Some_inj) (inj_eq quiver_dual_inj).
  case: (_ == _)=> //.
  congr (Some (Seq _ _ _)).
  rewrite assign_VkD ; done.
Qed.

Canonical Exist_b d := bitactic_Build (Exist d) (Exist d).



Definition Apply k1 k2 '(Seq lQ pr g) :=
  match nth FTrue pr k2 with
  | Forall Q f =>
    if onth lQ k1 == Some Q then
      Some (Seq lQ (rcons pr (assign_Vk (Var k1) 0 f)) g)
    else
      None
  | Imply f1 f2 =>
    if nth FTrue pr k1 == f1 then
      Some (Seq lQ (rcons pr f2) g)
    else
      None
  | _ => None
  end.

Lemma Apply_wf k1 k2 : tactic_wfP (Apply k1 k2).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf /= /[dup] pr_wf' /allP pr_wf g_wf.
  case: (ltnP k2 (size pr))=> [/(mem_nth FTrue) /pr_wf|?] ; last by rewrite nth_default.
  case: nth=> // [Q f /= /andP[] Q_wf f_wf | f1 f2 /= /andP[] f1_wf f2_wf].
  - (* Forall *)
    case: eqP=> // eq_Q [] <-.
    rewrite/sequent_wf all_rcons /= pr_wf' g_wf andbC/= andbC/= -[lQ]cat0s.
    rewrite (assign_Vk_wf (Q0 := Q)) ?f_wf ; done.
  - (* Imply *)
    case: (_ == _)=> // [] [] <-.
    rewrite/sequent_wf all_rcons /= lQ_wf f2_wf pr_wf' ; done.
Qed.

Lemma Apply_valid k1 k2 : tactic_validP diagram (Apply k1 k2).
Proof.
  move=> [lQ pr g] s' /= _.
  case: (ltnP k2 (size pr))=> [/(mem_nth FTrue)|?] ; last by rewrite nth_default.
  case: nth=> // [Q f in_pr | f1 f2 in_pr].
  + (* Forall *)
    case: eqP => // eq_Q [] <- s'_v /= lD /= eq_lQ pr_v.
    apply s'_v=> //= p ; rewrite mem_rcons in_cons=> /orP[/eqP-> | /pr_v//].
    move: eq_Q ; rewrite -eq_lQ onth_map.
    case eq_D: onth=> [D|//]/= [] => eq_Q.
    rewrite -[lD]cat0s (assign_Vk_valid (D0 := D))//=.
    exact: pr_v _ in_pr D eq_Q.
  + (* Imply *)
    case: ifP => // /eqP eq_f1 [] <- s'_v lD /= size_eq pr_v.
    apply s'_v=> //= p ; rewrite mem_rcons in_cons=> /orP[/eqP -> | /pr_v//].
    apply: (pr_v _ in_pr) ; rewrite -/formula_eval -eq_f1.
    case: (ltnP k1 (size pr))=> [|?] ; last by rewrite nth_default.
    move/(mem_nth FTrue)/pr_v ; done.
Qed.

Canonical Apply_v k1 k2 := valid_tactic_Build (@Apply_wf k1 k2) (@Apply_valid k1 k2).

Lemma ApplyD k1 k2 : bitactic_DP (Apply k1 k2) (Apply k1 k2).
Proof.
  move=> [lQ pr g]/=.
  rewrite {1}dual_FTrue nth_map_default.
  case: nth=> //= [Q f | f1 f2].
  - (* Forall *)
    rewrite onth_map; case: onth=> //= Q'.
    rewrite !(inj_eq Some_inj) (inj_eq quiver_dual_inj).
    case: (_ == _)=> //=.
    by rewrite map_rcons assign_VkD.
  - (* Imply *)
    rewrite {1}dual_FTrue nth_map_default formula_dual_inj.
    case: (_ == _) => //=.
    by rewrite map_rcons.
Qed.

Canonical Apply_b k1 k2 := bitactic_Build (Apply k1 k2) (Apply k1 k2).



Definition ApplyL_lTac (lk : seq nat) k2 n : seq tactic :=
  (* n is the size of the stack of premisses *)
  match lk with
  | nil => nil
  | k1 :: lk' => (Apply k1 k2) :: [seq Apply t.1 t.2 | t <- zip lk' (iota n (size lk))]
  end.

Lemma ApplyL_lTac_wf lk k2 n : All tactic_wfP (ApplyL_lTac lk k2 n).
Proof.
  rewrite /All.
  case: lk => //= k1 lk'.
  split. exact: Apply_wf.
  elim: lk' n => //= k lk'' IH n.
  split. exact: Apply_wf.
  exact: IH.
Qed.

Lemma ApplyL_lTac_valid lk k2 n : All (tactic_validP diagram) (ApplyL_lTac lk k2 n).
Proof.
  rewrite /All.
  case: lk => //= k1 lk'.
  split. exact: Apply_valid.
  elim: lk' n => //= k lk'' IH n.
  split. exact: Apply_valid.
  exact: IH.
Qed.

Canonical ApplyL_lTac_v lk k2 n := valid_proof_Build (ApplyL_lTac_wf lk k2 n) (ApplyL_lTac_valid lk k2 n).

Definition ApplyL (lk : seq nat) (k2 : nat) '(Seq lQ pr g as s) :=
  NewTac (ApplyL_lTac lk k2 (size pr)) s.

Lemma ApplyL_D lk k2 : bitactic_DP (ApplyL lk k2) (ApplyL lk k2).
Proof.
  move=> [lQ pr g].
  apply: NewTac_D; rewrite size_map => //.
  case: lk => // k1 lk' /=.
  split. exact: ApplyD.
  pose n := size pr; rewrite -[size pr]/(n).
  elim: lk' n => // k lk'' IH n /=.
  split. exact: ApplyD.
  exact: IH.
Qed.

Canonical ApplyL_b lk k2 := bitactic_Build (ApplyL lk k2) (ApplyL lk k2).



Definition Have f pf '(Seq lQ pr g) :=
  if formula_wf lQ f && osequent_check_proof (Some (Seq lQ pr f)) pf then
    Some (Seq lQ (rcons pr f) g)
  else
    None.

Lemma Have_wf f pf : tactic_wfP (Have f pf).
Proof.
  move=> [lQ pr g] s' /andP[]/= pr_wf g_wf.
  case: ifP=> // /andP[] f_wf _ [] <-.
  by rewrite /sequent_wf all_rcons /= f_wf pr_wf.
Qed.

Lemma Have_valid f vpf : tactic_validP diagram (Have f (@valid_proof_to_proof diagram vpf)).
Proof.
  move=> [lQ pr g] s' /andP[]/= pr_wf g_wf.
  case: ifP=> // /andP[] f_wf /osequent_check_proof_valid.
  rewrite /sequent_wf pr_wf f_wf=> /(_ isT) sf_v [] <-.
  move=> s_v lD/= size_lD_eq pr_v ; apply s_v=> // p.
  rewrite mem_rcons in_cons=> /orP[/eqP -> | /pr_v//].
  exact: sf_v.
Qed.

Canonical Have_v f vpf :=
  valid_tactic_Build (@Have_wf f (valid_proof_to_proof vpf)) (@Have_valid f vpf).

Lemma HaveD f bpf :
  biproofD bpf ->
  bitactic_DP (Have f (biproof_primal bpf)) (Have (formula_dual f) (biproof_dual bpf)).
Proof.
  move=> bpfD [lQ pr g]/=.
  rewrite formula_wfD -osequent_check_proofD//=.
  case: ifP => //=.
  by rewrite map_rcons.
Qed.

Canonical Have_b f bpf :=
  bitactic_Build (Have f (biproof_primal bpf)) (Have (formula_dual f) (biproof_dual bpf)).



Fixpoint Intros_lTac g :=
  match g with
  | Forall Q f => Intro :: (Intros_lTac f)
  | Imply f1 f2 => Intro :: (Intros_lTac f2)
  | _ => nil
  end.

Lemma Intros_lTac_wf g : All tactic_wfP (Intros_lTac g).
Proof.
  elim: g => // /= [Q f IH|f1 IH1 f2 IH2];
  unfold All => /=; split; try apply Intro_wf.
  * apply IH.
  * apply IH2.
Qed.

Lemma Intros_lTac_valid g : All (tactic_validP diagram) (Intros_lTac g).
Proof.
  elim: g => // /= [Q f IH|f1 IH1 f2 IH2];
  unfold All => /=; split; try apply Intro_valid.
  * apply IH.
  * apply IH2.
Qed.

Canonical Intros_lTac_v g := valid_proof_Build (Intros_lTac_wf g) (Intros_lTac_valid g).

Definition Intros '(Seq lQ pr g as s) :=
  NewTac (Intros_lTac g) s.

Lemma Intros_D : bitactic_DP Intros Intros.
Proof.
  move=> [lQ pr g] /=.
  rewrite -(NewTac_D (blTac := Intros_lTac (formula_dual g))) => //.
  * elim g => //.
  * elim g => // [Q f IH | f1 IH1 f2 IH2] /=;
    unfold All => /=; split; try exact Intro_D.
    + exact IH.
    + exact IH2.
Qed.

Canonical Intros_b := bitactic_Build Intros Intros.



Fixpoint IntroAll_lTac_aux (f : formula) (k : nat) (n : nat) :=
  (* n is the size of the stack of premisses - 1 ;
     k is the index of f in the stack of premisses ;
     return lTac and the size of the stack - 1 after the tactics *)
  match f with
  | Exists Q' f' => let (lTac, n') := IntroAll_lTac_aux f' n n in
                    ((Destruct k) :: lTac, n')
  | And f1 f2 => let (lTac1, n1) := IntroAll_lTac_aux f1 n n.+1 in
                 let (lTac2, n2) := IntroAll_lTac_aux f2 n.+1 n1 in
                 ((Destruct k) :: lTac1 ++ lTac2, n2)
  | _ => (nil, n)
  end.

Lemma IntroAll_lTac_aux_wf f k n :
  forall P : Prop, P -> foldr and P [seq tactic_wfP i | i <- (IntroAll_lTac_aux f k n).1].
Proof.
  elim: f k n => // [Q f' IH | f1 IH1 f2 IH2] k n P HP /=.
  * specialize (IH n n).
    destruct (IntroAll_lTac_aux f' n n) as [lTac n'] => /=.
    split.
    exact: Destruct_wf.
    exact: IH.
  * specialize (IH1 n n.+1).
    destruct (IntroAll_lTac_aux f1 n n.+1) as [lTac1 n1] => /=.
    specialize (IH2 n.+1 n1 P HP).
    destruct (IntroAll_lTac_aux f2 n.+1 n1) as [lTac2 n2] => /=.
    split.
    exact: Destruct_wf.
    rewrite map_cat foldr_cat.
    exact: (IH1 _ IH2).
Qed.

Lemma IntroAll_lTac_aux_valid f k n :
  forall P : Prop, P -> foldr and P [seq tactic_validP diagram i | i <- (IntroAll_lTac_aux f k n).1].
Proof.
  elim: f k n => // [Q f' IH | f1 IH1 f2 IH2] k n P HP /=.
  * specialize (IH n n).
    destruct (IntroAll_lTac_aux f' n n) as [lTac n'] => /=.
    split.
    exact: Destruct_valid.
    exact: IH.
  * specialize (IH1 n n.+1).
    destruct (IntroAll_lTac_aux f1 n n.+1) as [lTac1 n1] => /=.
    specialize (IH2 n.+1 n1 P HP).
    destruct (IntroAll_lTac_aux f2 n.+1 n1) as [lTac2 n2] => /=.
    split.
    exact: Destruct_valid.
    rewrite map_cat foldr_cat.
    exact: (IH1 _ IH2).
Qed.

Canonical IntroAll_lTac_aux_v f k n := valid_proof_Build
  (IntroAll_lTac_aux_wf f k n I) (IntroAll_lTac_aux_valid f k n I).

Lemma IntroAll_lTac_aux_sizeD f k n :
  size (IntroAll_lTac_aux f k n).1 = size (IntroAll_lTac_aux (formula_dual f) k n).1
  /\ (IntroAll_lTac_aux f k n).2 = (IntroAll_lTac_aux (formula_dual f) k n).2.
Proof.
  elim: f k n => // [Q f' IH | f1 IH1 f2 IH2] k n /=.
  * specialize (IH n n); move: IH => [IHlTac IHn].
    destruct IntroAll_lTac_aux as [lTac n'] => /=.
    destruct IntroAll_lTac_aux as [dlTac dn'] => /=.
    split.
      by rewrite IHlTac.
      by simpl in IHn.
  * specialize (IH1 n n.+1); move: IH1 => [IHlTac1 IHn1].
    destruct IntroAll_lTac_aux as [lTac1 n1] => /=.
    destruct IntroAll_lTac_aux as [dlTac1 dn1] => /=.
    simpl in IHn1.
    specialize (IH2 n.+1 n1); move: IH2 => [IHlTac2 IHn2].
    destruct IntroAll_lTac_aux as [lTac2 n2] => /=.
    rewrite -IHn1; destruct IntroAll_lTac_aux as [dlTac2 dn2] => /=.
    split.
      by rewrite !size_cat IHlTac2 IHlTac1.
      by simpl in IHn2.
Qed.

Lemma IntroAll_lTac_aux_D f k n :
  forall (P : Prop), P -> foldr and P [seq (let '(tac, btac) := pat in bitactic_DP tac btac)
  | pat <- zip (IntroAll_lTac_aux f k n).1 (IntroAll_lTac_aux (formula_dual f) k n).1].
Proof.
  elim: f k n => // [Q f' IH | f1 IH1 f2 IH2] k n P HP /=.
  * specialize (IH n n P HP).
    destruct IntroAll_lTac_aux as [lTac n'] => /=.
    destruct IntroAll_lTac_aux as [dlTac dn'] => /=.
    split. exact: Destruct_D.
    exact IH.
  * specialize (IH1 n n.+1).
    case (IntroAll_lTac_aux_sizeD f1 n n.+1) => [HlTac1 Hn1].
    destruct IntroAll_lTac_aux as [lTac1 n1] => /=.
    destruct IntroAll_lTac_aux as [dlTac1 dn1] => /=.
    simpl in Hn1.
    rewrite -Hn1.
    specialize (IH2 n.+1 n1).
    destruct IntroAll_lTac_aux as [lTac2 n2] => /=.
    destruct IntroAll_lTac_aux as [dlTac2 dn2] => /=.
    split. exact: Destruct_D.
    rewrite zip_cat => //.
    rewrite map_cat foldr_cat.
    apply IH1. exact: IH2.
Qed.

Fixpoint IntroAll_lTac (g : formula) (n : nat) :=
  (* n is the size of the stack of premisses *)
  match g with
  | Forall Q f => Intro :: (IntroAll_lTac f n)
  | Imply f1 f2 => let (lTac, n') := IntroAll_lTac_aux f1 n n  in
                   Intro :: lTac ++ (IntroAll_lTac f2 n'.+1)
  | _ => nil
  end.

Lemma IntroAll_lTac_wf g n : All tactic_wfP (IntroAll_lTac g n).
Proof.
  unfold All.
  elim: g n => // [Q f IH | f1 IH1 f2 IH2] n /=.
  * split. exact: Intro_wf. exact: IH.
  * have aux_wf : forall P : Prop, P -> foldr and P [seq tactic_wfP i | i <- (IntroAll_lTac_aux f1 n n).1]
    by apply IntroAll_lTac_aux_wf.
    destruct (IntroAll_lTac_aux f1 n n) as [lTac n'] => /=.
    split.
    exact: Intro_wf.
    rewrite map_cat foldr_cat.
    exact: aux_wf.
Qed.

Lemma IntroAll_lTac_valid g n : All (tactic_validP diagram) (IntroAll_lTac g n).
Proof.
  unfold All.
  elim: g n => // [Q f IH | f1 IH1 f2 IH2] n /=.
  * split. exact: Intro_valid. exact: IH.
  * have aux_wf : forall P : Prop, P -> foldr and P [seq tactic_validP diagram i | i <- (IntroAll_lTac_aux f1 n n).1]
    by apply IntroAll_lTac_aux_valid.
    destruct (IntroAll_lTac_aux f1 n n) as [lTac n'] => /=.
    split.
    exact: Intro_valid.
    rewrite map_cat foldr_cat.
    exact: aux_wf.
Qed.

Canonical IntroAll_lTac_v (g : formula) (n : nat) := valid_proof_Build
  (IntroAll_lTac_wf g n) (IntroAll_lTac_valid g n).

Lemma IntroAll_lTac_sizeD g n :
  size (IntroAll_lTac g n) = size (IntroAll_lTac (formula_dual g) n).
Proof.
  elim: g n => // [Q f IH | f1 IH1 f2 IH2] n /=.
  * by rewrite IH.
  * case (IntroAll_lTac_aux_sizeD f1 n n) => [HlTac Hn].
    destruct IntroAll_lTac_aux as [lTac n'] => /=.
    destruct IntroAll_lTac_aux as [dlTac dn'] => /=.
    simpl in Hn.
    by rewrite !size_cat IH2 Hn HlTac.
Qed.

Definition IntroAll '(Seq lQ pr g as s) : option sequent :=
  NewTac (IntroAll_lTac g (size pr)) s.

Lemma IntroAll_wf : tactic_wfP IntroAll.
  move=> [lQ pr g] s' s_wf /=.
  apply NewTac_wf => //.
  exact: IntroAll_lTac_wf.
Qed.

Lemma IntroAll_valid : tactic_validP diagram IntroAll.
Proof.
  move=> [lQ pr g] s' s_wf.
  apply NewTac_valid => //.
  exact: IntroAll_lTac_wf.
  exact: IntroAll_lTac_valid.
Qed.

Canonical IntroAll_v := valid_tactic_Build IntroAll_wf IntroAll_valid.

Lemma IntroAll_D : bitactic_DP IntroAll IntroAll.
Proof.
  move=> [lQ pr g] /=.
  rewrite -(NewTac_D (blTac := IntroAll_lTac (formula_dual g) (size pr))) ?size_map => //.
  * elim g => // f1 IH1 f2 /eqP IH2 /=.
    case (IntroAll_lTac_aux_sizeD f1 (size pr) (size pr)) => [HlTac1 Hn1].
    case: IntroAll_lTac_aux HlTac1 Hn1 => [lTac1 n1] HlTac1 Hn1 /=.
    case: IntroAll_lTac_aux HlTac1 Hn1 => [dlTac1 dn1] HlTac1 Hn1 /=.
    simpl in Hn1.
    by rewrite !size_cat Hn1 HlTac1 IntroAll_lTac_sizeD.
  * unfold All.
    pose n := size pr.
    rewrite -[size pr]/(n).
    elim: g n => // [Q f IH | f1 IH1 f2 IH2] n /=.
    + split. exact Intro_D. exact: IH.
    + case (IntroAll_lTac_aux_sizeD f1 n n) as [HlTac Hn'].
      case lTac_n'_def : IntroAll_lTac_aux HlTac Hn' => [lTac n'] HlTac Hn' /=.
      case dlTac_dn'_def : IntroAll_lTac_aux HlTac Hn' => [dlTac dn'] HlTac Hn' /=.
      simpl in Hn'.
      split. exact Intro_D.
      rewrite zip_cat => //. rewrite map_cat Hn' foldr_cat.
      have lTac_def: lTac = (IntroAll_lTac_aux f1 n n).1 by rewrite lTac_n'_def.
      have dlTac_def: dlTac = (IntroAll_lTac_aux (formula_dual f1) n n).1 by rewrite dlTac_dn'_def.
      rewrite lTac_def dlTac_def.
      apply IntroAll_lTac_aux_D.
      exact: IH2.
Qed.

Canonical IntroAll_b := bitactic_Build IntroAll IntroAll.



Definition DestructLast '(Seq lQ pr g as s) :=
  NewTac (IntroAll_lTac_aux (last FTrue pr) (size pr).-1 (size pr).-1).1 s.

Lemma DestructLast_wf : tactic_wfP DestructLast.
  move=> [lQ pr g] s' s_wf.
  rewrite /DestructLast.
  apply NewTac_wf => //.
  exact: IntroAll_lTac_aux_wf.
Qed.

Lemma DestructLast_valid : tactic_validP diagram DestructLast.
Proof.
  move=> [lQ pr g] s' s_wf.
  rewrite /DestructLast.
  apply NewTac_valid => //.
  * exact: IntroAll_lTac_aux_wf.
  * exact: IntroAll_lTac_aux_valid.
Qed.

Canonical DestructLast_v := valid_tactic_Build DestructLast_wf DestructLast_valid.

Lemma DestructLast_D : bitactic_DP DestructLast DestructLast.
Proof.
  move=> [lQ pr g].
  apply: NewTac_D => /=;
  rewrite size_map {2}dual_FTrue last_map.
  * by rewrite (IntroAll_lTac_aux_sizeD _ _ _).1.
  * exact: IntroAll_lTac_aux_D.
Qed.

Canonical DestructLast_b := bitactic_Build DestructLast DestructLast.



Notation ApplyLD := (fun lk k2 =>
  NewTac [:: (ApplyL lk k2); DestructLast]).

Lemma ApplyLD_D lk k2 : bitactic_DP (ApplyLD lk k2) (ApplyLD lk k2).
Proof.
  apply NewTac_D => //=; repeat split.
  * exact: ApplyL_D.
  * exact: DestructLast_D.
Qed.



(* Use *)
Structure external := external_Build {
  external_to_formula : formula;
  external_to_wf : bool;
}.

Structure biexternal := biexternal_Build {
  biexternal_primal : external;
  biexternal_dual : external;
}.

Definition external_dual '(external_Build f f_wf) :=
  external_Build (formula_dual f) f_wf.

Definition external_wfP e :=
  external_to_wf e -> formula_wf [::] (external_to_formula e).

Definition external_validP e :=
  external_to_wf e -> @formula_eval diagram [::] (external_to_formula e).

Structure valid_external := valid_external_Build {
  valid_external_to_external : external;
  valid_external_wf : external_wfP valid_external_to_external;
  valid_external_valid : external_validP valid_external_to_external;
}.


Definition Use e '(Seq lQ pr g) :=
  if external_to_wf e then
    Some (Seq lQ (rcons pr (external_to_formula e)) g)
  else
    None.

Lemma Use_wf e : external_wfP e -> tactic_wfP (Use e).
Proof.
  move: e => [e_f []] => [/(_ isT)|_ []//].
  move=> e_wf [lQ pr g] ? /andP[]/= pr_wf g_wf [] <-.
  rewrite/sequent_wf all_rcons /= -[lQ]cat0s formula_higher_depth_wf// pr_wf ; done.
Qed.

Lemma Use_valid e : external_wfP e -> external_validP e -> tactic_validP diagram (Use e).
Proof.
  move: e => [e_f []] => [/(_ isT) e_wf /(_ isT) e_v|_ _ []//].
  move=> [lQ pr g] ? s_wf [] <- s'_v lD/= size_lD_eq pr_v.
  apply s'_v=> //=.
  move=> f ; rewrite mem_rcons in_cons=> /orP[/eqP->|/pr_v//].
  rewrite -[lD]cat0s formula_higher_depth_valid ; done.
Qed.

Canonical Use_v ve :=
  valid_tactic_Build
    (Use_wf (@valid_external_wf ve))
    (Use_valid (@valid_external_wf ve) (@valid_external_valid ve)).

Lemma Use_D e : bitactic_DP (Use e) (Use (external_dual e)).
Proof.
  move: e=> [e_f e_wf] [lQ pr g]/=.
  case: e_wf => //=.
  by rewrite map_rcons.
Qed.

Canonical Use_b be := bitactic_Build (Use (biexternal_primal be)) (Use (biexternal_dual be)).


Definition ApplyEL (e : external) (lk : seq nat) '(Seq lQ pr g as s) :=
  NewTac ([:: (Use e); (ApplyL lk (size pr))] ++ (nseq (size lk) (Clear (size pr)))) s.

Lemma ApplyEL_D e lk : bitactic_DP (ApplyEL e lk) (ApplyEL (external_dual e) lk).
Proof.
  move=> [lQ pr g].
  apply: NewTac_D => /=; rewrite !size_map => //; repeat split.
  * exact: Use_D.
  * exact: ApplyL_D.
  * elim: (size lk) (size pr) => // slk IH spr /=.
    split. exact: Clear_D.
    exact: IH.
Qed.

Canonical ApplyEL_b be lk := bitactic_Build (ApplyEL (biexternal_primal be) lk) (ApplyEL (biexternal_dual be) lk).


Notation ApplyELD := (fun (e : external) (lk : seq nat) =>
  NewTac [:: (ApplyEL e lk); DestructLast]).



(* Some externals *)
Open Scope fanl_scope.

Definition EqD_refl Q :=
  external_Build (Forall Q (EqD $0 $0)) (quiver_wf Q).

Lemma EqD_refl_wf Q : external_wfP (EqD_refl Q).
Proof. rewrite/external_wfP/= eqxx => -> ; done. Qed.

Lemma EqD_refl_valid Q : external_validP (EqD_refl Q).
Proof.
by move=> /=; case: diagram=> d [k j [r [h1 h2 h3]] _] /= h /= x.
Qed.

Canonical EqD_refl_v Q :=
  valid_external_Build (@EqD_refl_wf Q) (@EqD_refl_valid Q).

Canonical EqD_refl_b Q :=
  biexternal_Build (EqD_refl Q) (EqD_refl (quiver_dual Q)).


Definition EqD_sym Q :=
  external_Build (Forall Q (Forall Q(EqD $0 $1 -=> EqD $1 $0))) (quiver_wf Q).

Lemma EqD_sym_wf Q : external_wfP (EqD_sym Q).
Proof. rewrite/external_wfP/= !eqxx => -> ; done. Qed.

Lemma EqD_sym_valid Q : external_validP (EqD_sym Q).
Proof.
move=> /=; case: diagram=> d [k j [r [h1 h2 h3]] _] /= h /= x _ y _.
exact: h3.
Qed.

Canonical EqD_sym_v Q :=
  valid_external_Build (@EqD_sym_wf Q) (@EqD_sym_valid Q).

Canonical EqD_sym_b Q :=
  biexternal_Build (EqD_sym Q) (EqD_sym (quiver_dual Q)).

Definition EqD_trans Q := external_Build
  (Forall Q (Forall Q (Forall Q
    (EqD $0 $1 /\ EqD $1 $2 -=> EqD $0 $2)))) (quiver_wf Q).

Lemma EqD_trans_wf Q : external_wfP (EqD_trans Q).
Proof. rewrite/external_wfP/= !eqxx => -> ; done. Qed.

Lemma EqD_trans_valid Q : external_validP (EqD_trans Q).
Proof.
move=> /=; case: diagram=> d [k j [r [h1 h2 h3]] _] /= h /= x _ y _ z _ [].
exact: h2.
Qed.

Canonical EqD_trans_v Q :=
  valid_external_Build (@EqD_trans_wf Q) (@EqD_trans_valid Q).

Canonical EqD_trans_b Q :=
  biexternal_Build (EqD_trans Q) (EqD_trans (quiver_dual Q)).

Close Scope fanl_scope.

End Basic_tactics.





Section Compatibility_tactics.

(* Now we assume basic compatibility axioms *)
Hypothesis (diagram : compatible_diagram_type).

Notation base := (base diagram).
(* Notation diagram_restr := (diagram_restr diagram). *)
(* Notation base := (base diagram). *)

Implicit Type (D : diagram).

Lemma eqD_commute D1 D2 : eqD diagram D1 D2 -> commute D1 <-> commute D2.
Proof.
  suff h: forall D1 D2, eqD diagram D1 D2 -> commute D1 -> commute D2.
    move=> /[dup] _ e12; split; first exact: h.
    by apply: h; case: diagram D1 D2 e12=> T /= [] /= [h1 h2 [r [h3 h4 h5] _ /=]] _.
  move=> {}D1 {}D2 eq12 com1 u p1 p2 v.
  move: (eqD_eq_base diagram eq12)=> <- path1 path2.
  apply: (eqD_comp _ eq12)=> // ; first by apply compatible_diagram_to_mixin.
  apply com1 ; done.
Qed.


Lemma commute_total_sub sQ D :
  quiver_wf (base D) ->
  quiver_restr_wf sQ (base D) ->
  commute (diagram_restr diagram sQ D) <-> path_total_sub (base D) sQ (eq_comp diagram D).
Proof.
  case eq_Q: (base D)=> [n A].
  set Q := quiver_Build _ _ in eq_Q.
  move: sQ=> [sV sA]/= Q_wf /[dup] sQ_wf /andP[] sA_wf
    /[dup] sV_wf' /andP[]/andP[] sV_uq sV_lt.
  rewrite all_predI=> /andP[] /allP sV_wf1 /allP sV_wf2.
  (* set Q := quiver_Build _ _ ; set sQ := subquiver_Build _ _. *)

  split=> com u p q v /= path_p path_q.
  - case e: ((p == [::]) && (q == [::])).
      move: e=> /andP[]/eqP -> /eqP ->.
      apply Relation_Definitions.equiv_refl ; apply π_equiv ; done.
    have : u \in sV /\ v \in sV.
      move: path_p path_q e=> /andP[] /path_in + + /andP[] /path_in.
      case: eqP=> [_ _|_ []// e /allP p_in _ _ _].
      1: case: eqP=> // _ _ []// ; move: p q=> q p e /allP p_in _.
      1,2: move: e=> [] i [] j [] /p_in i_in [] /p_in j_in [] /eqP-> /eqP-> ;
      split ; [apply sV_wf1 | apply sV_wf2] ; apply map_f ; done.
    move=> [] u_in v_in.
    rewrite -[u](@nth_index _ 0 _ sV)// -[v](@nth_index _ 0 _ sV)//.
    have p_eq : forall p, path_sub A sA u p v -> map (nth 0 sA) (map (index^~ sA) p) = p.
      move=> p' /andP[] _ /allP p_in ; rewrite -map_comp.
      apply map_id_in=> i /p_in /(nth_index 0) ; done.
    rewrite -(p_eq p)// -(p_eq q)//.
    rewrite restr_comp//.
    2: apply compatible_diagram_to_mixin.
    apply com.
    1-4: rewrite base_restr ?eq_Q ; try apply compatible_diagram_to_mixin.
    1-4: rewrite path_restr_V// path_restr_A ; done.
  - rewrite -restr_comp// ; try by apply compatible_diagram_to_mixin.
    apply com ; [move: q path_q=> _ _ | move: q path_q=> {}p {}path_p] ;
    move/path_in: (path_p) (path_p)=> [/eqP->/=|].
      1,3: rewrite/path_sub all_nil !path0 andbC=> /eqP ->/= ; done.
    1,2: rewrite/path base_restr/= ?eq_Q ?size_map ; try apply compatible_diagram_to_mixin ;
    move=> [] i [] j [] i_in [] j_in [] + + /andP[] /allP p_lt _ ;
    move: i_in j_in=> /p_lt i_lt /p_lt j_lt ;
    rewrite !(nth_map (0,0)) ?size_map// ;
    rewrite !(nth_map 0)//= => /eqP u_eq /eqP v_eq ;
    have : u < size sV /\ v < size sV.
      1,3: rewrite u_eq v_eq !index_mem ; split ; [apply sV_wf1 | apply sV_wf2] ;
      apply map_f ; apply mem_nth ; done.
    all:move=> [u_lt v_lt] ;
    rewrite -[A]/(quiver_to_arc Q) ;
    rewrite -path_restr_A// -(@path_restr_V sV) ?mem_nth// ;
    rewrite !index_uniq//= ;
    move: path_p ; rewrite/path !size_map=> /andP[] _ ;
    set a := sub _ _ ; set b := sub _ _ ; have -> : a = b ; rewrite{}/a{}/b.
      1,3: congr Graph.FiniteOrientedGraph.Build ;
      rewrite -!map_comp -eq_in_map=> i' /p_lt ? /= ;
      rewrite base_restr ; try apply compatible_diagram_to_mixin ;
      rewrite eq_Q/= -zip_map nth_zip ?size_map// !(nth_map 0) ?index_mem ?mem_nth//= ;
      rewrite nth_index ?mem_nth// !(nth_map (0,0)) ?size_map// (nth_map 0) ; done.
    all: rewrite andbC=> ->/= ; apply/allP=> i' /mapP[] j' /mapP[] k /p_lt /(mem_nth 0)
      j_in -> ->/= ;
    rewrite index_mem ; done.
Qed.



(* ApplyF, ApplyT *) (* Rq : peut-être pas nécessairement avec compatible_diagram_type *)

Fixpoint term_var t :=
  match t with
  | Var n => n
  | Restr _ t' => term_var t'
  end.

Fixpoint term_restr_of d H_full t :=
  match t with
  | Var n =>
    if d == n then Some H_full else None
  | Restr H' t' =>
    if term_restr_of d H_full t' is Some H then
      Some (nths 0 H H')
    else
      None
  end.

Fixpoint term_subquiver (d : nat) (Q : quiver) (t : term) : option subquiver :=
  match t with
  | Var n =>
    if d == n then Some (subquiver_full Q) else None
  | Restr ssQ t' =>
    if term_subquiver d Q t' is Some sQ then
      Some (subquiver_restr ssQ sQ)
    else
      None
  end.

Lemma term_restr_of_subquiver d Q t :
  term_restr_of d (subquiver_full Q) t = option_map subquiver_to_arc (term_subquiver d Q t).
Proof.
  elim: t=> [n|[ssV ssA] t Hr] /= ; first by case: eqP.
  rewrite Hr ; case: term_subquiver=> //= [] [sV sA] ; done.
Qed.

Lemma term_oeval_obase lD t :
  if term_obase (map base lD) t is Some Q then
    exists D, term_oeval lD t = Some D /\ base D = Q
  else
    True.
Proof.
  elim: t=> [n|sQ t] /=.
  - rewrite onth_map ; case: onth=> //= D.
    exists D ; done.
  - case: term_obase=> //= Q [] D [] -> eq_Q.
    case: ifP=> // _.
    set D' := diagram_restr _ sQ D.
    exists D'=> /= ; split=> // ; rewrite base_restr ?eq_Q//.
    apply compatible_diagram_to_mixin ; done.
Qed.

Lemma term_subquiver_oeval (lD : seq diagram) (d : nat) (D : diagram) (t : term) (sQ : subquiver) :
  onth lD d = Some D ->
  quiver_wf (base D) ->
  term_wf (map base lD) t ->
  term_subquiver d (base D) t = Some sQ ->
  quiver_restr_wf sQ (base D) /\
  exists D',
    term_oeval lD t = Some D' /\
    eqD diagram D' (diagram_restr diagram sQ D).
Proof.
  move=> eq_D Q_wf.
  elim: t sQ=> [n|ssQ t Hr] sQ/=.
    case: eqP=> // <- _ [] <- ; split ; first by rewrite quiver_restr_full_wf.
  exists D ; rewrite eq_D ; split=> //.
  apply: Relation_Definitions.equiv_sym; last by apply eqD_full ; apply compatible_diagram_to_mixin.
  case: diagram=> T [] /= [] /= h1 h2 [r hr] _ _; exact:  hr.
  rewrite/term_wf/=.
  move: (term_oeval_obase lD t).
  case t_wf: term_obase=> [Q|//] [] D' []eq_D' eq_Q.
  case: ifP=> // ssQ_wf _.
  case: term_subquiver Hr=> // sQ'.
  rewrite /term_wf t_wf=> /(_ sQ' isT (erefl _)) [] sQ'_wf [] D'' [].
  rewrite eq_D'=> [] [] <- /[dup] D''_eq /(eqD_eq_base diagram)/=.
  rewrite base_restr ?eq_Q ; try apply compatible_diagram_to_mixin.
  move=> Q_eq [] <- ; split.
  - apply quiver_restr_QQ_wf=> // ; rewrite -Q_eq ; done.
  - exists (diagram_restr diagram ssQ D') ; split=> //.
    have h : compatible_diagram_package diagram by apply compatible_diagram_to_mixin.
    have hh := restr_restr h.
    have hhh := eqD_restr h _ D''_eq.
    case: (eqD diagram) hh hhh => r [rr rt rs] /= hh hhh.
    by apply: rt (hh _ _ _).
Qed.



Definition normal_term (lQ : seq quiver) (t : term) : option term :=
  if onth lQ (term_var t) is Some Q
  then if term_subquiver (term_var t) Q t is Some sQ
       then Some (Restr sQ (Var (term_var t)))
       else None
  else None.

Lemma normal_term_Some (lQ : seq quiver) (t : term) :
  term_wf lQ t -> exists nt, normal_term lQ t = Some nt.
Proof.
  rewrite /term_wf.
  move=> t_wf.
  rewrite /normal_term.
  case tQ : onth => [Q|].
  case tsQ : term_subquiver => [sQ|].
  * exists (Restr sQ (Var (term_var t))) => //.
  * elim: t t_wf tQ tsQ => [d|sQ t'] /=.
    + move=> _ _ /=.
      have dd : d == d by elim: d.
      by rewrite dd.
    + move=> IH.
      case t'Q' : term_obase => [Q'|//].
      case sQQ'_wf : quiver_restr_wf => // sQQ' t'Q.
      case t'sQ' : term_subquiver => // _.
      rewrite t'Q' in IH.
      have q : Some Q' by [].
      specialize (IH q t'Q t'sQ').
      move: IH => [nt] //.

      * elim: t t_wf tQ => [d|sQ t'].
    + move=> /= t_wf nNone.
      by rewrite nNone in t_wf.
    + move=> IH /=.
      case t'Q' : term_obase => [Q'|//].
      case sQQ'_wf : quiver_restr_wf => // sQQ' tNone.
      rewrite t'Q' in IH.
      have q : Some Q' by [].
      specialize (IH q tNone).
      move: IH => [nt] //.
Qed.

Lemma normal_term_oeval (lD : seq diagram) (t nt : term) (D nD D' : diagram) :
  onth lD (term_var t) = Some D' ->
  quiver_wf (base D') ->
  term_wf (map base lD) t ->
  normal_term (map base lD) t = Some nt ->
  term_oeval lD t = Some D -> term_oeval lD nt = Some nD ->
  eqD diagram D nD.
Proof.
  move=> tD' D'_wf t_wf.
  rewrite /normal_term.
  case tQ : onth => [Q|//].
  case tsQ : term_subquiver => [sQ|//] [] <- /=.
  case tD'' : onth => [D''|] tD // [] <-.
  rewrite tD'' in tD'.
  move: tD' => [] tD'.
  rewrite -tD' in D'_wf.
  rewrite onth_map tD'' /= in tQ.
  move: tQ => [] tQ.
  rewrite -tQ in tsQ.
  move: (term_subquiver_oeval tD'' D'_wf t_wf tsQ) => [sQ_wf [D2 [tD2 D2D'']]].
  move: tD2 tD => -> [] <- //.
Qed.

Fixpoint normal_formula (lQ : seq quiver) (f : formula) : option formula :=
  match f with
  | Forall Q f' => if normal_formula (Q::lQ) f' is Some nf'
                   then Some (Forall Q nf')
                   else None
  | Exists Q f' => if normal_formula (Q::lQ) f' is Some nf'
                   then Some (Exists Q nf')
                   else None
  | Imply f1 f2 => if normal_formula lQ f1 is Some nf1
                   then if normal_formula lQ f2 is Some nf2
                   then Some (Imply nf1 nf2)
                   else None else None
  | And f1 f2 => if normal_formula lQ f1 is Some nf1
                 then if normal_formula lQ f2 is Some nf2
                 then Some (And nf1 nf2)
                 else None else None
  | Commute t => if normal_term lQ t is Some nt
                 then Some (Commute nt)
                 else None
  | EqD t1 t2 => if normal_term lQ t1 is Some nt1
                 then if normal_term lQ t2 is Some nt2
                 then Some (EqD nt1 nt2)
                 else None else None
  | FTrue => Some FTrue
  end.

Lemma normal_formula_Some (lQ : seq quiver) (f : formula) :
  formula_wf lQ f -> exists nf, normal_formula lQ f = Some nf.
Proof.
  elim: f lQ => /=.
  * 1,2 : move=> Q f' IH lQ /andP[Q_wf f'_wf];
          specialize (IH _ f'_wf);
          move: IH => [nf' f'nf'].
    1 : exists (Forall Q nf').
    2 : exists (Exists Q nf').
    1,2 : rewrite f'nf' //.
  * 1,2 : move=> f1 IH1 f2 IH2 lQ /andP[f1_wf f2_wf];
          specialize (IH1 _ f1_wf);
          move: IH1 => [nf1 f1nf1];
          specialize (IH2 _ f2_wf);
          move: IH2 => [nf2 f2nf2].
    1 : exists (Imply nf1 nf2).
    2 : exists (And nf1 nf2).
    1,2 : rewrite f1nf1 f2nf2 //.
  * move=> _ _.
    by exists FTrue.
  * move=> t lQ t_wf.
    move: (normal_term_Some t_wf) => [nt tnt].
    rewrite tnt.
    by exists (Commute nt).
  * move=> t1 t2 lQ.
    case t1Q1 : term_obase => [Q1|//].
    case t2Q2 : term_obase => [Q2|//] Q1Q2.
    have t1_wf : term_wf lQ t1 by rewrite /term_wf t1Q1.
    have t2_wf : term_wf lQ t2 by rewrite /term_wf t2Q2.
    move: (normal_term_Some t1_wf) => [nt1 t1nt1].
    move: (normal_term_Some t2_wf) => [nt2 t2nt2].
    rewrite t1nt1 t2nt2.
    by exists (EqD nt1 nt2).
Qed.

Lemma normal_formula_valid lD f nf :
  all quiver_wf (map base lD) ->
  formula_wf (map base lD) f ->
  normal_formula (map base lD) f = Some nf ->
  formula_eval lD f <-> formula_eval lD nf.
Proof.
  elim: f lD nf => /=.
  * 1,2 : move=> Q f' IH lD nf lD_wf /andP[Q_wf f'_wf];
          case f'nf' : normal_formula => [nf'|//] [] <- /=; split; move=> H.
    1,2 : move=> D base_D; specialize (H D base_D).
    3,4 : move: H => [D [base_D f'_eval]]; exists D; split => //.
    1-4 : rewrite -base_D -map_cons in f'nf';
          rewrite -base_D in f'_wf.
    1,3 : apply: (IH (D::lD) _ _ _ f'nf').1 => //=.
    3,4 : apply: (IH (D::lD) _ _ _ f'nf').2 => //=.
    1-4 : rewrite lD_wf base_D Q_wf //.
  * 1,2 : move=> f1 IH1 f2 IH2 lD nf lD_wf /andP[f1_wf f2_wf];
          case f1nf1 : normal_formula => [nf1|//];
          case f2nf2 : normal_formula => [nf2|//] [] <- /=; split.
    + move=> H nf1_eval.
      exact: ((IH2 _ _ _ _ f2nf2).1 (H ((IH1 _ _ _ _ f1nf1).2 nf1_eval))).
    + move=> H f1_eval.
      exact: ((IH2 _ _ _ _ f2nf2).2 (H ((IH1 _ _ _ _ f1nf1).1 f1_eval))).
    + move=> [f1_eval f2_eval]; split.
      exact: (IH1 _ _ _ _ f1nf1).1.
      exact: (IH2 _ _ _ _ f2nf2).1.
    + move=> [nf1_eval nf2_eval]; split.
      exact: (IH1 _ _ _ _ f1nf1).2.
      exact: (IH2 _ _ _ _ f2nf2).2.
  * move=> lD nf _ _ [] <- //.
  * move=> t lD nf lD_wf t_wf.
    case tnt : normal_term => [nt|//] [] <- /=.
    case tD : term_oeval => [D|];
    case ntnD : term_oeval => [nD|] //.
    case tD' : (onth lD (term_var t)) => [D'|].
    + have Q_wf : quiver_wf (base D').
        move: lD_wf => /allP lD_wf.
        apply lD_wf.
        apply (mem_onth (n := (term_var t))).
        rewrite onth_map tD' //.
      have DnD : eqD diagram D nD
        by apply (normal_term_oeval tD' Q_wf t_wf tnt tD ntnD).
      exact: eqD_commute.
    1-2 : move: tnt;
          rewrite /normal_term;
          case tQ : onth => [Q|//];
          rewrite onth_map in tQ.
    + rewrite tD' // in tQ.
    + case tsQ : term_subquiver => [sQ|//] [] tnt.
      move: ntnD.
      rewrite -tnt /=.
      case tD' : onth => [D'|] //.
      rewrite tD' // in tQ.
    + rewrite /term_wf in t_wf.
      move: (term_oeval_obase lD t).
      case base_t : term_obase => [Q'|].
      - move=> [D' [tD' D'Q']].
        by rewrite tD in tD'.
      - by rewrite base_t in t_wf.
  * move=> t1 t2 lD nf lD_wf.
    case t1Q1 : term_obase => [Q1|//].
    case t2Q2 : term_obase => [Q2|//] Q1Q2.
    case t1nt1 : normal_term => [nt1|//].
    case t2nt2 : normal_term => [nt2|//] [] <- /=.
    move: t1nt1. rewrite /normal_term.
    case t1Q1' : onth => [Q1'|//].
    case t1sQ1 : term_subquiver => [sQ1|//] [] <- /=.
    move: t2nt2. rewrite /normal_term.
    case t2Q2' : onth => [Q2'|//].
    case t2sQ2 : term_subquiver => [sQ2|//] [] <- /=.
    move: (term_oeval_obase lD t1).
    rewrite t1Q1 => [[D1 [t1D1 D1Q1]]].
    move: (term_oeval_obase lD t2).
    rewrite t2Q2 => [[D2 [t2D2 D2Q2]]].
    rewrite t1D1 t2D2.
    move: t1Q1' t2Q2'.
    rewrite !onth_map.
    case t1D1' : onth => [D1'|//] /= [] D1'Q1'.
    case t2D2' : onth => [D2'|//] /= [] D2'Q2'.
    rewrite -D1'Q1' in t1sQ1.
    rewrite -D2'Q2' in t2sQ2.
    move: lD_wf => /allP lD_wf.
    have Q1_wf : quiver_wf (base D1').
      apply lD_wf.
      apply (mem_onth (n := (term_var t1))).
      by rewrite onth_map t1D1'.
    have Q2_wf : quiver_wf (base D2').
      apply lD_wf.
      apply (mem_onth (n := (term_var t2))).
      by rewrite onth_map t2D2'.
    have t1_wf : term_wf (map base lD) t1 by rewrite /term_wf t1Q1.
    have t2_wf : term_wf (map base lD) t2 by rewrite /term_wf t2Q2.
    move: (term_subquiver_oeval t1D1' Q1_wf t1_wf t1sQ1) => [sQ1D1' [D1'' [t1D1'' D1''D1']]].
    move: (term_subquiver_oeval t2D2' Q2_wf t2_wf t2sQ2) => [sQ2D2' [D2'' [t2D2'' D2''D2']]].
    move: t1D1''; rewrite t1D1 => [[]] ->.
    move: t2D2''; rewrite t2D2 => [[]] ->.
    move: D1''D1' D2''D2'; case:  (eqD diagram) => [r [rr rt rs]] /= D1''D1' D2''D2'.
    split.
      + move=> D1''D2''. move/rs: D1''D1' => D1''D1'.
        by apply: (rt) _ D2''D2'; apply: rt D1''D1' _.
      + move=> D1'D2'; move/rs: D2''D2' => D2''D2'.
        apply: (rt) D2''D2'.
        by apply: rt D1''D1' _.
Qed.



Definition ExactN k '(Seq lQ pr g) :=
  if normal_formula lQ g == normal_formula lQ (nth FTrue pr k) then
    Some True_sequent
  else
    None.

Lemma ExactN_wf k : tactic_wfP (ExactN k).
Proof.
  move=> [lQ pr g] s' s_wf /= ; case: (_ == _)=> // [[]] <- ; done.
Qed.

Lemma ExactN_valid k : tactic_validP diagram (ExactN k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf /allP pr_wf g_wf /=.
  case: (@idP (_ == _)) => // /eqP ngnk [] <- _ lD lDlQ pr_eval.
  move: (normal_formula_Some g_wf) ngnk.
  rewrite -lDlQ => [[ng gng] ngnk].
  rewrite (normal_formula_valid _ _ gng).
  rewrite ngnk in gng.
  rewrite -(normal_formula_valid _ _ gng).
  2,4,5 : by rewrite lDlQ.
  1,2 : case kpr : (size pr <= k).
  1,3 : rewrite nth_default => //.
  1 : apply pr_eval.
  2 : rewrite lDlQ; apply pr_wf.
  1,2 : by apply mem_nth; rewrite ltnNge kpr.
Qed.

Canonical ExactN_v k := valid_tactic_Build (@ExactN_wf k) (@ExactN_valid k).

Canonical ExactN_b k := bitactic_Build (ExactN k) (ExactN k).



Definition ApplyF (k' : nat) (k : nat) '(Seq lQ pr g) : option sequent :=
  match nth FTrue pr k with
  | Imply f1 f2 => if (onth pr k') is Some f'
                   then if (normal_formula lQ f') == (normal_formula lQ f1)
                        then Some (Seq lQ (rcons pr f2) g)
                        else None
                   else None
  | _ => None
  end.

Lemma ApplyF_wf k' k : tactic_wfP (ApplyF k' k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf pr_wf g_wf /=.
  case nth_f : nth => [||f1 f2||||] //.
  case: onth => // f'.
  case: (_ == _) => // [[]] <- /=.
  rewrite all_rcons lQ_wf pr_wf g_wf andbC /= andbC /=.
  have f_wf : formula_wf lQ (Imply f1 f2) by exact: (nth_all_bool _ pr_wf nth_f).
  move: f_wf => /= /andP [f1_wf f2_wf] //.
Qed.

Lemma ApplyF_valid k' k : tactic_validP diagram (ApplyF k' k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf pr_wf g_wf /=.
  case nth_f : nth => [||f1 f2||||] //.
  case nth_f' : onth => [f'|//].
  case f'f1 : (_ == _) => [|//] [] <- /= s'_eval lD lDlQ pr_eval.
  move: f'f1 => /eqP f'f1.
  apply s'_eval => // f.
  rewrite mem_rcons in_cons => /orP [/eqP ff2|fpr];
  last by apply pr_eval.
  have f_eval : formula_eval lD (Imply f1 f2) by exact: (nth_all_prop _ pr_eval nth_f).
  move: f_eval => /= Hf1f2.
  rewrite ff2; apply Hf1f2.
  have f_wf : formula_wf lQ (Imply f1 f2) by apply: (nth_all_bool _ pr_wf nth_f).
  move: f_wf => /= /andP[f1_wf f2_wf].
  move: lQ_wf f1_wf (normal_formula_Some f1_wf); rewrite -!lDlQ => lQ_wf f1_wf [nf1 f1nf1].
  apply (normal_formula_valid lQ_wf f1_wf f1nf1).2.
  rewrite -lDlQ f1nf1 in f'f1.
  have f'_wf : formula_wf lQ f'.
    case f'FTrue : (f' == FTrue).
    + by move: f'FTrue => /eqP ->.
    + apply: (nth_all_bool _ pr_wf (onth_nth FTrue nth_f')) => eqf'FTrue.
      by move: eqf'FTrue f'FTrue => /eqP eqf'FTrue; rewrite eqf'FTrue.
  rewrite -lDlQ in f'_wf.
  apply (normal_formula_valid lQ_wf f'_wf f'f1).1.
  apply pr_eval.
  rewrite -(onth_nth FTrue nth_f'); apply mem_nth.
  by rewrite -onth_size nth_f'.
Qed.

Canonical ApplyF_v k' k := valid_tactic_Build (@ApplyF_wf k' k) (@ApplyF_valid k' k).



Definition ApplyT (t : term) (k : nat) '(Seq lQ pr g) : option sequent :=
  if term_wf lQ t
  then match nth FTrue pr k with
        | Forall Q f => if term_obase lQ t == Some Q
                        then Some (Seq lQ (rcons pr (assign_Vk t 0 f)) g)
                        else None
        | _ => None
        end
  else None.

Lemma ApplyT_wf t k : tactic_wfP (ApplyT t k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf pr_wf g_wf /=.
  case t_wf : term_wf => //.
  case nth_f' : nth => [Q f'||||||] //.
  case tQ : (_ == Some Q) => //.
  move: tQ => /eqP tQ [] <- /=.
  rewrite all_rcons lQ_wf pr_wf g_wf andbC /= andbC /=.
  rewrite -(cat0s lQ) (assign_Vk_wf _ _ tQ) /=.
  have f_wf : formula_wf lQ (Forall Q f') by exact: (nth_all_bool _ pr_wf nth_f').
  move: f_wf => /andP[] //.
Qed.

Lemma ApplyT_valid t k : tactic_validP diagram (ApplyT t k).
Proof.
  move=> [lQ pr g] s' s_wf /=.
  case t_wf : term_wf => //.
  case nth_f' : nth => [Q f'||||||] //.
  case tQ : (_ == Some Q) => //.
  move: tQ => /eqP tQ [] <- /= eval_s' lD lDlQ pr_eval.
  apply eval_s' => // f.
  rewrite mem_rcons in_cons => /orP [/eqP ->|f_pr].
  * move: (term_oeval_obase lD t).
    rewrite lDlQ tQ => [[D] [tD DQ]].
    rewrite -(cat0s lD) (assign_Vk_valid _ _ tD) /=.
    exact: (nth_all_prop _ pr_eval nth_f').
    exact: pr_eval.
Qed.

Canonical ApplyT_v t k := valid_tactic_Build (@ApplyT_wf t k) (@ApplyT_valid t k).

Canonical ApplyT_b t k := bitactic_Build (ApplyT t k) (ApplyT t k).



Inductive appType :=
  | aT of term
  | aP of nat.

Definition Apply_appType (a : appType) (k : nat) : tactic :=
  match a with
  | aT t => ApplyT t k
  | aP k' => ApplyF k' k
  end.

Lemma Apply_appType_wf a k : tactic_wfP (Apply_appType a k).
Proof.
  case a => [t|k'] /=.
  * exact: ApplyT_wf.
  * exact: ApplyF_wf.
Qed.

Lemma Apply_appType_valid a k : tactic_validP diagram (Apply_appType a k).
Proof.
  case a => [t|k'] /=.
  * exact: ApplyT_valid.
  * exact: ApplyF_valid.
Qed.

Definition ApplyFT_lTac (l : seq appType) k n : seq tactic :=
  (* n is the size of the stack of premisses *)
  match l with
  | nil => nil
  | a :: l' => (Apply_appType a k) :: [seq Apply_appType t.1 t.2 | t <- zip l' (iota n (size l'))]
  end.

Lemma ApplyFT_lTac_wf l k n : All tactic_wfP (ApplyFT_lTac l k n).
Proof.
  rewrite /All.
  case: l => //= a l'.
  split. exact: Apply_appType_wf.
  elim: l' n => //= a' l'' IH n.
  split. exact: Apply_appType_wf.
  exact: IH.
Qed.

Lemma ApplyFT_lTac_valid l k n : All (tactic_validP diagram) (ApplyFT_lTac l k n).
Proof.
  rewrite /All.
  case: l => //= a l'.
  split. exact: Apply_appType_valid.
  elim: l' n => //= a' l'' IH n.
  split. exact: Apply_appType_valid.
  exact: IH.
Qed.

Definition ApplyFT (l : seq appType) (k : nat) '(Seq lQ pr g as s) :=
  NewTac ((ApplyFT_lTac l k (size pr)) ++ (nseq (size l).-1 (Clear (size pr)))) s.

Lemma ApplyFT_wf l k : tactic_wfP (ApplyFT l k).
Proof.
  move=> [lQ pr g] s' s_wf.
  apply NewTac_wf => //.
  rewrite All_cat; split.
  * exact: ApplyFT_lTac_wf.
  * elim: (size l).-1 => //= n H.
    split => //.
    exact: Clear_wf.
Qed.

Lemma ApplyFT_valid l k : tactic_validP diagram (ApplyFT l k).
Proof.
  move=> [lQ pr g] s' s_wf.
  apply NewTac_valid => //;
  rewrite All_cat; split.
  1 : exact: ApplyFT_lTac_wf.
  2 : exact: ApplyFT_lTac_valid.
  1,2 : elim: (size l).-1 => //= n H;
        split => //.
        * exact: Clear_wf.
        * exact: Clear_valid.
Qed.

Canonical ApplyFT_v l k := valid_tactic_Build (@ApplyFT_wf l k) (@ApplyFT_valid l k).

Canonical ApplyFT_b l k := bitactic_Build (ApplyFT l k) (ApplyFT l k).

Notation ApplyFTD := (fun (l : seq appType) (k : nat) =>
  (NewTac [:: ApplyFT l k; DestructLast])).

Definition ApplyEFT (e : external) (l : seq appType) '(Seq lQ pr g as s) :=
  NewTac ([:: (Use e); (ApplyFTD l (size pr)); (Clear (size pr))]) s.

Lemma ApplyEFT_wf e l : external_wfP e -> tactic_wfP (ApplyEFT e l).
Proof.
  move=> e_wf [lQ pr g] s' s_wf.
  apply NewTac_wf => //; repeat split.
  * exact: Use_wf.
  * apply NewTac_wf; repeat split.
    + exact: ApplyFT_wf.
    + exact: DestructLast_wf diagram.
  * exact: Clear_wf.
Qed.

Lemma ApplyEFT_valid e l : external_wfP e -> external_validP diagram e -> tactic_validP diagram (ApplyEFT e l).
Proof.
  move=> e_wf e_valid [lQ pr g] s' s_wf.
  apply NewTac_valid => //.
  1,2 : split.
  2,4 : split.
  2 : apply NewTac_wf.
  4 : apply NewTac_valid.
  2-6 : split => //.
  3,6,8 : split => //.
  exact: Use_wf.
  1,6 : exact: ApplyFT_wf.
  1,2 : exact: DestructLast_wf diagram.
  exact: DestructLast_valid.
  exact: Clear_wf.
  exact: ApplyFT_valid.
  exact: Clear_valid.
  exact: Use_valid.
Qed.

Canonical ApplyEFT_v (ve : valid_external diagram) (l : seq appType) :=
  valid_tactic_Build
    (ApplyEFT_wf (l:=l) (@valid_external_wf diagram ve))
    (ApplyEFT_valid (l:=l) (@valid_external_wf diagram ve) (@valid_external_valid diagram ve)).

Canonical ApplyEFT_b be l := bitactic_Build (ApplyEFT (biexternal_primal be) l) (ApplyEFT (biexternal_dual be) l).



Definition ExistT (t : term) '(Seq lQ pr g) :=
  match g with
  | Exists Q f =>
    if term_obase lQ t == Some Q then
      Some (Seq lQ pr (assign_Vk t 0 f))
    else
      None
  | _ => None
  end.

Lemma ExistT_wf t : tactic_wfP (ExistT t).
Proof.
  move=> [lQ pr g] s' /andP[]/= pr_wf g_wf.
  case: g g_wf => //= Q f /andP[] Q_wf f_wf.
  case: eqP => // eq_Q [] <-.
  rewrite/sequent_wf pr_wf/=.
  by rewrite -[lQ]cat0s (assign_Vk_wf (Q0 := Q)).
Qed.

Lemma ExistT_valid t : tactic_validP diagram (ExistT t).
Proof.
  move=> [lQ pr g] s' _ /=.
  case: g => // Q f.
  case: eqP => // eq_Q [] <- s'_v lD /= eq_lQ pr_v.
  move: eq_Q (term_oeval_obase lD t).
  rewrite -eq_lQ => -> [D [tD DQ]].
  exists D; split => //.
  rewrite -[_ :: _]cat0s -(@assign_Vk_valid _ _ t)=> //.
  exact: s'_v.
Qed.

Canonical ExistT_v t := valid_tactic_Build (@ExistT_wf t) (@ExistT_valid t).

Canonical ExistT_b t := bitactic_Build (ExistT t) (ExistT t).



(* Rewrite *)
Fixpoint oterm_rewrite t0 t1 t : option term :=
  if t == t0 then
    Some t1
  else match t with
    | Var _ => None
    | Restr Q t => option_map (Restr Q) (oterm_rewrite t0 t1 t)
    end.

Definition formula_rewrite_aux t0 t1 i depth t : term * nat :=
  match oterm_rewrite (term_shift_depth depth t0) (term_shift_depth depth t1) t, i with
  | Some t', 1 => (t', 0)
  | Some _, i'.+1 => (t, i')
  | _, _ => (t, i)
  end.

(* If i = 0, does nothing,
  if i = i'.+1, replace the i'-th occurence of t0 by t1 in f *)
Fixpoint formula_rewrite t0 t1 i depth f : formula * nat :=
  match f with
  | Forall Q f =>
    let fi' := formula_rewrite t0 t1 i depth.+1 f in
    (Forall Q fi'.1, fi'.2)
  | Exists Q f =>
    let fi' := formula_rewrite t0 t1 i depth.+1 f in
    (Exists Q fi'.1, fi'.2)
  | Imply f1 f2 =>
    let fi1' := formula_rewrite t0 t1 i depth f1 in
    let fi2' := formula_rewrite t0 t1 fi1'.2 depth f2 in
    (Imply fi1'.1 fi2'.1, fi2'.2)
  | And f1 f2 =>
    let fi1' := formula_rewrite t0 t1 i depth f1 in
    let fi2' := formula_rewrite t0 t1 fi1'.2 depth f2 in
    (And fi1'.1 fi2'.1, fi2'.2)
  | FTrue =>
    (FTrue, i)
  | Commute t =>
    let ti' := formula_rewrite_aux t0 t1 i depth t in
    (Commute ti'.1, ti'.2)
  | EqD t_1 t_2 =>
    let ti1' := formula_rewrite_aux t0 t1 i depth t_1 in
    let ti2' := formula_rewrite_aux t0 t1 ti1'.2 depth t_2 in
    (EqD ti1'.1 ti2'.1, ti2'.2)
  end.


Definition Rewrite k1 k2 i '(Seq lQ pr g) :=
  match nth FTrue pr k1 with
  | EqD t0 t1 =>
    let fi' := formula_rewrite t0 t1 i.+1 0 (nth FTrue pr k2) in
    if fi'.2 == 0 then
      Some (Seq lQ (rcons pr fi'.1) g)
    else
      None
  | _ => None
  end.



Lemma unfold_oterm_rewrite t0 t1 t :
  oterm_rewrite t0 t1 t =
  if t == t0 then
    Some t1
  else match t with
    | Var _ => None
    | Restr Q t2 => option_map (Restr Q) (oterm_rewrite t0 t1 t2)
    end.
Proof. by elim: t. Qed.

Lemma oterm_rewrite_obase t0 t1 lQ t t' :
  term_obase lQ t0 = term_obase lQ t1 ->
  oterm_rewrite t0 t1 t = Some t'
  -> term_obase lQ t = term_obase lQ t'.
Proof.
  move=> eq_base.
  elim: t t'=> [n|pQ t H] ;
  rewrite unfold_oterm_rewrite ; case: eqP=> //.
  1,2: move=> -> ? [] <- ; done.
  - case: oterm_rewrite H=> //= t'1 /(_ t'1 (erefl _)) -> _ t' [] <- ; done.
Qed.

Lemma formula_rewrite_aux_obase t0 t1 i lQ lQ' t :
  term_obase lQ' t0 = term_obase lQ' t1 ->
  term_obase (lQ ++ lQ') (formula_rewrite_aux t0 t1 i (size lQ) t).1
    = term_obase (lQ ++ lQ') t.
Proof.
  rewrite/formula_rewrite_aux -!(term_shift_depth_obase lQ lQ' _)=> eq_base.
  case t'_eq: oterm_rewrite=> [t'|] ; last by case: i.
  case: i=> // i'.
  case: i'=> //.
  rewrite/term_wf (oterm_rewrite_obase eq_base t'_eq) ; done.
Qed.

Lemma formula_rewrite_wf t0 t1 i lQ lQ' f :
  term_obase lQ' t0 = term_obase lQ' t1 -> formula_wf (lQ ++ lQ') f ->
  formula_wf (lQ ++ lQ') (formula_rewrite t0 t1 i (size lQ) f).1.
Proof.
  move=> eq_base.
  elim: f lQ i=> //.
  1,2: move=> Q f H ? i /=/andP[] ->/= ; rewrite -cat_cons => /(H _ i) ; done.
  1,2: move=> f1 H1 f2 H2 ? i /= /andP[] /(H1 _ i) ;
    case: formula_rewrite=> f1' i1' f1'_wf ;
    move/(H2 _ i1') ; case: formula_rewrite => f2' i2' f2'_wf ;
    apply/andP ; done.
  - move=> t lQ i /= t_wf.
    rewrite/term_wf formula_rewrite_aux_obase ; done.
  - move=> t_1 t_2 lQ i /=.
    rewrite 2?formula_rewrite_aux_obase// ; done.
Qed.


Lemma Rewrite_wf k1 k2 i : tactic_wfP (Rewrite k1 k2 i).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf /= /[dup] pr_wf' /allP pr_wf g_wf.
  case: (ltnP k1 (size pr))=> [/(mem_nth FTrue) /pr_wf|?] ; last by rewrite nth_default.
  case: nth => // t0 t1 /=.
  case t0_base: term_obase=> [Q0|//].
  case t1_base: term_obase=> [Q1|//]=> /eqP same_base.
  case: (_ == _)=> // [[]] <-.
  rewrite/sequent_wf all_rcons /= lQ_wf pr_wf' g_wf andbC/= andbC/= -[lQ]cat0s.
  apply formula_rewrite_wf ; rewrite ?t0_base ?t1_base ?same_base// cat0s.
  case: (ltnP k2 (size pr))=> [/(mem_nth FTrue) /pr_wf //|?].
  rewrite nth_default ; done.
Qed.


Definition eqD_eval lD t0 D0 t1 D1 :=
  term_oeval lD t0 = Some D0 /\ term_oeval lD t1 = Some D1 /\ eqD diagram D0 D1.

Lemma oterm_rewrite_eqD lD t0 t1 t t' D0 D1 D D' :
  eqD_eval lD t1 D1 t0 D0 -> oterm_rewrite t0 t1 t = Some t' ->
  term_oeval lD t = Some D -> term_oeval lD t' = Some D' ->
  eqD diagram D' D.
Proof.
  move=> [] eq1 [] eq0 eqD10.
  elim: t t' D D' => [lQ | pQ t H] t' D D' ;
  rewrite unfold_oterm_rewrite ; case: ifP=> [/eqP eqt0 [] eqt1t' | _] //.
  1,2: rewrite eqt0 eq0 -eqt1t' eq1 => [[]] <- [] <-; done.
  case: oterm_rewrite H=> //= t_ H [] <- /=.
  case t_eq: term_oeval=> [DD|//] [] <-.
  case t__eq: term_oeval=> [D_|//] [] <-.
  apply: eqD_restr=> // ; try by apply compatible_diagram_to_mixin.
  apply: (H t_) ; done.
Qed.

Lemma formula_rewrite_aux_eqD lD lD' t0 D0 t1 D1 i t D D' :
  eqD_eval lD t1 D1 t0 D0 -> term_oeval (lD' ++ lD) t = Some D ->
  term_oeval (lD' ++ lD) (formula_rewrite_aux t0 t1 i (size lD') t).1 = Some D' ->
  eqD diagram D' D.
Proof.
  rewrite/formula_rewrite_aux ; set ot' := oterm_rewrite _ _ _.
  have rr x : eqD diagram x x by case: (eqD diagram) => r [rr rt rs] /=.
  case eq_t': ot'=> [t'|] ; case: i=> [/=|i].
  2: case: i=> /= [|_].
  1,3,4,5: move=> _ ; case: term_oeval => // ? [] <- [] <- //.
  move=> eqD_eval10 eq_D eq_D' ; apply: (oterm_rewrite_eqD (D0 := D0) (D1 := D1) _ eq_t' eq_D eq_D').
  rewrite/eqD_eval !term_shift_depth_valid ; done.
Qed.

Lemma formula_rewrite_aux_oeval lD lD' t0 D0 t1 D1 i t :
  term_wf (map base lD) t0 -> term_wf (map base lD) t1 ->
  eqD_eval lD t1 D1 t0 D0 ->
  term_wf (map base (lD' ++ lD)) t ->
  exists D' D,
  term_oeval (lD' ++ lD) (formula_rewrite_aux t0 t1 i (size lD') t).1
    = Some D' /\
  term_oeval (lD' ++ lD) t = Some D /\ eqD diagram D' D.
Proof.
  move=> t0_wf t1_wf /[dup] eqD_eval10 []eq_D1 []eq_D0 eq_D10.
  set t' := _.1.
  move: (term_oeval_obase (lD' ++ lD) t) (term_oeval_obase (lD' ++ lD) t').
  set base_ := term_obase _.
  have: base_ t' = base_ t.
    rewrite/base_ map_cat /t' -(size_map base) formula_rewrite_aux_obase//.
    move: t0_wf t1_wf
      (term_oeval_obase lD t0) (term_oeval_obase lD t1) eq_D10.
    rewrite/term_wf ; case: term_obase=> // Q0 ; case: term_obase=> // Q1.
    move=> _ _ []D0' + []D1' ; rewrite eq_D0 eq_D1=> [][][] -> <- [][]-> <-.
    move/(eqD_eq_base diagram)=> /= -> ; done.
  rewrite/base_/term_wf.
  case: (term_obase _ t)=> // Q.
  case: term_obase=> // Q' [] ? []D []eq_D eq_Q []D' []eq_D' eq_Q' _.
  exists D' ; exists D ; do!split=> //.
  apply: (formula_rewrite_aux_eqD eqD_eval10 eq_D eq_D') ; done.
Qed.



Theorem formula_rewrite_valid lD lD' t0 D0 t1 D1 i f :
  formula_wf (map base (lD' ++ lD)) f ->
  term_wf (map base lD) t0 -> term_wf (map base lD) t1 ->
  eqD_eval lD t1 D1 t0 D0 ->
  formula_eval (lD' ++ lD) (formula_rewrite t0 t1 i (size lD') f).1 <->
  formula_eval (lD' ++ lD) f.
Proof.
  move=> + t0_wf t1_wf eqD_eval10.
  elim: f lD' i=> //.
  - move=> Q f H lD' i /=/andP[] ; split => + D eq_Q ; move/(_ D) ; rewrite -cat_cons ; rewrite H/= eq_Q// => /(_ (erefl _)) ; done.
  - move=> Q f H lD' i /=/andP[] ; split => [] [] D []eq_Q h ; exists D ; move: h ; rewrite -cat_cons ; rewrite H//= eq_Q ; done.
  1,2: move=> f1 H1 f2 H2 lD' i /= /andP[] ?? ; rewrite H1 // H2 ; done.
  - move=> t lD' i /= t_wf.
    move: (formula_rewrite_aux_oeval i t0_wf t1_wf eqD_eval10 t_wf)
      => []D' []D []-> []-> ?.
    apply eqD_commute ; done.
  - move=> t_1 t_2 lD' i /= h ; set i' := (_.2).
    have t_1_wf: term_wf (map base (lD' ++ lD)) t_1.
      rewrite/term_wf ; case: term_obase h ; done.
    have t_2_wf: term_wf (map base (lD' ++ lD)) t_2.
      rewrite/term_wf ; case: term_obase h ; case: term_obase ; done.
    move: (formula_rewrite_aux_oeval i t0_wf t1_wf eqD_eval10 t_1_wf)
      => []D_1' []D_1 []-> []-> eqD1'1.
    move: (formula_rewrite_aux_oeval i' t0_wf t1_wf eqD_eval10 t_2_wf)
      => []D_2' []D_2 []-> []-> eqD2'2.
    move: eqD1'1 eqD2'2.
    case: (eqD diagram) => r [rr rt rs] /=  eqD1'1 eqD2'2; split=> hh.
    - apply: (rt) _ eqD2'2. apply: rt _ hh. exact: rs.
    -  apply: (rt) eqD1'1 _. apply: rt hh _. exact: rs.
Qed.


Lemma Rewrite_valid k1 k2 i : tactic_validP diagram (Rewrite k1 k2 i).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf /= /allP pr_wf g_wf.
  case: (ltnP k1 (size pr))=> [/(mem_nth FTrue)| ?]; last by rewrite nth_default.
  case: nth=> // t0 t1 eqD01_in_pr ; set i' := _.2 ; set f' := _.1.
  case: i'=> //= [[]] <-.
  rewrite/eval_sequent/= => s'_v lD eq_lQ pr_v.
  move/pr_wf: (eqD01_in_pr)=> /=.
  case t0_wf: term_obase=> [Q0|//].
  case t1_wf: term_obase=> [Q1|//].
  move/pr_v: eqD01_in_pr=> /=.
  case eq1: term_oeval=> [D0|//] ; case eq0: term_oeval=> [D1|//].
  move=> eqD10 _.
  have eqD_eval01: eqD_eval lD t1 D1 t0 D0.
  rewrite/eqD_eval; split=> //; split=> //.
  move: eqD10; case: (eqD diagram) => r [rr rt rs] /=; exact: rs.
  apply s'_v=> // f ; rewrite mem_rcons in_cons=> /orP[/eqP -> | /pr_v//].
  rewrite -[lD]cat0s (formula_rewrite_valid _ _ _ _ eqD_eval01) ;
  case: (ltnP k2 (size pr))=> [/(mem_nth FTrue)|?].
    2,4: rewrite nth_default ; done.
    3-6: rewrite /term_wf// eq_lQ ?t0_wf ?t1_wf ; done.
  - move/pr_v ; done.
  - move/pr_wf=> /= ; rewrite eq_lQ ; done.
Qed.

Canonical Rewrite_v k1 k2 i := valid_tactic_Build (@Rewrite_wf k1 k2 i) (@Rewrite_valid k1 k2 i).


Lemma formula_rewriteD t0 t1 i depth f :
  let fi' := formula_rewrite t0 t1 i depth f in
  formula_rewrite t0 t1 i depth (formula_dual f) =
  (formula_dual fi'.1, fi'.2).
Proof.
  elim: f i depth=> //.
  1,2: move=> Q f + i depth ; move/(_ i depth.+1)=> /= -> ; done.
  1,2: move=> f1 + f2 + i depth /= ; move/(_ i depth)=> /= -> /= ;
    set i' := (formula_rewrite _ _ _ _ _).2 ;
    move=> /(_ i' depth)=> /= -> ; done.
Qed.

Lemma RewriteD k1 k2 i : bitactic_DP (Rewrite k1 k2 i) (Rewrite k1 k2 i).
Proof.
  move=> [lQ pr g]/=.
  rewrite {1}dual_FTrue nth_map_default.
  case: nth=> //= t0 t1.
  rewrite {1}dual_FTrue nth_map_default formula_rewriteD /=.
  case: formula_rewrite=> // ? [] //=.
  by rewrite map_rcons.
Qed.

Canonical Rewrite_b k1 k2 i := bitactic_Build (Rewrite k1 k2 i) (Rewrite k1 k2 i).


Fixpoint term_rewrite t0 t1 t : term :=
  if t == t0 then t1
  else match t with
    | Var _ => t
    | Restr Q t' => Restr Q (term_rewrite t0 t1 t')
    end.

Lemma term_rewrite_base t0 t1 t lQ :
  term_obase lQ t0 = term_obase lQ t1 -> term_wf lQ t0 -> term_wf lQ t1
  -> term_wf lQ t -> term_obase lQ t = term_obase lQ (term_rewrite t0 t1 t).
Proof.
  move=> Ht0t1 t0_wf t1_wf.
  elim test3 : t => [d|sQ t'] /=.
  * move=> t_wf.
    case e: (_ == t0) => //.
    move: e => /eqP e.
    by rewrite -Ht0t1 -e.
  * move=> IH t_wf.
    rewrite /term_wf /= in t_wf.
    rewrite /term_wf /= in IH.
    case t'_base : term_obase t_wf IH => [Q|//].
    case restr_wf : quiver_restr_wf => // _ IH.
    case t't0 : (_ == t0).
    + move: t't0 => /eqP t't0.
      by rewrite -Ht0t1 -t't0 /= t'_base restr_wf.
    + by rewrite /= -IH // restr_wf.
Qed.

Lemma term_rewrite_wf t0 t1 t lQ :
  term_obase lQ t0 = term_obase lQ t1 -> term_wf lQ t0 -> term_wf lQ t1
  -> term_wf lQ t -> term_wf lQ (term_rewrite t0 t1 t).
Proof.
  move=> Ht0t1 t0_wf t1_wf.
  elim e : t => [d|sQ t'].
  * move=> t_wf /=.
    case: (_ == t0) => //.
  * move=> IH t_wf /=.
    case: (_ == t0) => //.
    have t'_wf : term_wf lQ t'.
      rewrite /term_wf /= in t_wf.
      rewrite /term_wf /= in IH.
      case t'_base : term_obase t_wf IH => [Q|//] HQ IH.
      by rewrite /term_wf t'_base.
    rewrite /term_wf /=.
    by rewrite -term_rewrite_base.
Qed.

Lemma term_rewrite_valid (t0 t1 t : term) (lD : seq diagram) (D0 D1 D D' : diagram) :
  term_oeval lD t0 = Some D0 -> term_oeval lD t1 = Some D1 -> eqD diagram D0 D1
  -> term_oeval lD t = Some D -> term_oeval lD (term_rewrite t0 t1 t) = Some D'
  -> eqD diagram D D'.
Proof.
  move=> t0D0 t1D1 HD0D1.
  elim: t D D' => /= [d|sQ t' IH] D D' /=.
  * case tt0 : (_ == t0) => /= dD;
    move: tt0 => /eqP tt0.
    + rewrite t1D1.
      move=> [] <-.
      have [] : Some D = Some D0 by rewrite -dD -t0D0 -tt0.
      by move=> ->.
    +  rewrite dD; move=> [] ->.
       by case: (eqD diagram) => r [] /=.
  * case t'D'' : term_oeval => [D''|//].
    case tt0 : (_ == t0) => //=.
    + move: tt0 => /eqP tt0 [] <-.
      rewrite t1D1 => [[]] <-.
      rewrite -tt0 /= t'D'' in t0D0.
      by move: t0D0 => [] ->.
    + move=> [] <-.
      case rt'D''' : term_oeval => [D'''|//] [] <-.
      have D''D''' : eqD diagram D'' D''' by apply IH.
      apply eqD_restr => //.
      by apply compatible_diagram_to_mixin.
Qed.

Fixpoint Subst_aux t0 t1 f : formula :=
match f with
| Forall Q f' => Forall Q (Subst_aux (term_shift_depth 1 t0) (term_shift_depth 1 t1) f')
| Exists Q f' => Exists Q (Subst_aux (term_shift_depth 1 t0) (term_shift_depth 1 t1) f')
| Imply f1 f2 => Imply (Subst_aux t0 t1 f1) (Subst_aux t0 t1 f2)
| And f1 f2 => And (Subst_aux t0 t1 f1) (Subst_aux t0 t1 f2)
| FTrue => FTrue
| Commute t => Commute (term_rewrite t0 t1 t)
| EqD t t' => EqD (term_rewrite t0 t1 t) (term_rewrite t0 t1 t')
end.

Lemma Subst_aux_wf t0 t1 f lQ :
  term_wf lQ t0 -> term_wf lQ t1 -> term_obase lQ t0 = term_obase lQ t1
  -> formula_wf lQ f -> formula_wf lQ (Subst_aux t0 t1 f).
Proof.
  elim: f lQ t0 t1 => //=.
  1,2 : move=> Q f IH lQ t0 t1 t0_wf t1_wf Ht0t1 /andP [Q_wf f_wf];

               rewrite Q_wf IH ?f_wf => /= //; rewrite -cat1s;
        try by rewrite -term_shift_depth_wf.
        1,2 : by rewrite !term_shift_depth_obase.
  1,2 : move=> f1 IH1 f2 IH2 lQ t0 t1 t0_wf t1_wf Ht0t1 /andP [f1_wf f2_wf];
        rewrite IH1 ?f1_wf //= IH2 ?f2_wf //.
  * move=> t lQ t0 t1 t0_wf t1_wf Ht0t1.
    apply: term_rewrite_wf => //.
  * move=> t t' lQ t0 t1 t0_wf t1_wf Ht0t1.
    case base_t : term_obase => [Q|] //.
    case base_t' : term_obase => [Q'|] // QQ'.
    rewrite -!(term_rewrite_base Ht0t1) /term_wf ?base_t ?base_t' => //.
Qed.

Lemma term_rewrite_None (t0 t1 t : term) (lD : seq diagram) (D0 D1 : diagram) :
  term_oeval lD t0 = Some D0 -> term_oeval lD t1 = Some D1 -> eqD diagram D0 D1
  -> term_oeval lD t = None <-> term_oeval lD (term_rewrite t0 t1 t) = None.
Proof.
  move=> t0D0 t1D1 HD0D1; split;
  elim: t => [d|sQ t' IH] /=;
  case tt0 : (_ == t0) => //=.
  1,2 : move: tt0 t0D0 => /eqP tt0;
        by rewrite -tt0 /= => ->.
  2,3 : by rewrite t1D1.
  1,2 : case t'N : term_oeval => // _;
        by rewrite IH.
Qed.

Lemma Subst_aux_valid (t0 t1 : term) (f : formula) (lD : seq diagram) (D0 D1 : diagram) :
  term_oeval lD t0 = Some D0 -> term_oeval lD t1 = Some D1 -> eqD diagram D0 D1
  -> formula_eval lD (Subst_aux t0 t1 f) <-> formula_eval lD f.
Proof.
  move=> t0D0 t1D1 HD0D1.
  elim: f lD t0 t1 t0D0 t1D1 HD0D1 => //=.
  * 1,2 : move=> Q f IH lD t0 t1 t0D0 t1D1 HD0D1; split; move=> H.
    1,2 : move=> D base_D; specialize (H D base_D).
    3,4 : move: H => [D [base_D H]]; exists D; split => //.
    1-4 : rewrite -(term_shift_depth_valid [:: D] _ t0) /= in t0D0;
          rewrite -(term_shift_depth_valid [:: D] _ t1) /= in t1D1.
    1,3 : exact: (IH _ _ _ t0D0 t1D1 HD0D1).1.
    1,2 : exact: (IH _ _ _ t0D0 t1D1 HD0D1).2.
  * 1,2 : move=> f1 IH1 f2 IH2 lD t0 t1 t0D0 t1D1 HD0D1; split.
    1,2 : move=> H H1.
    + exact: (IH2 _ _ _ t0D0 t1D1 HD0D1).1 (H ((IH1 _ _ _ t0D0 t1D1 HD0D1).2 H1)).
    + exact: (IH2 _ _ _ t0D0 t1D1 HD0D1).2 (H ((IH1 _ _ _ t0D0 t1D1 HD0D1).1 H1)).
    1,2 : move=> [eval_Rf1 eval_Rf2]; split.
    + exact: (IH1 _ _ _ t0D0 t1D1 HD0D1).1.
    + exact: (IH2 _ _ _ t0D0 t1D1 HD0D1).1.
    + exact: (IH1 _ _ _ t0D0 t1D1 HD0D1).2.
    + exact: (IH2 _ _ _ t0D0 t1D1 HD0D1).2.
  * move=> t lD t0 t1 t0D0 t1D1 HD0D1; split;
    case H : term_oeval => [D|//];
    case H' : term_oeval => [D'|//].
    + have DD' : eqD diagram D' D by apply: (term_rewrite_valid _ _ HD0D1 H' H).
      by apply eqD_commute.
    + by rewrite (term_rewrite_None _ t0D0 t1D1 HD0D1).1 in H.
    + have DD' : eqD diagram D D' by apply: (term_rewrite_valid _ _ HD0D1 H H').
      by apply (eqD_commute DD').1.
    + by rewrite (term_rewrite_None _ t0D0 t1D1 HD0D1).2 in H.
  * move=> t t' lD t0 t1 t0D0 t1D1 HD0D1; split;
    case rH : term_oeval => [rD|//];
    case rH' : term_oeval => [rD'|//] rDrD';
    case H : term_oeval => [D|];
    try case H' : term_oeval => [D'|].
    + have DrD : eqD diagram D rD by apply: (term_rewrite_valid _ _ HD0D1 H rH).
      have D'rD' : eqD diagram D' rD' by apply: (term_rewrite_valid _ _ HD0D1 H' rH').
      move: DrD D'rD' rDrD'.
      case: (eqD diagram) => r [rr rt rs] /= h1 h2 h3.
      apply: (rt) h1 _. apply: (rt) h3 _. exact: rs.
    + by rewrite (term_rewrite_None _ t0D0 t1D1 HD0D1).1 in rH'.
    + by rewrite (term_rewrite_None _ t0D0 t1D1 HD0D1).1 in rH.
    + have rDD : eqD diagram rD D by apply: (term_rewrite_valid _ _ HD0D1 rH H).
      have rD'D' : eqD diagram rD' D' by apply: (term_rewrite_valid _ _ HD0D1 rH' H').
      move: rDD rD'D' rDrD'.
      case: (eqD diagram) => r [rr rt rs] /= h1 h2 h3.
      apply: (rt) h2. apply: (rt) h3. exact: rs.
    + by rewrite (term_rewrite_None _ t0D0 t1D1 HD0D1).2 in rH'.
    + by rewrite (term_rewrite_None _ t0D0 t1D1 HD0D1).2 in rH.
Qed.

Definition Subst k '(Seq lQ pr g) :=
  match (nth FTrue pr k) with
  | EqD (Var d) t
  | EqD t (Var d) => Some (Seq lQ [seq Subst_aux (Var d) t f | f <- pr] (Subst_aux (Var d) t g))
  | _ => None
  end.

Lemma Subst_wf k : tactic_wfP (Subst k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf pr_wf g_wf /=.
  case nth_t0t1 : nth => [||||||t0 t1] //.
  have eq_t0t1_wf : formula_wf lQ (EqD t0 t1) by exact: (nth_all_bool _ pr_wf nth_t0t1).
  move: eq_t0t1_wf; rewrite /formula_wf.
  case base_t0 : term_obase => [Q|] //.
  case base_t1 : term_obase => [Q'|] // /eqP QQ'.
  case: t0 base_t0 nth_t0t1 => [d|sQ t] base_t0 nth_t0t1.
  * move=> [] <- /=.
    rewrite andbC lQ_wf Subst_aux_wf ?/term_wf ?base_t0 ?base_t1 ?QQ' => //=.
    rewrite all_map; apply/allP; move=> f f_pr.
    suff f_wf : formula_wf lQ (Subst_aux (Var d) t1 f) => //.
    rewrite Subst_aux_wf /term_wf ?base_t0 ?base_t1 ?QQ' => //.
    move: pr_wf => /allP pr_wf.
    exact: pr_wf.
  * case: t1 base_t1 nth_t0t1 => // d base_t1 nth_t0t1 [] <- /=.
    rewrite andbC lQ_wf Subst_aux_wf ?/term_wf ?base_t0 ?base_t1 ?QQ' => //=.
    rewrite all_map; apply/allP; move=> f f_pr.
    suff f_wf : formula_wf lQ (Subst_aux (Var d) (Restr sQ t) f) => //.
    rewrite Subst_aux_wf /term_wf ?base_t0 ?base_t1 ?QQ' => //.
    move: pr_wf => /allP pr_wf.
    exact: pr_wf.
Qed.

Lemma Subst_valid k : tactic_validP diagram (Subst k).
Proof.
  move=> [lQ pr g] s' /andP[] /andP[] lQ_wf pr_wf g_wf /=.
  case nth_t0t1 : nth => [||||||t0 t1] // Hs' s_eval lD lDlQ pr_eval.
  have eq_t0t1_eval : formula_eval lD (EqD t0 t1) by exact: (nth_all_prop _ pr_eval nth_t0t1).
  have rs x y : eqD diagram x y -> eqD diagram y x.
    by case: (eqD diagram) => r [rr rt rs]; exact: rs.
  case t0t' : t0 nth_t0t1 Hs' s_eval lD lDlQ pr_eval eq_t0t1_eval => [d|sQ t'].
  1 : move=> nth_t0t1 [] <- /= s_eval lD lDlQ pr_eval.
  2 : rewrite -t0t' /=;
      case: t1 => [d|//] /= nth_t0t1;
      move=> [] <- /= s_eval lD lDlQ pr_eval.
  1,2 : case dD : onth => [D|//];
        case t1D1 : term_oeval => [D1|//] DD1 //.
  2 : move/rs: DD1 => DD1.
  1,2 : have t0D : term_oeval lD (Var d) = Some D by [];
        apply (Subst_aux_valid _ t0D t1D1 DD1).1;
        apply s_eval => // f' /mapP [f f_pr ->];
        apply (Subst_aux_valid _ t0D t1D1 DD1).2;
        exact: pr_eval.
Qed.

Canonical Subst_v k := valid_tactic_Build (@Subst_wf k) (@Subst_valid k).

Canonical Subst_b k := bitactic_Build (Subst k) (Subst k).



(* Comauto *)

(* Almost everywhere below, we forget about vertices and we remember only arcs *)

Fixpoint Comauto_aux d H_full H0 l :=
  match l with
  | Commute t :: l' =>
    if term_restr_of d H_full t is Some H then
      preinter H0 H :: Comauto_aux d H_full H0 l'
    else
      Comauto_aux d H_full H0 l'
  | _ :: l' => Comauto_aux d H_full H0 l'
  | [::] => [::]
  end.

Definition Comauto d '(subquiver_Build sV H0 as sQ) '(Seq lQ pr g) :=
  match onth lQ d with
  | Some (quiver_Build n G as Q) =>
    if quiver_restr_wf sQ Q then
      if Commerge.auto_commerge (map (nth (0,0) G) H0)
          (Comauto_aux d (iota 0 (size G)) H0 pr) then
        Some (Seq lQ (rcons pr (Commute (Restr (subquiver_Build sV H0) (Var d)))) g)
      else
        None
    else
      None
  | _ => None
  end.



Lemma Comauto_wf d sQ : tactic_wfP (Comauto d sQ).
Proof.
  move: sQ=> [sV sA] [lQ pr g] s' /andP[]/andP[] lQ_wf pr_wf g_wf/=.
  case eq_Q: onth=> [[n A]|//].
  case: ifP=> // sQ_wf.
  case: ifP=> // com [] <- /=.
  rewrite all_rcons lQ_wf pr_wf g_wf/= andbC/= andbC/=.
  rewrite/term_wf/= eq_Q sQ_wf ; done.
Qed.



Lemma preinter_subset H0 H :
  {subset nths 0 H0 (preinter H0 H) <= H}.
Proof.
  move=> i /mapP[] j /mapP[] k ; rewrite mem_filter=> /andP[] ?? ->.
  rewrite nth_index// => -> ; done.
Qed.

Arguments preinter_subset : clear implicits.

Lemma quiver_restr_preinter_wf sQ sQ0 Q :
  quiver_restr_wf sQ Q ->
  quiver_restr_wf sQ0 Q ->
  quiver_restr_wf (subquiver_preinter sQ sQ0) (quiver_restr sQ0 Q).
Proof.
  move: sQ sQ0 Q=> [sV sA] [sV0 sA0] [n A] /andP[]/= sA_lt /andP[]/andP[] sV_uq
    sV_lt /allP sV_wf /andP[] _ /andP[]/andP[] _ _ /allP sV0_wf.
  do!(apply/andP ; split)=> /=.
  1,3: apply/allP=> i /mapP[] j ; rewrite mem_filter ?size_map/= -index_mem
      => /andP[] + _ -> ; done.
  - rewrite map_inj_in_uniq ?filter_uniq// => i j.
    rewrite !mem_filter=> /=/andP[] i_in _ /andP[] j_in _ index_eq.
    apply/eqP ; rewrite -(inj_in_index i_in) ?index_eq ; done.
  - apply/allP=> a=> /mapP[] i /mapP[] j.
    rewrite mem_filter=> /andP[] j_in0 j_in -> ->.
    rewrite (nth_map (0,0)) ?(nth_map 0) ?nth_index ?size_map ?index_mem//=.
    set a' := nth _ _ _.
    move: (sV_wf a') ; rewrite map_f// => /(_ isT)=> /andP[] ??.
    move: (sV0_wf a') ; rewrite map_f// => /(_ isT)=> /andP[] ??.
    rewrite !(map_f (index^~ sV0))// mem_filter ; apply/andP ; done.
Qed.

Lemma path_total_sub_subset G H H' r :
  {subset H <= H'} ->
  path_total_sub G H' r ->
  path_total_sub G H r.
Proof.
  move=> H_lt tot_H' u p q v path_p path_q ; apply tot_H'.
  2: move: q path_q=> {}p {}path_p.
  all: move: path_p ; rewrite/path_sub=> /andP[] -> /allP/= p_in_H ;
  apply/allP=> i /p_in_H/H_lt ; done.
Qed.



Lemma Comauto_valid d sQ : tactic_validP diagram (Comauto d sQ).
Proof.
  have ?: compatible_diagram_package diagram by apply compatible_diagram_to_mixin.
  move: sQ=> [sV H0] [lQ pr g] s' /andP[] /andP[] /allP lQ_wf pr_wf g_wf/=.
  case eq_Q: onth=> [[n G]|//].
  set Q := quiver_Build _ _ in eq_Q ; set sQ := (X in Restr X).
  have Q_wf : quiver_wf Q.
    apply lQ_wf ; apply (mem_onth eq_Q) ; done.
  case: ifP=> // /[dup] sQ_wf /andP[] sA_wf sV_wf.
  case: ifP=> // com [] <- s'_v lD eq_lQ pr_v.
  apply s'_v=> // f ; rewrite mem_rcons in_cons -/lD => /orP[/eqP->/= | /pr_v//].
  move: (term_oeval_obase lD (Var d)).
  rewrite/= eq_lQ eq_Q=> [[]]D=>[][] lD_d_eq base_D_eq.

  rewrite lD_d_eq commute_total_sub ?base_D_eq// -base_D_eq
    -(commute_total_sub (sQ := subquiver_spanning sQ Q))// /commute ?base_D_eq// ;
    last by apply quiver_restr_spanning_wf.
  set D' := diagram_restr _ _ _.
  have eq_base' : nths (0,0) G H0 = base D'.
    rewrite base_restr// -[quiver_restr _ _]/(quiver_restr_V (iota _ _) _) base_D_eq quiver_restr_fullV//.
    apply (@quiver_restr_A_wf_valid sQ Q) ; done.
  rewrite -eq_base'.
  apply (Commerge.auto_commerge_correct com).

  move=> H ; elim: pr pr_wf pr_v {com s'_v}=> // {}f pr Hr /= /andP[] + pr_wf fpr_v.
  have pr_v: {in pr, forall f, formula_eval lD f}.
    move=> p p_in ; apply fpr_v ; rewrite in_cons orbC p_in ; done.
  move: (fpr_v f) ; rewrite in_cons eqxx=> /(_ isT) {fpr_v}.
  case: f ; try (move => * ; apply Hr ; done).
  move=> t f_v /[dup] f_wf /= t_wf.
  rewrite (term_restr_of_subquiver _ Q).
  case eq_sQ': term_subquiver=> [[sV' H']|//=].
  rewrite in_cons=> /orP[/eqP ->|].
  2,3: apply Hr=> //.

  set sQ' := subquiver_Build _ _.
  rewrite -[map _ _]/(nths _ _ _) eq_base'.
  rewrite -[preinter _ _]/(subquiver_to_arc (subquiver_preinter sQ' (subquiver_spanning sQ Q))).
  move: (Q_wf)=> Q_wf'.
  rewrite -base_D_eq in Q_wf, eq_sQ'.
  rewrite -eq_lQ in t_wf.
  move: (term_subquiver_oeval lD_d_eq Q_wf t_wf eq_sQ')=> [] sQ'_wf []D'2 []eq_D'2 eqD_D'2.

  rewrite -commute_total_sub /D' ?base_restr// ; first last.
    - apply quiver_restr_preinter_wf=> // ; rewrite base_D_eq.
      apply quiver_restr_spanning_wf ; done.
    - apply quiver_restr_wf_valid=> // ; rewrite base_D_eq.
      apply quiver_restr_spanning_wf; done.

  rewrite (eqD_commute (restr_restr _ _ _ _))// ; try by apply compatible_to_mixin.
  rewrite commute_total_sub// ; first last.
    apply quiver_restr_QQ_wf ; last by rewrite base_D_eq ; apply quiver_restr_spanning_wf.
    apply quiver_restr_preinter_wf=> // ; rewrite base_D_eq.
    apply quiver_restr_spanning_wf ; done.

  apply: (path_total_sub_subset (preinter_subset _ _)).
  rewrite -[H']/(subquiver_to_arc sQ') -commute_total_sub//.
  rewrite -(eqD_commute eqD_D'2).
  move: f_v=> /= ; rewrite eq_D'2 ; done.
Qed.

Canonical Comauto_v d sQ :=
  valid_tactic_Build (@Comauto_wf d sQ) (@Comauto_valid d sQ).

(* Duality is too complicated to prove *)

Canonical Comauto_d d sQ :=
  bitactic_Build (Comauto d sQ) (Comauto d sQ).




Definition Sym k '(Seq lQ pr g) :=
  match nth FTrue pr k with
  | EqD t1 t2 => Some (Seq lQ (rcons pr (EqD t2 t1)) g)
  | _ => None
  end.

Lemma Sym_wf k : tactic_wfP (Sym k).
Proof.
  move=> [lQ pr g] s' /= /andP[] /andP[] lQ_wf /[dup] pr_wf' /allP pr_wf g_wf.
  case: (@ltnP k (size pr))=> [/(mem_nth FTrue)|/(nth_default FTrue) ->//].
  case: nth=> // t1 t2 /pr_wf/= + [] <- /=.
  rewrite all_rcons => /=.
  case t1_wf: term_obase=> [Q1|//].
  case t2_wf: term_obase=> [Q2|//] /eqP ->.
  rewrite lQ_wf eqxx pr_wf' ; done.
Qed.

Lemma Sym_valid k : tactic_validP diagram (Sym k).
Proof.
  move=> [lQ pr g] s' /= /andP[] /andP[] lQ_wf /[dup] pr_wf' /allP pr_wf g_wf.
  case: (@ltnP k (size pr))=> [/(mem_nth FTrue)|/(nth_default FTrue) ->//].
  case: nth=> // t1 t2 /[dup] in_pr /pr_wf/= t12_wf [] <- /= s'_v lD eq_lQ pr_v.
  apply s'_v=> // f ; rewrite mem_rcons in_cons=> /orP[/eqP ->/=|/pr_v//].
  move: (pr_v _ in_pr)=> /=.
  move: t12_wf (term_oeval_obase lD t1) (term_oeval_obase lD t2).
  rewrite eq_lQ.
  case: term_obase=> // Q1.
  case: term_obase=> // Q2 /eqP <- [] D1[] -> <- [] D2[] -> eq_base.
  by case: (eqD diagram) => r [rr rt rs]; exact: rs.
Qed.

Canonical Sym_v k := valid_tactic_Build (@Sym_wf k) (@Sym_valid k).

Canonical Sym_d k := bitactic_Build (Sym k) (Sym k).



Definition Reflag t1 t2 '(Seq lQ pr g) :=
  if onth lQ (term_var t1) is Some Q then
    if term_wf lQ t1 && term_wf lQ t2
       && (term_subquiver (term_var t1) Q t1 == term_subquiver (term_var t2) Q t2)
       && (term_var t1 == term_var t2) then
      Some (Seq lQ (rcons pr (EqD t1 t2)) g)
    else
      None
  else
    None.

Lemma term_wf_obase lQ t : term_wf lQ t -> all quiver_wf lQ ->
  exists Q sQ,
  onth lQ (term_var t) = Some Q /\
  term_subquiver (term_var t) Q t = Some sQ /\
  term_obase lQ t = Some (quiver_restr sQ Q) /\
  quiver_wf Q /\
  quiver_restr_wf sQ Q.
Proof.
  move=> +/allP lQ_wf.
  elim: t => [n|sQ' t] /=.
  - rewrite/term_wf/= ; case eq_Q: onth => [Q|//] _.
    exists Q ; exists (subquiver_full Q).
    have Q_wf : quiver_wf Q by apply lQ_wf ; apply (mem_onth eq_Q).
    do!split ; rewrite ?eqxx ?quiver_restr_full ?quiver_restr_full_wf// ; done.
  - rewrite/term_wf/= ; case eq_Q': term_obase=> [Q'|//] /(_ isT)
      []Q []sQ []eq_Q []eq_sQ [][]-> []Q_wf sQ_wf.
    case: ifP=> // sQ'_wf _.
    exists Q ; exists (subquiver_restr sQ' sQ).
    do!split ; rewrite ?eq_sQ -?quiver_restr_QQ//.
    apply quiver_restr_QQ_wf ; done.
Qed.

Lemma Reflag_wf t1 t2 : tactic_wfP (Reflag t1 t2).
Proof.
  move=> [lQ pr g] s' /= /andP[]/andP[]/[dup] lQ_wf' /allP lQ_wf pr_wf g_wf.
  case lQ_t1_eq: onth=> [Q|//].
  case: ifP=> // /andP[]/andP[]/andP[] t1_wf t2_wf /eqP subquiver_eq /eqP var_eq [] <-/=.
  rewrite all_rcons => /=.
  move: (term_wf_obase t1_wf lQ_wf') (term_wf_obase t2_wf lQ_wf').
  rewrite -{1}var_eq lQ_t1_eq => [] []Q1 []sQ1 [][]<- []eq_sQ1 []-> []Q1_wf sQ1_wf
    []Q2 []sQ2 [][]<-.
  rewrite -subquiver_eq eq_sQ1=> [][][]<- []-> _.
  rewrite lQ_wf' eqxx pr_wf ; done.
Qed.


Lemma term_wf_oeval lD t : term_wf (map base lD) t -> all quiver_wf (map base lD) ->
  exists D D' Q' sQ,
  term_oeval lD t = Some D /\
  term_subquiver (term_var t) Q' t = Some sQ /\
  onth (map base lD) (term_var t) = Some Q' /\
  base D' = Q' /\
  onth lD (term_var t) = Some D' /\
  eqD diagram (diagram_restr diagram sQ D') D.
Proof.
  move=> + lQ_wf.
  elim: t=> [n|sQ t].
  - move: (term_oeval_obase lD (Var n)) ; rewrite/term_wf/=.
    case lQ_n_eq: onth=> [Q'|//] [] D' [] eq_D' eq_Q' _.
    exists D' ; exists D' ; exists Q' ; exists (subquiver_full Q').
    do!split ; rewrite ?eqxx=> //.
    rewrite -eq_Q'.
    apply eqD_full ; apply compatible_diagram_to_mixin ; done.
  - rewrite/term_wf/= ; case: term_obase=>
      [Q|//] /= /(_ isT) []D []D' []Q' []sQ' []-> []eq_sQ' []-> []eq_Q' []-> eqDD.
    case: ifP=> // sQ_wf _.
    exists (diagram_restr diagram sQ D) ; exists D' ; exists Q' ; exists (subquiver_restr sQ sQ').
    do!split ; rewrite ?eq_sQ'=> //.
    have rs x y : eqD diagram x y -> eqD diagram y x.
      by case: (eqD diagram) => r [rr rt rs]; exact: rs.
    have rt x y z : eqD diagram x y -> eqD diagram y z  -> eqD diagram x z.
      by case: (eqD diagram) => r [rr rt _] /=; exact: rt.
    have h := restr_restr diagram.
      apply: (rs). apply: (rt) _ (h _ _ _). apply: (eqD_restr _); last exact: rs.
    by case: diagram => T [] //=.
Qed.

Lemma Reflag_valid t1 t2 : tactic_validP diagram (Reflag t1 t2).
Proof.
  move=> [lQ pr g] s' /= /andP[]/andP[] lQ_wf pr_wf g_wf.
  case eq_Q: onth=> [Q|//].
  case: ifP=> // /andP[]/andP[]/andP[] t1_wf t2_wf /eqP base_eq /eqP var_eq [] <- s'_v lD eq_lQ pr_v.
  apply s'_v=> // f ; rewrite mem_rcons in_cons=> /orP[/eqP -> /=|/pr_v//].
  rewrite -eq_lQ in t1_wf, t2_wf, lQ_wf.
  move: (term_wf_oeval t1_wf lQ_wf) (term_wf_oeval t2_wf lQ_wf)
  => []D1 []D'1 []Q'1 []sQ1 []-> []eq_sQ1 []+ []eq_Q'1 []+ eqD1
     []D2 []D'2 []Q'2 []sQ2 []-> []eq_sQ2 []+ []eq_Q'2 []+ eqD2.
  rewrite -var_eq eq_lQ eq_Q=> [][] Q_eq1 -> []Q_eq2 [] D'_eq.
  move: D'_eq Q_eq1 Q_eq2 eq_sQ1 eq_sQ2 eqD1 eqD2=> <- <- <-.
  rewrite base_eq=> -> [] <- eqD1 eqD2.
  have rt x y z : eqD diagram x y -> eqD diagram y z  -> eqD diagram x z.
    by case: (eqD diagram) => r [rr rt _] /=; exact: rt.
  have rs x y : eqD diagram x y -> eqD diagram y x.
    by case: (eqD diagram) => r [rr _ rs] /=; exact: rs.
  apply: (rt) _ eqD2; exact: rs.
Qed.

Canonical Reflag_v t1 t2 :=
  valid_tactic_Build (@Reflag_wf t1 t2) (@Reflag_valid t1 t2).
Canonical Reflag_d t1 t2 :=
  bitactic_Build (Reflag t1 t2) (Reflag t1 t2).



(* More externals *)
Open Scope fanl_scope.

Definition EqD_full Q := external_Build
  (Forall Q (EqD (Restr (subquiver_full Q) $0) $0)) (quiver_wf Q).

Lemma EqD_full_wf Q : external_wfP (EqD_full Q).
Proof.
  move=>/= Q_wf.
  rewrite/external_wfP/= quiver_restr_full_wf// quiver_restr_full Q_wf/= ; done.
Qed.

Lemma EqD_full_valid Q : external_validP diagram (EqD_full Q).
Proof.
  rewrite/external_validP/= => Q_wf D <-.
  apply (eqD_full diagram) ; done.
Qed.

Canonical EqD_full_v Q :=
  valid_external_Build (@EqD_full_wf Q) (@EqD_full_valid Q).

Canonical EqD_full_b Q :=
  biexternal_Build (EqD_full Q) (EqD_full (quiver_dual Q)).

End Compatibility_tactics.



Section Category_tactics.

(* Now we assume axioms for category *)
Hypothesis (diagram : category_diagram_type).

Notation base := (@base _).

Implicit Type (D : diagram).

(* Definition Pushforward sQ1 sQ2 Q1 Q2 Q :=
  Lambda [:: Q1 ; Q2 ; Q] (
    EqD (Restr sQ1 $0) $2 /\ EqD (Restr sQ2 $0) $1 ). *)

Definition Pushforward_ex sQ1 sQ2 Q := external_Build
  ( Forall (quiver_restr sQ1 Q) (
    Forall (quiver_restr sQ2 Q) (
    EqD (Restr (subquiver_preinter sQ2 sQ1) $1) (Restr (subquiver_preinter2 sQ1 sQ2) $0) -=>
    Exists Q (EqD (Restr sQ1 $0) $2 /\ EqD (Restr sQ2 $0) $1))))%fanl
  (quiver_wf Q && quiver_restr_wf sQ1 Q && quiver_restr_wf sQ2 Q && quiver_pushforward_wf sQ1 sQ2 (quiver_restr sQ1 Q) (quiver_restr sQ2 Q) Q).

Lemma quiver_restr_preinter2_wf sQ sQ0 Q :
  quiver_restr_wf sQ Q ->
  quiver_restr_wf sQ0 Q ->
  quiver_restr_wf (subquiver_preinter2 sQ sQ0) (quiver_restr sQ0 Q).
Proof.
  move: sQ sQ0 Q=> [sV sA] [sV0 sA0] [n A] /andP[]/= _ /andP[]/andP[] _
    _ /allP sV_wf /andP[] _ /andP[]/andP[] sV0_uq _ /allP sV0_wf.
  do!(apply/andP ; split)=> /=.
  1,3: apply/allP=> i /mapP[] j ; rewrite mem_filter ?size_map/= -!index_mem
      => /andP[] _ + -> ; done.
  - rewrite map_inj_in_uniq ?filter_uniq// => i j.
    rewrite !mem_filter=> /=/andP[] _ i_in /andP[] _ j_in index_eq.
    apply/eqP ; rewrite -(inj_in_index i_in) ?index_eq ; done.
  - apply/allP=> a=> /mapP[] i /mapP[] j.
    rewrite mem_filter=> /andP[] j_in0 j_in -> ->.
    rewrite (nth_map (0,0)) ?(nth_map 0) ?nth_index ?size_map ?index_mem//=.
    set a' := nth _ _ _.
    move: (sV_wf a') ; rewrite map_f// => /(_ isT)=> /andP[] ??.
    move: (sV0_wf a') ; rewrite map_f// => /(_ isT)=> /andP[] ??.
    rewrite !(map_f (index^~ sV0))// mem_filter ; apply/andP ; done.
Qed.

Lemma preinter_C sQ1 sQ2 Q :
  quiver_restr_wf sQ1 Q ->
  quiver_restr_wf sQ2 Q ->
  quiver_restr (subquiver_preinter sQ2 sQ1) (quiver_restr sQ1 Q) =
  quiver_restr (subquiver_preinter2 sQ1 sQ2) (quiver_restr sQ2 Q).
Proof.
  move: sQ1 sQ2=> [sV1 sA1] [sV2 sA2] sQ1_wf sQ2_wf.
  rewrite !quiver_restr_QQ ?quiver_restr_preinter_wf ?quiver_restr_preinter2_wf//=.
  rewrite !preinter_nths ; done.
Qed.

Lemma Pushforward_ex_wf sQ1 sQ2 Q : external_wfP (Pushforward_ex sQ1 sQ2 Q).
Proof.
  move=>/= /andP[] /andP[] /andP[] Q_wf sQ1_wf sQ2_wf _.
  do!(apply/andP ; split).
  - by apply quiver_restr_wf_valid.
  - by apply quiver_restr_wf_valid.
  - rewrite quiver_restr_preinter_wf ?quiver_restr_preinter2_wf ?preinter_C ; done.
  - done.
  - by rewrite sQ1_wf.
  - by rewrite sQ2_wf.
Qed.

Lemma Pushforward_ex_valid sQ1 sQ2 Q : external_validP diagram (Pushforward_ex sQ1 sQ2 Q).
Proof.
  move=> /= /andP[]=> ?.
  apply (pushforward_ex diagram) ; done.
Qed.

Canonical Pushforward_ex_v sQ1 sQ2 Q :=
  valid_external_Build (@Pushforward_ex_wf sQ1 sQ2 Q) (@Pushforward_ex_valid sQ1 sQ2 Q).

Canonical Pushforward_ex_b sQ1 sQ2 Q :=
  biexternal_Build (Pushforward_ex sQ1 sQ2 Q) (Pushforward_ex sQ1 sQ2 (quiver_dual Q)).




Fixpoint incl_vertices (sQ1_v sQ2_v : seq nat) (Q2_v i : nat) : seq nat :=
  match Q2_v with
  | 0 => nil
  | Q2_v'.+1 => if Q2_v' \in sQ2_v
                then rcons (incl_vertices sQ1_v sQ2_v Q2_v' i) (nth 0 sQ1_v (index Q2_v' sQ2_v))
                else rcons (incl_vertices sQ1_v sQ2_v Q2_v' i.+1) i
  end.

Fixpoint add_arcs (sQ1_arcs sQ2_arcs incl_v : seq nat) (fullQ_arcs Q2_arcs : seq (nat * nat)) (k : nat)
: (seq (nat * nat)) * (seq nat) :=
  match Q2_arcs with
  | nil => (fullQ_arcs, nil)
  | (i,j) :: Q2_arcs' => let '(new_arcs, incl_a) := add_arcs sQ1_arcs sQ2_arcs incl_v fullQ_arcs Q2_arcs' k.+1 in
                         if k \in sQ2_arcs then (new_arcs, (nth 0 sQ1_arcs (index k sQ2_arcs)) :: incl_a)
                         else (rcons new_arcs (nth 0 incl_v i, nth 0 incl_v j), (size new_arcs) :: incl_a)
  end.

Definition Merge (k : nat) '(Seq lQ pr g as s) : option sequent :=
  match normal_formula lQ (nth FTrue pr k) with
  | Some (EqD (Restr sQ1 (Var q1)) (Restr sQ2 (Var q2)))
    => let Q1 := nth {Q nil} lQ q1 in
       let Q2 := nth {Q nil} lQ q2 in
       let incl_v := incl_vertices (subquiver_vertex sQ1) (subquiver_vertex sQ2)
                                   (quiver_nb_vertex Q2) (quiver_nb_vertex Q1) in
       let '(fullQ_arcs, incl_a) := add_arcs (subquiver_arc sQ1) (subquiver_arc sQ2) incl_v
                                             (quiver_arc Q1) (quiver_arc Q2) 0 in
       let fullQ := quiver_Build (quiver_nb_vertex Q1 + quiver_nb_vertex Q2 - size (subquiver_vertex sQ1))
                                 fullQ_arcs in
       NewTac [:: ApplyEFT (Pushforward_ex (subquiver_Build (iota 0 (quiver_nb_vertex Q1)) (iota 0 (size (quiver_arc Q1))))
                                           (subquiver_Build incl_v incl_a)
                                           fullQ)
                           [:: aT (Var q1); aT (Var q2); aP k];
                  Subst (size pr);
                  Subst (size pr).+1] s
  | _ => None
  end.

Lemma Merge_wf k : tactic_wfP (Merge k).
Proof.
  move=> [lQ pr g] s' s_wf.
  rewrite /Merge.
  case: normal_formula => // f.
  case: f => // t1 t2.
  case: t1 => // sQ1 t1'.
  case: t1' => // q1.
  case: t2 => // sQ2 t2'.
  case: t2' => // q2.
  case: add_arcs => fullQ_arcs incl_a.
  apply NewTac_wf => //; repeat split.
  2,3 : by exact: Subst_wf.
  apply ApplyEFT_wf; first by exact: diagram.
  exact: Pushforward_ex_wf.
Qed.

Lemma Merge_valid k : tactic_validP diagram (Merge k).
Proof.
  move=> [lQ pr g] s' s_wf.
  rewrite /Merge.
  case: normal_formula => // f.
  case: f => // t1 t2.
  case: t1 => // sQ1 t1'.
  case: t1' => // q1.
  case: t2 => // sQ2 t2'.
  case: t2' => // q2.
  case: add_arcs => fullQ_arcs incl_a.
  apply NewTac_valid => //; repeat split.
  2,3 : by exact: Subst_wf.
  3,4 : by exact: Subst_valid.
  1 : apply ApplyEFT_wf; first by exact: diagram.
  2 : apply ApplyEFT_valid.
  1,2 : exact: Pushforward_ex_wf.
  exact: Pushforward_ex_valid.
Qed.

Canonical Merge_v k := valid_tactic_Build (@Merge_wf k) (@Merge_valid k).

Canonical Merge_b k := bitactic_Build (Merge k) (Merge k).



Definition Intro_Restr sQ Q := external_Build
  (Forall Q (
    Exists (quiver_restr sQ Q) (
      EqD (Restr sQ $1) $0
    )))%fanl
  (quiver_wf Q && quiver_restr_wf sQ Q).

Lemma Intro_Restr_wf sQ Q : external_wfP (Intro_Restr sQ Q).
Proof.
  move=> /andP[] Q_wf sQ_wf/=.
  do!(apply/andP ; split)=> //.
  - apply quiver_restr_wf_valid ; done.
  - rewrite sQ_wf ; done.
Qed.

Lemma Intro_Restr_valid sQ Q : external_validP diagram (Intro_Restr sQ Q).
Proof.
  move=> _ D eq_Q.
  exists (diagram_restr diagram sQ D).
  split.
  - by rewrite -eq_Q ; apply base_restr ; apply compatible_diagram_to_mixin.
  - rewrite /=.
    by case: (eqD diagram) => r [rr _ _ ] /=.
Qed.

Canonical Intro_Restr_v sQ Q :=
  valid_external_Build (@Intro_Restr_wf sQ Q) (@Intro_Restr_valid sQ Q).

Canonical Intro_Restr_b sQ Q :=
  biexternal_Build (Intro_Restr sQ Q) (Intro_Restr sQ (quiver_dual Q)).



Open Scope fanl_scope.

Definition CompF :=
  formula_fill_vertices [::] (
    Forall bimapQ (
      Exists compQ (
        EqD (Restr {sA [:: 0;2]} $0) $1 /\
        Commute $0
      ))).

Definition CompF' :=
          Forall bimapQ (
            Exists compQ (
              EqD (Restr {sQ [:: 0;2] <o compQ} $0) $1 /\
              Commute $0
            )).

Definition Comp := external_Build
  CompF true.

Lemma Comp_wf : external_wfP Comp.
Proof. done. Qed.

Lemma Comp_valid : external_validP diagram Comp.
Proof. move=> _ /= ; apply (comp diagram) ; done. Qed.

Canonical Comp_v :=
  valid_external_Build Comp_wf Comp_valid.

Definition CompFD := Eval compute in formula_dual CompF.

Definition CompD := Eval compute in external_dual Comp.

Lemma CompD_wf : external_wfP CompD.
Proof. done. Qed.

Definition CompF_demo : demo CompF.
Proof.
  unfold CompF.
  fanl (Use Comp).
  apply (cons_demo (Exact 0)) ; last by [].
  fanl_done.
Defined.

Definition CompFD_pf : proof.
Proof.
  fanlu IntroAll.
  pose sQrevbi := subquiver_Build [::2;1;0] [::1;0].
  fanlu (ApplyEFT Comp [:: aT (Restr sQrevbi (Var 0))]).
  fanlu (Sym 0).
  fanlu (Merge 2).
  pose sQrevcomp := subquiver_Build [::0;1;2] [::0;2;1].
  fanlu (ExistT (Restr sQrevcomp (Var 2))).
  fanlu (Comauto 2 sQrevcomp).
  fanlu (Conj 3 5).
  fanlu (ExactN 6).
  fanlu_done.
Defined.

Definition CompFD_vpf : valid_proof diagram.
Proof. validify CompFD_pf. Defined.

Lemma compD : @formula_eval diagram [::] CompFD.
Proof.
  apply: (check_proof_valid (pf := CompFD_vpf)) ; first by [].
  by vm_compute.
Qed.

Lemma CompD_valid : external_validP diagram CompD.
Proof. move=> _; apply compD. Qed.

Canonical CompD_v := valid_external_Build CompD_wf CompD_valid.

Canonical CompD_b :=
  biexternal_Build Comp CompD.

Close Scope fanl_scope.

End Category_tactics.
