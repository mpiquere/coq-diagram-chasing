
(* If `G` is acyclic and the arcs are decreasing, this file define an algorithm
  `compute_pm` to compute a `path_matrix` from `G` such that
  - `path_matrix nat := seq (seq (seq nat))`,
  - `exists w, v \in (compute_pm G)[u][w]` iff there is a path from `u` to `v`,
  - `v \in (compute_pm Ĝ)[u][w]` implies that there is a path of the form
  `u,w,...,v` *)

From mathcomp Require Import all_ssreflect.
From DiagramChasing Require Import Graph TopologicalSort.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.




Module PathMatrix.

Section Algorithm.

Definition path_matrix (V : Type) := seq (seq (seq V)).


Section path_matrix_nat.

(** In this algorithm, arcs are supposed to be decreasing *)
Let V := nat.

Implicit Type (pm : path_matrix V) (u v : V).

Definition path_next pm u v := find (fun l => v \in l) (nth [::] pm u).

Definition is_proper_path pm u v := (u != v) && has (fun l => v \in l) (nth [::] pm u).

Definition is_path pm u v := (u == v) || has (fun l => v \in l) (nth [::] pm u).

Definition accessible_from pm u := u :: flatten (nth [::] pm u).

Definition from_arc pm u v := nth [::] (nth [::] pm u) v.

Fixpoint compute_path_aux pm (n : nat) u v :=
  if u == v then
    [::]
  else
    if n is n'.+1 then
      let u' := path_next pm u v in
      u' :: compute_path_aux pm n' u' v
    else
      [::].

Definition compute_path pm (u : V) :=
  compute_path_aux pm u u.

Definition path_matrix_add_arc a1 a2 pm : seq (seq (seq V)) :=
  let new_accessible := [seq u <- accessible_from pm a2
  | u \notin accessible_from pm a1 ] in
  set_nth [::] pm a1 (set_nth [::] (nth [::] pm a1) a2 new_accessible).

Definition compute_pm arc : path_matrix V := foldr (fun a pm => path_matrix_add_arc a.1 a.2 pm) [::] arc.

End path_matrix_nat.

End Algorithm.

End PathMatrix.




Module Theory.

Import Graph.Theory.
Import FiniteOrientedGraph OrientedGraph.
Export PathMatrix.


Section path_matrix_nat_Theory.

Local Definition V := nat.

Implicit Type
  (G : FiniteOrientedGraph.type V)
  (u v : V)
  (p : seq V)
  (n : nat)
  (pm : path_matrix V).

Definition decreasing_arc_prop G := forall u v, arc G u v -> u > v.


Section Preliminary_Lemmas.

Lemma is_path_proper pm u v : is_path pm u v = (u == v) || is_proper_path pm u v.
Proof.
  rewrite/is_path/is_proper_path.
  by case: (u == v).
Qed.

Lemma is_properW_path pm u v : is_proper_path pm u v -> is_path pm u v.
Proof.
  move=>/andP[] _? ; apply/orP ; right ; done.
Qed.

Lemma from_arc_is_path pm u v w : v \in from_arc pm u w -> is_path pm u v.
Proof.
  set from_uu' := from_arc _ _ _ => v_in_from_uu'.
  apply/orP ; right.
  apply/hasP ; exists from_uu' => //.
  rewrite mem_nth// ; case: leqP v_in_from_uu' => // ; rewrite/from_uu'/from_arc.
  move/(nth_default [::]) -> ; done.
Qed.

Lemma is_proper_path_from_arc pm u v : is_proper_path pm u v -> v \in from_arc pm u (path_next pm u v).
Proof.
  move/andP=> [] _.
  rewrite -[v \in _]/((fun l => v \in l) _) ; apply nth_find.
Qed.

Lemma is_path_accessible pm u v : is_path pm u v = (v \in accessible_from pm u).
Proof.
  rewrite/accessible_from/is_path/is_proper_path in_cons eq_sym.
  case: (v == u) => //=.
  case: (@idP (_ \in _)) ; last apply contra_notF.
  - move/flattenP=> ? ; apply/hasP ; done.
  - move/hasP=> ? ; apply/flattenP ; done.
Qed.

Lemma path_decreasing G u p v (decreasing_arc : decreasing_arc_prop G) : path G u p v -> u >= v.
Proof.
  elim: p u => [|u' p H] u.
  - rewrite path0 ; case: ltngtP => //.
  - rewrite cons_path=> /andP[] /decreasing_arc/ltnW /[swap] /H.
    apply leq_trans.
Qed.

End Preliminary_Lemmas.



Definition path_is_path_prop G pm := forall u p v,
  path G u p v -> is_path pm u v.

Definition is_path_path_prop G pm := forall u v,
  is_path pm u v -> path G u (compute_path pm u v) v.

Record path_matrix_correct_prop G pm := {
  π_path_is_path : path_is_path_prop G pm;
  π_is_path_path : is_path_path_prop G pm;
}.


Section add_arc.

Variables
  (G : FiniteOrientedGraph.type V)
  (pm : path_matrix V)
  (a1 a2 : V).

Hypotheses
  (decreasing_arc : decreasing_arc_prop G)
  (a_decreasing : a1 > a2)
  (minorate_a : forall u v, arc G u v -> u <= a1)
  (a_new_arc : ~~ arc G a1 a2)
  (pm_correct : path_matrix_correct_prop G pm).

Let G' := add_arc G a1 a2.

Let pm' := path_matrix_add_arc a1 a2 pm.


Lemma decreasing_arc' : decreasing_arc_prop G'.
Proof.
  move=>?? ; rewrite add_arc_arc=> /orP[/eqP[]->-> |] ; [apply a_decreasing | apply decreasing_arc].
Qed.

Lemma minorate_a' u v : arc G' u v -> u <= a1.
Proof.
  rewrite add_arc_arc=> /orP[/eqP[]-> //|] ; apply minorate_a ; done.
Qed.

Lemma second_arc_neq_a u v w : arc G' u v -> (v, w) != (a1, a2).
Proof.
  move=> /[dup]/minorate_a' /=? /decreasing_arc' /=v_lt_u.
  suff : v < a1.
    rewrite/negb/eq_op/= ; case: (ltngtP v a1) ; done.
  by apply (leq_trans v_lt_u).
Qed.

Lemma arc_preserved u v : arc G u v -> arc G' u v.
Proof.
  rewrite add_arc_arc orbC=> -> ; done.
Qed.

Lemma a1_neq_0 : a1 != 0.
Proof.
  apply lt0n_neq0 ; move: a_decreasing ; apply leq_trans ; done.
Qed.

Lemma elim_first_arc u p v : path G' u p v -> first_arc u p v == (a1, a2) = (u == a1) && (p == a2 :: behead p).
Proof.
  rewrite/first_arc{1}/eq_op/=.
  case: (@idP (_ == _)) => //=/eqP->.
  case: p => [|u' p _/=] ; first by move: a_decreasing=> /[swap]/eqP<- ; case: ltngtP.
  by rewrite -[RHS]/(_ && (p == p)) eq_refl andbC.
Qed.


Lemma path_preserved u p v : path G u p v -> path G' u p v.
Proof.
  elim: p u => [//|u' p H u].
  rewrite !cons_path=> /andP[]/arc_preserved-> /H ; done.
Qed.

Lemma path_preserved_first_arc u p v : first_arc u p v != (a1, a2) -> path G' u p v = path G u p v.
Proof.
  elim: p u => [//| u' p H u /negPf] ; rewrite /first_arc [head _ _]/=.
  rewrite !cons_path add_arc_arc=> ->.
  case: (@idP (arc _ _ _)) => [/[dup] uu'_arc/decreasing_arc/= u_gt_u'|//].
  apply H ; apply (@second_arc_neq_a u) ; apply arc_preserved ; done.
Qed.


Section path_matrix_Theory.

Lemma path_next_a1v_neq_a2 v : is_proper_path pm a1 v -> path_next pm a1 v != a2.
Proof.
  case: (@idP (_ == _)) => ///eqP path_next_eq_a2
    /[dup]/andP[] /negPf a1_neq_v _ /is_properW_path/(π_is_path_path pm_correct).
  rewrite/compute_path/compute_path_aux.
  case: {1 3}a1 a1_neq_0 => [//|? _].
  rewrite a1_neq_v path_next_eq_a2 cons_path=> /andP[].
  by move/negPf: a_new_arc ->.
Qed.

Lemma nth_path_matrix_preserved u : u != a1 -> nth [::] pm u = nth [::] pm' u.
Proof.
  by rewrite/pm'/path_matrix_add_arc nth_set_nth/= => /negPf->.
Qed.

Lemma from_arc_preserved u v : (u != a1) || (v != a2) -> from_arc pm u v = from_arc pm' u v.
Proof.
  rewrite{2}/from_arc/pm'/path_matrix_add_arc nth_set_nth/=.
  case: (@idP (u == a1)) => [/eqP->|//].
  rewrite nth_set_nth/=.
  by case: (v == a2).
Qed.

Lemma from_arc'_a : from_arc pm' a1 a2 = [seq u <- accessible_from pm a2 | u \notin accessible_from pm a1].
Proof.
  by rewrite/from_arc/pm'/path_matrix_add_arc nth_set_nth/= eq_refl nth_set_nth/= eq_refl.
Qed.

Lemma size_nth'_a1 : size (nth [::] pm' a1) > a2.
Proof.
  by rewrite/pm'/path_matrix_add_arc nth_set_nth/= eq_refl size_set_nth leq_maxl.
Qed.

Lemma is_proper_path'_a1 v : a1 != v  -> path_next pm' a1 v <= a2 -> is_proper_path pm' a1 v.
Proof.
  rewrite -ltnS /is_proper_path => -> _leq_a2 /=.
  rewrite has_find/=.
  apply (leq_trans _leq_a2).
  by rewrite/pm'/path_matrix_add_arc nth_set_nth/= eq_refl
    size_set_nth leq_maxl.
Qed.

Lemma path_next'_a1 v : a1 != v -> ~~ is_path pm a1 v && is_path pm a2 v = (path_next pm' a1 v == a2).
Proof.
  move => v_neq_a1.
  case: (@idP (_ == a2)) => [/eqP _eq_a2|] ; last apply contra_notF.
  - have/is_proper_path_from_arc: is_proper_path pm' a1 v.
      by apply is_proper_path'_a1 ; rewrite ?_eq_a2.
    rewrite _eq_a2 from_arc'_a mem_filter.
    by rewrite -!is_path_accessible.
  - move=> /andP[] /negPf Npath_a1v path_a2v ; case: ltngtP => //.
    - move=> /[dup] _ltn_a2 /ltnW/(is_proper_path'_a1 v_neq_a1)/is_proper_path_from_arc.
      rewrite -from_arc_preserved.
      - by move/from_arc_is_path ; rewrite Npath_a1v.
      - by rewrite eq_refl neq_ltn _ltn_a2.
    - move/(before_find [::]).
      rewrite -[X in v \in X]/(from_arc _ _ _) from_arc'_a mem_filter.
      rewrite -!is_path_accessible Npath_a1v path_a2v ; done.
Qed.

Lemma path_next_preserved u v : is_proper_path pm u v -> path_next pm u v = path_next pm' u v.
Proof.
  case: (@idP (u == a1)) => [/eqP->|/negP?] ;
    last by rewrite/path_next nth_path_matrix_preserved.
  move=>/[dup]/[dup]/andP[] a1_neq_v ; rewrite has_find=> path_next_lt_size is_path_a1v /path_next_a1v_neq_a2 path_next_neq_a2.
  have path_next'_neq_a2 : path_next pm' a1 v != a2.
    case: (@idP (_ == _)) => //.
    rewrite-path_next'_a1 ?(is_properW_path is_path_a1v) ; done.
  apply/eqP ; move: isT ; apply contraPT ; rewrite neq_ltn => /orP[|/[dup] path_next'_ltn] ;
  move/(before_find [::]) ; rewrite -[X in v \in X]/(from_arc _ _ _) ;
  [rewrite -from_arc_preserved | rewrite from_arc_preserved] ; rewrite ?eq_refl//.
  - rewrite is_proper_path_from_arc ; done.
  - rewrite is_proper_path_from_arc// /is_proper_path a1_neq_v/= has_find.
    apply (leq_trans path_next'_ltn).
    apply (leq_trans (find_size _ _)).
    rewrite/pm'/path_matrix_add_arc nth_set_nth/= eq_refl size_set_nth leq_maxr ; done.
Qed.

Lemma compute_path_auxW n u v : is_path pm u v -> n >= u -> compute_path_aux pm n u v = compute_path pm u v.
Proof.
  elim: u {-2}u n (leqnn u) => [u|u H U] n.
    rewrite leqn0=> /eqP-> /(π_is_path_path pm_correct)/(path_decreasing decreasing_arc).
    by rewrite leqn0=> /eqP-> ; case: n.
  rewrite leq_eqVlt => /orP [/eqP->|] ; last first.
    by rewrite ltnS ; apply H.
  move/(π_is_path_path pm_correct) ; rewrite/compute_path.
  case: n => [//|n /[swap] U_leq_n/=].
  case: (_ == _) => //.
  rewrite cons_path=> /andP[] /decreasing_arc/= _leq_U /(π_path_is_path pm_correct)=> ?.
  move: (leq_trans _leq_U U_leq_n) ; rewrite ltnS=> ?.
  by rewrite !H.
Qed.

Lemma compute_path_aux_preserved n u v : is_path pm u v -> n >= u -> compute_path_aux pm' n u v = compute_path_aux pm n u v.
Proof.
  elim: u.+1 {-2}u n (leqnn u.+1)=> [//|U H {}u n u_lt_SU].
  move=> /[dup] is_path_uv /(π_is_path_path pm_correct) /[swap] u_leq_n.
  rewrite -(compute_path_auxW _ (n := n))//.
  case: n u_leq_n => [//|n u_le_Sn /=].
  rewrite is_path_proper in is_path_uv.
  case: (_ == _) is_path_uv => [//|].
  rewrite cons_path=> /= is_proper_path_uv /andP[]/decreasing_arc/= _leq_U /(π_path_is_path pm_correct)=> ?.
  rewrite -path_next_preserved=> //.
  rewrite H =>// ; [|rewrite -ltnS] ; apply (leq_trans _leq_U) ; done.
Qed.

Lemma compute_path_preserved u v : is_path pm u v -> compute_path pm' u v = compute_path pm u v.
Proof.
  by move=>* ; rewrite/compute_path compute_path_aux_preserved.
Qed.

Lemma is_path_path'_aux n u v : is_path pm u v -> n >= u -> path G' u (compute_path_aux pm' n u v) v.
Proof.
  move=> /[swap] u_leq_n /[dup] is_path_uv /(compute_path_aux_preserved) ->//.
  apply path_preserved.
  rewrite compute_path_auxW//.
  by apply (π_is_path_path pm_correct).
Qed.

Lemma is_path_path'_ u v : is_path pm u v -> path G' u (compute_path pm' u v) v.
Proof.
  rewrite/compute_path ; move/is_path_path'_aux=> -> ; done.
Qed.

Lemma is_path_preserved u v : is_path pm u v -> is_path pm' u v.
Proof.
  rewrite !is_path_proper ; case (@idP (u == v))=> //= /negP/negPf u_neq_v /[dup] is_proper_path_uv.
  move/is_proper_path_from_arc ; rewrite from_arc_preserved.
  - move/from_arc_is_path ; rewrite is_path_proper u_neq_v ; done.
  - case: (@idP (u == a1)) is_proper_path_uv u_neq_v => //= /eqP-> is_proper_path_a1v a1_neq_v.
    rewrite path_next_preserved//.
    rewrite -path_next'_a1 ; last by rewrite a1_neq_v.
    rewrite is_path_proper is_proper_path_a1v orbC ; done.
Qed.

Lemma path_is_path' : path_is_path_prop G' pm'.
Proof.
  move=> u p v.
  move: (@path_preserved_first_arc u p v).
  case: (@idP (_ == _)) => [|_ /(_ isT) ->] ;
    last by move/(π_path_is_path pm_correct) ; apply is_path_preserved.
  case: (@idP (is_path pm u v))=> [/is_path_preserved//|/negP/[swap]].
  case: p => [_|u' p] ; first by rewrite path0 /is_path=> _ _->.
  rewrite/first_arc/head/eq_op [X in X -> _]/= cons_path => /andP[] /eqP-> /eqP-> /[swap] _ Npath_a1v /andP[] _.
  rewrite path_preserved_first_arc ; last by rewrite/first_arc/eq_op/= ; case: (ltngtP a2 a1) a_decreasing.
  move/(π_path_is_path pm_correct)=> is_path_a2v.
  rewrite is_path_proper ; case: (@idP (_ == _)) => [//|/negP/= ?].
  apply is_proper_path'_a1 => //.
  suff/eqP-> : path_next pm' a1 v == a2 by [].
  rewrite -path_next'_a1 ?Npath_a1v ?is_path_a2v ; done.
Qed.

Lemma is_path_path' : is_path_path_prop G' pm'.
Proof.
  move=> u v.
  rewrite is_path_proper.
  case: (@idP (_ == _)) => [/eqP->|/negP u_neq_v] ; first by case: v => [|?]  ; rewrite/compute_path/= ?eq_refl ?path0.
  move/is_proper_path_from_arc.
  move: (@from_arc_preserved u (path_next pm' u v)).
  case: (@idP (_ || _))=> [_ /(_ isT) <-|/[swap] _/[swap] _] ; first by move/from_arc_is_path/is_path_path'_.
  case: (@idP (_ == a1)) u_neq_v => [/eqP->/= a1_neq_v /negP/[dup]|//] ; rewrite negbK -{1}path_next'_a1// => /andP[] _ is_path_a2v.
  case: {-2}a1 (eq_refl a1) a1_neq_0 a1_neq_v a_decreasing => [//|a1'].
  rewrite/compute_path/= ltnS => /eqP-> _ /negPf-> a2_neq_a1' /eqP->.
  rewrite cons_path add_arc_arc eq_refl/=.
  by apply is_path_path'_aux.
Qed.

Proposition path_matrix_add_arc_correct : path_matrix_correct_prop G' pm'.
Proof.
  by move: path_is_path' is_path_path'.
Qed.

End path_matrix_Theory.

End add_arc.

End path_matrix_nat_Theory.

End Theory.
