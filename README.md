# coq-diagram-chasing

This Coq library aims to offer a practical tool to perform formalized diagrammatic proofs, mainly for category theory, in Coq. In particular, we have in mind diagram chasing as one uses in homological algebra over Abelian Categories.

## Usage

This library works with [mathcomp](https://math-comp.github.io/) version 1.16 and [Coq](https://coq.inria.fr/) version 8.18.

## Authors

Contributors are
- Benoît Guillemet
- [Assia Mahboubi](https://people.rennes.inria.fr/Assia.Mahboubi/)
- [Matthieu Piquerez](http://matthieu.piquerez.fr)

## Papers

Research papers related this project are :
- Assia Mahboubi and Matthieu Piquerez. [A First Order Theory of Diagram Chasing](https://hal.science/hal-04266479). In *32nd EACSL Annual Conference on Computer Science Logic 2024* (CSL’24), Naples (Université Federico II), Italy, 2024.

## Content

- The library is in the folder `diagram_chasing`. To see a complete example on how to use it, look at the file `tests_and_examples/mono_monom.v`.
- The folder `metaduality_theorem` shows a better way to deal with duality than the one currently in use in the library.
- The folder `comcut` contains an algorithm to help one to prove that a diagram is commutative. The algorithm is proven to be correct. See the file in `tests_and_examples/Example_comcut.v` for an example on how to use it.
- A different algorithm called `commerge`, and the proof of its correctness, can be found in `diagram_chasing/Commerge.v`. Look at `Test_Commerge.v` and `mono_monom.v` in `tests_and_examples` to see how to use it.

## Roadmap

This project is under development. The next steps will be :
- use the metaduality theorem in the main development,
- apply the library to get results on Z-module as defined in mathcomp,
- develop the theory to be able to perform easily diagram chasing,
- develop an interactive user interface to perform diagram chasing as on a blackboard,
- ...

## Help

If you have any question or remark, you can contact [Matthieu Piquerez](http://matthieu.piquerez.fr).

