(** This self-contained file formalizes duality theorems for a multi-sorted 
   first-order theory designed to describe statements proved by diagrammatic
   reasoning in 1-category. 
   It was linked in the paper https://doi.org/10.4230/LIPIcs.CSL.2024.38 **)
From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Section Utils.

Definition onth T (s : seq T) n :=
  nth None (map Some s) n.

Definition nths T (t : T) (s : seq T) :=
  map (nth t s).

Lemma nth_map_default T1 x1 T2 (f : T1 -> T2) n s :
  nth (f x1) [seq f i | i <- s] n = f (nth x1 s n).
Proof.
  case: (ltnP n (size s))=> [|n_ge] ; first by apply nth_map.
  rewrite !nth_default ?size_map ; done.
Qed.

End Utils.



Section quiver.

Record quiver := quiver_Build {
  quiver_nb_vertex : nat;
  quiver_arc : seq (nat * nat);
}.

(* Condition to be well-formed *)
Definition quiver_wf '(quiver_Build n A) :=
  all (fun a => (a.1 < n) && (a.2 < n)) A.

Definition quiver_Empty :=
  quiver_Build 0 [::].

(* Most of the time, our quivers do not contains lonely vertices. All along this
   file, we write  functions to automatically compute the vertices part in these
   cases. *)
Definition quiver_from_arc A :=
  quiver_Build
    (foldr (fun a m => (maxn (maxn a.1.+1 a.2.+1) m)) 0 A)
    A.

Definition quiver_dual '(quiver_Build n A) :=
  quiver_Build n (map (fun a => (a.2,a.1)) A).


Section subquiver.

(* A way to encodes embeddings between quivers *)
Record subquiver := subquiver_Build {
  subquiver_vertex : seq nat;
  subquiver_arc : seq nat;
}.

Definition subquiver_full '(quiver_Build n A) :=
  subquiver_Build (iota 0 n) (iota 0 (size A)).

Definition subquiver_from_arc '(quiver_Build n A) sA :=
  let A' :=  map (nth (0,0) A) sA in
  subquiver_Build
    (undup (sort leq (unzip1 A' ++ unzip2 A')))
    sA.

Definition subquiver_restr_V ssV '(subquiver_Build sV sA) :=
  subquiver_Build (nths 0 sV ssV) sA.

Definition subquiver_restr_A ssA '(subquiver_Build sV sA) :=
  subquiver_Build sV (nths 0 sA ssA).

Definition subquiver_restr '(subquiver_Build ssV ssA) sQ :=
  subquiver_restr_V ssV (subquiver_restr_A ssA sQ).

End subquiver.


Definition quiver_restr_A_wf sA '(quiver_Build n A) :=
  all (gtn (size A)) sA.

Definition quiver_restr_A sA '(quiver_Build n A) :=
  quiver_Build n (nths (0,0) A sA).

Definition quiver_restr_V_wf sV '(quiver_Build n A) :=
  uniq sV &&
  all (gtn n) sV &&
  all (fun a => (a.1 \in sV) && (a.2 \in sV)) A.

Definition quiver_restr_V sV '(quiver_Build n A) :=
  quiver_Build (size sV) (map (fun a => (index a.1 sV, index a.2 sV)) A).

Definition quiver_restr_wf '(subquiver_Build sV sA) Q :=
  quiver_restr_A_wf sA Q &&
  quiver_restr_V_wf sV (quiver_restr_A sA Q).

Definition quiver_restr '(subquiver_Build sV sA) Q :=
  quiver_restr_V sV (quiver_restr_A sA Q).

End quiver.


(* Forgetting vertices *)
Coercion quiver_to_arc := quiver_arc.
Coercion subquiver_to_arc := subquiver_arc.


(* Boilerplate code for endowing the quiver with a structures of eqType *)

(* Bijection btw quiver and (seq nat) * (seq (nat * nat * nat)), as the
   latter triggers automated inference of eq/choice/count-type structures*)
Definition pair_of_quiver (pQ : quiver) :=
  let: quiver_Build n A := pQ  in (n, A).

Definition quiver_of_pair '(n, A) := quiver_Build n A.

Lemma pair_of_quiverK : cancel pair_of_quiver quiver_of_pair.
Proof. by case. Qed.

Definition quiver_eqMixin := CanEqMixin pair_of_quiverK.
Canonical quiver_eqType :=
  Eval hnf in EqType quiver quiver_eqMixin.


(* Some notations to define quivers and subquivers *)
Notation "{ 'Q' A }" := (quiver_from_arc A).
Notation "{ 'sQ' sA <o Q }" := (subquiver_from_arc Q sA).
Notation "{ 'sA' sA }" := (subquiver_Build [::] sA).

(* Some quivers and their dual *)
Definition mapQ := {Q [:: (0,1)]}.
Definition bimapQ := {Q [:: (0,1); (1,2)]}.
Definition monoQ := {Q [:: (0,1);(0,1);(0,2);(1,2)]}.
Definition compQ := {Q [:: (0,1);(0,2);(1,2)]}.

Definition mapQD   := Eval compute in quiver_dual mapQ.
Definition bimapQD := Eval compute in quiver_dual bimapQ.
Definition monoQD  := Eval compute in quiver_dual monoQ.
Definition compQD  := Eval compute in quiver_dual compQ.



Section model.

(* Structure of a model of diagrams *)
Record model (diagram : Type) := model_Build {
  (* The quiver underlying a diagram *)
  sort: diagram -> quiver;
  (* Restriction of a diagram to a subquiver *)
  diagram_restr: subquiver -> diagram -> diagram;
  (* Equality of diagrams *)
  eqD: diagram -> diagram -> Prop;
  (* Commutativity of a diagram *)
  commute: diagram -> Prop;
}.

(* We fix a notion of diagram for the rest of the file *)
Variable (diagram : Type).

(* The dual model. For the model associated to a category for instance,
  we would get the dual category *)
Definition model_dual (M : model diagram) : model diagram.
Proof.
  move: M=> [sort restr eqD commute].
  exact (model_Build (fun D => quiver_dual (sort D)) restr eqD commute).
Defined.


Definition diagram_restr_arc (M : model diagram) sA (D : diagram) :=
  let sQ := subquiver_from_arc (sort M D) sA in
  diagram_restr M sQ D.


(* Some notations to talk about diagrams *)
Definition predicate_coq (lQ : seq quiver) :=
  foldr (fun _ p => diagram -> p) Prop lQ.

Notation "'funD' D1 .. Dn 'on' lQ => P" :=
  ((fun D1 => ( .. (fun Dn => P) .. )) : predicate_coq lQ)
  (at level 200, D1 binder, Dn binder, lQ at level 10, right associativity)
  : fanl_coq_scope.

Notation "'forallD' D :: M 'on' Q , P" :=
  (forall D, sort M D = Q -> P)
  (at level 200, D name, Q at level 200, right associativity)
  : fanl_coq_scope.

Notation "'existsD' D :: M 'on' Q , P" := (
  exists D, sort M D = Q /\ P)
  (at level 200, D name, Q at level 200, right associativity)
  : fanl_coq_scope.


Section mono.

Open Scope fanl_coq_scope.

Variable (M : model diagram).

(* Predicate which says that D is a monomorphism *)
(* We will have the equivalent formula stating in FANL later *)
Definition mono :=
  funD D on [:: mapQ] =>
  forallD D' :: M on monoQ,
    eqD M (diagram_restr M {sQ [:: 3] <o monoQ} D') D /\
    commute M (diagram_restr M {sQ [:: 0;2;3] <o monoQ} D') /\
    commute M (diagram_restr M {sQ [:: 1;2;3] <o monoQ} D') ->
    commute M (diagram_restr M {sQ [:: 0;1] <o monoQ} D').

(* A proposition telling that if the composition of two maps is a mono,
  then the first map is a mono. *)
Definition mono_monomP :=
  forallD D :: M on compQ,
    commute M D /\
    mono (diagram_restr M {sQ [:: 1] <o compQ} D) ->
    mono (diagram_restr M {sQ [:: 0] <o compQ} D).

(* The dual versions *)
Definition epi :=
  funD D on [::mapQD] =>
  forallD D' :: M on monoQD,
  eqD M (diagram_restr M {sQ [:: 3] <o monoQ} D') D /\
  commute M (diagram_restr M {sQ [:: 0;2;3] <o monoQ} D') /\
  commute M (diagram_restr M {sQ [:: 1;2;3] <o monoQ} D') ->
  commute M (diagram_restr M {sQ [:: 0;1] <o monoQ} D').

Definition epi_mepiP :=
  forallD D :: M on compQD,
    commute M D /\
    epi (diagram_restr M {sQ [:: 1] <o compQ} D) ->
    epi (diagram_restr M {sQ [:: 0] <o compQ} D).

Close Scope fanl_coq_scope.

End mono.



Section fanl.

(* Definition of the language FANL *)
Inductive term :=
  | Var of nat
  | Restr of subquiver & term.

Inductive formula :=
  | Forall of quiver & formula
  | Exists of quiver & formula
  | Imply of formula & formula
  | And of formula & formula
  | FTrue
  | Commute of term
  | EqD of term & term.

Inductive predicate :=
  | Lambda of seq quiver & formula.



(* If you create a formula with empty vertices for `subquivers`,
  the following functions will compute the vertices part for you. *)
Fixpoint term_get_quiver (lQ : seq quiver) (t : term) :=
  match t with
  | Var n => nth quiver_Empty lQ n
  | Restr _ t' => term_get_quiver lQ t'
  end.

Fixpoint term_fill_vertices (Q : quiver) (t : term) :=
  match t with
  | Restr sA t =>
    let sQ := (subquiver_from_arc Q sA) in
    Restr sQ (term_fill_vertices (quiver_restr sQ Q) t)
  | _ => t
  end.

Fixpoint formula_fill_vertices (lQ : seq quiver) (f : formula) :=
  match f with
  | Forall Q f' => Forall Q (formula_fill_vertices (Q :: lQ) f')
  | Exists Q f' => Exists Q (formula_fill_vertices (Q :: lQ) f')
  | Imply f1 f2 =>
    Imply (formula_fill_vertices lQ f1) (formula_fill_vertices lQ f2)
  | And f1 f2 => And (formula_fill_vertices lQ f1) (formula_fill_vertices lQ f2)
  | FTrue => FTrue
  | Commute t => Commute (term_fill_vertices (term_get_quiver lQ t) t)
  | EqD t1 t2 => EqD
    (term_fill_vertices (term_get_quiver lQ t1) t1)
    (term_fill_vertices (term_get_quiver lQ t2) t2)
  end.

Definition Lambda_arc lQ f :=
  Lambda lQ (formula_fill_vertices lQ f).



(* Substitution varia *)
Fixpoint term_shift_depth depth (t : term) :=
  match t with
  | Var n => Var (depth + n)
  | Restr Q t' => Restr Q (term_shift_depth depth t')
  end.

Fixpoint term_assign_var t0 depth t :=
  match t with
  | Var n =>
    if n == depth then
      term_shift_depth depth t0
    else if n > depth then
      Var n.-1
    else
      Var n
  | Restr Q t' =>
    Restr Q (term_assign_var t0 depth t')
  end.

Fixpoint assign_Vk t0 k f :=
  match f with
  | Forall Q f' => Forall Q (assign_Vk t0 k.+1 f')
  | Exists Q f' => Exists Q (assign_Vk t0 k.+1 f')
  | Imply f1 f2 => Imply (assign_Vk t0 k f1) (assign_Vk t0 k f2)
  | And f1 f2 => And (assign_Vk t0 k f1) (assign_Vk t0 k f2)
  | FTrue => FTrue
  | Commute t => Commute (term_assign_var t0 k t)
  | EqD t1 t2 => EqD (term_assign_var t0 k t1) (term_assign_var t0 k t2)
  end.

Definition predicate_assign t0 '(Lambda stack f) :=
  Lambda (behead stack) (assign_Vk t0 0 f).

Definition predicate_to_formula '(Lambda stack f) := f.

End fanl.


(* Some notations to write formulas in FANL *)
Notation "P /\ Q" := (And P Q) : fanl_scope.
Notation "P -=> Q" := (Imply P Q) (at level 99, right associativity)
  : fanl_scope.
Notation "$ n" := (Var n) (at level 2) : fanl_scope.
Notation "p 'App' a1 .. an" :=
  (predicate_to_formula (predicate_assign a1 .. (predicate_assign an p) .. ))
  (at level 10, a1, an at level 9)
  : fanl_scope.



Section monoF.

Open Scope fanl_scope.

(* The analog of the previously defined predicates and formulas in FANL *)
Definition monoF :=
  Lambda_arc [:: mapQ]
  (Forall monoQ (
    EqD (Restr {sA [:: 3]} $0) $1
    /\ Commute (Restr {sA [:: 0 ; 2 ; 3]} $0)
    /\ Commute (Restr {sA [:: 1 ; 2 ; 3]} $0)
    -=> Commute (Restr {sA [:: 0 ; 1]} $0))).

Definition mono_monomPF :=
  formula_fill_vertices [::] (
    Forall compQ (
      Commute $0 /\
      monoF App (Restr {sA [:: 1]} $0)
      -=> monoF App (Restr {sA [:: 0]} $0))).

Definition epiF :=
  Lambda_arc [:: mapQD]
  (Forall monoQD (
    EqD (Restr {sA [:: 3]} $0) $1
    /\ Commute (Restr {sA [:: 0 ; 2 ; 3]} $0)
    /\ Commute (Restr {sA [:: 1 ; 2 ; 3]} $0)
    -=> Commute (Restr {sA [:: 0 ; 1]} $0))).

Definition epi_mepiPF :=
  formula_fill_vertices [::] (
    Forall compQD (
      Commute $0 /\
      epiF App (Restr {sA [:: 1]} $0)
      -=> epiF App (Restr {sA [:: 0]} $0))).

Close Scope fanl_scope.

End monoF.


Section fanl.

(* Well-definedness *)
Fixpoint term_osort (stack : seq quiver) t : option quiver :=
  match t with
  | Var n => onth stack n
  | Restr sQ t' =>
    if term_osort stack t' is Some Q then
      if quiver_restr_wf sQ Q then
        Some (quiver_restr sQ Q)
      else
        None
    else
      None
  end.

Definition term_wf : _ -> _ -> bool := term_osort.

Fixpoint formula_wf (stack : seq quiver) f :=
  match f with
  | Forall Q f'
  | Exists Q f' => quiver_wf Q && formula_wf (Q :: stack) f'
  | Imply f1 f2
  | And f1 f2 => formula_wf stack f1 && formula_wf stack f2
  | FTrue => true
  | Commute t' => term_wf stack t'
  | EqD t1 t2 =>
    match term_osort stack t1, term_osort stack t2 with
    | Some Q1, Some Q2 => Q1 == Q2
    | _, _ => false
    end
  end.

Definition predicate_wf '(Lambda stack f) :=
  formula_wf stack f.



(* Interpretation of a function with respect to a model M *)
Section eval.

Open Scope fanl_coq_scope.

Implicit Type (M : model diagram).

Fixpoint term_oeval M (stack : seq diagram) (t : term) :=
  match t with
  | Var n => onth stack n
  | Restr sQ t' =>
    if term_oeval M stack t' is Some D then
      Some (diagram_restr M sQ D)
    else
      None
  end.

Fixpoint formula_eval M stack f : Prop :=
  match f with
  | Forall Q f => forallD D :: M on Q, formula_eval M (D :: stack) f
  | Exists Q f => existsD D :: M on Q, formula_eval M (D :: stack) f
  | Imply f1 f2 => formula_eval M stack f1 -> formula_eval M stack f2
  | And f1 f2 => formula_eval M stack f1 /\ formula_eval M stack f2
  | FTrue => True
  | Commute t => if term_oeval M stack t is Some D then commute M D else False
  | EqD t1 t2 =>
    match term_oeval M stack t1, term_oeval M stack t2 with
    | Some DG1, Some DG2 => eqD M DG1 DG2
    | _, _ => False
    end
  end.



(* The evaluation of the FANL formula are indeed equal to the corresponding
  formulas written as coq terms *)
Section Test.

Open Scope fanl_scope.

Variable (M : model diagram).

Goal (forall D, sort M D = mapQ -> mono M D) =
  formula_eval M [::] (Forall mapQ (monoF App $0)).
Proof.
  done.
Qed.

Goal (forall D, sort M D = mapQD -> epi M D) =
  formula_eval M [::] (Forall mapQD (epiF App $0)).
Proof.
  done.
Qed.

Goal mono_monomP M = @formula_eval M [::] mono_monomPF.
  done.
Qed.

Goal epi_mepiP M = @formula_eval M [::] epi_mepiPF.
  done.
Qed.

Close Scope fanl_scope.

End Test.


(* The dual of a formula *)
Fixpoint formula_dual f :=
  match f with
  | Forall Q f => Forall (quiver_dual Q) (formula_dual f)
  | Exists Q f => Exists (quiver_dual Q) (formula_dual f)
  | Imply f1 f2 => Imply (formula_dual f1) (formula_dual f2)
  | And f1 f2 => And (formula_dual f1) (formula_dual f2)
  | _ => f
  end.

Definition predicate_dual '(Lambda lQ f) :=
  Lambda (map quiver_dual lQ) (formula_dual f).


Section Test.

(* The dual of mono_monom is epi_mepi *)
Goal formula_dual mono_monomPF = epi_mepiPF.
Proof. done. Qed.

Goal forall M, epi_mepiP M = formula_eval M [::] (formula_dual mono_monomPF).
Proof. done. Qed.

End Test.


Close Scope fanl_coq_scope.

End eval.

End fanl.



Section duality.

(* Some preliminaries to the duality theorem *)
Lemma quiver_dual_inv : involutive quiver_dual.
Proof.
  move=> [V A]/= ; congr quiver_Build=> /=.
  elim: A=> // [] [] [] * ; congr cons ; done.
Qed.

Definition quiver_dual_inj := inv_inj quiver_dual_inv.

Lemma formula_dual_inv : involutive formula_dual.
Proof.
  elim=> //=.
  1,2: move=> Q f -> ; rewrite quiver_dual_inv ; done.
  1,2: move=> f1 -> f2 -> ; done.
Qed.



Lemma sort_duality (M : model diagram) (D : diagram) :
  sort (model_dual M) D = quiver_dual (sort M D).
Proof.
  move: M=> [] */= ; done.
Qed.

Lemma commute_duality (M : model diagram) (D : diagram) :
  commute (model_dual M) D <->
  commute M D.
Proof.
  move: M=> [] * ; done.
Qed.

Lemma eqD_duality (M : model diagram) (D1 D2 : diagram) :
  eqD (model_dual M) D1 D2 <->
  eqD M D1 D2.
Proof.
  move: M=> [] * ; done.
Qed.

Lemma term_oeval_duality (M : model diagram) (ctx : seq diagram) (t : term) :
  term_oeval (model_dual M) ctx t = term_oeval M ctx t.
Proof.
  move: M=> [] *.
  elim: t ctx=> //= sQ t h ctx.
  rewrite h.
  case: term_oeval=> //.
Qed.

Lemma formula_eval_duality (M : model diagram) (ctx : seq diagram) (f : formula) :
  formula_eval (model_dual M) ctx (formula_dual f)
    <-> formula_eval M ctx f.
Proof.
  elim: f ctx=> [Q f H|Q f H|f1 H1 f2 H2|f1 H1 f2 H2|//|t|t1 t2] ctx /=.
  - split=> h D sortD_eq ; [rewrite -H | rewrite H] ; apply h ; [| apply quiver_dual_inj].
    - by rewrite sort_duality sortD_eq.
    - by rewrite -sort_duality sortD_eq.
  - split.
    1,2: move=> [D [sortD_eq h]] ; exists D ; split ; last by apply H ; apply h.
    1: apply quiver_dual_inj.
    1,2: rewrite -sortD_eq ?sort_duality// -sort_duality ; done.
  - split=> h f1_v ; [rewrite -H2 | rewrite H2] ; apply h ;
    [rewrite H1 | rewrite -H1] ; done.
  - split=> h ; [rewrite -H1 -H2|rewrite H1 H2] ; done.
  - rewrite term_oeval_duality ; case: term_oeval=> // D.
    apply commute_duality ; done.
  - rewrite !term_oeval_duality.
    case: term_oeval=> // D1 ; case: term_oeval=> // D2.
    apply eqD_duality ; done.
Qed.


(* If a formula of FANL is valid in every model, then so is the dual formula *)
Theorem duality_theorem (ctx : seq diagram) (f : formula) :
  (forall M : model diagram, formula_eval M ctx f)
  -> forall M : model diagram, formula_eval M ctx (formula_dual f).
Proof.
  move=> h M.
  apply formula_eval_duality.
  rewrite formula_dual_inv ; done.
Qed.


(* If one interprets `P M` as `M` is a model of some theory T, then
  the following state that if for every model of T the dual model is also a
  model of T, then, for every formula `f` which is valid for every model of T,
  the dual formula is valid for every model of T *)
Theorem duality_theorem_with_theory (ctx : seq diagram) (f : formula) (P : model diagram -> Prop) :
  (forall M : model diagram, P M -> P (model_dual M))
  -> (forall M : model diagram, P M -> formula_eval M ctx f)
  -> forall M : model diagram, P M -> formula_eval M ctx (formula_dual f).
Proof.
  move=> hP h M PM.
  apply formula_eval_duality.
  rewrite formula_dual_inv.
  apply h ; apply hP ; done.
Qed.

End duality.

End model.
