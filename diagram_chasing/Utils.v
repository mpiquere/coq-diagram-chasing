From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Definition enum T (s : seq T) := zip s (iota 0 (size s)).

Lemma enum_nth (T : eqType) (d : T) s p : p \in enum s = (p.2 < size s) && (nth d s p.2 == p.1).
Proof.
  case: (@idP (_ && _)) ; last apply contra_notF.
  - move=> /andP[] ?/eqP nth_eq ; apply/(nthP (d,0)) ; exists p.2.
    - rewrite size_zip size_iota minnn ; done.
    - rewrite nth_zip ?size_iota// nth_iota// nth_eq (surjective_pairing p) ; done.
  - move/(nthP (d,0))=> []i.
    rewrite size_zip size_iota minnn=> ?.
    rewrite nth_zip ?size_iota// nth_iota// add0n {1}(surjective_pairing p)=> [][] <- <-.
    apply/andP ; done.
Qed.

Fixpoint foldr2 A1 A2 B (f : A1 -> A2 -> B -> B) (b : B) (s1 : seq A1) (s2 :seq A2) : B :=
  match s1, s2 with
  | x1 :: s1', x2 :: s2' => f x1 x2 (foldr2 f b s1' s2')
  | _, _ => b
  end.

Definition option_map2 A B C (f : A -> B -> C) oa ob :=
  match oa, ob with
  | Some a, Some b => Some (f a b)
  | _, _ => None
  end.

Definition option_omap A B (f : A -> option B) oa :=
  match oa with
  | Some a => f a
  | _ => None
  end.

Definition onth T (s : seq T) n :=
  nth None (map Some s) n.

Lemma onth_size T (s : seq T) n : (onth s n : bool) = (n < size s).
Proof.
  case: (ltnP n _)=> n_lt_s ; last by rewrite /onth nth_default ?size_map.
  case: s n_lt_s => // t s ?.
  rewrite/onth (nth_map t) ; done.
Qed.

Lemma onth_rcons T s (a : T) n : size s = n -> onth (rcons s a) n = Some a.
Proof.
  move=> <- ; rewrite/onth map_rcons nth_rcons size_map ltnn eqxx ; done.
Qed.

Lemma nth_map_default T1 x1 T2 (f : T1 -> T2) n s :
  nth (f x1) [seq f i | i <- s] n = f (nth x1 s n).
Proof.
  case: (ltnP n (size s))=> [|n_ge] ; first by apply nth_map.
  rewrite !nth_default ?size_map ; done.
Qed.

Lemma onth_map T U s (f : T -> U) n : onth (map f s) n = option_map f (onth s n).
Proof.
  have Some_base_C : Some \o f =1 (option_map f) \o Some by done.
  rewrite /onth -map_comp -[None]/(option_map f None)
    (eq_map Some_base_C) map_comp nth_map_default ; done.
Qed.

Lemma mem_onth (T : eqType) s (t : T) n : onth s n = Some t -> t \in s.
Proof.
  move=> /[dup] onth_some.
  rewrite -(mem_map Some_inj) /onth=> <- ; apply mem_nth.
  rewrite size_map -onth_size onth_some ; done.
Qed.

Lemma onth_nth (T : eqType) (s : seq T) (t : T) (n : nat) :
  forall (d : T), onth s n = Some t -> nth d s n = t.
Proof.
  move=> d nt.
  have ns : n < size s by rewrite -onth_size nt.
  move: nt.
  rewrite /onth (nth_map d None Some ns) => [[]] //.
Qed.

Lemma iota_index i k n : i < n -> index (i + k) (iota k n) = i.
Proof.
  elim: i k n=> [|i H] k n ; first by case: n ; rewrite//= eqxx.
  case: n=> // n ; rewrite ltnS/= -{1}[k]add0n eqn_add2r/= addSnnS => i_lt.
  apply congr1 ; apply H ; done.
Qed.


Lemma iota_index0 i n : i < n -> index i (iota 0 n) = i.
Proof.
  rewrite -{2}[i]addn0 ; apply iota_index ; done.
Qed.

Definition All T (P : T -> Prop) (s : seq T) := foldr and True (map P s).

Lemma All_cat T (P : T -> Prop) (s1 s2 : seq T) :
  All P (s1 ++ s2) <-> All P s1 /\ All P s2.
Proof.
  rewrite /All map_cat foldr_cat.
  elim: s1 => [|t1 s1'] /=.
  * split => // [[_ H]] //.
  * move=> [IH IH']; split.
    + move=> [Pt1 H].
      by specialize (IH H); move: IH => [H1 H2].
    + move=> [[Pt1 H1] H2].
      by specialize (IH' (conj H1 H2)).
Qed.

Lemma split_nth (T : eqType) (a0 : T) i s : i < size s -> s = take i s ++ nth a0 s i :: drop i.+1 s.
Proof.
  move/(drop_nth a0)=> <- ; rewrite cat_take_drop ; done.
Qed.

Definition nths T (t : T) (s : seq T) :=
  map (nth t s).

Definition nths_wf T (s : seq T) :=
  all (gtn (size s)).

Lemma nths_map T U  t u (f : T -> U) s l :
  nths_wf s l ->
  nths u (map f s) l = map f (nths t s l).
Proof.
  move=> /allP l_wf.
  rewrite -map_comp /nths -eq_in_map=> i /l_wf/= i_lt.
  rewrite (nth_map t) ; done.
Qed.

Lemma nths_s_iota T (t : T) s :
  nths t s (iota 0 (size s)) = s.
Proof.
 rewrite/nths map_nth_iota0// take_size ; done.
Qed.

Lemma nths_iota_s t n s :
  all (gtn n) s -> nths t (iota 0 n) s = s.
Proof.
  move=> /allP s_lt.
  apply map_id_in=> i /s_lt /(nth_iota t 0) ; done.
Qed.

Lemma nth_all_prop (T : eqType) (P : T -> Prop) (s : seq T) (def : T) (k : nat) (x : T) :
  x <> def -> {in s, forall x : T, P x} -> nth def s k = x -> P x.
Proof.
  move=> Hx s_wf nth_x.
  apply s_wf.
  rewrite -nth_x.
  apply mem_nth.
  case sk : (size s <= k).
  * rewrite nth_default in nth_x => //.
    by rewrite nth_x in Hx.
  * by rewrite ltnNge sk.
Qed.

Lemma nth_all_bool (T : eqType) (P : pred T) (s : seq T) (def : T) (k : nat) (x : T) :
  x <> def -> all P s -> nth def s k = x -> P x.
Proof.
  move=> Hx /allP s_wf nth_x.
  exact: (@nth_all_prop _ (fun t => P t = true) s def k x Hx).
Qed.
