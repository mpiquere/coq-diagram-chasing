(* Some tests around canonicity *)

From mathcomp Require Import all_ssreflect.
From DiagramChasing Require Import FanL.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.


Section Test.

Variable (tac : tactic) (diagram : diagram_type).
Hypotheses (tac_wf : tactic_wfP tac) (tac_v : tactic_validP diagram tac).

Definition vtac := valid_tactic_Build tac_wf tac_v.

Canonical vtac.

Lemma foo (vpf : valid_proof diagram) : size (valid_proof_to_proof vpf) > 0 -> True.
Proof. done. Qed.

Arguments foo : clear implicits.

Definition pf := [:: tac ; tac].

Lemma ofo : size pf > 0.
Proof. done. Qed.

Lemma oof : True.
Proof. apply: (foo _ ofo). Defined.

End Test.



Section Test.

Variables tac dtac : tactic.

Definition btac := bitactic_Build tac dtac.

Canonical btac.

Lemma foo' (bpf : biproof) : size (biproof_primal bpf) > 0 -> True.
Proof. done. Qed.

Arguments foo' : clear implicits.

Definition pf' := [:: tac ; tac].

Lemma ofo' : size pf' > 0.
Proof. done. Qed.

Lemma oof' : True.
Proof. apply: (foo' _ ofo'). Defined.

End Test.



Section Test_Canonicity.

Variable (f : formula) (diagram : diagram_type).

Definition e := external_Build f true.
Definition de := external_Build (formula_dual f) true.

Hypothesis (e_wf : external_wfP e) (e_valid : external_validP diagram e).

Canonical e_v := valid_external_Build e_wf e_valid.
Canonical e_b := biexternal_Build e de.

Lemma foo2 (vpf : valid_proof diagram) : size (valid_proof_to_proof vpf) > 0 -> True.
Proof. done. Qed.

Lemma foo2_d (bpf : biproof) : size (biproof_primal bpf) > 0 -> True.
Proof. done. Qed.

Arguments foo2 : clear implicits.
Arguments foo2_d : clear implicits.

Definition pf2 := [:: Use e].

Lemma ofo2 : size pf2 > 0.
Proof. done. Qed.

Lemma oof2 : True.
Proof. apply: foo2 ofo2. Defined.

Lemma oof2_d : True.
Proof. apply: foo2_d ofo2. Defined.

End Test_Canonicity.
