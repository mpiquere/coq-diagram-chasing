From mathcomp Require Import all_ssreflect.
Require Import Relations.


Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.



(********************************************************************************)
(* STRUCTURE DEFINITION                                                         *)
(* Definition of three slightly different structures for graphs                 *)
(********************************************************************************)
Module OrientedGraph.

Structure type (vertex : eqType) := Build {
  arc : rel vertex;
}.

Arguments arc : simpl never.


Section Theory.

Definition path (V : eqType) (G : OrientedGraph.type V) (u : V) (p : seq V) (v : V) :=
  path.path (arc G) u p && (last u p == v).

Record path_relation (V : eqType) := {
  π_r :> forall u v : V, relation (seq V) ;
  π_equiv : forall u v, equivalence _ (π_r u v) ;
  π_cat_stable : forall u v w p p' q q', π_r u v p p' -> π_r v w q q' -> π_r u w (p ++ q) (p' ++ q') ;
}.

Definition path_total V G (r : path_relation V) :=
  forall u p p' v, path G u p v -> path G u p' v -> r u v p p'.

Definition pullback (V V' : eqType) (f : V -> V') (G' : OrientedGraph.type V') := {| arc := relpre f (arc G') |}.

Definition subgraph (V : eqType) (G G' : OrientedGraph.type V) := subrel (arc G) (arc G').

Definition morph (V V' : eqType) (f : V -> V') (G : OrientedGraph.type V) (G' : OrientedGraph.type V') := subgraph G (pullback  f G').

End Theory.

End OrientedGraph.


Module LocallyFiniteOrientedGraph.

Structure type (vertex : Type) := Build {
  arc : vertex -> seq vertex;
}.

Arguments arc : simpl never.

Definition to_OrientedGraph (V : eqType) (G : LocallyFiniteOrientedGraph.type V) := OrientedGraph.Build (fun u v => v \in arc G u).

Coercion to_OrientedGraph : LocallyFiniteOrientedGraph.type >-> OrientedGraph.type.
Canonical to_OrientedGraph.

End LocallyFiniteOrientedGraph.


Module FiniteOrientedGraph.

Structure type (vertex : Type) := Build {
  arc : seq (vertex * vertex);
}.


Section Coercions.

Variables (V : eqType).
Implicit Type (G : FiniteOrientedGraph.type V).

Definition to_LocallyFiniteOrientedGraph G := LocallyFiniteOrientedGraph.Build (fun v => undup (map snd (filter (fun a => a.1 == v) (arc G)))).

Definition to_OrientedGraph G := LocallyFiniteOrientedGraph.to_OrientedGraph (to_LocallyFiniteOrientedGraph G).

End Coercions.

Coercion to_LocallyFiniteOrientedGraph : FiniteOrientedGraph.type >-> LocallyFiniteOrientedGraph.type.
Canonical to_LocallyFiniteOrientedGraph.

Coercion to_OrientedGraph : type >-> OrientedGraph.type.
Canonical to_OrientedGraph.


Section Theory.

Variables (V : eqType).
Implicit Type (G : FiniteOrientedGraph.type V).

Lemma mem_arc G : forall u v : V, OrientedGraph.arc G u v = ((u, v) \in arc G).
Proof.
  move=> u v.
  case: (@idP (_ \in _)) ; rewrite/OrientedGraph.arc/= mem_undup.
  - move=> uv_in_arc ; apply /mapP ; exists (u, v) => // ; rewrite mem_filter eq_refl ; done.
  - apply contra_notF => /mapP[] a ; rewrite mem_filter => /andP[] /eqP<- ? -> ; rewrite -surjective_pairing ; done.
Qed.

Lemma mem_arcP G : forall u v : V, reflect ((u,v) \in arc G) (OrientedGraph.arc G u v).
Proof.
  move=> * ; rewrite -mem_arc ; apply idP.
Qed.

Definition relevant_vertex_seq G := undup (unzip1 (arc G) ++ unzip2 (arc G)).

Definition relevant_vertex G := { v : V | v \in relevant_vertex_seq G }.


Definition vertex_from_relevant_vertex G : relevant_vertex G -> V.
move=>[] ; done.
Defined.

Coercion vertex_from_relevant_vertex : relevant_vertex >-> Equality.sort.


Definition add_arc (V : eqType) (G : FiniteOrientedGraph.type V) (a1 a2 : V) := FiniteOrientedGraph.Build ((a1, a2) :: (FiniteOrientedGraph.arc G)).

Definition pushforward (V V' : eqType) (f : V -> V') (G : FiniteOrientedGraph.type V) := {| arc :=  map (fun a => (f a.1, f a.2)) (arc G) |}.

End Theory.

End FiniteOrientedGraph.



Module Theory.

Section OrientedGraphTheory.

Import OrientedGraph.

Variable (V : eqType) (G : OrientedGraph.type V).

Implicit Type (u v w : V) (p : seq V).

Lemma cons_path u w p v : path G u (w::p) v = arc G u w && path G w p v.
Proof.
  rewrite/path/= ; by rewrite andbA.
Qed.

Lemma last_path u p v : path G u p v -> last u p = v.
Proof.
  by rewrite/path=> /andP[] _ /eqP.
Qed.

Lemma path0 u v : path G u [::] v = (u == v).
Proof.
  done.
Qed.

Lemma cat_path u v p p' : path G u (p ++ p') v = path G u p (last u p) && path G (last u p) p' v.
Proof.
  rewrite/path path.cat_path last_cat eq_refl [_ && true]andbC !andbA ; done.
Qed.

Definition first_arc u p v := (u, head v p).

Lemma first_arc_arc u p v : let: (a1, a2) := first_arc u p v in p != [::] -> path G u p v -> arc G a1 a2.
Proof.
  by case: p => //= ?? _ ; rewrite cons_path=> /andP[].
Qed.

Definition last_arc u p v := (last u (belast u p), v).

Lemma last_arc_arc u p v : let: (a1, a2) := last_arc u p v in p != [::] -> path G u p v -> arc G a1 a2.
Proof.
  move=> /[swap]/andP[].
  case: (lastP p)=> [//|??].
  rewrite rcons_path belast_rcons last_rcons last_cons=> /andP[] _ /[swap] /eqP-> ; done.
Qed.

Lemma rcons_path u p v' v :
  OrientedGraph.path G u (rcons p v') v =
  OrientedGraph.path G u p (last u p) && (OrientedGraph.arc G (last u p) v') && (v' == v).
Proof.
  rewrite/OrientedGraph.path last_rcons eq_refl -!andbA/= !andbA rcons_path ; done.
Qed.

Definition cyclic G := exists u p, (p != [::]) && OrientedGraph.path G u p u.

Definition acyclic G := forall u p, OrientedGraph.path G u p u -> p == [::].

End OrientedGraphTheory.


Section FiniteOrientedGraphTheory.

Import FiniteOrientedGraph OrientedGraph.

Variable (V : eqType) (G : FiniteOrientedGraph.type V).

Lemma arc1_relevant u v : arc G u v -> u \in relevant_vertex_seq G.
  move=>/mem_arcP uv_arc.
  rewrite /relevant_vertex_seq mem_undup mem_cat ; apply/orP ; left.
  rewrite -[u]/((u,v).1) ; apply map_f ; done.
Qed.

Lemma arc2_relevant u v : arc G u v -> v \in relevant_vertex_seq G.
  move=>/mem_arcP uv_arc.
  rewrite /relevant_vertex_seq mem_undup mem_cat ; apply/orP ; right.
  rewrite -[v]/((u,v).2) ; apply map_f ; done.
Qed.

Lemma add_arc_arc a1 a2 u v : arc (add_arc G a1 a2) u v = ((u, v) == (a1, a2)) || arc G u v.
Proof.
  rewrite !mem_arc in_cons ; done.
Qed.

Lemma path_subset_relevant u p v : p != [::] -> OrientedGraph.path G u p v -> {subset u :: p <= relevant_vertex_seq G }.
Proof.
  case: p => [//|u' p _].
  elim: p u u'=> [u u'|u'' l H u u'] ;
  rewrite cons_path=> /andP[] arc_uu' ? w ;
  rewrite in_cons ; case: (@idP (_ == _))=> [/eqP-> _|_ /=] ;
  rewrite ?(arc1_relevant arc_uu')//.
  - rewrite in_cons orbC in_nil/= => /eqP-> ; apply (arc2_relevant arc_uu').
  - by apply H.
Qed.

End FiniteOrientedGraphTheory.


Section GraphMorphismTheory.

Import FiniteOrientedGraph OrientedGraph.

Variables (V V' : eqType) (f : V -> V').

Lemma morph_pullback (G' : OrientedGraph.type V') : morph f (pullback f G') G'.
Proof.
  done.
Qed.

Lemma morph_pushforward (G : FiniteOrientedGraph.type V) : morph f G (pushforward f G).
Proof.
  move=> u v /mem_arcP ? ; apply/mem_arcP.
  apply/mapP ; exists (u, v) ; done.
Qed.

Lemma sub_path (G G' : OrientedGraph.type V) u p v : subgraph G G' -> path G u p v -> path G' u p v.
Proof.
  rewrite/path => sub_GG'/andP[]?/eqP-> ; rewrite andbC eq_refl/=.
  apply (sub_path sub_GG') ; done.
Qed.

Lemma morph_path (G : OrientedGraph.type V) (G' : OrientedGraph.type V') u p v : morph f G G' -> path G u p v -> path G' (f u) (map f p) (f v).
Proof.
  move=> morph_f ; rewrite/path path_map last_map=> /andP[] /[swap]/eqP-> ; rewrite eq_refl andbC/=.
  apply path.sub_path ; done.
Qed.

End GraphMorphismTheory.

End Theory.
