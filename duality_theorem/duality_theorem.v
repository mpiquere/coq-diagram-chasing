From mathcomp Require Import all_ssreflect.

Set Implicit Arguments.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

From Coq Require Import Relations.
From DiagramChasing Require Import Commerge Utils.
From DiagramChasing Require Import FanL.

(** A shallow duality theorem for deep-embedded formulas. **)


(* TODO : cons_path should be phrased as [:: a, b & c] *)
Lemma path_dual (q : quiver) u p v : path q u p v -> path (quiver_dual q) v (rev p) u.
Proof.
elim: p u v => /= [| e p ihp] u v /=.
  by rewrite path0; move/eqP->; rewrite path0 eqxx.
rewrite cons_path; case/andP; case/andP=> ltesq e1 hp.
rewrite rev_cons rcons_path size_quiver_dual ltesq.
have /= -> : (nth (0, 0) (quiver_dual q) e).2 == u.
  case: q {ihp hp} ltesq e1 => n l /= ltesq e1. 
  by rewrite (nth_map (0, 0)) //= (eqP e1).
rewrite ihp //.
suff -> : (nth (0, 0) (quiver_dual q) e).1 = (nth (0, 0) q e).2 by [].
  case: q {ihp hp} ltesq e1 => n l /= ltesq e1. 
  by rewrite (nth_map (0, 0)) //= (eqP e1).
Qed.

Lemma path_dualP (q : quiver) u p v : path (quiver_dual q) v (rev p) u = path q u p v.
Proof.
apply/idP/idP=> /path_dual //.
by rewrite quiver_dual_inv revK.
Qed.

(* TODO : change order of args in stability axiom of path relations *)
Definition path_rel_swap (r : nat -> nat -> relation (seq nat)) :=
  fun  u v l1 l2 => r v u (rev l1) (rev l2).

(* Swapping preserves the fact of being a path relation *)
Definition path_rel_swap_pr (r : path_relation) : path_relation.
case: r => r equiv stable.
pose swap_r := path_rel_swap r.
have swap_r_refl u v : Relation_Definitions.reflexive _ (path_rel_swap r u v).
  by move=> l; apply equiv.
have swap_r_sym u v : Relation_Definitions.symmetric _ ( path_rel_swap r u v).
  by move=> l1 l2; apply equiv.
have swap_r_trans u v : Relation_Definitions.transitive _ ( path_rel_swap r u v).
  move=> l1 l2 l3 h1 h2. 
  exact: (equiv_trans _ _ (equiv v u)) h2.
have swap_r_stable u v w p p' q q' :
  path_rel_swap r u v p p' ->   path_rel_swap r v w q q' ->
   path_rel_swap r u w (p ++ q) (p' ++ q').
  by move=> h1 h2; rewrite /path_rel_swap !rev_cat; apply: (stable _ v).
eapply Build_path_relation; last exact: swap_r_stable.
by move=> u v; constructor.
Defined.

(* Variants of 'commute' and 'eqD' operating on unbundled data *)
Definition commute_of_pack (diagram : Type) (dT : diagram_package diagram) (D : diagram) :=
    path_total (diagram_to_quiver dT D) (eq_comp dT D).

Definition eqD_of_pack (diagram : Type) (dT : diagram_package diagram) := eqD dT.

(* Now some preliminaries to the duality theorem *)
Section duality.

Variable diagram : Type.

(* The dual model: keep the same data but dualize the underlying quiver and the path relation. 
   For the model associated to a category for instance, we would get the dual category *) 
Definition dual_diagram_Pack (dP : diagram_package diagram) : diagram_package diagram :=
  let: (diagram_Pack quiv restr eqD comp) := dP in
  diagram_Pack
    (fun D : _ => quiver_dual (quiv D))
    restr
    eqD
    (fun d => path_rel_swap_pr (comp d)).

(* This is in fact a definitional identity *)
Lemma sort_duality (M : diagram_package diagram) (D : diagram) :
  base (dual_diagram_Pack M) D = quiver_dual (base M D).
Proof. by case: M. Qed.


(* This is the main lemma a diagram is commutative iff it is commutative in the dual model *)
Lemma commute_duality (M : diagram_package diagram) (D : diagram) :
  commute_of_pack (dual_diagram_Pack M) D <->
  commute_of_pack M D.
Proof.
  case: M => quiv restr eqD eq_comp /=; rewrite /commute_of_pack /=.
  move: (eq_comp D) (quiv D) => r q {restr eq_comp}.
  suff step rr (qq : quiver) :
    path_total qq rr -> path_total (quiver_dual qq) (path_rel_swap rr).
    split; last by case: r => *; exact: step.
    move/step; rewrite quiver_dual_inv.
    apply: path_total_eq => u v. rewrite /path_rel_swap=> l1 l2.
    by case: r => r * /=; rewrite /path_rel_swap !revK.
  by move=> h u p p' v hp hp'; rewrite /path_rel_swap; apply: h; rewrite -path_dualP revK.
Qed.


Lemma eqD_duality (M : diagram_package diagram) (D1 D2 : diagram) :
  eqD (dual_diagram_Pack M) D1 D2 <->
  eqD M D1 D2.
Proof. move: M=> [] * ; done. Qed.

Lemma term_oeval_duality (M : diagram_package diagram) (ctx : seq diagram) (t : term) :
  @term_oeval (diagram_type_Build (dual_diagram_Pack M)) ctx t =
  @term_oeval (diagram_type_Build M) ctx t.
Proof.

  move: M=> [] *.
  elim: t ctx=> //= sQ t h ctx.
  rewrite h.
  case: term_oeval=> //.
Qed.
End duality.


Section duality_theorems.

Variable d : Type.

(* A few local notations and coercions to ease reading *)
Local Notation model := diagram_package.
Local Notation model_dual := dual_diagram_Pack.

Local Coercion diagram_type_of := (@diagram_type_Build d).

Theorem formula_eval_duality (M : model d) (ctx : seq d) (f : formula) :
  @formula_eval (model_dual M) ctx (formula_dual f) <-> @formula_eval M ctx f.
Proof.
elim: f ctx=> [Q f H|Q f H|f1 H1 f2 H2|f1 H1 f2 H2|//|t|t1 t2] ctx /=.
- split=> h D sortD_eq ; [rewrite -H | rewrite H] ; apply h; [| apply quiver_dual_inj].
  - by rewrite sort_duality sortD_eq.
  - by rewrite -sort_duality sortD_eq.
- split.
  1,2: move=> [D [sortD_eq h]] ; exists D ; split ; last by apply H ; apply h.
  1: apply quiver_dual_inj.
  1,2: rewrite -sortD_eq ?sort_duality// -sort_duality ; done.
- split=> h f1_v ; [rewrite -H2 | rewrite H2] ; apply h ;
     [rewrite H1 | rewrite -H1] ; done.
- split=> h ; [rewrite -H1 -H2|rewrite H1 H2] ; done.
- rewrite term_oeval_duality ; case: term_oeval=> // D.
  apply commute_duality ; done.
- rewrite !term_oeval_duality.
  case: term_oeval=> // D1 ; case: term_oeval=> // D2.
  apply eqD_duality ; done.
Qed.


(* If a formula of FANL is valid in every model, then so is the dual formula *)
Corollary duality_theorem (ctx : seq d) (f : formula) :
  (forall dP : model d, @formula_eval dP ctx f) ->
   forall dP : model d, @formula_eval dP ctx (formula_dual f). 
Proof.
move=> h M.
apply formula_eval_duality.
rewrite formula_dual_inv ; done.
Qed.


(* If one interprets `P M` as `M` is a model of some theory T, then
  the following state that if for every model of T the dual model is also a
  model of T, then, for every formula `f` which is valid for every model of T,
  the dual formula is valid for every model of T *)
Corollary duality_theorem_with_theory (ctx : seq d) (f : formula) (P : model d -> Prop) :
  (forall M, P M -> P (model_dual M)) -> (forall M, P M -> @formula_eval M ctx f)
   -> forall M, P M -> @formula_eval M ctx (formula_dual f).
Proof.
move=> hP h M PM.
apply formula_eval_duality.
rewrite formula_dual_inv.
apply h ; apply hP ; done.
Qed.

End duality_theorems.


